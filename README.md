# Job board Application

This repository contains the code that powers job board -- including the webapp (frontend) and API (backend)

# Dependencies

1. [Haskell/GHC](https://www.haskell.org/)
2. [Stack](https://docs.haskellstack.org/en/stable/README/)
3. [Docker](https://www.docker.com/)
4. [Selenium Standalone V2.53.1](https://selenium-release.storage.googleapis.com/2.53/selenium-server-standalone-2.53.1.jar)
   (`$SELENIUM_JAR` and `$CHROMEDRIVER_BIN` environment variables must be set properly, and must point to the right things)

# Getting started

To get everything running containerized on your local machine:

0. Clone this repo
1. `make all`

*NOTE* The API image may fail to build if you haven't already built the `builder-base` image, and pushed it to gitlab. In that case you neeed to run:

`make api-image-builder-base-image api-image` -- builds utility images for speeding up local builds and the main api image (static)

## A note about the api docker building process

In order to achieve the smallest possible contianer to ship over the wire to the registry, the haskell binary is staticly built. Unfortunately, on some distros (like arch linux), this process doesn't go quite as smoothly, so the build process was dockerized. While the process to do that is relatively straightforward, it becomes unbearably slow  due to commands like `stack setup` and all the resolving that needs to happen when the code is fresh loaded.

To circumvent that, quite a bit of complexity is added. there is a `builder-base` and a `builder` container for the API. The `builder-base` container's only purpose is to create a base container that removes the need to re-do `stack setup` and associated commands. Outside of haskell version/resolver changes this container should not change much. By using `builder-base`, builder can just re-do some steps (copying files over, doing the actual static install), and obtain quite a speedup -- it's almost like manually doing the caching for the dockerfile.

# Testing

To run the app's tests:

```
make tests
```

# Local Development

To get started developing locally, you'll want to set up all development aids:

`make dev-env-setup` (installs git hooks, other development aids)

When you want to refresh it (for example if you make changes to the pre-push hook)
`make dev-env-teardown dev-env-setup` (installs git hooks, other development aids)

## Stack ##

`stack build`

If you're running the project from the parent directory (Makefile), run:

`make api`

**NOTE** After this command is run, stack will have build the executable `job-board-api` in `api/target`.

To get into GHCI:

`stack ghci`
`λ> run RunServer`

## Database

To create the local database:

`make db`

To destroy/clean out the database:

`make clean-db`

To clean out, reload and re-populate the database with development fixtures:

`make local-dev-db`

## Webapp

As the frontend webapp uses must be transpiled, it has a requried **build** step. To build the frontend:

`make webapp`

To serve the frontend (this is required unless you have CORS rules somehow pointing "/api" to the appropriate backend):

`make served-webapp`

The `served-webapp` make target does nothing more than serve the `web` directory as-is, and redirect all unknown queries to `localhost:5001`, using [`http-server`](https://www.npmjs.com/package/http-server).

# Deploying

It is also possible to generate docker containers that power the front and backend of the service for easy deployment.

Before deploying a new version, make sure to:

0. Run the E2E tests (`make tests`)
1. Update the VERSION in `Makefile`
2. Update the version in `job-board-api.cabal`
2. Update the versions in all related `Dockerfile`s (for api and webapp)

## Building the components (api, webapp container)

To build the webapp container (generally just NGINX + the resources from the frontend webapp):

`make components`

## Building the requisite containers

To build the API container that powers an instance of the application:

`make containers`

## Deploying the application (containerized)

For a containerized deployment process (containers are pushed to a registry, then some other process either on the server or elsewhere pulls down the package and runs it), after the containers are built, they should be pushed to the gitlab registry.

`make deploy`

Lots of stuff is happening when this is run, primarily:
- `tests` (run all tests)
- `published-api-image` (build the local static api docker image, build the registry one,  and push it to the registry)
- `published-webapp-image` (build the local webapp docker image, build the registry one, and push it to the registry)
`
## Deploying the application (direct)

Deploying the application directly to the server to run is easy:

`make manual-deploy SSH_ADDR=<address of SSH target>`
