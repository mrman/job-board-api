{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module App ( startApp
           , buildApp
           , startDBBackend
           , startMailerBackend
           ) where

import           AuthN (CookieAuth)
import           Cache.CacheBackend (Cache(..), CacheBackend(..), CacheKey(..), HasCacheBackend(..), makeConnectedCacheBackend)
import           Config
import           Control.Exception (SomeException, Exception, try)
import           Control.Monad.IO.Class
import           Control.Monad.State (StateT, liftIO, gets, when, unless, evalStateT)
import           Data.Aeson (encode)
import           Data.DateTime (getCurrentTime)
import           Data.Maybe (fromJust, fromMaybe)
import           Data.Monoid ((<>))
import           Data.Proxy (Proxy(..))
import           Database.SQLite.Backend
import           Mailer.MailerBackend (Mailer(..), EmailLogHandler(..), MailerBackend, HasMailerBackend(..), makeConnectedMailerBackend)
import           Metrics.MetricsBackend (Metrics(..), MetricsBackend(..), makeConnectedMetricsBackend)
import           Network.URI
import           Network.Wai (Response, Request, requestHeaderUserAgent, requestHeaderReferer, remoteHost)
import           Network.Wai.Handler.Warp
import           Search.SearchBackend (SearchBackend(..), SQLiteFTS, HasSearchBackend(..), makeConnectedSearchBackend)
import           Servant
import           Servant.Server (hoistServerWithContext)
import           Servant.Server.Internal.ServerError (responseServerError)
import           Servant.Swagger (HasSwagger(..))
import           System.IO (stdout, stderr, Handle)
import           System.Log.Formatter (LogFormatter, simpleLogFormatter)
import           System.Log.Handler (setFormatter)
import           System.Log.Handler.Simple
import           System.Log.Logger
import           TaskQueue.TaskQueueBackend (TaskQueueBackend(..), HasTaskQueueBackend(..), TaskQueueM(..), makeConnectedTaskQueueBackend)
import           Tracing.TracingBackend (Tracing(..), makeConnectedTracingBackend)
import           Types
import           UserContentStore.UserContentStoreBackend (UserContentStoreBackend(..), UserContentStore(..), HasUCSBackend(..), makeUserContentStoreBackend)
import           Util (ifNothingThrowIOError, whenNothing, makeBrandingDataFromConfig, whenLeft, throwIfLeft)
import           Web.Cookie (renderSetCookie)
import qualified APIServer as API
import qualified AuthN
import qualified BounceServer
import qualified Data.ByteString.Char8 as B8
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.DateTime as DT
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Errors as Err
import qualified Web.ClientSession as WCS

-- ^ User Content Store
type UCS = "ucs" :> Raw

-- ^ Frontend API
type FRONTEND = Raw

-- ^ API Route
type API = "api" :> "v1" :> API.V1
    :<|> BounceServer.Routes

server :: ServerT API (WithAppGlobals Handler)
server = API.server :<|> BounceServer.server

instance (HasSwagger sub) => HasSwagger (CookieAuth :> sub) where
    toSwagger _ = toSwagger (Proxy :: Proxy sub)

type ServerGroup = API :<|> UCS :<|> FRONTEND

allServersProxy :: Proxy ServerGroup
allServersProxy = Proxy

apiProxy :: Proxy API
apiProxy = Proxy

contextProxy :: Proxy '[AuthN.AuthHandler Request SessionInfo]
contextProxy = Proxy

stateToHandler :: AppGlobals -> WithAppGlobals Handler a -> Handler a
stateToHandler globals action = evalStateT action globals

makeApp :: AppGlobals -> WCS.Key -> Application
makeApp appGlobals cookieKey = serveWithContext allServersProxy context allServers
    where
      appConfig = globalConfig appGlobals
      context = AuthN.genAuthServerContext cookieKey

      -- Create & join together servers that comprise the application
      ucsServer = serveUCS appConfig
      frontendServer = serveFrontend appConfig

      -- WithAppGlobals Handler (StateT AppGlobals Handler) needs to get converted to Handler
      -- Translate server in custom monad to regular Handler
      translation = stateToHandler appGlobals
      apiServer = hoistServerWithContext apiProxy contextProxy translation server

      allServers :: Server (API :<|> UCS :<|> FRONTEND)
      allServers = apiServer :<|> ucsServer :<|> frontendServer

defaultLoggerFormat :: LogFormatter a
defaultLoggerFormat = simpleLogFormatter "[$time : $loggername : $prio] $msg"

removeRootLogger :: IO ()
removeRootLogger = updateGlobalLogger rootLoggerName removeHandler

logLevelsWithHandles :: [(Priority, Handle)]
logLevelsWithHandles = [(DEBUG, stdout)]

defaultLogHandlers :: IO [GenericHandler Handle]
defaultLogHandlers = mapM (fmap (`setFormatter`defaultLoggerFormat) . makeHandler) logLevelsWithHandles
    where
      makeHandler (l, h) = fmap (\handler -> handler { formatter=defaultLoggerFormat }) (streamHandler h l)

buildLogger :: String -> Priority -> IO Logger
buildLogger loggerName p = defaultLogHandlers
                           >>= \hs -> (`fmap`System.Log.Logger.getLogger loggerName) (setHandlers hs . setLevel p)

addEmailLogHandler :: MailerBackend -> AppConfig -> Logger -> Logger
addEmailLogHandler mailer c = addHandler (EmailLogHandler mailer p defaultLoggerFormat lc)
    where
      lc = appLoggerConfig c
      p = loggerEmailPriority lc

-- Set up the app wide logger, provided a mailer backend is given
setupAppLogger :: MailerBackend -> AppConfig -> IO Logger
setupAppLogger mailer c = buildLogger "App" (appLogLevel c)
                          -- Send an emails for every log message
                          >>= pure . addEmailLogHandler mailer c
                          >>= \logger -> logL logger INFO ("\n" ++ show c)
                          >> pure logger

hourInSeconds :: Int
hourInSeconds = 60 * 60

buildApp :: AppConfig -> IO (Application, Settings, AppGlobals)
buildApp c = do
  _ <- removeRootLogger

  -- Set up the mailer
  mailerLogger <- buildLogger "App.Mailer" (mailLogLevel mailCfg)
  mailerBackendOrError <- makeConnectedMailerBackend mailCfg (Just mailerLogger) brandingData serverInfo
  mailerBackend <- case mailerBackendOrError of
                     Left err -> error ("Failed to initialize mailer backend: \n" ++ show err)
                     Right b -> pure b

  -- Set up app-wide logger, utilizing the mailer
  appLogger <- setupAppLogger mailerBackend c

  -- Set up the user content store
  ucsLogger <- buildLogger "App.UserContentStore" (ucsLogLevel ucsCfg)
  ucsBackendOrError <- makeUserContentStoreBackend ucsCfg (Just ucsLogger)
  ucsBackend <- case ucsBackendOrError of
                     Left err -> error ("Failed to initialize UCS backend: \n" ++ show err)
                     Right b -> pure b

  -- Start backend
  databaseBackend <- startDBBackend dbCfg

  -- Set up the search backend
  searchLogger <- buildLogger "App.Search" (searchLogLevel searchCfg)
  searchBackendOrError <- makeConnectedSearchBackend searchCfg (Just searchLogger) databaseBackend
  searchBackend <- case searchBackendOrError of
                     Left err -> error ("Failed to initialize Search backend: \n" ++ show err)
                     Right b -> pure b

  -- Set up the cache backend
  cacheLogger <- buildLogger "App.Cache" (cacheLogLevel cacheCfg)
  cacheBackendOrError <- makeConnectedCacheBackend cacheCfg (Just cacheLogger)
  cacheBackend <- case cacheBackendOrError of
                    Left err -> error ("Failed to initialize Cache backend: \n" ++ show err)
                    Right b -> pure b

  -- Get/create the existing client side cookie key
  cookieKey <- WCS.getKey (cookieSecretFilePath cookieCfg)

  -- Set up metrics/instrumentation
  metricsLogger <- buildLogger "App.Metrics" (metricsLogLevel metricsCfg)
  metricsBackendOrError <- makeConnectedMetricsBackend metricsCfg (Just metricsLogger)
  metricsBackend <- case metricsBackendOrError of
                      Left err -> error ("Failed to initialize Metrics backend: \n" ++ show err)
                      Right b -> pure b

  -- Set up tracing
  tracingLogger <- buildLogger "App.Tracing" (tracingLogLevel tracingCfg)
  tracingBackendOrError <- makeConnectedTracingBackend tracingCfg (Just tracingLogger)
  tracingBackend <- case tracingBackendOrError of
                      Left err -> error ("Failed to initialize Tracing backend: \n" ++ show err)
                      Right b -> pure b

  -- Set up task queue backend
  taskQueueLogger <- buildLogger "App.TaskQueue" (taskQueueLogLevel taskQueueCfg)
  taskQueueBackendOrError <- makeConnectedTaskQueueBackend taskQueueCfg (Just taskQueueLogger) databaseBackend
  taskQueueBackend <- case taskQueueBackendOrError of
                        Left err -> error ("Failed to initialize TaskQueue backend: \n" ++ show err)
                        Right b -> pure b

  -- Start RandomJobRefresh maintenance task
  logMsg taskQueueBackend INFO "Starting RandomJobRefresh recurring task..."
  _ <- runRecurringTask taskQueueBackend RandomJobRefresh Nothing Nothing (Seconds hourInSeconds)

  -- Assemble the globals
  let appGlobals = AppGlobals { globalConfig=c
                              , globalDBBackend=databaseBackend
                              , globalMailer=mailerBackend
                              , globalUserContentStore=ucsBackend
                              , globalSearchBackend=searchBackend
                              , globalCacheBackend=cacheBackend
                              , globalMetricsBackend=metricsBackend
                              , globalTaskQueueBackend=taskQueueBackend
                              , globalLogger=appLogger
                              , globalCookieKey=cookieKey
                              }

  -- Set up instrumentation
  appWithMetrics <- metricsBackend `instrument` makeApp appGlobals cookieKey

  -- Build the application
  app <- tracingBackend `traceApplication` appWithMetrics

  -- Initialize the application
  _ <- initApp appGlobals

  -- Set the default application-wide on-exception handler
  let appSettings = ( setTimeout appTimeoutSeconds
                    . setOnException (appOnExceptionHandler appLogger)
                    . setOnExceptionResponse (appOnExceptionResponse appEnv)
                    . setPort port
                    ) defaultSettings

  return (app, appSettings, appGlobals)
      where
        port = appPort c
        dbCfg = appBackendConfig c
        mailCfg = appMailerConfig c
        ucsCfg = appUCSConfig c
        cookieCfg = appCookieConfig c
        searchCfg = appSearchConfig c
        cacheCfg = appCacheConfig c
        metricsCfg = appMetricsConfig c
        tracingCfg = appTracingConfig c
        taskQueueCfg = appTaskQueueConfig c
        appEnv = appEnvironment c

        brandingData = makeBrandingDataFromConfig $ appBrandingConfig c
        serverInfo = ServerInfo { siBackendURL=appBackendExternalURL c
                                , siAdminURL=appAdminExternalURL c
                                , siFrontendURL=appFrontendExternalURL c
                                }

startApp :: AppConfig -> IO ()
startApp cfg = buildApp cfg
               >>= \(app, settings, globals) -> getCurrentTime
               -- Log that the application is starting
               >>= \now -> logL (globalLogger globals) INFO ("Starting application @ [" <> show now <> "]")
               -- Run the application
               >> runSettings settings app

initApp :: AppGlobals -> IO ()
initApp globals = logL appLogger INFO "Initializing application...."
                  -- Ensure the root user has been created
                  >> ensureRootUser
                  >> logL appLogger INFO "Finished app initialization"
    where
      cfg = globalConfig globals
      dbBackend = globalDBBackend globals
      appLogger = globalLogger globals
      rootEmail = appRootUserEmail cfg
      rootPassword = appRootUserPassword cfg

      makeRootUser now = User { firstName="root"
                              , lastName="user"
                              , emailAddress=rootEmail
                              , password=rootPassword
                              , policyAndSalt=Nothing
                              , userRole=Administrator
                              , joinedAt=now -- This is just going to get replaced in addUser
                              , homeLocationId=Nothing
                              , lastLoggedIn=now -- This should be replaced later
                              }
      addRootUser = getCurrentTime
                    >>= \now -> logL appLogger INFO "Creating root user..."
                    >> addUser dbBackend (makeRootUser now) rootPassword
                    >>= whenLeft (error "Failed to create missing root user")

      ensureRootUser = getUserByEmail dbBackend rootEmail
                       >>= either (const addRootUser) pure
                       >> logL appLogger INFO "Successfully ensured root user account exists"

appTimeoutSeconds :: Int
appTimeoutSeconds = 10

startDBBackend :: BackendConfig -> IO SqliteBackend
startDBBackend c = removeRootLogger
                   >> buildLogger "App.Backend" logLevel
                   >>= \l -> logL l INFO "Starting Backend..."
                   >> connect (Database.SQLite.Backend.makeBackend c (Just l))
                   >>= migrateToVersion targetVersion
                   >>= whenNothing (error ("Backend failed to migration to Backend version" ++ show targetVersion))
    where
      targetVersion = dbVersion c
      logLevel = dbLogLevel c

startMailerBackend :: MailerConfig -> BrandingData -> ServerInfo -> IO MailerBackend
startMailerBackend c b si = removeRootLogger
                            >> buildLogger "App.Mailer" logLevel
                            >>= \l -> logL l INFO "Starting Mailer..."
                            >> makeConnectedMailerBackend c (Just l) b si
                            >>= throwIfLeft
    where
      logLevel = mailLogLevel c


serveFrontend :: AppConfig -> Server Raw
serveFrontend = serveDirectoryFileServer . appFrontendFolder

serveUCS :: AppConfig -> Server UCS
serveUCS = serveDirectoryWebApp . ucsLocalBaseDir . appUCSConfig

appOnExceptionHandler :: Logger -> Maybe Request -> SomeException -> IO ()
appOnExceptionHandler logger _ e = logL logger ERROR (show e)
                                   >> when (defaultShouldDisplayException e) (TIO.hPutStrLn stderr packedErrStr)
    where
      packedErrStr = T.pack $ show e

appOnExceptionResponse :: AppEnvironment -> SomeException -> Response
appOnExceptionResponse env ex = responseServerError $ Err.makeGenericError msg
    where
      msg = case env of
              Production -> "An unexpected exception occurred"
              _          -> show ex

-- FIXME: these are more concrete than they need to be

-- | Globals that are used by the top level application
data AppGlobals = AppGlobals { globalConfig           :: AppConfig
                             , globalDBBackend        :: SqliteBackend
                             , globalMailer           :: MailerBackend
                             , globalUserContentStore :: UserContentStoreBackend
                             , globalSearchBackend    :: SQLiteFTS
                             , globalCacheBackend     :: CacheBackend
                             , globalMetricsBackend   :: MetricsBackend
                             , globalTaskQueueBackend :: TaskQueueBackend SqliteBackend IO
                             , globalLogger           :: Logger
                             , globalCookieKey        :: WCS.Key
                             }

type WithAppGlobals a = StateT AppGlobals a

instance HasDBBackend (WithAppGlobals Handler) SqliteBackend where
    getDBBackend = gets globalDBBackend

instance HasAppConfig (WithAppGlobals Handler) where
    getAppConfig = gets globalConfig

instance HasCookieConfig (WithAppGlobals Handler) where
    getCookieConfig = gets $ appCookieConfig . globalConfig

instance HasLoggerBackend (WithAppGlobals Handler) where
    getLogger = gets $ Just . globalLogger

instance HasCookieKey (WithAppGlobals Handler) where
    getCookieKey = gets globalCookieKey

instance HasUCSBackend (WithAppGlobals Handler) UserContentStoreBackend where
    getUCSBackend = gets globalUserContentStore

instance HasCacheBackend (WithAppGlobals Handler) CacheBackend where
    getCacheBackend = gets globalCacheBackend

instance HasSearchBackend (WithAppGlobals Handler) SQLiteFTS where
    getSearchBackend = gets globalSearchBackend

instance HasMailerBackend (WithAppGlobals Handler) MailerBackend where
    getMailerBackend = gets globalMailer
