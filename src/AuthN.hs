{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module AuthN
    ( genAuthServerContext
    , getRoleFromSessionInfo
    , getUserIDFromSessionInfo
    , authCookieName
    , sessionUserID
    , sessionUserRole
    , FullRequest
    , AuthHandler
    , CookieAuth
    ) where

import           Data.Maybe (isNothing, fromJust, fromMaybe)
import           Control.Monad.State (when, liftIO)
import           Data.Aeson (decode)
import           Data.ByteString (ByteString)
import           Data.Typeable (Typeable)
import           Network.HTTP.Types.Header (hCookie)
import           Network.Wai (Request, vault, requestHeaders)
import           Network.Wai.Session (Session)
import           Servant
import           Servant.Server.Experimental.Auth (AuthHandler, mkAuthHandler, AuthServerData)
import           Servant.Server.Internal.Delayed (passToServer)
import           System.Log.Logger (warningM)
import           Types
import           Util (ifNothingThrowIOError, throwErrorIf)
import           Web.ClientSession (Key, decrypt)
import           Web.Cookie (parseCookies)
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.Vault.Lazy as V
import qualified Errors as Err
import qualified Data.DateTime as DT

 -- For Servant generalized auth
type instance AuthServerData (AuthProtect "cookie-auth") = SessionInfo

authCookieName :: ByteString
authCookieName = "job-board-auth"

-- Generate a cookie auth handler, given a session store cookie
genCookieAuthHandler :: Key -> AuthHandler Request SessionInfo
genCookieAuthHandler serverKey = mkAuthHandler handler
    where
      handler req = liftIO DT.getCurrentTime
                    >>= \now -> getAuthCookieValue req
                    >>= ifNothingThrowIOError Err.notLoggedIn
                    >>= decryptCookie serverKey
                    >>= ifNothingThrowIOError Err.invalidAuthHeaders
                    -- Throw error if is cookie has expired
                    >>= \c -> if now > expires c then throwError Err.invalidSession else pure c

getAuthCookieValue :: Request -> Handler (Maybe ByteString)
getAuthCookieValue = return . maybe Nothing (lookup authCookieName) . (parseCookies<$>) . lookup hCookie .  requestHeaders

decryptCookie :: Key -> ByteString -> Handler (Maybe SessionInfo)
decryptCookie key str = return . fromMaybe Nothing $ decode . BSL8.fromStrict <$> decrypt key str

-- ^ Generate an context that contains the necessary auth handler, given a key to use for client side cookie encryption
genAuthServerContext :: Key -> Context '[AuthHandler Request SessionInfo]
genAuthServerContext = (:. EmptyContext) . genCookieAuthHandler

getRoleFromSessionInfo :: SessionInfo -> Role
getRoleFromSessionInfo = role . sessionUserInfo

getUserIDFromSessionInfo :: SessionInfo -> UserID
getUserIDFromSessionInfo = userID . sessionUserInfo

data FullRequest deriving Typeable

instance HasServer api context => HasServer (FullRequest :> api) context where
  type ServerT (FullRequest :> api) m = Request -> ServerT api m

  hoistServerWithContext _ pc nt s = hoistServerWithContext (Proxy :: Proxy api) pc nt . s

  route Proxy context subserver =
    route (Proxy :: Proxy api) context (passToServer subserver getRequest)

    where getRequest req = req

sessionUserID :: SessionInfo -> UserID
sessionUserID = userID . sessionUserInfo

sessionUserRole :: SessionInfo -> Role
sessionUserRole = role . sessionUserInfo

type CookieAuth = AuthProtect "cookie-auth"
