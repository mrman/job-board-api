{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE FlexibleContexts  #-}

module APIServer ( V1
                 , server
                 ) where

import           Servant
import           Types
import           Control.Monad.Except (MonadError)
import           UserContentStore.UserContentStoreBackend (HasUCSBackend(..))
import           Cache.CacheBackend (HasCacheBackend(..))
import           Search.SearchBackend (HasSearchBackend(..))
import           Mailer.MailerBackend (HasMailerBackend(..))
import qualified API.V1.Auth as AuthAPIV1
import qualified API.V1.Companies as CompaniesAPIV1
import qualified API.V1.Email as EmailAPIV1
import qualified API.V1.Jobs as JobsAPIV1
import qualified API.V1.Location as LocationAPIV1
import qualified API.V1.Promotions as PromotionsAPIV1
import qualified API.V1.StarredJobs as StarredJobsAPIV1
import qualified API.V1.Subscriptions as SubscriptionsAPIV1
import qualified API.V1.Support as SupportAPIV1
import qualified API.V1.Tags as TagsAPIV1
import qualified API.V1.Users as UsersAPIV1
import qualified API.V1.URLBounceConfigs as URLBounceConfigsAPIV1
import qualified API.V1.Analytics as AnalyticsAPIV1

-- | API server top level type
type V1 = JobsAPIV1.Routes
    :<|> CompaniesAPIV1.Routes
    :<|> URLBounceConfigsAPIV1.Routes
    :<|> UsersAPIV1.Routes
    :<|> StarredJobsAPIV1.Routes
    :<|> AuthAPIV1.Routes
    :<|> SupportAPIV1.Routes
    :<|> LocationAPIV1.Routes
    :<|> TagsAPIV1.Routes
    :<|> SubscriptionsAPIV1.Routes
    :<|> EmailAPIV1.Routes
    :<|> PromotionsAPIV1.Routes
    :<|> AnalyticsAPIV1.Routes

-- | API server
server :: ( HasAppConfig m
          , HasCookieConfig m
          , HasCookieKey m
          -- Required components/backends
          , HasUCSBackend m ucs
          , HasCacheBackend m c
          , HasSearchBackend m s
          , HasMailerBackend m mb
          , HasLoggerBackend m
          -- DB requirements
          , HasDBBackend m db
          , CompanyStore db m
          , UserManager db m
          , JobManager db m
          , URLBounceConfigStore db m
          , TagStore db m
          , EmailSubscriptionStore db m
          , SupportRequestStore db m
          , PromotionStore db m
          , TokenStore db m
          , JobPostingRequestStore db m
          , AnalyticsStore db m
          -- Servant
          , MonadError ServerError m
          ) =>
          ServerT V1 m
server = JobsAPIV1.server
         :<|> CompaniesAPIV1.server
         :<|> URLBounceConfigsAPIV1.server
         :<|> UsersAPIV1.server
         :<|> StarredJobsAPIV1.server
         :<|> AuthAPIV1.server
         :<|> SupportAPIV1.server
         :<|> LocationAPIV1.server
         :<|> TagsAPIV1.server
         :<|> SubscriptionsAPIV1.server
         :<|> EmailAPIV1.server
         :<|> PromotionsAPIV1.server
         :<|> AnalyticsAPIV1.server
