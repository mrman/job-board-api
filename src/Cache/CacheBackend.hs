{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE UndecidableInstances   #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE StandaloneDeriving     #-}
{-# LANGUAGE DeriveAnyClass         #-}
{-# LANGUAGE LambdaCase             #-}

module Cache.CacheBackend ( makeConnectedCacheBackend
                          , CacheBackend(..)
                          , Cache(..)
                          , CacheM(..)
                          , CacheKey(..)
                          , HasCacheBackend(..)
                          ) where

import           Config (LoggerConfig(..), CacheConfig(..))
import           Control.Concurrent (forkIO, threadDelay)
import           Control.Monad.IO.Class
import           Control.Concurrent.STM (STM, atomically)
import           Control.Concurrent.STM.TVar (TVar, newTVarIO, readTVarIO, writeTVar)
import           Control.Exception (Exception, SomeException, throw, try)
import           Control.Monad.State (liftIO, void)
import           Data.Hashable (Hashable)
import           Data.Maybe (Maybe, isJust)
import           Data.Monoid ((<>))
import           GHC.Generics (Generic)
import           System.Log.Logger (Logger, Priority(..))
import           Types
import qualified Data.HashMap.Strict as HMS
import           Data.Kind (Type)


-- | Available cache backends
data CacheBackend = LocalMemoryCache { cbCfg    :: CacheConfig
                                     , cbLogger :: Maybe Logger
                                     , cbCache  :: TVar (HMS.HashMap CacheKey CachedValue)
                                     }

-- | Cache keys usable with the cache
--   this must be updated when new values are storable in the cache
data CacheKey where
    JobFTS :: Massaged JobQuery -> CacheKey
    ActiveJobOnlyFTS :: Massaged JobQuery -> CacheKey
    CompanyStats :: CompanyID -> CacheKey
    StarredJobs :: UserID -> CacheKey
    CompanyListing :: Maybe Limit -> Maybe Offset -> CacheKey
    TagListing :: Maybe Limit -> Maybe Offset -> CacheKey
    EmailSubscriptionTags :: EmailSubscriptionID -> CacheKey
    TagFTS :: String -> CacheKey

deriving instance Generic CacheKey
deriving instance Eq CacheKey
deriving instance Show CacheKey
deriving instance Hashable CacheKey

-- | Errors that might be returned from cache operations
data CacheBackendError = InvalidCacheKey
                       | UnexpectedCacheFailure deriving (Eq)

instance Exception CacheBackendError

instance Show CacheBackendError where
    show UnexpectedCacheFailure = "An unexpected failure occurred"
    show InvalidCacheKey = "Invalid cache key"

-- | Create a connected cache backend
makeConnectedCacheBackend :: MonadIO m => CacheConfig -> Maybe Logger -> m (Either SomeException CacheBackend)
makeConnectedCacheBackend cfg maybeL = liftIO makeConnectedCacheBackend_
    where
      makeConnectedCacheBackend_ = newTVarIO HMS.empty
                                   >>= pure . LocalMemoryCache cfg maybeL
                                   >>= connectCacheBackend

-- | Log a cache error and return
logErrAndReturn :: CacheBackend -> String -> SomeException -> IO CacheBackend
logErrAndReturn c msg err = logMsg c ERROR (msg <> ": " <> show err) >> return c

-- | Get a cache entry
getCacheEntry :: MonadIO m
                 => CacheKey
                     -> TVar (HMS.HashMap CacheKey CachedValue)
                     -> m (Maybe CachedValue)
getCacheEntry k tv = liftIO getCacheEntry_
    where
      getCacheEntry_ = readTVarIO tv
                       >>= pure . HMS.lookup k

-- | Insert an entry into a given shared concurrent hash map, returning the updated map
insertCacheEntry :: MonadIO m
                    => CacheKey
                        -> CachedValue
                        -> TVar (HMS.HashMap CacheKey CachedValue)
                        -> m (HMS.HashMap CacheKey CachedValue)
insertCacheEntry k newValue tv = liftIO insertCacheEntry_
    where
      insertCacheEntry_ = readTVarIO tv
                          >>= pure . HMS.insert k newValue
                          >>= \updatedMap -> atomically (writeTVar tv updatedMap)
                          >> pure updatedMap

-- | Check whether a given key is in the cache
checkCacheEntry :: MonadIO m => CacheKey ->  TVar (HMS.HashMap CacheKey CachedValue) -> m Bool
checkCacheEntry k tv = liftIO checkCacheEntry_
    where
      checkCacheEntry_ = readTVarIO tv
                         >>= pure . HMS.member k

-- | Remove a single cache entry from the cache
removeCacheEntry :: MonadIO m => CacheKey ->  TVar (HMS.HashMap CacheKey CachedValue) -> m ()
removeCacheEntry k tv = liftIO removeCacheEntry_
    where
      removeCacheEntry_ = readTVarIO tv
                          >>= pure . HMS.delete k
                          >>= atomically . writeTVar tv

-- | Filter entries in the cache
filterCache :: MonadIO m => (CacheKey -> CachedValue -> Bool) -> TVar (HMS.HashMap CacheKey CachedValue) -> m ()
filterCache f tv = liftIO filterCache_
    where
      filterCache_ = readTVarIO tv
                     >>= pure . HMS.filterWithKey f
                     >>= atomically . writeTVar tv

-- | Empty out the cache
emptyCache :: MonadIO m => TVar (HMS.HashMap CacheKey CachedValue) -> m ()
emptyCache tv = liftIO $ atomically (writeTVar tv HMS.empty)

-- | Schedule & perform timed cache invalidation
timedCacheInvalidation :: (MonadIO m, HasLogger c, CacheM c IO, Show CacheKey) => CacheKey -> c -> Int -> m ()
timedCacheInvalidation k c ms = liftIO $ void $ forkIO (delayedInvalidation k c ms)
    where
      delayedInvalidation k c ms = threadDelay ms
                                   >> try (invalidate k c)
                                   >>= logTimedCacheInvalidation k c

-- | Log a cache hit or miss
logCacheHitOrMiss :: (MonadIO m, HasLogger c, Cache c, Show CacheKey) => c -> CacheKey -> Maybe a -> m (Maybe a)
logCacheHitOrMiss c k value = logMsg c DEBUG message
                              >> return value
    where
      missOrHit = if isJust value then "hit" else "miss"
      message = "Cache " <> missOrHit <> " for key" <> show k

logCacheUpdate :: (MonadIO m, HasLogger component, Show a) => component -> a -> m ()
logCacheUpdate comp = logMsg comp DEBUG . ("Cache Updated: "<>) . show

logTimedCacheInvalidation :: (HasLogger c, Show CacheKey) => CacheKey -> c -> Either SomeException () -> IO ()
logTimedCacheInvalidation k c (Left _) = logMsg c DEBUG ("Timed cache invalidation FAILED for key: " <> show k)
logTimedCacheInvalidation k c (Right _) = logMsg c DEBUG ("Timed cache invalidation SUCCESS for key: "<> show k)

-- | Values held by the cache
data CachedValue where
    JobFTSV                :: PaginatedList JobWithCompany -> CachedValue
    ActiveJobOnlyFTSV      :: PaginatedList JobWithCompany -> CachedValue
    CompanyStatsV          :: CompanyStats -> CachedValue
    StarredJobsV           :: PaginatedList JobWithCompany -> CachedValue
    CompanyListingV        :: PaginatedList (ModelWithID Company) -> CachedValue
    TagListingV            :: PaginatedList (ModelWithID Tag) -> CachedValue
    TagFTSV                :: PaginatedList TagID -> CachedValue
    EmailSubscriptionTagsV :: PaginatedList (ModelWithID Tag) -> CachedValue

deriving instance Generic CachedValue
deriving instance Show CachedValue
deriving instance Eq CachedValue

-- | Base requirements of a cache
class Cache c where
    getCacheLogger :: c -> Maybe Logger
    getCacheConfig :: c -> CacheConfig

-- | Requirements of a cache in a monadic context
class (Monad m, Cache c) => CacheM c m where
    -- | Connect to the cache backend
    connectCacheBackend :: c -> m (Either SomeException c)

    -- | Look up a value from the cache
    get :: CacheKey -> c -> m (Maybe CachedValue)

    -- | Insert a value into the cache
    insert :: CacheKey -> c -> CachedValue -> m ()

    -- Helper functions for looking up values
    getJobListing :: CacheKey -> c -> m (Maybe (PaginatedList JobWithCompany))
    getCompanyStats :: CacheKey -> c -> m (Maybe CompanyStats)
    getStarredJobs :: CacheKey -> c -> m (Maybe (PaginatedList JobWithCompany))
    getCompanyListing :: CacheKey -> c -> m (Maybe (PaginatedList (ModelWithID Company)))
    getTagListing :: CacheKey -> c -> m (Maybe (PaginatedList (ModelWithID Tag)))
    getTagFTSResults :: CacheKey -> c -> m (Maybe (PaginatedList TagID))
    getEmailSubscriptionTags :: CacheKey -> c -> m (Maybe (PaginatedList (ModelWithID Tag)))

    -- Helper functions for inserting values
    insertJobListing :: CacheKey -> c -> PaginatedList JobWithCompany -> m ()
    insertCompanyStats :: CacheKey -> c -> CompanyStats -> m ()
    insertStarredJobs :: CacheKey -> c -> PaginatedList JobWithCompany -> m ()
    insertCompanyListing :: CacheKey -> c -> PaginatedList (ModelWithID Company) -> m ()
    insertTagListing :: CacheKey -> c -> PaginatedList (ModelWithID Tag) -> m ()
    insertTagFTSResults :: CacheKey -> c -> PaginatedList TagID -> m ()
    insertEmailSubscriptionTags :: CacheKey -> c -> PaginatedList (ModelWithID Tag) -> m ()

    -- | Check whether a key has a value
    hasValue :: CacheKey -> c -> m Bool

    -- | Invalidate a value already in the cache
    invalidate :: CacheKey -> c -> m ()

    -- Helper functions for invalidating specific caches
    invalidateAllJobListings :: c -> m ()
    invalidateCompanyListing :: c -> m ()
    invalidateTagListing :: c -> m ()
    invalidateTagFTSResults :: c -> m ()


instance HasLogger CacheBackend where
    getComponentLogger = cbLogger

instance Cache CacheBackend where
    getCacheConfig = cbCfg
    getCacheLogger = cbLogger

instance MonadIO m => CacheM CacheBackend m where
    connectCacheBackend = return . Right -- This will have to chance once I have a non-local-memory type of CacheBackend

    get k c = getCacheEntry k (cbCache c)
              >>= logCacheHitOrMiss c k

    insert k c v = insertCacheEntry k v (cbCache c)
                   >>= logCacheUpdate c

    -- Helpers for getting
    getJobListing k c = get k c
                        >>= \case
                            Just (JobFTSV v) -> pure $ Just v
                            Just (ActiveJobOnlyFTSV v) -> pure $ Just v
                            Nothing -> pure Nothing
                            _ -> throw InvalidCacheKey

    getCompanyStats k c = get k c
                          >>= \case
                              Just (CompanyStatsV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    getStarredJobs k c = get k c
                          >>= \case
                              Just (StarredJobsV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    getCompanyListing k c = get k c
                          >>= \case
                              Just (CompanyListingV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    getTagListing k c = get k c
                          >>= \case
                              Just (TagListingV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    getTagFTSResults k c = get k c
                          >>= \case
                              Just (TagFTSV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    getEmailSubscriptionTags k c = get k c
                          >>= \case
                              Just (EmailSubscriptionTagsV v) -> pure $ Just v
                              Nothing -> pure Nothing
                              _ -> throw InvalidCacheKey

    -- Helpers for inserting
    insertJobListing k c v = case k of
                               (JobFTS _) -> insert k c (JobFTSV v)
                               (ActiveJobOnlyFTS _) -> insert k c (ActiveJobOnlyFTSV v)
                               _ -> throw InvalidCacheKey

    insertCompanyStats k c v = case k of
                               (Cache.CacheBackend.CompanyStats _) -> insert k c (CompanyStatsV v)
                                                                      -- | Timed invalidation of company stats
                                                                      >>= \res -> timedCacheInvalidation k c invalidationTimeMs
                                                                      >> logCacheUpdate c res
                               _ -> throw InvalidCacheKey
        where
          invalidationTimeMs = (cacheDefaultTimedInvalidationMs . getCacheConfig) c

    insertStarredJobs k c v = case k of
                               (StarredJobs _) -> insert k c (StarredJobsV v)
                               _ -> throw InvalidCacheKey

    insertCompanyListing k c v = case k of
                               (CompanyListing _ _) -> insert k c (CompanyListingV v)
                               _ -> throw InvalidCacheKey

    insertTagListing k c v = case k of
                               (TagListing _ _) -> insert k c (TagListingV v)
                               _ -> throw InvalidCacheKey

    insertTagFTSResults k c v = case k of
                               (TagFTS _) -> insert k c (TagFTSV v)
                               _ -> throw InvalidCacheKey

    insertEmailSubscriptionTags k c v = case k of
                               (EmailSubscriptionTags _) -> insert k c (EmailSubscriptionTagsV v)
                               _ -> throw InvalidCacheKey

    hasValue k = checkCacheEntry k . cbCache

    invalidate k = removeCacheEntry k . cbCache

    invalidateAllJobListings = filterCache filter . cbCache
        where
          filter k _ = case k of
                         (JobFTS _)           -> False
                         (ActiveJobOnlyFTS _) -> False
                         _ -> True

    invalidateCompanyListing = filterCache filter . cbCache
        where
          filter k _ = case k of
                         (CompanyListing _ _) -> False
                         _ -> True

    invalidateTagListing = filterCache filter . cbCache
        where
          filter k _ = case k of
                         (TagListing _ _) -> False
                         _ -> True

    invalidateTagFTSResults = filterCache filter . cbCache
        where
          filter k _ = case k of
                         (TagFTS _) -> False
                         _ -> True


class (MonadIO m, CacheM c m) => HasCacheBackend m c | m -> c where
    getCacheBackend :: m c
