{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE InstanceSigs           #-}

module Mailer.MailerBackend ( makeConnectedMailerBackend
                            , MailerBackend
                            , Mailer(..)
                            , MailerM(..)
                            , MailTemplate(..)
                            , EmailLogHandler(..)
                            , MailerBackendError(..)
                            , MailTemplateData(..)
                            , HasMailerBackend(..)
                            ) where

import           Config (LoggerConfig(..), MailerConfig(..), MailerBackendType(..))
import           Control.Exception (Exception, IOException, SomeException, try, throw)
import           Control.Monad.Writer
import           Data.DateTime (formatDateTime)
import           Data.Default (def)
import           Data.Either.Combinators (rightToMaybe)
import           Data.Hashable (Hashable)
import           Data.Maybe (isNothing, isJust, fromJust)
import           Data.Monoid ((<>))
import           Data.List (isInfixOf)
import           GHC.Generics (Generic)
import           Network.Mail.Mime (Address(..), Mail, renderMail', simpleMail)
import           Network.Mail.SMTP (sendMail', sendMailWithLogin')
import           System.FilePath ((</>))
import           System.IO (openFile, hGetContents, IOMode(ReadMode))
import           System.IO.Error (tryIOError)
import           System.Log (LogRecord)
import           System.Log.Formatter (LogFormatter)
import           System.Log.Logger (Logger, Priority(..), logL)
import           Text.Ginger (GingerContext, Template, VarName, GVal, parseGingerFile)
import           Text.Ginger.GVal (toGVal)
import           Text.Ginger.Html (Html, htmlSource)
import           Text.Ginger.Parse (ParserError, SourceName, SourcePos)
import           Text.Ginger.Run (Run, easyRender, runGinger, makeContextHtml, makeContextText)
import           Text.Ginger.Run.FuncUtils (unaryFunc)
import           Types (HasLogger(..), PasswordResetToken, BrandingData, User(..), Email, TagName, ServerInfo, ConfirmationCode, UnsubscribeCode, SponsoredCompany, ModelWithID, Job, EmailSubscription(..), JobWithCompany)
import           Util (throwIfLeft, throwIf, ifNothingThrowError)
import qualified Data.ByteString.Char8 as BS8
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as DT
import qualified Data.Text.Lazy as DTL
import qualified System.Log.Handler as SLH
import qualified System.IO.ExceptionFree as ExceptionFree

newtype Sender = Sender Address
newtype Recipient = Recipient Address
newtype TextTemplate = TextTemplate (Template SourcePos) deriving (Show)
newtype HtmlTemplate = HtmlTemplate (Template SourcePos) deriving (Show)

type TemplatePair = (TextTemplate, HtmlTemplate)
type Title = DT.Text
type TemplateRegistry = HMS.HashMap MailTemplate TemplatePair

data MailerCfg = MailerCfg MailerConfig BrandingData ServerInfo

data MailerBackend = LocalMailer (Maybe Logger) MailerCfg TemplateRegistry
                   | SMTPMailer (Maybe Logger) MailerCfg TemplateRegistry

data MailerBackendError = MissingTemplate String
                        | TemplateParseError String
                        | TemplateRenderingError deriving (Eq)

instance Exception MailerBackendError

instance Show MailerBackendError where
    show (MissingTemplate name) = "The html and/or text templates are missing for the following templates: " <> name
    show TemplateRenderingError = "An error occurred attempting to render the template"

data MailTemplate = ConfirmEmailSubscription
                  | PasswordResetInitiated
                  | PasswordResetCompleted
                  | TestEmail
                  | WeeklyNewsletter deriving (Read, Eq, Show, Generic, Enum)

instance Hashable MailTemplate

data MailTemplateData = ConfirmEmailSubscription_ ServerInfo BrandingData Email [TagName] ConfirmationCode
                  | PasswordResetInitiated_ BrandingData User PasswordResetToken
                  | PasswordResetCompleted_ BrandingData User
                  | TestEmail_ ServerInfo BrandingData Email User UnsubscribeCode
                  | WeeklyNewsletter_ ServerInfo BrandingData User UnsubscribeCode [JobWithCompany] (Maybe SponsoredCompany) deriving (Show, Read, Eq)

setTemplates :: MailerBackend -> TemplateRegistry -> MailerBackend
setTemplates (LocalMailer l c t) = LocalMailer l c
setTemplates (SMTPMailer l c t) = SMTPMailer l c

getFilePrefix :: MailTemplate -> String
getFilePrefix ConfirmEmailSubscription = "confirm-email-subscription"
getFilePrefix PasswordResetInitiated  = "password-reset-initiated"
getFilePrefix PasswordResetCompleted  = "password-reset-completed"
getFilePrefix TestEmail  = "test-email"
getFilePrefix WeeklyNewsletter  = "weekly-newsletter"

getMailerCtor :: MailerConfig -> Maybe Logger -> MailerCfg -> TemplateRegistry -> MailerBackend
getMailerCtor c = case backendType of
                    LocalMailerBackend -> LocalMailer
                    SMTP -> SMTPMailer
    where
      backendType = mailBackendType c

makeConnectedMailerBackend :: MonadIO m => MailerConfig -> Maybe Logger -> BrandingData -> ServerInfo -> m (Either SomeException MailerBackend)
makeConnectedMailerBackend c l b si = liftIO $ try startMailer
    where
      host = mailHostname c
      templatesPath = mailTemplatesPath c
      ctor = getMailerCtor c
      makeMailerWithRegistry = ctor l (MailerCfg c b si)
      startMailer = when (isJust l) (logL (fromJust l) INFO ("Attempting to start mailer, templatesPath = " ++ templatesPath))
                    -- Build template registry
                    >> buildRegistry templatesPath
                    >>= \registry -> when (isJust l) (logL (fromJust l) INFO ("Successfully build template registry from path [" <> templatesPath <> "]"))
                    -- Connect the mailer
                    >> connectMailer (makeMailerWithRegistry registry)
                    >>= \m -> logMsg m INFO ("Successfully loaded templates from " <> show templatesPath)
                    -- Return the built mailer
                    >> pure m

renderMailForLog :: MonadIO m => Mail -> m String
renderMailForLog = liftIO . (BSL8.unpack<$>) . renderMail'

wrapLogMsg :: String -> String
wrapLogMsg = ("\n----------------Sending email----------------\n"<>) . (<>"\n---------------------------------------------\n")

type HtmlContext = GingerContext SourcePos (Writer Html) Html
type TextContext = GingerContext SourcePos (Writer DT.Text) DT.Text
type ContextPair = (TextContext, HtmlContext)

makeMailContexts :: MailTemplateData -> (TextContext, HtmlContext)
makeMailContexts (PasswordResetInitiated_ bd user token) = (makeContextText lookup, makeContextHtml lookup)
    where
      lookup varName = case varName of
                         "branding" -> toGVal bd
                         "user"     -> toGVal user
                         "token"    -> toGVal token
                         _ -> def

makeMailContexts (PasswordResetCompleted_ bd user) = (makeContextText lookup, makeContextHtml lookup)
    where
      lookup varName = case varName of
                         "branding" -> toGVal bd
                         "user"     -> toGVal user
                         _ -> def

makeMailContexts (ConfirmEmailSubscription_ si bd email tags code) = (makeContextText lookup, makeContextHtml lookup)
    where
      lookup varName = case varName of
                         "serverInfo"      -> toGVal si
                         "branding"        -> toGVal bd
                         "subscriberEmail" -> toGVal email
                         "tagNames"        -> toGVal tags
                         "confirmcode"     -> toGVal code
                         _ -> def

makeMailContexts (TestEmail_ si bd email user unsubCode) = (makeContextText lookup, makeContextHtml lookup)
    where
      lookup varName = case varName of
                         "serverInfo"     -> toGVal si
                         "branding"       -> toGVal bd
                         "recipientEmail" -> toGVal email
                         "user"           -> toGVal user
                         "unsubCode"      -> toGVal unsubCode
                         _ -> def

makeMailContexts (WeeklyNewsletter_ si bd user unsubCode jobs sc) = (makeContextText lookup, makeContextHtml lookup)
    where
      lookup varName = case varName of
                         "serverInfo"       -> toGVal si
                         "branding"         -> toGVal bd
                         "user"             -> toGVal user
                         "unsubCode"        -> toGVal unsubCode
                         "jobs"             -> toGVal jobs
                         "sponsoredCompany" -> toGVal sc
                         _ -> def

-- | Load a single file
loadFileMay path = ExceptionFree.readFile path
                   >>= either (\err -> print err >> return Nothing) (return . Just)

buildRegistry :: MonadIO m => FilePath -> m TemplateRegistry
buildRegistry templateDir = liftIO loadAndCombine
    where
      logFailureAndReturn :: MailerBackend -> SomeException -> IO MailerBackend
      logFailureAndReturn s = (>> return s) . logMsg s INFO . ("Failed to load templates. Error: "<>) . show

      makeFilePaths :: String -> [String]
      makeFilePaths f = [templateDir </> f <> ".text.jinja2", templateDir </> f <> ".html.jinja2"]

      loadTemplatePair :: MailTemplate -> String -> IO TemplateRegistry
      loadTemplatePair name prefix = mapM (parseGingerFile loadFileMay) (makeFilePaths prefix)
                                     >>= mapM throwIfLeft
                                     >>= \[text, html] -> pure (HMS.singleton name (TextTemplate text, HtmlTemplate html))

      loadAndCombine = HMS.unions <$> mapM (\t -> loadTemplatePair t (getFilePrefix t)) ([toEnum 0 ..] :: [MailTemplate])

-- | Basic required functionality for a given Mailer
class Mailer mailer where
    getMailerLogger :: mailer -> Maybe Logger
    isLocalMailer :: mailer -> Bool
    isNotLocalMailer :: mailer -> Bool

    getFromAddress :: mailer -> String
    getBrandingData :: mailer -> BrandingData
    getServerInfo :: mailer -> ServerInfo

    -- ^ Email template related functionality
    getTemplateRegistry :: mailer -> TemplateRegistry
    getTemplatePair :: MailTemplate -> mailer -> Maybe TemplatePair
    hasTemplate :: MailTemplate -> mailer -> Bool

    renderTemplate :: MailTemplate -> MailTemplateData -> mailer -> Either MailerBackendError TemplatePair

-- | Monadic actions that mailers must exhibit
class (Mailer mailer, Monad m) => MailerM mailer m where
    connectMailer :: mailer -> m mailer -- ^ Take a possibly disconnected mailer and connect it
    sendMail :: mailer -> Mail -> m ()

    renderTemplateToMail :: mailer -> Title -> Sender -> Recipient -> ContextPair -> MailTemplate -> m Mail

    -- Password reset
    sendPasswordResetInitiated :: mailer -> User -> PasswordResetToken -> m (Either SomeException ())
    sendPasswordResetCompleted :: mailer -> User -> m (Either SomeException ())

    -- Mailing list
    sendSubscriptionConfirmation :: mailer -> Email -> [TagName] -> ConfirmationCode -> m (Either SomeException ())
    sendWeeklyNewsletter :: mailer -> User -> EmailSubscription -> [JobWithCompany] -> Maybe SponsoredCompany -> m (Either SomeException ())

    sendTestEmail :: mailer -> Email -> User -> EmailSubscription -> m (Either SomeException ())

instance Mailer MailerBackend where
    getMailerLogger (LocalMailer l _ _) = l
    getMailerLogger (SMTPMailer l _ _) = l

    isLocalMailer LocalMailer{} = True
    isLocalMailer _             = False

    isNotLocalMailer = not . isLocalMailer

    getFromAddress (LocalMailer _ (MailerCfg cfg  _ _) _) = mailFromAddress cfg
    getFromAddress (SMTPMailer _ (MailerCfg cfg _ _) _) = mailFromAddress cfg

    getBrandingData (LocalMailer _ (MailerCfg _ b _) _) = b
    getBrandingData (SMTPMailer _ (MailerCfg _  b _) _) = b

    getServerInfo (LocalMailer _ (MailerCfg _  _ si) _) = si
    getServerInfo (SMTPMailer _ (MailerCfg _  _ si) _) = si

    getTemplateRegistry (LocalMailer _ _ r) = r
    getTemplateRegistry (SMTPMailer _ _ r) = r

    hasTemplate t m =  HMS.member t (getTemplateRegistry m)

    getTemplatePair name = HMS.lookup name . getTemplateRegistry

    renderTemplate template tData = maybe (Left TemplateRenderingError) Right . getTemplatePair template


instance MonadIO m => MailerM MailerBackend m where
    connectMailer m@LocalMailer{} = logMsg m INFO "Connected to LocalMailer (No-op)" >> return m
    connectMailer s@(SMTPMailer maybeL cfg@(MailerCfg c branding si) templates) = logMsg s INFO "Set up SMTPMailer..."
                                                                                  >> pure (SMTPMailer maybeL (MailerCfg c branding si) templates)

    sendMail m@LocalMailer{}                      mail = renderMailForLog mail >>= logMsg m INFO . wrapLogMsg
    sendMail (SMTPMailer _ (MailerCfg cfg _ _) _) mail = liftIO (sendFn mail)
        where
          host = mailHostname cfg
          port = mailPortNumber cfg
          username = mailUsername cfg
          password = mailPassword cfg
          noLoginDetails = null username && null password
          sendFn = if noLoginDetails
                   then sendMail' host port
                   else sendMailWithLogin' host port username password

    renderTemplateToMail m title (Sender s) (Recipient r) (textContext, htmlContext) name = liftIO renderTemplateToMail_
        where
          genHtml (HtmlTemplate t) = DTL.fromStrict $ htmlSource $ runGinger htmlContext t
          genText (TextTemplate t) = DTL.fromStrict $ runGinger textContext t
          renderTemplateToMail_ = pure (getTemplatePair name m)
                                  >>= ifNothingThrowError (MissingTemplate (show name))
                                  >>= \(text, html) -> simpleMail r s title (genText text) (genHtml html) []

    sendPasswordResetInitiated m user token = liftIO $ try renderAndSend
        where
          sender = Sender $ Address Nothing $ DT.pack $ getFromAddress m
          recipient = Recipient $ Address Nothing $ DT.pack $ emailAddress user
          title = "Password reset initiated"
          contexts = makeMailContexts $ PasswordResetInitiated_ (getBrandingData m) user token
          renderAndSend = renderTemplateToMail m title sender recipient contexts PasswordResetInitiated >>= sendMail m

    sendPasswordResetCompleted m user = liftIO $ try renderAndSend
        where
          sender = Sender $ Address Nothing $ DT.pack $ getFromAddress m
          recipient = Recipient $ Address Nothing $ DT.pack $ emailAddress user
          title = "Password reset completed"
          contexts = makeMailContexts $ PasswordResetCompleted_ (getBrandingData m) user
          renderAndSend = renderTemplateToMail m title sender recipient contexts PasswordResetCompleted >>= sendMail m

    sendSubscriptionConfirmation m email tags code = liftIO $ try renderAndSend
        where
          sender = Sender $ Address Nothing $ DT.pack $ getFromAddress m
          recipient = Recipient $ Address Nothing $ DT.pack email
          title = "Confirm email subscription"
          contexts = makeMailContexts $ ConfirmEmailSubscription_ (getServerInfo m) (getBrandingData m) email tags code
          renderAndSend = renderTemplateToMail m title sender recipient contexts ConfirmEmailSubscription >>= sendMail m

    sendTestEmail m email user sub = liftIO $ try renderAndSend
        where
          sender = DT.pack $ getFromAddress m
          senderAddress = Sender $ Address Nothing sender
          recipient = DT.pack email
          unsubCode = esUnsubscribeCode sub
          recipientAddress = Recipient $ Address Nothing recipient
          title = "Test email (sent by " <> sender <> ")"
          contexts = makeMailContexts $ TestEmail_ (getServerInfo m) (getBrandingData m) email user unsubCode
          renderAndSend = renderTemplateToMail m title senderAddress recipientAddress contexts TestEmail >>= sendMail m

    sendWeeklyNewsletter m user sub js sc = liftIO $ try renderAndSend
        where
          sender = Sender $ Address Nothing $ DT.pack $ getFromAddress m
          unsubCode = esUnsubscribeCode sub
          recipient = Recipient $ Address Nothing $ DT.pack $ emailAddress user
          title = "TechJobs.Tokyo.Weekly"
          contexts = makeMailContexts $ WeeklyNewsletter_ (getServerInfo m) (getBrandingData m) user unsubCode js sc
          renderAndSend = renderTemplateToMail m title sender recipient contexts WeeklyNewsletter >>= sendMail m

instance HasLogger MailerBackend where
    getComponentLogger (LocalMailer l _ _) = l
    getComponentLogger (SMTPMailer l _ _) = l

class (MonadIO m, MailerM mb m) => HasMailerBackend m mb | m -> mb where
    getMailerBackend :: m mb

------------------------
-- Email log handling --
------------------------

data EmailLogHandler = EmailLogHandler { mlhBackend   :: MailerBackend
                                       , mlhLevel     :: Priority
                                       , mlhFormatter :: LogFormatter EmailLogHandler
                                       , loggerConfig :: LoggerConfig
                                       }

instance SLH.LogHandler EmailLogHandler where
    setLevel mlh l = mlh { mlhLevel=l }
    getLevel =  mlhLevel

    setFormatter mlh f = mlh { mlhFormatter=f }
    getFormatter = mlhFormatter

    close _ = pure ()

    emit :: EmailLogHandler -> LogRecord -> String -> IO ()
    emit mlh (priority, msg) logName = unless (isIgnoredRecord msg) sendLogEmail

        where
          ignoreConditions = [ isInfixOf "Client closed connection prematurely" ]
          isIgnoredRecord = or . (ignoreConditions <*>) . (:[])

          to = Nothing `Address` (DT.pack . loggerEmailToAddr . loggerConfig) mlh
          from = Nothing `Address` (DT.pack . loggerEmailFromAddr . loggerConfig) mlh
          subject = DT.pack $ logName ++ " - " ++ show priority
          body = DTL.pack $ "Log record:\n" ++ msg

          sendLogEmail = simpleMail to from subject body body []
                         >>= sendMail (mlhBackend mlh)
