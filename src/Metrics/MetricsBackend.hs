{-# LANGUAGE OverloadedStrings    #-}

module Metrics.MetricsBackend ( makeConnectedMetricsBackend
                              , MetricsBackend(..)
                              , Metrics(..)
                              ) where

import           Config (MetricsConfig(..))
import           Data.Maybe (Maybe, fromMaybe)
import           Control.Monad.State (when)
import           Data.Monoid ((<>))
import           Network.Wai (Application)
import           Network.Wai.Middleware.Prometheus (PrometheusSettings(..), prometheus, instrumentApp, instrumentIO)
import           System.Log.Logger (Logger, Priority(..))
import           Types (HasLogger(..))
import           Control.Exception (SomeException)
import           Data.Either (Either ( Right ))
import qualified Data.Text as DT

data MetricsBackend = MetricsBackend { mbCfg        :: MetricsConfig
                                     , mbLogger     :: Maybe Logger
                                     , mbPromConfig :: PrometheusSettings
                                     }

type MetricsSettings = PrometheusSettings

makeConnectedMetricsBackend :: MetricsConfig -> Maybe Logger -> IO (Either SomeException MetricsBackend)
makeConnectedMetricsBackend cfg maybeL = pure $ Right MetricsBackend { mbCfg=cfg
                                                                     , mbLogger=maybeL
                                                                     , mbPromConfig=makePromSettings cfg
                                                                     }

makePromSettings :: MetricsConfig -> PrometheusSettings
makePromSettings cfg = PrometheusSettings { prometheusEndPoint=metricsEndpointURLs cfg
                                          , prometheusInstrumentApp=metricsAppWide cfg
                                          , prometheusInstrumentPrometheus=metricsIncludeMetricsEndpoint cfg
                                          }

class Metrics m where
    getMetricsLogger :: m -> Maybe Logger
    getMetricsConfig :: m -> MetricsConfig

    instrument :: m -> Application -> IO Application
    instrumentAction :: m -> String -> IO a -> IO a

instance HasLogger MetricsBackend where
    getComponentLogger = mbLogger

instance Metrics MetricsBackend where
    getMetricsLogger = mbLogger
    getMetricsConfig = mbCfg

    instrument m app = logMsg m INFO ("App-wide Metrics: " <> if appWideInstrumentationEnabled then "ON" else "OFF")
                       >> if appWideInstrumentationEnabled then instrument else pure app
        where
          appWideInstrumentationEnabled = (metricsAppWide . getMetricsConfig) m
          instrument = logMsg m INFO "Setting up metrics WAI midleware..."
                       >> pure (prometheus (mbPromConfig m) app)
                       >>= \updatedApp -> logMsg m INFO "Finished setting up tracing middleware"
                       >> pure updatedApp


    instrumentAction m metric action = logMsg m DEBUG ("Instrumenting endpoint " <> metric)
                                       >> instrumentIO (DT.pack metric) action
