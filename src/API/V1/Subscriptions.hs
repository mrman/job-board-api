{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Subscriptions ( Routes
                             , server
                            ) where

import           AuthN (CookieAuth, sessionUserID)
import           AuthZ (requireRole)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (when, liftIO)
import           Data.Monoid ((<>))
import           Cache.CacheBackend (CacheBackend(..), Cache(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           Mailer.MailerBackend (MailerM(..), MailerBackend(..), HasMailerBackend(..))
import           Servant
import           Types
import           Util (ifNothingEnvelopeAndThrow, ifLeftEnvelopeAndThrow, ifLeftConvertAndThrow, lookupOrComputeAndSave)
import qualified Data.Text as DT
import qualified Errors as Err

-- | Subscriptions API routes
type Routes = "subscriptions" :> "email" :> QueryParam "email" DT.Text :> QueryParams "tagIds" TagID :> Post '[JSON] (EnvelopedResponse ())
    :<|> "subscriptions" :> "email" :> "confirm" :> QueryParam "token" ConfirmationCode :> Post '[JSON] (EnvelopedResponse ())
    :<|> "subscriptions" :> "email" :> "unsubscribe" :> QueryParam "token" ConfirmationCode :> Post '[JSON] (EnvelopedResponse ())
    :<|> "subscriptions" :> "email" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID EmailSubscription)))
    :<|> "subscriptions" :> "email" :> Capture "subscriptionId" EmailSubscriptionID :> "tags" :> CookieAuth :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Tag)))
    :<|> "subscriptions" :> "email" :> Capture "subscriptionId" EmailSubscriptionID :> CookieAuth :> Get '[JSON] (EnvelopedResponse (ModelWithID EmailSubscription))

-- | Subscriptions API server
server :: ( HasDBBackend m db
          , HasCacheBackend m c
          , HasMailerBackend m mb
          , MonadError ServerError m
          , TagStore db m
          , EmailSubscriptionStore db m
          )
         => ServerT Routes m
server = createEmailSubscription
         :<|> confirmSubscriptionEmail
         :<|> cancelSubscriptionEmail
         :<|> listEmailSubscriptions
         :<|> listTagsForEmailSubscription
         :<|> findEmailSubscriptionByID

-- | Limit of tags to subscribe to
tagSubLimit :: Int
tagSubLimit = 3

throwTagLimitExceeded :: MonadError ServerError m => m ()
throwTagLimitExceeded = throwError $ Err.tagLimitExceeded tagSubLimit

-- | List email subscriptions
listEmailSubscriptions :: ( HasDBBackend m db
                          , MonadError ServerError m
                          , EmailSubscriptionStore db m
                          ) => SessionInfo -> Maybe Limit -> Maybe Offset -> m (EnvelopedResponse (PaginatedList (ModelWithID EmailSubscription)))
listEmailSubscriptions sessionInfo limit offset  = requireRole Administrator sessionInfo
                                                   >> getDBBackend
                                                   -- Get the tags specified by the given IDs
                                                   >>= \backend -> getAllEmailSubscriptions backend limit offset
                                                   >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                                                   >>= pure . EnvelopedResponse "success" "Successfully retrieved all email subscriptions"

-- | Create an email subscription
createEmailSubscription :: ( HasDBBackend m db
                           , HasMailerBackend m mb
                           , MonadError ServerError m
                           , TagStore db m
                           , EmailSubscriptionStore db m
                           ) => Maybe DT.Text -> [TagID] -> m (EnvelopedResponse ())
createEmailSubscription Nothing _ = throwError Err.genericBadRequest
createEmailSubscription (Just email) tagIds = when (length tagIds > tagSubLimit) throwTagLimitExceeded
                                              >> getDBBackend
                                              >>= \backend -> getMailerBackend
                                              -- Get the tags specified by the given IDs
                                              >>= \mailer -> findTagsByIDs backend tagIds
                                              >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                              -- Create an email subscription with the given tags
                                              >>= \(PaginatedList ts  _) -> saveEmailSubscription backend email (mId <$> ts)
                                              >>= ifLeftEnvelopeAndThrow Err.failedToAddResource
                                              -- Send subscription confirmation email
                                              >>= \(ModelWithID _ sub) -> sendSubscriptionConfirmation mailer (DT.unpack (esEmailAddress sub)) (tagName . model <$> ts) (esConfirmationCode sub)
                                              >>= ifLeftEnvelopeAndThrow (Err.failedToSendEmail "subscription confirmation")
                                              >> pure (EnvelopedResponse "success" "Successfully created new email subscription" ())

-- | Confirm an email subscription
confirmSubscriptionEmail :: ( HasDBBackend m db
                            , HasMailerBackend m mb
                            , MonadError ServerError m
                            , EmailSubscriptionStore db m
                            ) => Maybe ConfirmationCode -> m (EnvelopedResponse ())
confirmSubscriptionEmail Nothing        = throwError Err.genericBadRequest
confirmSubscriptionEmail (Just code)    = getDBBackend
                                          >>= flip confirmEmailSubscription code
                                          >>= ifLeftEnvelopeAndThrow Err.genericError
                                          >> pure (EnvelopedResponse "success" "Successfully confirmed email subscription" ())

-- | Cancel an email subscription
cancelSubscriptionEmail :: ( HasDBBackend m db
                           , HasMailerBackend m mb
                           , MonadError ServerError m
                           , EmailSubscriptionStore db m
                           ) => Maybe ConfirmationCode -> m (EnvelopedResponse ())
cancelSubscriptionEmail Nothing     = throwError Err.genericBadRequest
cancelSubscriptionEmail (Just code) = getDBBackend
                                      >>= flip cancelEmailSubscription code
                                      >>= ifLeftEnvelopeAndThrow Err.genericError
                                      >> pure (EnvelopedResponse "success" "Successfully cancelled email subscription" ())


-- | List tags for a given email subscription
listTagsForEmailSubscription :: ( HasDBBackend m db
                                , HasCacheBackend m c
                                , MonadError ServerError m
                                , EmailSubscriptionStore db m
                                )
                                => EmailSubscriptionID
                                    -> SessionInfo
                                    -> m (EnvelopedResponse (PaginatedList (ModelWithID Tag)))
listTagsForEmailSubscription esId sessionInfo  = requireRole Administrator sessionInfo
                                                 >> getDBBackend
                                                 >>= \db -> getCacheBackend
                                                 -- Get the email subscription tags from cache if possible
                                                 >>= \cache -> getCachedEmailSubscriptionTags db cache
                                                 >>= ifNothingEnvelopeAndThrow Err.failedToFindEntities
                                                 -- Get the tags specified by the given IDs
                                                 >>= pure . EnvelopedResponse "success" "Successfully retrieved tags"
      where
      cacheKey = EmailSubscriptionTags esId

      lookup = getEmailSubscriptionTags cacheKey
      -- FIXME: Why do the monadic contexts not work here? lookupOrComputeAndSaveEither *should* work
      -- Possibly because the *same* monadic context is used for lookup & compute?
      compute db = either (const Nothing) Just <$> getTagsForEmailSubscription db esId
      updateCache = insertEmailSubscriptionTags cacheKey

      getCachedEmailSubscriptionTags db cache = lookupOrComputeAndSave (lookup cache) (compute db) (updateCache cache)

-- | Find an email subscription by ID
findEmailSubscriptionByID :: ( HasDBBackend m db
                             , MonadError ServerError m
                             , EmailSubscriptionStore db m
                             )
                            => EmailSubscriptionID
                               -> SessionInfo
                               -> m (EnvelopedResponse (ModelWithID EmailSubscription))
findEmailSubscriptionByID subId sessionInfo = requireRole Administrator sessionInfo
                                              >> getDBBackend
                                              >>= flip getEmailSubscriptionByID subId
                                              >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                              >>= pure . EnvelopedResponse "success" "Successfully retrieved email subscription"
