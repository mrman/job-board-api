{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Jobs ( Routes
                   , server
                   , jobSearch
                   , changeJobActivity_
                   ) where

import           AuthN (CookieAuth)
import           AuthZ (ensureUserIsAdminOrCompanyRep, requireRole)
import           Cache.CacheBackend (CacheBackend(..), Cache(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           Control.Exception (try)
import           Control.Monad (join, void)
import           Control.Monad.Except (MonadError)
import           Control.Monad.IO.Class
import           Search.SearchBackend (SearchBackend(..), SearchBackendM(..), HasSearchBackend(..))
import           Servant
import           Types
import           Util (ifNothingEnvelopeAndThrow, ifLeftEnvelopeAndThrow, trimSearchTerm, lookupOrComputeAndSaveEither, throwServantErrorIf)
import qualified Data.DateTime as DDT
import qualified Errors as Err

-- | Jobs API routes
type Routes = "jobs" :> "postings" :> "requests" :> ReqBody '[JSON] JobPostingRequest :> Post '[JSON] (EnvelopedResponse (ModelWithID JobPostingRequest))
    :<|> "jobs" :> "postings" :> "requests" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID JobPostingRequest)))
    :<|> "jobs" :> "postings" :> "requests" :> "pending" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID JobPostingRequest)))
    :<|> "jobs" :> "postings" :> "requests" :> CookieAuth :> Capture "id" JobPostingRequestID :> Get '[JSON] (EnvelopedResponse (ModelWithID JobPostingRequest))
    :<|> "jobs" :> "postings" :> "requests" :> CookieAuth :> Capture "id" JobPostingRequestID :> ReqBody '[PatchJSON] MergePatchChangeObject :> Patch '[JSON] (EnvelopedResponse (ModelWithID JobPostingRequest))
    :<|> "jobs" :> "postings" :> "requests" :> CookieAuth :> Capture "id" JobPostingRequestID :> "approve" :> Get '[JSON] (EnvelopedResponse (ModelWithID Job))
    :<|> "jobs" :> Capture "jobId" JobID :> Get '[JSON] (EnvelopedResponse (ModelWithID Job))
    :<|> "jobs" :> CookieAuth :> Capture "jobId" JobID :> "activate" :> Post '[JSON] (EnvelopedResponse (ModelWithID Job))
    :<|> "jobs" :> CookieAuth :> Capture "jobId" JobID :> "deactivate" :> Post '[JSON] (EnvelopedResponse (ModelWithID Job))

    :<|> "jobs" :> "search"
             :> QueryParam "searchTerm" String
             :> QueryParams "industries" JobIndustry
             :> QueryParams "companyIds" CompanyID
             :> QueryParam "pageSize" Limit
             :> QueryParam "offset" Offset
             :> QueryParams "tags" TagName
             :> Get '[JSON] (EnvelopedResponse (PaginatedList JobWithCompany))

    -- Full-powered (shows non-active) job searches
    :<|> "_jobs" :> "search"
             :> QueryParam "searchTerm" String
             :> QueryParams "industries" JobIndustry
             :> QueryParams "companyIds" CompanyID
             :> QueryParam "pageSize" Limit
             :> QueryParam "offset" Offset
             :> QueryParams "tags" TagName
             :> Get '[JSON] (EnvelopedResponse (PaginatedList JobWithCompany))
    -- | POST /jobs/:id
    :<|> "jobs" :> CookieAuth :> Capture "jobId" JobID :> ReqBody '[JSON] Job :> Post '[JSON] (EnvelopedResponse (ModelWithID Job))
    -- | PATCH /jobs/:id
    :<|> "jobs" :> CookieAuth :> Capture "id" JobID :> ReqBody '[PatchJSON] MergePatchChangeObject :> Patch '[JSON] (EnvelopedResponse (ModelWithID Job))

-- | Jobs API server
server :: ( MonadError ServerError m
          , HasSearchBackend m s
          , HasCacheBackend m c
          , HasDBBackend m db
          , JobPostingRequestStore db m
          , JobManager db m
          , CompanyStore db m
          ) => ServerT Routes m
server = createJobPostingRequest
         :<|> listJobPostingRequests
         :<|> listPendingJobPostingRequests
         :<|> findJobPostingRequestByID
         :<|> patchJobPostingRequestByID
         :<|> approveJobPostingRequest
         :<|> findJobByID
         :<|> changeJobActivity True
         :<|> changeJobActivity False
         :<|> jobSearch
         :<|> jobSearchAdmin
         :<|> updateJobByID
         :<|> patchJobByID

-- | Create a job posting request
createJobPostingRequest :: ( HasDBBackend m db
                           , MonadError ServerError m
                           , JobPostingRequestStore db m
                           ) => JobPostingRequest -> m (EnvelopedResponse (ModelWithID JobPostingRequest))
createJobPostingRequest jpr = throwServantErrorIf isInvalidJPR (Err.makeInvalidEntityError validationErrors)
                              >> getDBBackend
                              >>= \backend -> liftIO DDT.getCurrentTime
                              -- Update the posting request with server's "now" time
                              >>= \now -> pure (jpr { rRequestedAt=now })
                              >>= addJobPostingRequest backend
                              >>= ifLeftEnvelopeAndThrow Err.failedToCreateJobPostingRequest
                              >>= pure . EnvelopedResponse "success" "Successfully added job posting request"
      where
        validationErrors = validateWithErrors jpr
        isInvalidJPR = not (null validationErrors)

-- | List all job posting requests
listJobPostingRequests :: ( HasDBBackend m db
                          , MonadError ServerError m
                          , JobPostingRequestStore db m
                          )
                         => SessionInfo  -- ^ Session information
                             -> Maybe Limit  -- ^ Optional limit
                             -> Maybe Offset -- ^ Optional Offset
                             -> m (EnvelopedResponse (PaginatedList (ModelWithID JobPostingRequest)))
listJobPostingRequests sessionInfo limit offset = requireRole Administrator sessionInfo
                                                  >> getDBBackend
                                                  >>= listJobPostingRequests_ (maxLimitOr limit) offset ([] :: [JobPostingRequestFilter])

-- | List all pending job posting requests
listPendingJobPostingRequests :: ( HasDBBackend m db
                                 , MonadError ServerError m
                                 , JobPostingRequestStore db m
                                 )
                                => SessionInfo  -- ^ Session information
                                    -> Maybe Limit  -- ^ Optional limit
                                    -> Maybe Offset -- ^ Optional Offset
                                    -> m (EnvelopedResponse (PaginatedList (ModelWithID JobPostingRequest)))
listPendingJobPostingRequests sessionInfo limit offset = requireRole Administrator sessionInfo
                                                         >> getDBBackend
                                                         >>= listJobPostingRequests_ (maxLimitOr limit) offset [OnlyPending]

-- Helper method for  listing job posting requests
listJobPostingRequests_ :: ( HasDBBackend m db
                           , MonadError ServerError m
                           , JobPostingRequestStore db m
                           )
                          => Maybe Limit
                              -> Maybe Offset
                              -> [JobPostingRequestFilter]
                              -> db
                              -> m (EnvelopedResponse (PaginatedList (ModelWithID JobPostingRequest)))
listJobPostingRequests_ limit offset filters db = getAllJobPostingRequests db (maxLimitOr limit) offset filters
                                                  >>= ifLeftEnvelopeAndThrow Err.failedToFindJobPostingRequests
                                                  >>= pure . EnvelopedResponse "success" "Retrieved job posting requests"

-- | Find a single job posting request by ID
findJobPostingRequestByID :: ( HasDBBackend m db
                             , MonadError ServerError m
                             , JobPostingRequestStore db m
                             ) => SessionInfo -> JobPostingRequestID -> m (EnvelopedResponse (ModelWithID JobPostingRequest))
findJobPostingRequestByID sessionInfo jid = requireRole Administrator sessionInfo
                                    >> getDBBackend
                                    >>= \backend -> getJobPostingRequestByID backend jid
                                    >>= ifLeftEnvelopeAndThrow Err.failedToFindJobPostingRequest
                                    >>= pure . EnvelopedResponse "success" "Successfully retrieved job posting request"

{-# ANN patchJobPostingRequestByID ("HLint: ignore" :: String) #-}
-- | Update job posting request with a given ID
patchJobPostingRequestByID :: ( HasDBBackend m db
                              , MonadError ServerError m
                              , JobPostingRequestStore db m
                              )
                             => SessionInfo
                                 -> JobPostingRequestID
                                 -> MergePatchChangeObject
                                 -> m (EnvelopedResponse (ModelWithID JobPostingRequest))
patchJobPostingRequestByID sessionInfo jid changes = requireRole Administrator sessionInfo
                                           >> getDBBackend
                                           >>= \backend -> getJobPostingRequestByID backend jid
                                           >>= ifLeftEnvelopeAndThrow Err.failedToFindJobPostingRequest
                                           >>= return . mergeChanges changes . model
                                           >>= updateJobPostingRequestByID backend jid
                                           >>= ifLeftEnvelopeAndThrow Err.failedToUpdateResource
                                           >>= return . EnvelopedResponse "success" "Successfully patched job posting request"

-- | Approve a job posting request
approveJobPostingRequest :: ( HasSearchBackend m s
                            , HasCacheBackend m c
                            , HasDBBackend m db
                            , MonadError ServerError m
                            , JobPostingRequestStore db m
                            ) => SessionInfo -> JobPostingRequestID -> m (EnvelopedResponse (ModelWithID Job))
approveJobPostingRequest sessionInfo jid = requireRole Administrator sessionInfo
                                           >> getDBBackend
                                           >>= \db -> getCacheBackend
                                           >>= \cache -> getJobPostingRequestByID db jid
                                           >>= ifLeftEnvelopeAndThrow (Err.failedToFindEntity "Job Posting Request")
                                           -- Ensure the company id inside the found posting request is present
                                           >>= ifNothingEnvelopeAndThrow (Err.failedToFindEntity "Company (EmployerId)") . rCompanyId . model
                                           >>= approveJobPostingRequestByID db jid
                                           >>= ifLeftEnvelopeAndThrow (Err.makeGenericError "Failed to create approve job posting request")
                                           >>= \job -> getSearchBackend
                                           -- Since a job has just been promoted, refresh the jobs index
                                           >>= refreshJobsIndex
                                           >> invalidateAllJobListings cache
                                           >> pure (EnvelopedResponse "success" "Successfully approved job posting request" job)

-- | Find a single job by ID
findJobByID :: (HasDBBackend m db, MonadError ServerError m, JobStore db m) => JobID -> m (EnvelopedResponse (ModelWithID Job))
findJobByID jid = getDBBackend
                  >>= \backend -> getJobByID backend jid
                  >>= ifLeftEnvelopeAndThrow Err.failedToFindJob
                  >>= pure . EnvelopedResponse "success" "Successfully retrieved job"

-- | Change job activity for a given company
changeJobActivityForCompany :: ( HasDBBackend m db
                               , HasCacheBackend m c
                               , HasSearchBackend m s
                               , MonadError ServerError m
                               , CompanyStore db m
                               , JobManager db m
                               ) => Bool -> SessionInfo -> CompanyID -> JobID -> m (EnvelopedResponse (ModelWithID Job))
changeJobActivityForCompany active sessionInfo cid jid = ensureUserIsAdminOrCompanyRep cid sessionInfo
                                                         >> changeJobActivity_ active jid

-- | Update job activity (enable/disable)
changeJobActivity :: ( HasDBBackend m db
                     , HasCacheBackend m c
                     , HasSearchBackend m s
                     , MonadError ServerError m
                     , JobManager db m
                     ) => Bool -> SessionInfo -> JobID -> m (EnvelopedResponse (ModelWithID Job))
changeJobActivity active sessionInfo jid = requireRole Administrator sessionInfo
                                           >> changeJobActivity_ active jid

-- | Helper for updating job activity to enabled/disbaled
changeJobActivity_ :: ( HasDBBackend m db
                      , HasCacheBackend m c
                      , HasSearchBackend m s
                      , MonadError ServerError m
                      , JobManager db m
                      ) => Bool -> JobID -> m (EnvelopedResponse (ModelWithID Job))
changeJobActivity_ active jid = getDBBackend
                                >>= \backend -> getCacheBackend
                                >>= \cache -> getSearchBackend
                                >>= \search -> pure ()
                                -- | Update job activity
                                >> setJobActivity backend active jid
                                >>= ifLeftEnvelopeAndThrow Err.failedToChangeJobActivityStatus
                                >>= \res -> refreshJobsIndex search
                                >> invalidateAllJobListings cache
                                >> pure (EnvelopedResponse "success" "Successfully updated job active status" res)

-- | Raw job search (using the backend), searches inactive jobs as well as active ones
jobSearchAdmin :: ( HasDBBackend m db
                  , HasCacheBackend m c
                  , HasSearchBackend m s
                  , MonadError ServerError m
                  , JobStore db m
                  )
                 => Maybe String
                     -> [JobIndustry]
                     -> [CompanyID]
                     -> Maybe Limit
                     -> Maybe Offset
                     -> [TagName]
                     -> m (EnvelopedResponse (PaginatedList JobWithCompany))
jobSearchAdmin term is cs limit offset ts = getDBBackend
                                            >>= \db -> getCacheBackend
                                            >>= \cache -> lookupOrComputeAndSaveEither
                                                          (getJobListing cacheKey cache)    -- lookup
                                                          (findJobs db massagedJQ)          -- compute
                                                          (insertJobListing cacheKey cache) --  save
                                            >>= ifLeftEnvelopeAndThrow Err.jobSearchFailed
                                            >>= pure . EnvelopedResponse "success" "Successfully completed search"
    where
      massagedJQ = massage $ JobQuery (trimSearchTerm term) is cs limit offset Nothing ts True
      cacheKey = JobFTS massagedJQ

-- | Job search (only checks active, becuase that's all that's indexed), using the available search backend
jobSearch :: ( HasCacheBackend m c
             , HasSearchBackend m s
             , HasDBBackend m db
             , MonadError ServerError m
             , JobStore db m
             )
            => Maybe String
                -> [JobIndustry]
                -> [CompanyID]
                -> Maybe Limit
                -> Maybe Offset
                -> [TagName]
                -> m (EnvelopedResponse (PaginatedList JobWithCompany))
jobSearch term is cs limit offset ts = getDBBackend
                                       >>= \db -> getCacheBackend
                                       >>= \cache -> getSearchBackend
                                       >>= \search -> lookupOrComputeAndSaveEither
                                                      (getJobListing cacheKey cache)    -- lookup
                                                      (searchOrThrow search db)         -- compute
                                                      (insertJobListing cacheKey cache) -- save
                                       >>= ifLeftEnvelopeAndThrow Err.jobSearchFailed -- if the lookup compute worked, but returned nothing
                                       >>= pure . EnvelopedResponse "success" "Successfully completed search"
    where
      -- Don't include inactive jobs by default, use default ordering
      massagedJQ = massage $ JobQuery (trimSearchTerm term) is cs (maxLimitOr limit) offset Nothing ts False
      cacheKey = ActiveJobOnlyFTS massagedJQ

      searchOrThrow search db = searchJobs massagedJQ search      -- compute (not supposed to fail)
                                >>= ifLeftEnvelopeAndThrow Err.jobSearchFailed
                                >>= hydrateJobIDs db

-- | Update a job by ID
updateJobByID :: ( HasDBBackend m db
                 , HasCacheBackend m c
                 , HasSearchBackend m s
                 , MonadError ServerError m
                 , CompanyStore db m
                 , JobStore db m
                 ) => SessionInfo -> JobID -> Job -> m (EnvelopedResponse (ModelWithID Job))
updateJobByID sessionInfo jid updated = ensureUserIsAdminOrCompanyRep (employerId updated) sessionInfo
                                        >> getCacheBackend
                                        >>= \cache -> getSearchBackend
                                        >>= \search -> getDBBackend
                                        >>= \backend -> updateJob backend jid updated
                                        >>= ifLeftEnvelopeAndThrow Err.failedToUpdateResource
                                        -- | After successful job update, refresh the jobs index along with the job listings
                                        >>= \updatedJob -> refreshJobsIndex search
                                        >> invalidateAllJobListings cache
                                        >> pure (EnvelopedResponse "success" "Successfully updated job" updatedJob)

-- | Update a job posting request by patching it
patchJobByID :: ( HasDBBackend m db
                , HasCacheBackend m c
                , HasSearchBackend m s
                , MonadError ServerError m
                , JobStore db m
                )
               => SessionInfo
                   -> JobID
                   -> MergePatchChangeObject
                   -> m (EnvelopedResponse (ModelWithID Job))
patchJobByID sessionInfo jid changes = requireRole Administrator sessionInfo
                                       >> getDBBackend
                                       >>= \backend -> getSearchBackend
                                       >>= \search -> getCacheBackend
                                       >>= \cache -> pure ()
                                       -- | Get the original job
                                       >> getJobByID backend jid
                                       >>= ifLeftEnvelopeAndThrow Err.failedToFindJob
                                       -- | Merge in changes
                                       >>= return . mergeChanges changes . model
                                       -- | Update the job on the backend
                                       >>= updateJob backend jid
                                       >>= ifLeftEnvelopeAndThrow Err.failedToUpdateResource
                                        -- | After successful job update, refresh the jobs index along with the job listings
                                       >>= \updatedJob -> refreshJobsIndex search
                                       >> invalidateAllJobListings cache
                                       >> return (EnvelopedResponse "success" "Successfully patched job" updatedJob)

maxLimit :: Limit
maxLimit = 50

maxLimitOr :: Maybe Limit -> Maybe Limit
maxLimitOr Nothing  = Just 20
maxLimitOr (Just n) = Just $ minimum [maxLimit, n]
