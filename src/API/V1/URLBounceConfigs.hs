{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.URLBounceConfigs ( Routes
                               , server
                               ) where

import           AuthN (CookieAuth)
import           AuthZ (requireRole)
import           Cache.CacheBackend (CacheBackend(..), Cache(..), CacheKey(..), HasCacheBackend(..))
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Search.SearchBackend (SearchBackend(..), HasSearchBackend(..))
import           Servant
import           System.Log.Logger (Priority(..))
import           Types
import           Util (ifNothingThrowIOError, ifLeftConvertAndThrow, lookupOrComputeAndSave, lookupOrComputeAndSaveEither, trimSearchTerm, validateEntity, ifLeftEnvelopeAndThrow)
import qualified Errors as Err

-- ^ URL bouncing related endpoints
type Routes = "url-bounce-configs" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID URLBounceConfig)))
    :<|> "url-bounce-configs" :> CookieAuth :> ReqBody '[JSON] URLBounceConfig :> Post '[JSON] (EnvelopedResponse (ModelWithID URLBounceConfig))
    :<|> "url-bounce-configs" :> CookieAuth :> Capture "bounceConfigId" URLBounceConfigID :> Get '[JSON] (EnvelopedResponse (ModelWithID URLBounceConfig))
    :<|> "url-bounce-configs" :> CookieAuth :> Capture "bounceConfigId" URLBounceConfigID :> "activate" :> Post '[JSON] (EnvelopedResponse (ModelWithID URLBounceConfig))
    :<|> "url-bounce-configs" :> CookieAuth :> Capture "bounceConfigId" URLBounceConfigID :> "deactivate" :> Post '[JSON] (EnvelopedResponse (ModelWithID URLBounceConfig))
    :<|> "url-bounce-configs" :> CookieAuth :> Capture "bounceConfigId" URLBounceConfigID :> "info" :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID URLBounceInfo)))
    :<|> "url-bounce-configs" :> CookieAuth :> Capture "bounceConfigId" URLBounceConfigID :> "bounce-count" :> Get '[JSON] (EnvelopedResponse Int)

server :: (HasDBBackend m db, MonadError ServerError m, URLBounceConfigStore db m) => ServerT Routes m
server = allURLBounceConfigs
         :<|> createURLBounceConfig
         :<|> getURLBounceConfigByID
         :<|> changeURLBounceConfigActivity True
         :<|> changeURLBounceConfigActivity False
         :<|> getInfoForURLBounceConfig
         :<|> getBounceCountForURLBounceConfig

-- | List all URL bounce configs
allURLBounceConfigs :: ( HasDBBackend m db
                       , MonadError ServerError m
                       , URLBounceConfigStore db m
                       ) => SessionInfo -> Maybe Limit -> Maybe Offset -> m (EnvelopedResponse (PaginatedList (ModelWithID URLBounceConfig)))
allURLBounceConfigs sessionInfo limit offset  = requireRole Administrator sessionInfo
                                                >> getDBBackend
                                                >>= \backend -> getAllURLBounceConfigs backend limit offset
                                                >>= ifLeftEnvelopeAndThrow Err.errorOcurredWhileRetrievingResource
                                                >>= pure . EnvelopedResponse "success" "Retrieved all URL bounce configs"

-- | Create a URL bounce config
createURLBounceConfig :: ( HasDBBackend m db
                         , MonadError ServerError m
                         , URLBounceConfigStore db m
                         ) => SessionInfo -> URLBounceConfig -> m (EnvelopedResponse (ModelWithID URLBounceConfig))
createURLBounceConfig sessionInfo bounceConfig = requireRole Administrator sessionInfo
                                                 >> getDBBackend
                                                 >>= flip addURLBounceConfig bounceConfig
                                                 >>= ifLeftEnvelopeAndThrow Err.failedToAddResource
                                                 >>= pure . EnvelopedResponse "success" "Successfully created url bounce config"

-- | Get a single URL bounce config by ID
getURLBounceConfigByID :: ( HasDBBackend m db
                          , MonadError ServerError m
                          , URLBounceConfigStore db m
                          ) => SessionInfo -> URLBounceConfigID -> m (EnvelopedResponse (ModelWithID URLBounceConfig))
getURLBounceConfigByID sessionInfo cid = requireRole Administrator sessionInfo
                                         >> getDBBackend
                                         >>= flip findURLBounceConfigByID cid
                                         >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                         >>= pure . EnvelopedResponse "success" "Successfully retrieved url bounce config"

-- | Change the (enable/disable) a URL bounce config
changeURLBounceConfigActivity :: ( HasDBBackend m db
                                 , MonadError ServerError m
                                 , URLBounceConfigStore db m
                                 ) => Bool -> SessionInfo -> URLBounceConfigID -> m (EnvelopedResponse (ModelWithID URLBounceConfig))
changeURLBounceConfigActivity active sessionInfo bid  = requireRole Administrator sessionInfo
                                                        >> getDBBackend
                                                        >>= \backend -> setURLBounceConfigActivity backend active bid
                                                        >>= ifLeftEnvelopeAndThrow Err.failedToChangeJobActivityStatus
                                                        >>= pure . EnvelopedResponse "success" "Successfully updated url bounce active status"

-- | Get information about a single URL bounce config
getInfoForURLBounceConfig :: ( HasDBBackend m db
                             , MonadError ServerError m
                             , URLBounceConfigStore db m
                             )
                            => SessionInfo
                                -> URLBounceConfigID
                                -> Maybe Limit
                                -> Maybe Offset
                                -> m (EnvelopedResponse (PaginatedList (ModelWithID URLBounceInfo)))
getInfoForURLBounceConfig sessionInfo cid limit offset = requireRole Administrator sessionInfo
                                                         >> getDBBackend
                                                         >>= \backend -> findURLBounceInfoForConfig backend cid limit offset
                                                         >>= ifLeftEnvelopeAndThrow (Err.enveloped Err.failedToRetrieveResource)
                                                         >>= pure . EnvelopedResponse "success" "Sucessfully retrieved bounce information"

getBounceCountForURLBounceConfig :: ( HasDBBackend m db
                                    , MonadError ServerError m
                                    , URLBounceConfigStore db m
                                    ) => SessionInfo -> URLBounceConfigID -> m (EnvelopedResponse Int)
getBounceCountForURLBounceConfig sessionInfo bid = requireRole Administrator sessionInfo
                                                   >> getDBBackend
                                                   >>= flip getNumberHitsForBounceConfigByID bid
                                                   >>= ifLeftEnvelopeAndThrow (Err.enveloped Err.failedToRetrieveResource)
                                                   >>= pure . EnvelopedResponse "success" "Sucessfully retrieved bounce count" . fromIntegral . toInteger
