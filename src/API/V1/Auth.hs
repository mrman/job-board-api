{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Auth ( Routes
                   , server
                   ) where

import           AuthN (CookieAuth, authCookieName, sessionUserID, sessionUserRole)
import           AuthZ (requireRole)
import           Config (makeInstantExpireCookieWithSettings, makeAuthCookieWithSettings, CookieConfig)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Data.Aeson (encode)
import           Data.Maybe (fromMaybe)
import           Servant
import           Types
import           Util (ifLeftEnvelopeAndThrow, ifNothingEnvelopeAndThrow, cleanUser, entityCountToInt)
import           Web.Cookie (renderSetCookie)
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.DateTime as DDT
import qualified Data.Text as DT
import qualified Errors as Err
import qualified Web.ClientSession as WCS
import           Data.ByteString.Conversion

defaultUserSessionExpiryInSeconds :: Integer
defaultUserSessionExpiryInSeconds  = 36000000

-- ^ Authentication and session related APIs
type Routes = "login" :> ReqBody '[JSON] UserEmailAndPassword :> Post '[JSON] (Headers '[Header "Set-Cookie" DT.Text] (EnvelopedResponse SessionInfo))
    :<|> "logout" :> CookieAuth :> Get '[JSON] (Headers '[Header "Set-Cookie" DT.Text] (EnvelopedResponse SessionInfo))
    :<|> "permissions" :> CookieAuth :> Get '[JSON] (EnvelopedResponse [Permission])
    :<|> "roles" :> CookieAuth :> Get '[JSON] (EnvelopedResponse [Role])
    :<|> "admin" :> CookieAuth :> "statistics" :> Get '[JSON] (EnvelopedResponse AdminDashboardStats)
    :<|> "me" :> CookieAuth :> Get '[JSON] (EnvelopedResponse SessionInfo)
    :<|> "me" :> CookieAuth :> "change-password" :> ReqBody '[JSON] PasswordChangeRequest :> Post '[JSON] (EnvelopedResponse Bool)

server :: ( HasCookieKey m
          , HasCookieConfig m
          , HasDBBackend m db
          , MonadError ServerError m
          , SupportRequestStore db m
          , UserManager db m
          , JobStore db m
          , JobPostingRequestStore db m
          ) => ServerT Routes m
server = login
         :<|> logout
         :<|> listPermissions
         :<|> listRoles
         :<|> adminStats
         :<|> me
         :<|> changeCurrentUserPassword

login :: ( HasCookieKey m
         , HasCookieConfig m
         , HasDBBackend m db
         , MonadError ServerError m
         , UserManager db m
         ) => UserEmailAndPassword -> m (Headers '[Header "Set-Cookie" DT.Text] (EnvelopedResponse SessionInfo))
login (UserEmailAndPassword email password) = getDBBackend
                                              >>= \backend -> loginUser backend email password
                                              >>= ifLeftEnvelopeAndThrow Err.failedToLoginUser
                                              >>= createUserSession defaultUserSessionExpiryInSeconds
                                              >>= \session -> makeSessionCookie session
                                              >>= \cookie -> pure $ addHeader cookie $ EnvelopedResponse "success" "Successfully Logged in" session

logout :: (HasCookieKey m, HasCookieConfig m, MonadError ServerError m) => SessionInfo -> m (Headers '[Header "Set-Cookie" DT.Text] (EnvelopedResponse SessionInfo))
logout sessionInfo = clearSessionCookie
                     >>= pure . flip addHeader response
    where
      response = EnvelopedResponse "success" "Successfully logged out" sessionInfo

listPermissions :: MonadError ServerError m => SessionInfo -> m (EnvelopedResponse [Permission])
listPermissions sessionInfo = requireRole Administrator sessionInfo
                              >> pure (EnvelopedResponse "success" "Retrieved all permissions" [ManageJobPostings ..])

listRoles :: MonadError ServerError m => SessionInfo -> m (EnvelopedResponse [Role])
listRoles sessionInfo = requireRole Administrator sessionInfo
                        >> pure (EnvelopedResponse "success" "Retrieved all roles" [Administrator ..])

adminStats :: ( HasDBBackend m db
              , MonadError ServerError m
              , SupportRequestStore db m
              , JobStore db m
              , JobPostingRequestStore db m
              , UserManager db m
              ) => SessionInfo -> m (EnvelopedResponse AdminDashboardStats)
adminStats sessionInfo = requireRole Administrator sessionInfo
                         >> getDBBackend
                         >>= \db -> getUserCount db
                         >>= \userCount -> getActiveJobCount db
                         >>= \currentActiveJobCount -> getUnapprovedJobPostingRequests db
                         >>= \unapprovedJReqs -> getUnresolvedSupportRequestCount db
                         >>= \supportReqs -> makeResp $ AdminDashboardStats
                                             (either (const (-1)) entityCountToInt userCount)
                                             (either (const (-1)) entityCountToInt currentActiveJobCount)
                                             (either (const (-1)) entityCountToInt unapprovedJReqs)
                                             (either (const (-1)) entityCountToInt supportReqs)
    where
      makeResp = pure . EnvelopedResponse "success" "Retrieved admin stats"

-- Get the current user
me :: ( HasDBBackend m db
      , MonadError ServerError m
      , UserManager db m
      ) => SessionInfo -> m (EnvelopedResponse SessionInfo)
me sessionInfo = gatherSessionInfoForCookieUser sessionInfo
                 >>= pure . EnvelopedResponse "success" "Successfully retrieved user session"

changeCurrentUserPassword :: ( HasDBBackend m db
                             , MonadError ServerError m
                             , UserManager db m
                             ) => SessionInfo -> PasswordChangeRequest -> m (EnvelopedResponse Bool)
changeCurrentUserPassword sessionInfo req = getDBBackend
                                            >>= \backend -> processPasswordChangeRequest backend req uid
                                            >>= ifLeftEnvelopeAndThrow Err.genericError
                                            >>= pure . makeResp
    where
      uid = sessionUserID sessionInfo
      makeResp True  = EnvelopedResponse "success" "Successfully changed password." True
      makeResp False = EnvelopedResponse "error" "Failed to change password" False

clearSessionCookie :: (HasCookieConfig m, HasCookieKey m, MonadError ServerError m) => m DT.Text
clearSessionCookie = liftIO DDT.getCurrentTime
                     >>= \now -> getCookieConfigAndKey
                     >>= \(cookieCfg, _) -> pure (getText (makeInstantExpireCookieWithSettings cookieCfg now AuthN.authCookieName ""))
                     >>= ifNothingEnvelopeAndThrow (Err.makeGenericError "Failed to clear session cookie")
    where
      getText = fromByteString . toByteString' . renderSetCookie

gatherSessionInfoForCookieUser :: ( HasDBBackend m db
                                  , MonadError ServerError m
                                  , UserManager db m
                                  ) => SessionInfo -> m SessionInfo
gatherSessionInfoForCookieUser sessionInfo = getDBBackend
                                             >>= \backend -> getUserByID backend userId
                                             >>= ifLeftEnvelopeAndThrow Err.failedToFindUser
                                             >>= \userWithId@(ModelWithID uid user) -> getPermissionsForUser backend uid (userRole user)
                                             >>= ifLeftEnvelopeAndThrow Err.failedToRetrievePermissions
                                             >>= pure . SessionInfo (cleanUser userWithId) expiryDate
    where
      userId = sessionUserID sessionInfo
      expiryDate = expires sessionInfo

makeSessionCookie :: (HasCookieConfig m, HasCookieKey m, MonadError ServerError m) => SessionInfo -> m DT.Text
makeSessionCookie s = getCookieConfigAndKey
                      >>= \(cookieCfg, key) -> liftIO (WCS.encryptIO key (encodeSession s))
                      >>= pure . fromByteString . toByteString' . renderSetCookie . makeAuthCookieWithSettings  cookieCfg expiryDate AuthN.authCookieName
                      >>= ifNothingEnvelopeAndThrow (Err.makeGenericError "Failed to create session cookie")
    where
      encodeSession = BSL8.toStrict . encode
      expiryDate = expires s

createUserSession :: ( HasDBBackend m db
                     , MonadError ServerError m
                     , UserManager db m
                     ) => Integer -> ModelWithID User -> m SessionInfo
createUserSession mins userWithId@(ModelWithID uid u) = getDBBackend
                                                        >>= \backend -> liftIO DDT.getCurrentTime
                                                        >>= \now -> getPermissionsForUser backend uid (userRole u)
                                                        >>= ifLeftEnvelopeAndThrow Err.failedToCreateSession
                                                        >>= pure . SessionInfo (cleanUser userWithId) (DDT.addMinutes mins now)

getCookieConfigAndKey :: (HasCookieConfig m, HasCookieKey m) => m (CookieConfig, WCS.Key)
getCookieConfigAndKey = getCookieKey
                        >>= \key -> getCookieConfig
                        >>= \config -> pure (config, key)
