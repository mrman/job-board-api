{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Tags ( Routes
                   , server
                   ) where

import           AuthN (CookieAuth)
import           AuthZ (requireRole)
import           Cache.CacheBackend (CacheBackend(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Search.SearchBackend (SearchBackendM(..), HasSearchBackend(..))
import           Servant
import           System.Log.Logger (Priority(..))
import           Types
import           Util (ifNothingThrowIOError, ifLeftConvertAndThrow, lookupOrComputeAndSave, lookupOrComputeAndSaveEither, trimSearchTerm, validateEntity, ifLeftEnvelopeAndThrow)
import qualified Errors as Err

-- | Tag API routes
type Routes = "tags" :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Tag)))
    -- | GET /tags/search?searchTerm=$searchTerm
    :<|> "tags" :> "search" :> QueryParam "searchTerm" String :> Get '[JSON] (EnvelopedResponse (PaginatedList TagID))
    -- | POST /tags
    :<|> "tags" :> CookieAuth :> ReqBody '[JSON] Tag :> Post '[JSON] (EnvelopedResponse (ModelWithID Tag))
    -- | GET /tags/export
    :<|> "tags" :> "export" :> CookieAuth :> Get '[JSON] (EnvelopedResponse [Tag])
    -- | POST /tags/import
    :<|> "tags" :> "import" :> CookieAuth :> ReqBody '[JSON] [Tag] :> Post '[JSON] (EnvelopedResponse [TagImportResult])
    -- | PATCH /tags/:id
    :<|> "tags" :> CookieAuth :> Capture "id" TagID :> ReqBody '[JSON] MergePatchChangeObject :> Patch '[JSON] (EnvelopedResponse (ModelWithID Tag))
    -- | GET /tags/:id
    :<|> "tags" :> Capture "id" TagID :> Get '[JSON] (EnvelopedResponse (ModelWithID Tag))
    -- | DELETE /tags/:id
    :<|> "tags" :> CookieAuth :> Capture "id" TagID :> Delete '[JSON] (EnvelopedResponse (EntityCount Tag))

-- | Tag API server
server :: ( HasDBBackend m db
          , HasCacheBackend m c
          , HasSearchBackend m s
          , MonadError ServerError m
          , TagStore db m
          ) => ServerT Routes m
server = listTags
         :<|> tagFTS
         :<|> createTag
         :<|> exportTags
         :<|> importTags
         :<|> patchTagByID
         :<|> findTagByID
         :<|> deleteTagByID

-- | List tags
listTags :: ( HasDBBackend m db
            , HasCacheBackend m c
            , MonadError ServerError m
            , TagStore db m
            ) => Maybe Limit -> Maybe Offset -> m (EnvelopedResponse (PaginatedList (ModelWithID Tag)))
listTags limit offset = getDBBackend
                        >>= \db -> getCacheBackend
                        >>= \cache -> getCachedTagListing db cache
                        >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                        >>= pure . EnvelopedResponse "success" "Successfully retrieved all tags"
    where
      cacheKey = TagListing limit offset

      lookup = getTagListing cacheKey
      compute db = getAllTags db limit offset
      updateCache = insertTagListing cacheKey

      getCachedTagListing db cache = lookupOrComputeAndSaveEither (lookup cache) (compute db) (updateCache cache)

tagFTS :: ( HasCacheBackend m db
          , HasSearchBackend m s
          , MonadError ServerError m
          ) => Maybe String -> m (EnvelopedResponse (PaginatedList TagID))
tagFTS term = getCacheBackend
              >>= \cache -> getSearchBackend
              >>= \search -> getCachedTagFTSResults search cache
              >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
              >>= pure . EnvelopedResponse "success" "Successfully searched for tag"
    where
      trimmedTerm = trimSearchTerm term
      cacheKey = TagFTS trimmedTerm

      lookup = getTagFTSResults cacheKey
      compute = searchTags trimmedTerm
      updateCache = insertTagFTSResults cacheKey

      getCachedTagFTSResults search cache = lookupOrComputeAndSaveEither (lookup cache) (compute search) (updateCache cache)

createTag :: ( HasDBBackend m db
             , HasCacheBackend m c
             , MonadError ServerError m
             , TagStore db m
             ) => SessionInfo -> Tag -> m (EnvelopedResponse (ModelWithID Tag))
createTag s t = requireRole Administrator s
                >> validateEntity t
                >> getDBBackend
                >>= \db -> getCacheBackend
                >>= \cache -> addTag db t
                >>= ifLeftEnvelopeAndThrow Err.failedToCreateEntity
                -- invalidate cached tag listing & FTS results
                >>= \res -> invalidateTagListing cache
                >> invalidateTagFTSResults cache
                >> pure (EnvelopedResponse "success" "Successfully created new tag" res)

exportTags :: ( HasDBBackend m db
              , MonadError ServerError m
              , TagStore db m
              ) => SessionInfo -> m (EnvelopedResponse [Tag])
exportTags s = requireRole Administrator s
               >> getDBBackend
               -- | Get *all* tags in the DB
               >>= \db -> getAllTags db Nothing Nothing
               >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
               >>= pure . (model <$>) . items
               >>= pure . EnvelopedResponse "success" "Successfully retrieved all tags"

importTags :: ( HasDBBackend m db
              , HasCacheBackend m c
              , MonadError ServerError m
              , TagStore db m
              ) => SessionInfo -> [Tag] -> m (EnvelopedResponse [TagImportResult])
importTags s tags = requireRole Administrator s
                    >> getDBBackend
                    -- | Attempt to add all the tags to the DB
                    >>= \db -> getCacheBackend
                    >>= \cache -> mapM (addTag db) tags
                    >>= pure . (makeTagImportResult <$>)
                    -- | Invalid cached tag listing & FTS results
                    >>= \res -> invalidateTagListing cache
                    >> invalidateTagFTSResults cache
                    >> pure (EnvelopedResponse "success" "Successfully completed import operation (some imports may have failed)" res)

-- | Update a tag posting request by patching it
patchTagByID :: ( HasDBBackend m db
                , HasCacheBackend m c
                , HasSearchBackend m s
                , MonadError ServerError m
                , TagStore db m
                )
               => SessionInfo
                   -> TagID
                   -> MergePatchChangeObject
                   -> m (EnvelopedResponse (ModelWithID Tag))
patchTagByID sessionInfo tid changes = requireRole Administrator sessionInfo
                                       >> getDBBackend
                                       >>= \backend -> getSearchBackend
                                       >>= \search -> getCacheBackend
                                       >>= \cache -> pure ()
                                       -- | Get the original tag
                                       >> getTagByID backend tid
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToFindEntity "Tag")
                                       -- | Merge in changes on the first tag returned
                                       >>= return . mergeChanges changes . model
                                       -- | Update the tag on the backend
                                       >>= updateTag backend tid
                                       >>= ifLeftEnvelopeAndThrow Err.failedToUpdateResource
                                        -- | After successful tag update, refresh the tags index along with the tag listings
                                       >>= \updatedTag -> refreshTagsIndex search
                                       >> invalidateTagListing cache
                                       >> return (EnvelopedResponse "success" "Successfully patched tag" updatedTag)

-- | Find a single tag by ID
findTagByID :: (HasDBBackend m db, MonadError ServerError m, TagStore db m) => TagID -> m (EnvelopedResponse (ModelWithID Tag))
findTagByID tid = getDBBackend
                  >>= \backend -> getTagByID backend tid
                  >>= ifLeftEnvelopeAndThrow (Err.failedToFindEntity "Tag")
                  >>= pure . EnvelopedResponse "success" "Successfully retrieved tag"

-- | Delete a single tag by ID
deleteTagByID :: ( HasDBBackend m db
                 , HasCacheBackend m c
                 , HasSearchBackend m s
                 , MonadError ServerError m
                 , TagStore db m
                 ) => SessionInfo -> TagID -> m (EnvelopedResponse (EntityCount Tag))
deleteTagByID sessionInfo tid = requireRole Administrator sessionInfo
                                >> getDBBackend
                                >>= \backend -> getSearchBackend
                                >>= \search -> getCacheBackend
                                >>= \cache -> pure ()
                                -- | Delete the tag
                                >> deleteTag backend tid
                                >>= ifLeftEnvelopeAndThrow (Err.failedToDeleteEntity "Tag")
                                -- | After successful tag update, refresh the tags index along with the tag listings
                                >>= \res -> refreshTagsIndex search
                                >> invalidateTagListing cache
                                >> pure (EnvelopedResponse "success" "Successfully deleted tag" res)
