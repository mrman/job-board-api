{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.StarredJobs ( Routes
                          , server
                          ) where

import           Cache.CacheBackend (CacheBackend(..), Cache(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           AuthN (CookieAuth, sessionUserID)
import           Data.Monoid ((<>))
import           AuthZ (requireRole)
import           Control.Monad.Except (MonadError)
import           Util (ifLeftEnvelopeAndThrow, lookupOrComputeAndSaveEither, natToInt)
import           Servant
import           Types
import qualified Errors as Err

-- | Starred Jobs API routes
type Routes = "users" :> CookieAuth :> Capture "userId" UserID :> "starred-jobs" :> Get '[JSON] (EnvelopedResponse (PaginatedList JobWithCompany))
    :<|> "me" :> CookieAuth :> "starred-jobs" :> Get '[JSON] (EnvelopedResponse (PaginatedList JobWithCompany))
    :<|> "me" :> CookieAuth :> "starred-jobs" :> Capture "jobId" JobID :> Post '[JSON] (EnvelopedResponse Int)
    :<|> "me" :> CookieAuth :> "starred-jobs" :> Capture "jobId" JobID :> Delete '[JSON] (EnvelopedResponse Int)

-- | Starred Jobs API server
server :: ( HasDBBackend m db
          , HasCacheBackend m c
          , MonadError ServerError m
          , UserManager db m
          ) => ServerT Routes m
server = findStarredJobsForUser
         :<|> findStarredJobsForCurrentUser
         :<|> modifyStarredJobForUser Add
         :<|> modifyStarredJobForUser Remove

-- | Find all starred jobs for a given user
findStarredJobsForUser :: ( HasDBBackend m db
                          , HasCacheBackend m c
                          , MonadError ServerError m
                          , UserManager db m
                          ) => SessionInfo -> UserID -> m (EnvelopedResponse (PaginatedList JobWithCompany))
findStarredJobsForUser sessionInfo uid = requireRole Administrator sessionInfo
                                         >> getDBBackend
                                         >>= \db -> getCacheBackend
                                         >>= \cache -> lookupOrComputeAndSaveEither
                                                       (getStarredJobs cacheKey cache)    -- lookup
                                                       (getStarredJobsForUser db uid)     -- compute
                                                       (insertStarredJobs cacheKey cache) -- save
                                         >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                                         >>= pure . EnvelopedResponse "success" "Retrieved starred jobs for user"
    where
      cacheKey = StarredJobs uid

-- | Find all starred jobs for the current user
findStarredJobsForCurrentUser :: ( HasDBBackend m db
                                 , HasCacheBackend m c
                                 , MonadError ServerError m
                                 , UserManager db m
                                 ) => SessionInfo -> m (EnvelopedResponse (PaginatedList JobWithCompany))
findStarredJobsForCurrentUser sessionInfo = getDBBackend
                                            >>= \db -> getCacheBackend
                                            >>= \cache -> lookupOrComputeAndSaveEither (getStarredJobs cacheKey cache) (getStarredJobsForUser db uid) (insertStarredJobs cacheKey cache)
                                            >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                                            >>= pure . EnvelopedResponse "success" "Retrieved starred jobs for user"
    where
      uid = sessionUserID sessionInfo
      cacheKey = StarredJobs uid

-- | Modify a starred job for a given user
modifyStarredJobForUser :: ( HasDBBackend m db
                           , HasCacheBackend m c
                           , MonadError ServerError m
                           , UserManager db m
                           ) => GenericOperation -> SessionInfo -> JobID -> m (EnvelopedResponse Int)
modifyStarredJobForUser op sessionInfo jid = getDBBackend
                                             >>= \db -> getCacheBackend
                                             >>= \cache -> operateOnStarredJobForUser db op uid jid
                                             >>= ifLeftEnvelopeAndThrow Err.failedToChangeFavoritePreference
                                             >>= \(EntityCount res) -> invalidate (StarredJobs uid) cache
                                             >> pure (EnvelopedResponse "success" successMsg $ natToInt res)
      where
        uid = sessionUserID sessionInfo
        successMsg = "Successfully " <> getOperatedVerb op <> " starred Job"
