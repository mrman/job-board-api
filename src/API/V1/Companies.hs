{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Companies ( server
                        , Routes
                        ) where

import           AuthN (CookieAuth, sessionUserRole, sessionUserID)
import           AuthZ (ensureUserIsAdminOrCompanyRep, requireRole)
import           Cache.CacheBackend (CacheBackend(..), Cache(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Data.Monoid ((<>))
import           Network.URI (URI, parseURI)
import           Search.SearchBackend (SearchBackend(..), HasSearchBackend(..))
import           Servant
import           Types
import           Control.Monad.State (liftIO)
import           UserContentStore.UserContentStoreBackend (UserContentStoreBackend(..), UserContentStore(..), UserContentStoreM(..), HasUCSBackend(..))
import           Util
import qualified API.V1.Jobs as JobsAPI
import qualified Data.DateTime as DDT
import qualified Errors as Err

-- | Company API routes
type Routes = "companies" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Company)))
    :<|> "companies" :> Capture "companyId" CompanyID :> Get '[JSON] (EnvelopedResponse (ModelWithID Company))
    :<|> "companies" :> CookieAuth :> ReqBody '[JSON] Company :> Post '[JSON] (EnvelopedResponse (ModelWithID Company))
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "representatives" :> Get '[JSON] (EnvelopedResponse (PaginatedList UserWithoutSensitiveData))
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "representatives" :> Capture "userEmail" Email :> Post '[JSON] (EnvelopedResponse Int)
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "representatives" :> Capture "userEmail" Email :> Delete '[JSON] (EnvelopedResponse Int)
    :<|> "companies" :> CookieAuth :> "represented-by" :> Capture "userId" UserID :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Company)))
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "statistics" :> Get '[JSON] (EnvelopedResponse CompanyStats)
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "jobs" :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Job)))
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "jobs" :> Capture "jobId" JobID :> "activate" :> Post '[JSON] (EnvelopedResponse (ModelWithID Job))
    :<|> "companies" :> CookieAuth :> Capture "companyId" CompanyID :> "jobs" :> Capture "jobId" JobID :> "deactivate" :> Post '[JSON] (EnvelopedResponse (ModelWithID Job))
    :<|> "industries" :> Get '[JSON] (EnvelopedResponse [JobIndustry])

-- | Company API server
server :: ( HasUCSBackend m ucs
          , HasDBBackend m db
          , HasSearchBackend m s
          , HasCacheBackend m c
          , MonadError ServerError m
          , CompanyStore db m
          , JobManager db m
          , UserStore db m
          ) => ServerT Routes m
server = allCompanies
         :<|> findCompanyByID
         :<|> createCompany
         :<|> allRepresentativesForCompany
         :<|> modifyRepresentativesForCompany Add
         :<|> modifyRepresentativesForCompany Remove
         :<|> findRepresentedCompaniesByRepID
         :<|> companyStats
         :<|> listJobsForCompany
         :<|> changeJobActivityForCompany True
         :<|> changeJobActivityForCompany False
         :<|> industries

-- | List all companies
allCompanies :: ( HasDBBackend m db
                , HasCacheBackend m c
                , MonadError ServerError m
                , CompanyStore db m
                ) => SessionInfo -> Maybe Limit -> Maybe Offset -> m (EnvelopedResponse (PaginatedList (ModelWithID Company)))
allCompanies sessionInfo limit offset = requireRole Administrator sessionInfo
                                        >> getDBBackend
                                        >>= \db -> getCacheBackend
                                        >>= \cache -> lookupOrComputeAndSaveEither
                                                      (getCompanyListing cacheKey cache)    -- lookup
                                                      (getAllCompanies db limit offset)     -- compute
                                                      (insertCompanyListing cacheKey cache) -- save
                                        >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                        >>= pure . EnvelopedResponse "success" "Successfully retrieved companies"
    where
      cacheKey = CompanyListing limit offset

-- | Find a single company by ID
findCompanyByID :: (MonadError ServerError m, HasDBBackend m db, CompanyStore db m) => CompanyID -> m (EnvelopedResponse (ModelWithID Company))
findCompanyByID cid = getDBBackend
                      >>= \backend -> getCompanyByID backend cid
                      >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                      >>= pure . EnvelopedResponse "success" "Successfully retrieved company"

-- | Create a company
createCompany :: ( HasDBBackend m db
                 , HasCacheBackend m c
                 , HasUCSBackend m ucs
                 , MonadError ServerError m
                 , CompanyStore db m
                 )
                => SessionInfo
                    -> Company
                    -> m (EnvelopedResponse (ModelWithID Company))
createCompany sessionInfo newCompany = requireRole Administrator sessionInfo
                                       >> validateEntity newCompany
                                       >> getDBBackend
                                       >>= \db -> getCacheBackend
                                       >>= \cache -> getUCSBackend
                                       >>= \ucs -> ensureNotNothing (parseURI iconURI) Err.failedToParseDataURI
                                       -- Save company logo image file, convert URI to on-disk path (file://...)
                                       >>= convertAndStoreImageDataURI ucs
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToConvertAndStoreImage "icon")
                                       -- Parse & Save company banner image file, convert URI to on-disk path (file://...)
                                       >>= \iconURI' -> ensureNotNothing (parseURI bannerImageURI) Err.failedToParseDataURI
                                       >>= convertAndStoreImageDataURI ucs
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToConvertAndStoreImage "banner")
                                       -- Get the current time, use it to fill out fields in new company
                                       >>= \bannerImageURI' -> liftIO DDT.getCurrentTime
                                       >>= \now ->  pure (newCompany { companyCreatedAt=now
                                                                     , companyLastUpdatedAt=now
                                                                     , companyIconURI=show iconURI'
                                                                     , companyBannerImageURI=show bannerImageURI'
                                                                     })
                                       -- Add the company
                                       >>= addCompany db
                                       >>= ifLeftEnvelopeAndThrow Err.failedToCreateCompany
                                       >>= \createdCompany -> invalidateCompanyListing cache
                                       >> pure (EnvelopedResponse "success" "Successfully added company" createdCompany)
    where
      iconURI = companyIconURI newCompany
      bannerImageURI = companyBannerImageURI newCompany

-- | List all representatives for a given company
allRepresentativesForCompany :: ( MonadError ServerError m
                                , HasDBBackend m db
                                , CompanyStore db m
                                ) => SessionInfo -> CompanyID -> m (EnvelopedResponse (PaginatedList UserWithoutSensitiveData))
allRepresentativesForCompany sessionInfo cid = requireRole Administrator sessionInfo
                                               >> getDBBackend
                                               >>= \backend -> getAllCompanyRepresentatives backend cid
                                               >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                               >>= pure . EnvelopedResponse "success" "Successfully retrieved company representatives" . (cleanUser<$>)

-- | Modify representatives for a given company
modifyRepresentativesForCompany :: ( HasDBBackend m db
                                   , MonadError ServerError m
                                   , CompanyStore db m
                                   , UserStore db m
                                   ) => GenericOperation -> SessionInfo -> CompanyID -> Email -> m (EnvelopedResponse Int)
modifyRepresentativesForCompany op sessionInfo cid email = requireRole Administrator sessionInfo
                                                           >> getDBBackend
                                                           >>= \backend -> getUserByEmail backend email
                                                           >>= ifLeftEnvelopeAndThrow Err.failedToFindUser
                                                           >>= operateOnCompanyRepresentatives backend op cid . mId
                                                           >>= ifLeftEnvelopeAndThrow (Err.failedToModifyEntity "company")
                                                           >>= \(EntityCount c) -> pure $ EnvelopedResponse "success" successMsg $ natToInt c
      where
        successMsg = "Successfully " <> getOperatedVerb op <> "company representatives"

-- | Find companies represented by a given representative
findRepresentedCompaniesByRepID :: ( HasDBBackend m db
                                   , MonadError ServerError m
                                   , CompanyStore db m
                                   ) => SessionInfo -> UserID ->  m (EnvelopedResponse (PaginatedList (ModelWithID Company)))
findRepresentedCompaniesByRepID sessionInfo uid = throwServantErrorIf (cookieUserRole`notElem`[Administrator, CompanyRepresentative]) Err.unauthorized
                                                  -- Ensure the company representative only looks for their own companies
                                                  >> throwServantErrorIf (cookieUserRole == CompanyRepresentative && cookieUserId /= uid) Err.unauthorized
                                                  >> getDBBackend
                                                  >>= \backend -> getCompaniesRepresentedByUserWithID backend uid
                                                  >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                                                  >>= pure . EnvelopedResponse "success" "Successfully retrieved companies represented by given user"
    where
      cookieUserRole = sessionUserRole sessionInfo
      cookieUserId = sessionUserID sessionInfo

-- | Get stats for a given company
companyStats :: ( HasDBBackend m db
                , HasCacheBackend m c
                , MonadError ServerError m
                , CompanyStore db m
                ) => SessionInfo -> CompanyID ->  m (EnvelopedResponse CompanyStats)
companyStats sessionInfo cid = ensureUserIsAdminOrCompanyRep cid sessionInfo
                               >> getDBBackend
                               >>= \db -> getCacheBackend
                               >>= \cache -> lookupOrComputeAndSaveEither
                                             (getCompanyStats cacheKey cache)    -- lookup
                                             (getStatsForCompany db cid)         -- compute
                                             (insertCompanyStats cacheKey cache) -- save
                               >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                               >>= pure . EnvelopedResponse "success" "Successfully generated company stats"
    where
      cacheKey = Cache.CacheBackend.CompanyStats cid

-- | List jobs for a given company
listJobsForCompany :: ( HasDBBackend m db
                      , HasSearchBackend m s
                      , HasCacheBackend m c
                      , MonadError ServerError m
                      , CompanyStore db m
                      , JobStore db m
                      ) => SessionInfo -> CompanyID ->  m (EnvelopedResponse (PaginatedList (ModelWithID Job)))
listJobsForCompany sessionInfo cid = ensureUserIsAdminOrCompanyRep cid sessionInfo
                                     >> JobsAPI.jobSearch (Just "") [] [cid] Nothing Nothing []
                                     >>= \jwc -> pure $ jwc { respData=fmap jwcJob (respData jwc) }

-- | Toggle active status for a given company
changeJobActivityForCompany :: ( HasDBBackend m db
                               , HasCacheBackend m c
                               , HasSearchBackend m s
                               , MonadError ServerError m
                               , CompanyStore db m
                               , JobManager db m
                               ) => Bool -> SessionInfo -> CompanyID -> JobID -> m (EnvelopedResponse (ModelWithID Job))
changeJobActivityForCompany active sessionInfo cid jid = ensureUserIsAdminOrCompanyRep cid sessionInfo
                                                         >> JobsAPI.changeJobActivity_ active jid

-- | Retrun listing of industries
industries ::(MonadError ServerError m) => m (EnvelopedResponse [JobIndustry])
industries = return $ EnvelopedResponse "success" "Retrieved industries" [IT, BizDev]
