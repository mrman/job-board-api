{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Email ( Routes
                    , server
                    ) where

import           AuthN (CookieAuth)
import           AuthZ (requireRole)
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Data.Monoid ((<>))
import           Mailer.MailerBackend (Mailer(..), MailerM(..), MailerBackend(..), HasMailerBackend(..))
import           Servant
import           Types
import           Util (ifLeftEnvelopeAndThrow, ifNothingEnvelopeAndThrow)
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Errors as Err

-- | Email API routes
type Routes = "emails" :> "transactional" :> "test" :> CookieAuth :> QueryParam "email" Email :> Post '[JSON] (EnvelopedResponse ())
    :<|> "emails" :> "newsletter" :> "weekly" :> CookieAuth :> QueryParam "email" Email :> Post '[JSON] (EnvelopedResponse ())

-- | Email API server
server :: ( HasDBBackend m db
          , HasMailerBackend m mb
          , MonadError ServerError m
          , UserManager db m
          , EmailSubscriptionStore db m
          ) => ServerT Routes m
server = triggerTestEmail
         :<|> triggerWeeklyNewsletterEmail

-- | Trigger sending of test email
triggerTestEmail :: ( HasDBBackend m db
                    , HasMailerBackend m mb
                    , MonadError ServerError m
                    , UserStore db m
                    , EmailSubscriptionStore db m
                    ) => SessionInfo -> Maybe Email -> m (EnvelopedResponse ())
triggerTestEmail _           Nothing        = throwError Err.genericBadRequest
triggerTestEmail sessionInfo (Just address) = setupForSubscriptionMail address sessionInfo
                                              >>= \(_, mailer, ModelWithID _ u, ModelWithID _ s) -> sendTestEmail mailer address u s
                                              >>= ifLeftEnvelopeAndThrow (Err.failedToSendEmail "test email")
                                              >> return (EnvelopedResponse "success" "Successfully sent test email" ())

-- | Send the weekly newsletter email
triggerWeeklyNewsletterEmail :: ( HasDBBackend m db
                                , HasMailerBackend m mb
                                , MonadError ServerError m
                                , UserManager db m
                                , EmailSubscriptionStore db m
                             ) => SessionInfo -> Maybe Email -> m (EnvelopedResponse ())
triggerWeeklyNewsletterEmail _           Nothing        = throwError Err.genericBadRequest
triggerWeeklyNewsletterEmail sessionInfo (Just address) = setupForSubscriptionMail address sessionInfo
                                                   -- Retrieve interesting jobs for the relevant user
                                                   >>= \(db, mailer, u@(ModelWithID _ user), s@(ModelWithID _ sub)) -> getInterestingJobsForUser db u s
                                                   >>= ifLeftEnvelopeAndThrow (Err.failedToFindImpliedEntity "interesting job(s)")
                                                   -- Retrieve current sponsored company for the relevant user
                                                   >>= \(PaginatedList jobs _) -> getCurrentSponsoredCompanyForUser db u
                                                   >>= ifLeftEnvelopeAndThrow (Err.failedToFindImpliedEntity "sponsored company")
                                                   -- Send weekly newsletter email with the mailer
                                                   >>= sendWeeklyNewsletter mailer user sub jobs . fmap model
                                                   >>= ifLeftEnvelopeAndThrow (Err.failedToSendEmail "weekly newsletter")
                                                   >> return (EnvelopedResponse "success" "Successfully sent weekly newsletter email" ())

-- | Helper to set up for a sbuscription email
setupForSubscriptionMail :: ( HasDBBackend m db
                            , HasMailerBackend m mb
                            , MonadError ServerError m
                            , UserStore db m
                            , EmailSubscriptionStore db m
                            ) => Email -> SessionInfo -> m (db, mb, ModelWithID User, ModelWithID EmailSubscription)
setupForSubscriptionMail recipient s = requireRole Administrator s
                                       >> getDBBackend
                                       >>= \backend -> getMailerBackend
                                       >>= \mailer -> getUserByEmail backend recipient
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToFindEntity ("User with emailAddress " <> BSL8.pack recipient))
                                       >>= \user -> getEmailSubscriptionByEmail backend recipient
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToFindEntity "No valid email subscription for user")
                                       >>= \sub -> pure (backend, mailer, user, sub)
