{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Users ( Routes
                    , server
                    ) where

import           Mailer.MailerBackend (MailerM(..), MailerBackend(..), HasMailerBackend(..))
import           AuthN (CookieAuth)
import           AuthZ (requireRole)
import           Config (AppConfig(..), AppEnvironment(..))
import           Control.Exception (try)
import           Control.Monad.Except (MonadError, when)
import           Data.Maybe (fromMaybe)
import           Servant
import           Types
import           Util (ifNothingThrowError, ifNothingThrowIOError, ifLeftConvertAndThrow, cleanUser, ifLeftEnvelopeAndThrow, ifNothingEnvelopeAndThrow)
import qualified Errors as Err

-- | User API routes
type Routes = "users" :> ReqBody '[JSON] User :> Post '[JSON] (EnvelopedResponse UserWithoutSensitiveData)
    :<|> "users" :> CookieAuth :> Get '[JSON] (EnvelopedResponse (PaginatedList UserWithoutSensitiveData))
    :<|> "users" :> CookieAuth :> Capture "userId" UserID :> Get '[JSON] (EnvelopedResponse UserInfo)
    :<|> "users" :> CookieAuth :> Capture "userId" UserID :> "permissions" :> Capture "permission" Permission :> Post '[JSON] (EnvelopedResponse Int)
    :<|> "users" :> CookieAuth :> Capture "userId" UserID :> "permissions" :> Capture "permission" Permission :> Delete '[JSON] (EnvelopedResponse Int)
    :<|> "password-reset" :> "initiate" :> QueryParam "email" Email :> Post '[JSON] (EnvelopedResponse ())
    :<|> "password-reset" :> "complete" :> QueryParam "token" PasswordResetToken :> QueryParam "password" Password :> Post '[JSON] (EnvelopedResponse ())

-- | User API server
server :: ( HasMailerBackend m mailer
          , HasDBBackend m db
          , HasAppConfig m
          , MonadError ServerError m
          , UserStore db m
          , UserManager db m
          , TokenStore db m
          ) => ServerT Routes m
server = createUser
         :<|> listUsers
         :<|> findUserByID
         :<|> grantPermissionForUser
         :<|> revokePermissionForUser
         :<|> passwordResetInitiate
         :<|> passwordResetComplete

-- | Create a user
createUser :: ( HasAppConfig m
              , HasDBBackend m db
              , MonadError ServerError m
              , UserStore db m
              ) => User -> m (EnvelopedResponse UserWithoutSensitiveData)
createUser user = getDBBackend
                  >>= \backend -> fmap appEnvironment getAppConfig
                  >>= \appEnv -> when (appEnv /= Test && userRole user == Administrator) (throwError Err.failedToCreateNewUser)
                  >> addUser backend user (password user)
                  >>= ifLeftEnvelopeAndThrow Err.failedToCreateNewUser
                  >>= pure . EnvelopedResponse "success" "Successfully signed up new user" . cleanUser

-- | List users
listUsers :: ( HasDBBackend m db
             , MonadError ServerError m
             , UserStore db m
             ) => SessionInfo -> m (EnvelopedResponse (PaginatedList UserWithoutSensitiveData))
listUsers sessionInfo = requireRole Administrator sessionInfo
                        >> getDBBackend
                        >>= getAllUsers
                        >>= ifLeftEnvelopeAndThrow Err.failedToRetrieveResource
                        >>= pure . EnvelopedResponse "success" "Retrieved all users" . (cleanUser<$>)

-- | Find a single user by ID
findUserByID :: ( HasDBBackend m db
                , MonadError ServerError m
                , UserStore db m
                , UserManager db m
                ) => SessionInfo -> UserID -> m (EnvelopedResponse UserInfo)
findUserByID sessionInfo uid = requireRole Administrator sessionInfo
                               >> getDBBackend
                               >>= \backend -> getUserByID backend uid
                               >>= ifLeftEnvelopeAndThrow Err.failedToFindUser
                               -- Get the permissions for the user, then build it into the user info
                               >>= \u -> getPermissionsForUser backend uid (userRole (model u))
                               >>= ifLeftEnvelopeAndThrow Err.failedToRetrievePermissions
                               >>= pure . EnvelopedResponse "sucess" "Retrieved user" . UserInfo (cleanUser u)

-- | Grant a permission to a given user
grantPermissionForUser :: ( HasDBBackend m db
                          , MonadError ServerError m
                          , UserManager db m
                          ) => SessionInfo -> UserID -> Permission -> m (EnvelopedResponse Int)
grantPermissionForUser sessionInfo uid p = requireRole Administrator sessionInfo
                                           >> getDBBackend
                                           >>= \backend -> addPermissionForUser backend uid p
                                           >>= ifLeftEnvelopeAndThrow Err.failedToAddPermission
                                           >>= pure . EnvelopedResponse "success" "Successfully granted permission"

-- | Revoke permission for a given user
revokePermissionForUser :: ( HasDBBackend m db
                           , MonadError ServerError m
                           , UserManager db m
                           ) => SessionInfo -> UserID -> Permission -> m (EnvelopedResponse Int)
revokePermissionForUser sessionInfo uid p = requireRole Administrator sessionInfo
                                            >> getDBBackend
                                            >>= \backend -> removePermissionForUser backend uid p
                                            >>= ifLeftEnvelopeAndThrow Err.failedToRevokePermission
                                            >>= pure . EnvelopedResponse "success" "Successfully revoked permission"

-- | Initiate password reset
passwordResetInitiate :: ( HasMailerBackend m mailer
                         , HasDBBackend m db
                         , MonadError ServerError m
                         , UserStore db m
                         , TokenStore db m
                         ) => Maybe Email -> m (EnvelopedResponse ())
passwordResetInitiate Nothing        = throwError Err.genericBadRequest
passwordResetInitiate (Just address) = getDBBackend
                                       >>= \backend -> getMailerBackend
                                       -- Get the user by email
                                       >>= \mailer -> getUserByEmail backend address
                                       >>= ifLeftEnvelopeAndThrow (Err.failedToFindImpliedEntity "user")
                                       -- Create a password reset token for the user
                                       >>= \(ModelWithID uid user) -> createTokenForUser backend uid PasswordResetToken
                                       >>= ifLeftEnvelopeAndThrow Err.failedToCreateToken
                                       -- Send email
                                       >>= sendPasswordResetInitiated mailer user . tToken . model
                                       >>= ifLeftConvertAndThrow
                                       >> return (EnvelopedResponse "success" "Password reset email sent" ())
-- | Complete a password reset
passwordResetComplete :: ( HasMailerBackend m mailer
                         , HasDBBackend m db
                         , MonadError ServerError m
                         , UserManager db m
                         ) => Maybe PasswordResetToken -> Maybe Password -> m (EnvelopedResponse ())
passwordResetComplete Nothing      _              = throwError Err.genericBadRequest
passwordResetComplete _            Nothing        = throwError Err.genericBadRequest
passwordResetComplete (Just token) (Just newPass) = getDBBackend
                                                    >>= \backend -> getMailerBackend
                                                    -- Complete password reset with given token
                                                    >>= \mailer -> completePasswordReset backend token newPass
                                                    >>= ifLeftEnvelopeAndThrow (Err.failedToFindImpliedEntity "reset password request")
                                                    -- Send password reset completed email
                                                    >>= sendPasswordResetCompleted mailer . model
                                                    >>= ifLeftEnvelopeAndThrow (Err.failedToSendEmail "password reset complete")
                                                    >> return (EnvelopedResponse "success" "Password reset completed" ())
