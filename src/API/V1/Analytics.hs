{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Analytics ( Routes
                        , server
                        ) where

import           AuthN (CookieAuth)
import           AuthZ (requireRole)
import           Cache.CacheBackend (CacheBackend(..), CacheM(..), CacheKey(..), HasCacheBackend(..))
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Search.SearchBackend (SearchBackendM(..), HasSearchBackend(..))
import           Servant
import           System.Log.Logger (Priority(..))
import           Types
import           Util (ifNothingThrowIOError, ifLeftConvertAndThrow, lookupOrComputeAndSave, lookupOrComputeAndSaveEither, trimSearchTerm, validateEntity, ifLeftEnvelopeAndThrow)
import qualified Errors as Err
import qualified Data.DateTime as DDT
import qualified Data.HashMap.Strict as HMS

-- | Analytics API routes
type Routes =
    -- | GET /analytics/jobs-by-bounce-rate
    "analytics" :> "jobs-by-bounce-rate" :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (WithBounceCount JobWithCompany)))
    -- | GET /analytics/bounces-by-hour
    :<|> "analytics" :> "bounces" :> "hourly" :> QueryParam "from" DDT.DateTime :> QueryParam "to" DDT.DateTime :> Get '[JSON] (EnvelopedResponse (HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)))
    -- | GET /analytics/bounces-by-day
    :<|> "analytics" :> "bounces" :> "daily" :> QueryParam "from" DDT.DateTime :> QueryParam "to" DDT.DateTime :> Get '[JSON] (EnvelopedResponse (HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)))

-- | Analytics API server
server :: ( HasDBBackend m db
          , MonadError ServerError m
          , AnalyticsStore db m
          ) => ServerT Routes m
server = listJobsByBounceRate
         :<|> listBouncesByHour
         :<|> listBouncesByDay

-- | List jobs by bounce rage
listJobsByBounceRate :: ( HasDBBackend m db
                        , MonadError ServerError m
                        , AnalyticsStore db m
                        )
                       => Maybe Limit
                           -> Maybe Offset
                           -> m (EnvelopedResponse (PaginatedList (WithBounceCount JobWithCompany)))
listJobsByBounceRate limit offset = getDBBackend
                                    >>= \db -> getJobsByBounceRate db limit offset
                                    >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                                    >>= pure . EnvelopedResponse "success" "Successfully generated analytics"

-- | List bounces by hour of the day
listBouncesByHour :: ( HasDBBackend m db
                     , MonadError ServerError m
                     , AnalyticsStore db m
                     )
                    => Maybe StartTime
                        -> Maybe EndTime
                        -> m (EnvelopedResponse (HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)))
listBouncesByHour start end = getDBBackend
                              >>= \db -> getBouncesBinnedByTimePeriod db Hourly start end
                              >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                              >>= pure . EnvelopedResponse "success" "Successfully generated analytics"

-- | Get bounces by day of week
listBouncesByDay :: ( HasDBBackend m db
                    , MonadError ServerError m
                    , AnalyticsStore db m
                    )
                   => Maybe StartTime
                       -> Maybe EndTime
                       -> m (EnvelopedResponse (HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)))
listBouncesByDay start end = getDBBackend
                             >>= \db -> getBouncesBinnedByTimePeriod db Daily start end
                             >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                             >>= pure . EnvelopedResponse "success" "Successfully generated analytics"
