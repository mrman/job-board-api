{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Addresses ( Routes
                        , server
                        ) where

import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Servant
import           Types
import           Util (ifNothingThrowIOError)
import qualified Errors as Err

-- ^ User information retrieval endpoints
type Routes = "addresses" :> Capture "addressId" AddressID :> Get '[JSON] (EnvelopedResponse (ModelWithID Address))

server :: (HasDBBackend m db, MonadError ServerError m) => ServerT Routes m
server = findAddressByID

findAddressByID :: (HasDBBackend m db, MonadError ServerError m) => AddressID -> m (EnvelopedResponse (ModelWithID Address))
findAddressByID cid = getDBBackend
                      >>= liftIO . flip getAddressByID cid
                      >>= ifNothingThrowIOError Err.failedToRetrieveResource
                      >>= pure . EnvelopedResponse "success" "Successfully retrieved address"
