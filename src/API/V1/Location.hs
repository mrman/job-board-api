{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE ConstraintKinds  #-}

module API.V1.Location ( Routes
                       , server
                       ) where

import Control.Monad.Except (MonadError)
import Control.Monad.State (liftIO)
import Servant
import Types
import Util (throwServantErrorIfLeft)

-- ^ Location related APIs
type Routes = "locations" :> "user-home" :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID Location)))

type SupportsLocationAPI db m = (HasDBBackend m db, MonadError ServerError m, UserManager db m)

server :: SupportsLocationAPI db m => ServerT Routes m
server = getAllUserHomeLocations

getAllUserHomeLocations :: SupportsLocationAPI db m => m (EnvelopedResponse (PaginatedList (ModelWithID Location)))
getAllUserHomeLocations = getDBBackend
                          >>= getSupportedUserHomeLocations
                          >>= throwServantErrorIfLeft
                          >>= pure . EnvelopedResponse "success" "Successfully retrieved all user home locations"
