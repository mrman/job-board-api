{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Promotions ( Routes
                         , server
                         ) where

import Control.Monad.Except (MonadError)
import Control.Monad.State (liftIO)
import Servant

import AuthN (CookieAuth)
import AuthZ (requireRole)
import Util (throwServantErrorIfLeft)
import Types

-- | Promoition API routes
type Routes = "promotions" :> CookieAuth :> ReqBody '[JSON] Promotion :> Post '[JSON] (EnvelopedResponse (ModelWithID Promotion))

-- | Promotion API server
server :: (HasDBBackend m db, MonadError ServerError m, PromotionStore db m) => ServerT Routes m
server = createPromotion

-- | Create a promotion
createPromotion :: ( HasDBBackend m db
                   , MonadError ServerError m
                   , PromotionStore db m
                   )
                  => SessionInfo
                      -> Promotion
                      -> m (EnvelopedResponse (ModelWithID Promotion))
createPromotion s pr = requireRole Administrator s
                       >> getDBBackend
                       >>= flip addPromotion pr
                       >>= throwServantErrorIfLeft
                       >>= pure . EnvelopedResponse "success" "Successfully created promotion"
