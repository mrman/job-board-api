{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE TypeOperators     #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module API.V1.Support ( Routes
                      , server
                      ) where

import           AuthN (CookieAuth, sessionUserID)
import           AuthZ (requireRole)
import           Control.Exception (try)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (when, liftIO)
import           Data.Monoid ((<>))
import           Mailer.MailerBackend (Mailer(..), MailerBackend(..), HasMailerBackend(..))
import           Servant
import           Types
import           System.Log.Logger (Priority(..))
import           Util (ifLeftEnvelopeAndThrow, whenNothing, whenLeft, logInternalThrowExternal)
import qualified Data.Text as DT
import qualified Data.DateTime as DDT
import qualified Errors as Err

-- | Support API routes
type Routes = "support-requests" :> CookieAuth :> QueryParam "pageSize" Limit :> QueryParam "offset" Offset :> Get '[JSON] (EnvelopedResponse (PaginatedList (ModelWithID SupportRequest)))
    :<|> "support-requests" :> CookieAuth :> ReqBody '[JSON] SupportRequest :> Post '[JSON] (EnvelopedResponse (ModelWithID SupportRequest))

-- | Support API server
server :: ( HasLoggerBackend m
          , HasDBBackend m db
          , MonadError ServerError m
          , SupportRequestStore db m
          ) => ServerT Routes m
server = listSupportRequests
         :<|> createSupportRequest

-- | Find support requests
listSupportRequests :: ( HasDBBackend m db
                       , MonadError ServerError m
                       , SupportRequestStore db m
                       ) => SessionInfo -> Maybe Limit -> Maybe Offset -> m (EnvelopedResponse (PaginatedList (ModelWithID SupportRequest)))
listSupportRequests sessionInfo limit offset = requireRole Administrator sessionInfo
                                               >> getDBBackend
                                               >>= \backend -> getAllSupportRequests backend limit offset
                                               >>= ifLeftEnvelopeAndThrow Err.failedToFindEntities
                                               >>= pure . EnvelopedResponse "success" "Successfully retrieved support requests"
-- | Create a support request
createSupportRequest :: ( HasDBBackend m db
                        , HasLoggerBackend m
                        , MonadError ServerError m
                        , SupportRequestStore db m
                        ) => SessionInfo -> SupportRequest -> m (EnvelopedResponse (ModelWithID SupportRequest))
createSupportRequest sessionInfo req = requireRole CompanyRepresentative sessionInfo
                                       >> getDBBackend
                                       >>= \backend -> liftIO DDT.getCurrentTime
                                       >>= \now -> pure (req { srSubmittedBy=sessionUserID sessionInfo, srCreatedAt=now })
                                       >>= addSupportRequest backend
                                       >>= whenLeft (logInternalThrowExternal ERROR "Support Request creation failed" Err.genericError)
                                       >>= pure . EnvelopedResponse "success" "Successfully created support request"
