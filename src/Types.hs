{-# LANGUAGE TemplateHaskell        #-}
{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE DeriveGeneric          #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE DeriveDataTypeable     #-}
{-# LANGUAGE RankNTypes             #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE ScopedTypeVariables    #-}

module Types where

import           Config (BackendConfig, CookieConfig, AppConfig)
import           Control.Concurrent (ThreadId)
import           Control.Exception (Exception, SomeException)
import           Control.Monad.Error.Class
import           Control.Monad.IO.Class
import           Data.Aeson (encode, (.:), fromJSON, Result(..))
import           Data.Aeson.TH
import           Data.Aeson.Types (FromJSON(..), ToJSON(..), Value(..), Object(..), object, (.=), prependFailure, typeMismatch)
import           Data.Data
import           Data.DateTime
import           Data.HashMap.Strict as HMS (HashMap, toList, foldrWithKey, lookup, fromList, keys)
import           Data.Hashable (Hashable)
import           Data.Hashable.Time
import           Data.Maybe (fromMaybe)
import           Data.Monoid ((<>))
import           Data.Natural (Natural, natural, indeterm)
import           Data.Proxy
import           Data.Scientific (Scientific, floatingOrInteger, isFloating)
import           Data.Swagger (ToParamSchema(..), ToSchema(..), NamedSchema(..), paramSchemaToSchema, SwaggerType(SwaggerInteger))
import           Data.Time.Clock (NominalDiffTime)
import           Data.Typeable (Typeable)
import           Data.UUID (UUID)
import           Data.UUID.V4 (nextRandom)
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.FromRow (RowParser)
import           Database.SQLite.Simple.Ok
import           Database.SQLite.Simple.ToField
import           GHC.Generics
import           Network.HTTP.Media ((//), (/:))
import           Servant (Accept(..), MimeUnrender(..), MimeRender(..))
import           Servant.API.ContentTypes (eitherDecodeLenient)
import           System.Log.Logger
import           Text.Ginger.GVal as GVal (ToGVal(..))
import           Text.Ginger.Html (Html)
import           Text.Ginger.Run (Run)
import qualified Data.ByteString.Char8 as B8
import qualified Data.DateTime as DDT
import qualified Data.List as L
import qualified Data.Map as M
import qualified Data.Set as Set
import qualified Data.Text as DT
import qualified Database.SQLite.Simple as S
import qualified Database.SQLite.Simple.Types as ST
import qualified Web.ClientSession as WCS
import qualified Web.HttpApiData

newtype Massaged a = Massaged a deriving (Generic, Show, Eq)

instance (Hashable a) => Hashable (Massaged a)

type AddressID = Int
type BackendVersion = Int
type CompanyID = Int
type ConfirmationCode = DT.Text
type DBEntityID = Int
type Email = String
type EmailSubscriptionID = Int
type FieldName = DT.Text
type ForeignKey = Int
type IncludeInactive = Bool
type JobID = Int
type JobPostingRequestID = Int
type Limit = Int
type MergePatchChangeObject = Object
type Offset = Int
type PageNumber = Int
type Password = String
type PasswordResetToken = DT.Text
type PermissionRecordID = Int
type PrimaryKey = Int
type TableName = DT.Text
type TagID = Int
type TaskID = Int
type TokenID = DBEntityID
type URLBounceConfigID = Int
type UnsubscribeCode = DT.Text
type UserID = Int
type Port = Int
type StartTime = DDT.DateTime
type EndTime = DDT.DateTime

convertToInt :: Scientific -> Maybe Int
convertToInt n = either (const Nothing) (Just . fromIntegral) (floatingOrInteger n :: Either Double Integer)

convertToInteger :: Scientific -> Maybe Integer
convertToInteger n = either (const Nothing) Just (floatingOrInteger n :: Either Double Integer)

-- | SQLColumn name that includes a phantom type to tie it to a given entity
type SQLColumnName entity = DT.Text
type SQLDateColumnName entity = DT.Text

-- | EntityCount includes a phantom type to tie it to a given entity
newtype EntityCount entity = EntityCount { _etTotal :: Natural } deriving (Generic, Show, Eq)

instance ToJSON (EntityCount entity)
instance ToSchema (EntityCount entity)
instance FromJSON (EntityCount entity)

instance ToJSON Natural where
    toJSON = toJSON . toInteger

instance FromJSON Natural where
    parseJSON obj@(Number num) = case convertToInteger num of
                                   Just n -> pure $ natural n
                                   Nothing -> prependFailure "Invalid natural number, is floating" (typeMismatch "Number" obj)
    parseJSON invalid = prependFailure "parsing ModelWithID failed, " (typeMismatch "Object" invalid)

instance ToSchema Natural where
    declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy Int)

-- | Seconds
newtype Seconds = Seconds { _seconds :: Int } deriving (Show, Eq)

data ID = UUID | Int

data PaginationOptions = PaginationOptions { limit   :: Maybe Limit
                                           , offset  :: Maybe Offset
                                           , afterId :: Maybe ID
                                           }

data Order entity = ASC (SQLColumnName entity)
                  | DateTimeASC (SQLDateColumnName entity)
                  | DESC (SQLColumnName entity)
                  | DateTimeDESC (SQLDateColumnName entity)
                    deriving (Generic, Eq, Read, Show)

instance Hashable (Order entity)
instance ToSchema (Order entity)

class HasOrderByClause a where
    makeOrderBy :: a -> Maybe DT.Text -> DT.Text

instance HasOrderByClause (Order entity) where
    makeOrderBy order maybePrefix = case order of 
                                      (ASC s)          -> prefix <> s <> " ASC"
                                      (DateTimeASC s)  -> "DATETIME(" <> prefix <> s <> ") ASC"
                                      (DESC s)         -> prefix <> s <> " DESC"
                                      (DateTimeDESC s) -> "DATETIME(" <> prefix <> s <> ") DESC"
        where
          prefix = fromMaybe "" maybePrefix

-- | The class for column names that are shared between entities
--   this type actually doesn't provide much safety outside of ensuring that the specified 
--   instances exist (ex. instance ConvertableColumn Job JobWithCompany)
class ConvertableColumn a b where
    convertOrder :: Order a -> Order b
    convertOrder = read . show

    -- | TODO: it *might* be advantageous to define the columns that are shared to restrict
    --   the columns that can be converted. However, it's commonly the case that *every* column is shared,
    --   or none are.
    --
    -- sharedColumns _ = [""]

-- | Get the raw column name from any Order value
getRawColumnName :: forall entity. Order entity -> DT.Text
getRawColumnName (ASC s)          = s
getRawColumnName (DateTimeASC s)  = s
getRawColumnName (DESC s)         = s
getRawColumnName (DateTimeDESC s) = s

-- https://github.com/GetShopTV/swagger2/issues/86
-- TODO: Fix this, this shouldn't be the schema for string
instance ToSchema Value where
    declareNamedSchema _ = declareNamedSchema (Proxy :: Proxy String)

data CookieSession a = LoggedOut
                     | LoggedIn a deriving (Eq, Show)

data GenericOperation = Add
                      | Remove deriving (Eq, Show)

class HasOperatedVerb a where
    getOperatedVerb :: a -> String

class MergePatchable a where
    mergePatchableFields :: a -> HashMap DT.Text (Value -> a -> a)

    mergePatchFields :: a -> Object -> a
    mergePatchFields old = foldrWithKey (mergePatch old) old

    mergeChanges :: Object -> a -> a
    mergeChanges changes obj = mergePatchFields obj changes

    mergePatch :: a -> DT.Text -> Value -> a -> a
    mergePatch current k v old = maybe old (\f -> f v old) $ HMS.lookup k (mergePatchableFields current)

-- | A class enacpsulating entities that can be inserted
class (S.ToRow a, DBEntity a) => InsertableDBEntity a where
    entityInsertFields :: a -> [DT.Text]

    entityInsertFieldsWithID :: a ->  [DT.Text]
    entityInsertFieldsWithID = ("id":) . entityInsertFields

    insertQuery :: a -> ST.Query

    doInsert :: MonadIO m => a -> S.Connection -> m Integer
    doInsert obj c = liftIO doInsert_
        where
          doInsert_ = S.execute c (insertQuery obj) obj
                      >> fromIntegral <$> S.lastInsertRowId c

class S.ToRow a => UpdatableDBEntity a where
    entityUpdateFields :: a -> [DT.Text]

class S.ToRow a => DBEntity a where
    entityTableName :: a -> DT.Text
    idField :: a -> DT.Text

    validateWithErrors :: a -> [ValidationError]
    validateWithErrors _ = []

    isValid :: a -> Bool
    isValid = null . validateWithErrors

instance (S.ToRow a, DBEntity a) => S.ToRow (Proxy a) where
    toRow c = error "Attempt to ToRow a proxy"

-- | Allow getting an entity's table name and id fields via a proxy
-- WARNING: this *ONLY* works because most of the  know entityTableName and idField do *not* depend on the actual argument
-- entityTableName never uses the first argument
--
-- FIXME: there's probably a safer way to do this, maybe define the naming-related stuff on proxies to start with?
-- soemthing like DBEntity a => DBMeta (Proxy a) where ...
instance (S.ToRow a, DBEntity a) => DBEntity (Proxy a) where
    entityTableName (_ :: Proxy a) = entityTableName (undefined :: a)
    idField (_ :: Proxy a) = idField (undefined :: a)

    validateWithErrors _ = error "cannot validate proxies"
    isValid _ = error "cannot validate proxies"

type ValidationFieldName = String
type ValidationErrorReason = String

data ValidationError = MissingRequiredField ValidationFieldName
                     | InvalidValue ValidationFieldName ValidationErrorReason deriving (Eq, Read)

instance Show ValidationError where
    show (MissingRequiredField fname) = "Missing required field " <> fname
    show (InvalidValue fname reason) = "Value for [" <> fname <> "] is invalid: " <> reason

instance HasOperatedVerb GenericOperation where
    getOperatedVerb Add = "added"
    getOperatedVerb Remove = "removed"

data ModelWithID a = ModelWithID
    { mId   :: Int
    , model :: a
    } deriving (Generic, Eq, Show, Read)

instance Functor ModelWithID where
    fmap f (ModelWithID mId m) = ModelWithID mId (f m)

instance ToSchema a => ToSchema (ModelWithID a)
instance (ToJSON a, ToGVal m a) => GVal.ToGVal m (ModelWithID a) where
    toGVal = toGVal . toJSON

instance S.FromRow a => S.FromRow (ModelWithID a) where
    fromRow = do
      modelId <- S.field
      obj <- S.fromRow :: (S.FromRow a) => RowParser a
      return (ModelWithID modelId obj)

instance S.ToRow a => S.ToRow (ModelWithID a) where
    -- ^ Convert job to row, without ID, which should be managed by Backend (PK)
    toRow m = toField modelId:S.toRow internalModel
        where
          modelId = mId m
          internalModel = model m

instance ToJSON a => ToJSON (ModelWithID a) where
    toJSON (ModelWithID mid m) = object $ ("id" .= mid):toList objectMap
        where
          objectMap = case toJSON m of
                        (Data.Aeson.Types.Object obj) -> obj
                        _ -> undefined

instance FromJSON a => FromJSON (ModelWithID a) where
    parseJSON obj@(Object v) = (v .: "id")
                           >>= \id -> parseJSON obj
                           >>= pure . ModelWithID id
    parseJSON invalid = prependFailure "parsing ModelWithID failed, " (typeMismatch "Object" invalid)

data WithBounceCount a = WithBounceCount
    { wbcCount  :: Int
    , wbcTarget :: a
    } deriving (Generic, Eq, Show, Read)

instance Functor WithBounceCount where
    fmap f (WithBounceCount c t) = WithBounceCount c (f t)

instance ToJSON entity => ToJSON (WithBounceCount entity)
instance ToSchema entity => ToSchema (WithBounceCount entity)

instance S.FromRow a => S.FromRow (WithBounceCount a) where
    fromRow = do
      count <- S.field
      obj <- S.fromRow :: (S.FromRow a) => RowParser a
      return (WithBounceCount count obj)

data EnvelopedResponse a = EnvelopedResponse
    { status :: String
    , message :: String
    , respData :: a
    } deriving (Generic, Eq, Show)

instance ToSchema a => ToSchema (EnvelopedResponse a)

data BrandingData = BrandingData { bdSiteName :: String
                                 , bdSubURL :: String
                                 , bdUnsubURL :: String
                                 } deriving (Show, Read, Eq, Data, Typeable)

instance ToGVal m BrandingData where
    toGVal = toGVal . toJSON

data ServerInfo = ServerInfo { siBackendURL :: String
                             , siAdminURL :: String
                             , siFrontendURL :: String
                             } deriving (Show, Read, Eq, Data, Typeable)

instance ToGVal m ServerInfo where
    toGVal = toGVal . toJSON

data UserEmailAndPassword = UserEmailAndPassword
  { userEmail     :: Email
  , userPassword  :: String
  } deriving (Generic, Eq, Show)

instance ToSchema UserEmailAndPassword

data SessionInfo = SessionInfo
    { sessionUserInfo        :: UserWithoutSensitiveData
    , expires                :: DateTime
    , sessionUserPermissions :: [PermissionRecord]
    } deriving (Generic, Eq, Show, Read)

instance ToSchema SessionInfo

-- Information stored for users
data User = User
  { firstName      :: String
  , lastName       :: String
  , emailAddress   :: Email
  , password       :: Password
  , policyAndSalt  :: Maybe String
  , userRole       :: Role
  , joinedAt       :: DateTime
  , homeLocationId :: Maybe ForeignKey -- ^ References location table
  , lastLoggedIn   :: DateTime
  } deriving (Generic, Eq, Show, Read, Data, Typeable)

instance ToSchema User
instance ToGVal m User where
    toGVal = toGVal . toJSON

data UserWithoutSensitiveData = UserWithoutSensitiveData
    { userID             :: UserID
    , userFirstName      :: String
    , userLastName       :: String
    , userEmailAddress   :: Email
    , role               :: Role
    , userJoinedAt       :: DateTime
    , userHomeLocationId :: Maybe ForeignKey
    } deriving (Generic, Eq, Show, Read)

instance ToSchema UserWithoutSensitiveData


data UserInfo = UserInfo
    { userInfoUser        :: UserWithoutSensitiveData
    , userInfoPermissions :: [PermissionRecord]
    } deriving (Generic, Eq, Show)

instance ToSchema UserInfo


-- Information stored for companies
data Company = Company { companyName               :: String
                       , companyDescription        :: Maybe DT.Text
                       , companyCultureDescription :: Maybe DT.Text
                       , companyHomepageURL        :: String
                       , companyIconURI            :: String
                       , companyBannerImageURI     :: String
                       , companyPrimaryColorCSS    :: String
                       , companyCreatedAt          :: DateTime
                       , companyLastUpdatedAt      :: DateTime
                       , companyAddressID          :: Maybe ForeignKey -- ^ FK To Address
                       } deriving (Generic, Eq, Show, Read)

instance MergePatchable Company where
    mergePatchableFields _ = HMS.fromList [ ("companyName", \v old -> case v of {(String s) -> old { companyName=DT.unpack s }; _ -> old })
                                          , ("companyDescription", \v old -> case v of {(String s) -> old { companyDescription=Just s }; _ -> old })
                                          , ("companyCultureDescription", \v old -> case v of {(String s) -> old { companyCultureDescription=Just s }; _ -> old })
                                          , ("companyHomepageURL", \v old -> case v of {(String s) -> old { companyHomepageURL=(read . DT.unpack) s }; _ -> old })
                                          , ("companyBannerImageURI", \v old -> case v of {(String s) -> old { companyBannerImageURI=(read . DT.unpack) s }; _ -> old })
                                          , ("companyIconURI", \v old -> case v of {(String s) -> old { companyIconURI=(read . DT.unpack) s }; _ -> old })
                                          , ("companyPrimaryColorCSS", \v old -> case v of {(String s) -> old { companyPrimaryColorCSS=(read . DT.unpack) s }; _ -> old })
                                          , ("companyAddressID", \v old -> case v of
                                                                             Number n -> old { companyAddressID=convertToInt n }
                                                                             Null -> old { companyAddressID=Nothing }
                                                                             _ -> old )
                                          ]

newtype SponsoredCompany = SponsoredCompany Company deriving (Eq, Show, Read)

instance ToSchema Company
instance ToGVal m SponsoredCompany where
    toGVal = toGVal . toJSON

data Address = Address
    { addressLine1   :: String
    , addressLine2   :: String
    , addressZipCode :: String
    , addressTown    :: String
    , addressState   :: String -- State/Province
    , addressCountry :: String
    } deriving (Generic, Eq, Show)

instance ToSchema Address

instance S.FromRow Address where
    fromRow = Address <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field

instance S.ToRow Address where
    toRow c = [ toField (addressLine1 c)
              , toField (addressLine2 c)
              , toField (addressZipCode c)
              , toField (addressTown c)
              , toField (addressState c)
              , toField (addressCountry c)
              ]

data Location = Location
    { addressId :: ForeignKey
    , name      :: String
    } deriving (Generic, Eq, Show)

instance ToSchema Location

data JobIndustry = IT
                 | BizDev deriving (Generic, Eq, Enum, Show, Read)

instance Hashable JobIndustry
instance ToParamSchema JobIndustry
instance ToSchema JobIndustry

instance Web.HttpApiData.ToHttpApiData JobIndustry where
    toUrlPiece = DT.pack . show

instance Web.HttpApiData.FromHttpApiData JobIndustry where
    parseUrlPiece = read . DT.unpack

data JobType = FullTime
             | PartTime
             | Freelance
             | Contract deriving (Generic, Eq, Enum, Show, Read)

instance ToSchema JobType

instance Web.HttpApiData.ToHttpApiData JobType where
    toUrlPiece = DT.pack . show

instance Web.HttpApiData.FromHttpApiData JobType where
    parseUrlPiece = read  . DT.unpack

data Currency = YEN
              | USD deriving (Generic, Eq, Enum, Show, Read)

instance ToSchema Currency

type Language = DT.Text

validLanguages :: [DT.Text]
validLanguages = ["EN_US", "JA_JP"]

isValidLanguage :: Language -> Bool
isValidLanguage = (`elem` validLanguages)

type TagName = String

data Tag = Tag
    { tagName      :: TagName -- name of the tag in english (EN_US)
    , tagNames     :: HashMap DT.Text TagName -- name of the tag in multiple languages (containing EN_US)
    , tagCSSColor  :: String
    , tagCreatedAt :: DateTime
    } deriving (Generic, Eq, Show, Read, Ord)

instance ToSchema Tag

instance MergePatchable Tag where
    mergePatchableFields _ = HMS.fromList [ ("tagName", \v old -> case v of {(String s) -> old { tagName=DT.unpack s }; _ -> old })
                                          , ("tagNames", \v old -> case fromJSON v of
                                                                     (Data.Aeson.Success names) -> old { tagNames=names }
                                                                     (Error _)                  -> old
                                          )
                                          , ("tagCSSColor", \v old -> case v of {(String s) -> old { tagCSSColor=DT.unpack s }; _ -> old })
                                          ]

-- Information stored for jobs
data Job = Job
    { title                :: String
    , description          :: String
    , industry             :: JobIndustry
    , jobType              :: JobType
    , minSalary            :: Maybe Int
    , maxSalary            :: Maybe Int
    , salaryCurrency       :: Maybe Currency
    , postingDate          :: DateTime
    , employerId           :: ForeignKey -- ^ FK into company table, company representatives should have this set
    , isActive             :: Bool
    , applyLink            :: String
    , jobURLBounceConfigId :: ForeignKey
    , tags                 :: [Tag]
    , lastCheckedAt        :: Maybe DateTime -- TODO: IMPLEMENT & MIGRATE FOR
    } deriving (Generic, Eq, Show, Read)

instance ToSchema Job
instance ToGVal m Job where toGVal = toGVal . toJSON

instance MergePatchable Job where
    mergePatchableFields _ = HMS.fromList [ ("title", \v old -> case v of {(String s) -> old { title=DT.unpack s }; _ -> old })
                                          , ("description", \v old -> case v of {(String s) -> old { description=DT.unpack s }; _ -> old })
                                          , ("industry", \v old -> case v of {(String s) -> old { industry=(read . DT.unpack) s }; _ -> old })
                                          , ("jobType", \v old -> case v of {(String s) -> old { jobType=(read . DT.unpack) s }; _ -> old })
                                          , ("minSalary", \v old -> case v of
                                                                       Number n -> old { minSalary=convertToInt n }
                                                                       Null -> old { minSalary=Nothing }
                                                                       _ -> old
                                            )
                                          , ("maxSalary", \v old -> case v of
                                                                       Number n -> old { maxSalary=convertToInt n }
                                                                       Null -> old { maxSalary=Nothing }
                                                                       _ -> old
                                            )
                                          , ("salaryCurrency", \v old -> case v of {(String s) -> old { salaryCurrency=(read . DT.unpack) s }; _ -> old })
                                          , ("applyLink", \v old -> case v of {(String s) -> old { applyLink=DT.unpack s }; _ -> old })
                                          , ("employerId", \v old -> case v of
                                                                       Number n -> old { employerId=fromMaybe (employerId old) (convertToInt n) }
                                                                       _ -> old
                                            )
                                          , ("tags", \v old -> case v of
                                                                 Array new -> old { tags=case fromJSON v of
                                                                                           (Data.Aeson.Success ts) -> Set.toList $ Set.fromList $ tags old <> ts
                                                                                           (Error _) -> tags old
                                                                                  }
                                                                 _ -> old
                                            )
                                          ]

data JobWithCompany = JobWithCompany { jwcJob     :: ModelWithID Job
                                     , jwcCompany :: ModelWithID Company
                                     } deriving (Generic, Eq, Show, Read)

instance ToSchema JobWithCompany
instance ToGVal m JobWithCompany where toGVal = toGVal . toJSON

instance ToJSON JobWithCompany where
    toJSON (JobWithCompany j c) = object ["job" .= toJSON j, "company" .= toJSON c]

instance FromJSON JobWithCompany where
    parseJSON (Object v) = (v .: "job")
                           >>= parseJSON
                           >>= \parsedJob -> (v .: "company")
                           >>= parseJSON
                           >>= \parsedCompany -> pure (JobWithCompany parsedJob parsedCompany)
    parseJSON invalid = prependFailure "parsing JobWithCompany failed, " (typeMismatch "Object" invalid)

data JobPostingRequest = JobPostingRequest
    { rTitle          :: String
    , rDescription    :: String
    , rIndustry       :: JobIndustry
    , rJobType        :: JobType
    , rMinSalary      :: Maybe Int
    , rMaxSalary      :: Maybe Int
    , rSalaryCurrency :: Maybe Currency
    , rPosterEmail    :: Email
    , rApplyLink      :: String
    , rCompanyId      :: Maybe CompanyID
    , rApprovedJobId  :: Maybe ForeignKey
    , rRequestedAt    :: DateTime
    } deriving (Generic, Eq, Show)

instance ToSchema JobPostingRequest

instance MergePatchable JobPostingRequest where
    mergePatchableFields _ = HMS.fromList [ ("rTitle", \v old -> case v of {(String s) -> old { rTitle=DT.unpack s }; _ -> old })
                                          , ("rDescription", \v old -> case v of {(String s) -> old { rDescription=DT.unpack s }; _ -> old })
                                          , ("rIndustry", \v old -> case v of {(String s) -> old { rIndustry=(read . DT.unpack) s }; _ -> old })
                                          , ("rJobType", \v old -> case v of {(String s) -> old { rJobType=(read . DT.unpack) s }; _ -> old })
                                          , ("rMinSalary", \v old -> case v of
                                                                       Number n -> old { rMinSalary=convertToInt n }
                                                                       Null -> old { rMinSalary=Nothing }
                                                                       _ -> old )
                                          , ("rMaxSalary", \v old -> case v of
                                                                       Number n -> old { rMaxSalary=convertToInt n }
                                                                       Null -> old { rMaxSalary=Nothing }
                                                                       _ -> old )
                                          , ("rSalaryCurrency", \v old -> case v of {(String s) -> old { rSalaryCurrency=(read . DT.unpack) s }; _ -> old })
                                          , ("rPosterEmail", \v old -> case v of {(String s) -> old { rPosterEmail=DT.unpack s }; _ -> old })
                                          , ("rApplyLink", \v old -> case v of {(String s) -> old { rApplyLink=DT.unpack s }; _ -> old })
                                          , ("rCompanyId", \v old -> case v of
                                                                       Number n -> old { rCompanyId=convertToInt n }
                                                                       Null -> old { rCompanyId=Nothing }
                                                                       _ -> old )
                                          ]


-- | Filter for pending statuses used by the API
data JobPostingRequestFilter = OnlyPending
                             | OnlyApproved deriving (Eq, Show, Read)

-- | Used by filter clauses to determine comparison method
data SqlWherePredicate = IsNull
                       | IsNonNull
                       | EqualTo S.SQLData
                       | GreaterThanOrEqualTo S.SQLData
                       | LessThan S.SQLData
                         deriving (Eq, Show)

instance Eq S.NamedParam where
    (==) a b = show a == show b

-- | Class of types that represents filterable properties
class DBEntity entity => FilterClause entity filter | filter -> entity where
    -- | Generate filtering clause to be used in a SQL query
    genWhere :: filter -> (SQLColumnName entity, SqlWherePredicate)

makeActiveJobFromPostingRequest :: JobPostingRequest -> CompanyID -> URLBounceConfigID -> DateTime -> Maybe Job
makeActiveJobFromPostingRequest r cid bounceCfgId now = Just Job { title=rTitle r
                                                                 , description=rDescription r
                                                                 , industry=rIndustry r
                                                                 , jobType=rJobType r
                                                                 , minSalary=rMinSalary r
                                                                 , maxSalary=rMaxSalary r
                                                                 , salaryCurrency=rSalaryCurrency r
                                                                 , employerId=cid
                                                                 , isActive=True
                                                                 , applyLink=rApplyLink r
                                                                 , postingDate=now
                                                                 , jobURLBounceConfigId=bounceCfgId
                                                                 , tags=[]
                                                                 , lastCheckedAt=Nothing
                                                                 }
class Massagable a where
    massage :: a -> Massaged a

data JobQuery = JobQuery
                { jqTerm            :: String
                , jqIndustries      :: [JobIndustry]
                , jqCompanies       :: [CompanyID]
                , jqLimit           :: Maybe Limit
                , jqOffset          :: Maybe Offset
                , jqOrder           :: Maybe (Order JobQuery)
                , jqTags            :: [TagName]
                , jqIncludeInactive :: Bool
                } deriving (Generic, Eq, Show)

instance Hashable JobQuery
instance ToSchema JobQuery

instance Massagable JobQuery where
    massage jq = Massaged $ jq { jqLimit=massagedLimit }
        where
          limit = 50
          massagedLimit = fmap (`min`limit) (jqLimit jq)

data SearchHistoryEntry = SearchHistoryEntry
                { sheTerm       :: String
                , sheIndustries :: [JobIndustry]
                , sheCompanies  :: [CompanyID]
                , sheTags       :: [String]
                , sheRecordedAt :: DateTime
                } deriving (Generic, Eq, Show)

instance ToSchema SearchHistoryEntry

data PaginatedList a = PaginatedList
    { items :: [a]
    , total :: Int
    } deriving (Generic, Eq, Show)

makePaginatedList :: [a] -> PaginatedList a
makePaginatedList as = PaginatedList as (length as)

instance ToSchema a => ToSchema (PaginatedList a)

instance Functor PaginatedList where
    fmap f list@(PaginatedList oldItems _) = list { items=fmap f oldItems }

instance Foldable PaginatedList where
    foldMap f (PaginatedList oldItems _) = foldMap f oldItems

data Role = Administrator
            | CompanyRepresentative
            | Recruiter
            | JobSeeker deriving (Generic, Eq, Enum, Show, Read, Data)

instance ToSchema Role

data Permission = ManageJobPostings
                | ManageJobs deriving (Generic, Eq, Enum, Show, Read)

instance ToParamSchema Permission
instance ToSchema Permission

instance Web.HttpApiData.ToHttpApiData Permission   where
    toUrlPiece = DT.pack . show

instance Web.HttpApiData.FromHttpApiData Permission where
    parseUrlPiece = read . DT.unpack

data PermissionRecord = PermissionRecord
                  { permissionRecordId  :: PermissionRecordID
                  , permission    :: Permission
                  , permittedRole :: Maybe Role
                  , permittedUser :: Maybe ForeignKey -- ^ FK to user table
                  } deriving (Generic, Eq, Show, Read)

instance ToSchema PermissionRecord

data AdminDashboardStats = AdminDashboardStats
    { totalUsers                       :: Int
    , activeJobCount                   :: Int
    , unapprovedJobPostingRequestCount :: Int
    , unresolvedSupportRequestCount    :: Int
    } deriving (Generic, Eq, Show)

instance ToSchema AdminDashboardStats

data CompanyStats = CompanyStats
    { cdsNumPostedJobs      :: Int
    , cdsOutclicks          :: Int
    , cdsNumFavoritesOnJobs :: Int
    } deriving (Generic, Eq, Show, Read)

instance ToSchema CompanyStats

data URLBounceConfig = URLBounceConfig
    { bounceName      :: Maybe String
    , bounceTargetUrl :: String
    , bounceIsActive  :: Bool
    , bounceCreatedAt :: DateTime
    } deriving (Generic, Eq, Show)

instance ToSchema URLBounceConfig

data URLBounceInfo = URLBounceInfo
    { bouncedHost      :: String
    , bouncedAt        :: DateTime
    , bouncedReferer   :: Maybe String
    , bouncedUserAgent :: Maybe String
    , bounceConfigId   :: ForeignKey
    } deriving (Generic, Eq, Show)

instance ToSchema URLBounceInfo

-- | Filter for URL bounce info
data URLBounceInfoFilter = BouncedBefore DDT.DateTime
                         | BouncedAfter DDT.DateTime
                           deriving (Eq, Show, Read)

data SupportRequestType = General
                        | Billing
                        | JobCreation
                        | Other deriving (Generic, Eq, Enum, Show, Read)

instance ToParamSchema SupportRequestType
instance ToSchema SupportRequestType

data SupportRequest = SupportRequest
    { srType            :: SupportRequestType
    , srTitle           :: String
    , srDescription     :: String
    , srIsResolved      :: Bool
    , srResolutionNotes :: Maybe String
    , srSubmittedBy     :: ForeignKey -- ^ FK to users table
    , srCreatedAt       :: DateTime
    } deriving (Generic, Eq, Show)

instance ToSchema SupportRequest

data PasswordChangeRequest = PasswordChangeRequest
    { pcrOldPassword      :: Password
    , pcrNewPassword      :: Password
    , pcrNewPasswordAgain :: Password
    } deriving (Generic, Eq, Show)

instance ToSchema PasswordChangeRequest

data TokenType = PasswordResetToken deriving (Generic, Eq, Read, Show)

instance ToSchema TokenType

data Token = Token { tType      :: TokenType
                   , tToken     :: DT.Text
                   , tCreatorId :: ForeignKey
                   , tCreatedAt :: DateTime
                   , tExpiresAt :: Maybe DateTime
                   } deriving (Generic, Eq, Read, Show)

instance ToSchema Token

data EmailSubscription = EmailSubscription { esEmailAddress     :: DT.Text
                                           , esConfirmationCode :: ConfirmationCode
                                           , esConfirmationDate :: Maybe DateTime
                                           , esUnsubscribeCode  :: UnsubscribeCode
                                           , esUnsubscribeDate  :: Maybe DateTime
                                           , esSubscribeDate    :: DateTime
                                           } deriving (Generic, Eq, Read, Show)

instance ToSchema EmailSubscription

data PromotionType = CompanyPromotion
                   | JobPromotion deriving (Eq, Show, Read, Generic)

instance ToSchema PromotionType

data Promotion = Promotion { prName        :: String
                           , prDescription :: Maybe String
                           , prType        :: PromotionType
                           , prPromoterId  :: ForeignKey
                           , prObjectId    :: ForeignKey
                           , prStart       :: DateTime
                           , prEnd         :: DateTime
                           , prCreatedBy   :: ForeignKey
                           , prIsActive    :: Bool
                           , prCreatedAt   :: DateTime
                           } deriving (Generic, Eq, Read, Show)

instance ToSchema Promotion

-- | Types of tasks
data TaskType = RandomJobRefresh deriving (Generic, Eq, Show, Read)

instance Hashable TaskType

-- | Tasks runnable with the task runner
data Task = Task { taskType           :: TaskType         -- ^ Type of task
                 , taskStatus         :: TaskStatus       -- ^ Current status of task
                 , taskJsonData       :: TaskData         -- ^ JSON data to be used for the task
                 , taskScheduledBy    :: Maybe ForeignKey -- ^ User who scheduled the task
                 , taskKilledBy       :: Maybe ForeignKey -- ^ User who killed task (if it was killed)
                 , taskRunAt          :: Maybe DateTime   -- ^ Time for task to run
                 , taskCreatedAt      :: DateTime         -- ^ When the task was created
                 , taskError          :: Maybe String     -- ^ If the task errors this will be present
                 } deriving (Generic, Eq, Read, Show)

-- | For representing task status
data TaskStatus = NotRunning
                | Success
                | Running
                | ExpectedFailure
                | UnexpectedFailure
                | Killed
                  deriving (Generic, Eq, Show, Read)

instance ToSchema TaskStatus
instance ToSchema TaskType
instance ToSchema Task

type Interval = NominalDiffTime
type TaskThreadMap = HMS.HashMap TaskType [ThreadId]
type TaskData = Maybe Value
type TaskRunner m = TaskData -> m (Either SomeException TaskResult)

-- | Used to return
data TaskResult = TaskResult { triStatus :: TaskStatus
                             , triMessage :: DT.Text
                             , triData :: Value
                             } deriving (Generic, Eq, Show, Read)

-- | Status of a tag import operation
data TagImportStatus = Imported
                     | TagAlreadyExists
                     | UnexpectedImportFailure
                       deriving (Generic, Eq, Show, Read)

instance ToSchema TagImportStatus

-- | Result of an attempt to import a tag
data TagImportResult = TagImportResult { tirTag    :: Maybe Tag
                                       , tirStatus :: TagImportStatus
                                       } deriving (Generic, Eq, Show, Read)

instance ToSchema TagImportResult

-- | Period for analytics
data AnalyticsPeriod = Hourly
                     | Daily deriving (Generic, Eq, Show, Read)

instance ToSchema AnalyticsPeriod

-- Automatic JSON typeclass derivations for types
$(deriveJSON defaultOptions ''Address)
$(deriveJSON defaultOptions ''AdminDashboardStats)
$(deriveJSON defaultOptions ''BrandingData)
$(deriveJSON defaultOptions ''Company)
$(deriveJSON defaultOptions ''CompanyStats)
$(deriveJSON defaultOptions ''Currency)
$(deriveJSON defaultOptions ''EmailSubscription)
$(deriveJSON defaultOptions ''EnvelopedResponse)
$(deriveJSON defaultOptions ''Job)
$(deriveJSON defaultOptions ''JobIndustry)
$(deriveJSON defaultOptions ''JobPostingRequest)
$(deriveJSON defaultOptions ''JobType)
$(deriveJSON defaultOptions ''Location)
$(deriveJSON defaultOptions ''PaginatedList)
$(deriveJSON defaultOptions ''PasswordChangeRequest)
$(deriveJSON defaultOptions ''Permission)
$(deriveJSON defaultOptions ''PermissionRecord)
$(deriveJSON defaultOptions ''Promotion)
$(deriveJSON defaultOptions ''PromotionType)
$(deriveJSON defaultOptions ''Role)
$(deriveJSON defaultOptions ''SearchHistoryEntry)
$(deriveJSON defaultOptions ''ServerInfo)
$(deriveJSON defaultOptions ''SessionInfo)
$(deriveJSON defaultOptions ''SponsoredCompany)
$(deriveJSON defaultOptions ''SupportRequest)
$(deriveJSON defaultOptions ''SupportRequestType)
$(deriveJSON defaultOptions ''Tag)
$(deriveJSON defaultOptions ''TagImportResult)
$(deriveJSON defaultOptions ''TagImportStatus)
$(deriveJSON defaultOptions ''Task)
$(deriveJSON defaultOptions ''TaskResult)
$(deriveJSON defaultOptions ''TaskStatus)
$(deriveJSON defaultOptions ''TaskType)
$(deriveJSON defaultOptions ''Token)
$(deriveJSON defaultOptions ''TokenType)
$(deriveJSON defaultOptions ''URLBounceConfig)
$(deriveJSON defaultOptions ''URLBounceInfo)
$(deriveJSON defaultOptions ''User)
$(deriveJSON defaultOptions ''UserEmailAndPassword)
$(deriveJSON defaultOptions ''UserInfo)
$(deriveJSON defaultOptions ''UserWithoutSensitiveData)
$(deriveJSON defaultOptions ''ValidationError)

-- Custom content type for JSON merge patch (application/merge-patch+json)
-- See: https://tools.ietf.org/html/rfc7386
data PatchJSON deriving Typeable

instance Accept PatchJSON where
   contentType _ = "application" // "merge-patch+json" /: ("charset", "utf-8")

instance (Show a, Read a, FromJSON a) => MimeUnrender PatchJSON a where
   mimeUnrender _ = eitherDecodeLenient

instance (Show a, ToJSON a) => MimeRender PatchJSON a where
   mimeRender _ = encode

-------------
-- Logging --
-------------

-- ^ Typeclass for app components (ex. Backend, Mailer) that have loggers
class HasLogger c where
    getComponentLogger :: c -> Maybe Logger

    logMsg :: MonadIO m => c -> Priority -> String -> m ()
    logMsg c p s = maybe (return ()) liftedLogL (getComponentLogger c)
        where
          liftedLogL l = liftIO (logL l p s)

    logTextMsg :: MonadIO m => c -> Priority -> DT.Text -> m ()
    logTextMsg c p t = maybe (return ()) liftedLogL (getComponentLogger c)
        where
          liftedLogL l = liftIO (logL l p (DT.unpack t))

----------------
-- DB Backend --
----------------

data SqliteBackend = SqliteBackend
    { backendCfg    :: BackendConfig
    , backendLogger :: Maybe Logger
    , backendConn   :: Maybe S.Connection
    }

data DBError = FailedToConnect
             | FailedToFindEntity DT.Text
             | FailedToFindEntities DT.Text
             | FailedToCreateEntity DT.Text
             | DuplicateRecordExisted
             | FailedToUpdateEntity DT.Text
             | UnexpectedDBFailure DT.Text
             | QueryFailure DT.Text
               deriving (Eq)

instance Exception DBError

instance Show DBError where
    show (FailedToFindEntity desc) = "Failed to find entity: " <> DT.unpack desc
    show (FailedToFindEntities desc) = "Failed to find entities: " <> DT.unpack desc
    show FailedToConnect = "Failed to connect to database"
    show DuplicateRecordExisted = "A duplciate record already exists and is protected by a UNIQUE constraint"
    show (FailedToCreateEntity desc) = "Failed to create entity: " <> DT.unpack desc
    show (FailedToUpdateEntity desc) = "Failed to update entity: " <> DT.unpack desc
    show (UnexpectedDBFailure desc) = "Unexpected DB failure: " <> DT.unpack desc
    show (QueryFailure desc) = "Unexpected query failure: " <> DT.unpack desc

-- | Convert a DBError to an import result
makeTagImportResult :: Either DBError (ModelWithID Tag) -> TagImportResult
makeTagImportResult (Left DuplicateRecordExisted) = TagImportResult Nothing TagAlreadyExists
makeTagImportResult (Left _)                      = TagImportResult Nothing UnexpectedImportFailure
makeTagImportResult (Right (ModelWithID _ tag))   = TagImportResult (Just tag) Imported

-- | Typeclass for App backend
class (HasLogger b, MonadIO m) => DatabaseBackend b m where
    -- | Connect the backend
    connect :: b -> m b
    -- | Disconnect the backend
    disconnect :: b -> m b
    -- | Get the current backend version
    getCurrentVersion :: b -> m BackendVersion
    -- | Migrate to a given backend version
    migrateToVersion :: BackendVersion -> b -> m (Maybe b)

-- | Entity stores must be capable of performing various functions on entities
class DatabaseBackend b m => EntityStore b m where
    create :: InsertableDBEntity entity => b -> entity -> m (Either DBError (ModelWithID e))
    getById :: DBEntity entity => b -> ID -> m (Either DBError (ModelWithID entity))
    getBySimpleField :: InsertableDBEntity entity => b -> SQLColumnName entity -> Value -> m (Either DBError (ModelWithID entity))
    deleteById :: DBEntity entity => b -> ID -> m (Either DBError (ModelWithID entity))
    updateById :: UpdatableDBEntity entity => b -> ID -> [(SQLColumnName entity, S.SQLData)] -> m (Either DBError (ModelWithID entity))
    list :: DBEntity entity => b -> PaginationOptions -> m (Either DBError (PaginatedList (ModelWithID entity)))
    count :: DBEntity entity => b -> m (Either DBError (EntityCount entity))

class EntityStore b m => UserStore b m where
    -- | Add a new user
    addUser :: b -> User -> Password -> m (Either DBError (ModelWithID User))
    -- | Retrieve all users
    getAllUsers :: b -> m (Either DBError (PaginatedList (ModelWithID User)))
    -- | Get count of users
    getUserCount :: b -> m (Either DBError (EntityCount User))
    -- | Login a user by email
    getUserByEmail :: b -> Email -> m (Either DBError (ModelWithID User))
    -- | Login a user by ID
    getUserByID :: b -> UserID -> m (Either DBError (ModelWithID User))

class UserStore b m => UserManager b m where
    -- | Login a user by email
    loginUser :: b -> Email -> Password -> m (Either DBError (ModelWithID User))
    -- | Complete a password reset
    completePasswordReset :: b -> PasswordResetToken -> Password -> m (Either DBError (ModelWithID User))
    -- | Process password change request for a given user
    processPasswordChangeRequest :: b -> PasswordChangeRequest -> UserID -> m (Either DBError Bool)

    -- | Get all permissions for a given user
    getPermissionsForUser :: b -> UserID -> Role -> m (Either DBError [PermissionRecord])
    -- | Add a permissions for a given user
    addPermissionForUser :: b -> UserID -> Permission -> m (Either DBError Int)
    -- | Remove a permission for a given user
    removePermissionForUser :: b -> UserID -> Permission -> m (Either DBError Int)

    -- | Get starred jobs for a given user
    getStarredJobsForUser :: b -> UserID -> m (Either DBError (PaginatedList JobWithCompany))
    -- | Perform some operation on a starred job for a given user
    operateOnStarredJobForUser :: b -> GenericOperation -> UserID -> JobID -> m (Either DBError (EntityCount User))

    -- | Get supported user home locations
    getSupportedUserHomeLocations :: b -> m (Either DBError (PaginatedList (ModelWithID Location)))
    -- | Get interesting jobs for a given user
    getInterestingJobsForUser :: b -> ModelWithID User -> ModelWithID EmailSubscription -> m (Either DBError (PaginatedList JobWithCompany))

    -- | Get the current sponsored company for a user
    getCurrentSponsoredCompanyForUser :: b -> ModelWithID User -> m (Either DBError (Maybe (ModelWithID SponsoredCompany)))

class EntityStore b m => TokenStore b m where
    -- | Create a token for a given user
    createTokenForUser :: b -> UserID -> TokenType -> m (Either DBError (ModelWithID Token))
    -- | Remove token by ID
    removeTokenByID :: b -> TokenID -> m (Either DBError (EntityCount Token))
    -- | Remove all tokens for a given creator user
    removeTokensByTypeForCreatorWithID :: b -> TokenType -> UserID -> m (Either DBError (EntityCount Token))
    -- | Get a single token by it's content (the stringified token)
    getTokenByContent :: b -> DT.Text -> m (Either DBError (ModelWithID Token))

class EntityStore b m => JobStore b m where
    -- | Get a job by ID
    getJobByID :: b -> JobID -> m (Either DBError (ModelWithID Job))
    -- | Add a new job posting
    addJob :: b -> Job -> m (Either DBError (ModelWithID Job))
    -- | Get all jobs in the database
    getAllJobs :: b -> m (Either DBError (PaginatedList (ModelWithID Job)))
    -- | Update a single job by ID
    updateJob ::  b -> JobID -> Job -> m (Either DBError (ModelWithID Job))
    -- | Get multiple jobs by ID
    findJobsByIDs :: b -> [JobID] -> m (Either DBError (PaginatedList JobWithCompany))
    -- | Get count of active jobs
    getActiveJobCount :: b -> m (Either DBError (EntityCount Job))
    -- | Find a job posting using a query
    findJobs :: b -> Massaged JobQuery -> m (Either DBError (PaginatedList JobWithCompany))
    -- | Hydrate a paginated list of Job IDs
    hydrateJobIDs :: b -> PaginatedList JobID -> m (Either DBError (PaginatedList JobWithCompany))
    -- | Get a random job
    getRandomJob :: b -> m (Either DBError (ModelWithID Job))

class JobStore b m => JobManager b m where
    -- | Update job activity for a single job
    setJobActivity :: b -> Bool -> JobID -> m (Either DBError (ModelWithID Job))

class EntityStore db m => JobPostingRequestStore db m where
    -- | Get all job posting requests
    getAllJobPostingRequests :: FilterClause JobPostingRequest filter
                                => db
                                    -> Maybe Limit   -- ^ Optional limit
                                    -> Maybe Offset  -- ^ Optional offset
                                    -> [filter]      -- ^ Zero or more filter clauses
                                    -> m (Either DBError (PaginatedList (ModelWithID JobPostingRequest)))

    -- | Get a job posting request by ID
    getJobPostingRequestByID :: db -> JobPostingRequestID -> m (Either DBError (ModelWithID JobPostingRequest))
    -- | Add a new job posting request
    addJobPostingRequest :: db -> JobPostingRequest -> m (Either DBError (ModelWithID JobPostingRequest))
    -- | Approve a job posting request by ID
    approveJobPostingRequestByID :: db -> JobPostingRequestID -> CompanyID -> m (Either DBError (ModelWithID Job))
    -- | Get count of job posting requests
    getUnapprovedJobPostingRequests :: db -> m (Either DBError (EntityCount JobPostingRequest))
    -- | Update job posting request by ID
    updateJobPostingRequestByID :: db -> JobPostingRequestID -> JobPostingRequest -> m (Either DBError (ModelWithID JobPostingRequest))

class EntityStore b m => URLBounceConfigStore b m where
    -- | Add a URL bounce configuration
    addURLBounceConfig :: b -> URLBounceConfig -> m (Either DBError (ModelWithID URLBounceConfig))
    -- | Set URL bounce configuration activity
    setURLBounceConfigActivity :: b -> Bool -> URLBounceConfigID -> m (Either DBError (ModelWithID URLBounceConfig))
    -- | Get all URL bounce configurations
    getAllURLBounceConfigs :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID URLBounceConfig)))
    -- | URL bounce configs
    findURLBounceConfigByID :: b -> URLBounceConfigID -> m (Either DBError (ModelWithID URLBounceConfig))
    -- | Find a url bounce configuration by name
    findURLBounceConfigByName :: b -> String -> m (Either DBError (ModelWithID URLBounceConfig))
    -- | Find the bounce info for ag iven configuration
    findURLBounceInfoForConfig :: b -> URLBounceConfigID -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID URLBounceInfo)))
    -- | Get the number of hits by a bounce config ID
    getNumberHitsForBounceConfigByID :: b -> URLBounceConfigID -> m (Either DBError Natural)
    -- | Save URL bounce information
    saveURLBounceInfo :: b -> URLBounceInfo -> m (Either DBError (ModelWithID URLBounceInfo))

class EntityStore b m => CompanyStore b m where
    -- | Get all companies
    getAllCompanies :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID Company)))
    -- | Get companies by ID
    getCompanyByID :: b -> CompanyID -> m (Either DBError (ModelWithID Company))
    -- | Add company
    addCompany :: b -> Company -> m (Either DBError (ModelWithID Company))
    -- | Operate on company representatives
    operateOnCompanyRepresentatives :: b -> GenericOperation -> CompanyID -> UserID -> m (Either DBError (EntityCount Company))
    -- | Get all company representatives
    getAllCompanyRepresentatives :: b -> CompanyID -> m (Either DBError (PaginatedList (ModelWithID User)))
    -- | Get companies represented by user with ID
    getCompaniesRepresentedByUserWithID :: b -> UserID -> m (Either DBError (PaginatedList (ModelWithID Company)))
    -- | Get stats for a given company
    getStatsForCompany :: b -> CompanyID -> m (Either DBError CompanyStats)

class EntityStore b m => SupportRequestStore b m where
    -- | Retrieve all support requests
    getAllSupportRequests :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID SupportRequest)))
    -- | Add a support request
    addSupportRequest :: b -> SupportRequest -> m (Either DBError (ModelWithID SupportRequest))
    -- | Get number of unresolved support requests
    getUnresolvedSupportRequestCount :: b -> m (Either DBError (EntityCount SupportRequest))

class EntityStore b m => TagStore b m where
    -- | Add a new tag
    addTag :: b -> Tag -> m (Either DBError (ModelWithID Tag))
    -- | Get all tags
    getAllTags :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID Tag)))
    -- | Find tags by a given ID
    findTagsByIDs :: b -> [TagID] -> m (Either DBError (PaginatedList (ModelWithID Tag)))
    -- | Get a single tag by ID
    getTagByID :: b -> TagID -> m (Either DBError (ModelWithID Tag))
    -- | Find tags by a given ID
    updateTag :: b -> TagID -> Tag -> m (Either DBError (ModelWithID Tag))
    -- | Delete a single tag by ID
    deleteTag :: b -> TagID -> m (Either DBError (EntityCount Tag))

class EntityStore b m => EmailSubscriptionStore b m where
    -- | Email Subscriptions
    saveEmailSubscription :: b -> DT.Text -> [TagID] -> m (Either DBError (ModelWithID EmailSubscription))
    -- | Confirm an email subscription
    confirmEmailSubscription :: b -> ConfirmationCode -> m (Either DBError (ModelWithID EmailSubscription))
    -- | Cancel an email subscription
    cancelEmailSubscription :: b -> UnsubscribeCode -> m (Either DBError (ModelWithID EmailSubscription))
    -- | Get all email subscriptions
    getAllEmailSubscriptions :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID EmailSubscription)))
    -- | Get all subscribers
    getEmailSubscribers :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID EmailSubscription)))
    -- | Get a subscription by email
    getEmailSubscriptionByEmail :: b -> Email -> m (Either DBError (ModelWithID EmailSubscription))
    -- | Get tags for a given email subscription
    getTagsForEmailSubscription :: b -> EmailSubscriptionID -> m (Either DBError (PaginatedList (ModelWithID Tag)))
    -- | Get a subscription by ID
    getEmailSubscriptionByID :: b -> EmailSubscriptionID -> m (Either DBError (ModelWithID EmailSubscription))

class EntityStore b m => PromotionStore b m where
    -- | Add a promotion
    addPromotion :: b -> Promotion -> m (Either DBError (ModelWithID Promotion))

class EntityStore b m => AddressStore b m where
    -- | Retrieve an address by ID
    getAddressByID :: b -> AddressID -> m (Either DBError (ModelWithID Types.Address))

class EntityStore b m => TaskStore b m where
    -- | Add a task
    addTask :: b -> TaskType -> TaskData -> Maybe UserID -> m (Either DBError (ModelWithID Task))
    -- | Retrieve a task by ID
    getTaskByID :: b -> TaskID -> m (Either DBError (ModelWithID Task))
    -- | Get all tasks
    getAllTasks :: b -> Maybe Limit -> Maybe Offset -> m (Either DBError (PaginatedList (ModelWithID Task)))

class (EntityStore db m, TaskStore db m) => TaskManager db m where
    -- | Update status for a task by ID
    updateTaskStatusByID :: db -> TaskID -> TaskStatus -> m (Either DBError (ModelWithID Task))
    -- | Record that a task has been started
    recordTaskStartedByID :: db -> TaskID -> m (Either DBError ())
    -- | Update error text fo ra task by ID
    updateTaskErrorByID :: db -> TaskID -> Maybe String -> m (Either DBError (ModelWithID Task))

class ( JobStore b m
      , URLBounceConfigStore b m
      , CompanyStore b m
      , UserStore b m
      ) => AnalyticsStore b m where

    -- | List jobs by bounce reate
    getJobsByBounceRate :: b
                        -> Maybe Limit
                        -> Maybe Offset
                        -> m (Either DBError (PaginatedList (WithBounceCount JobWithCompany)))

    -- | Get bounces of by time of day
    getBouncesBinnedByTimePeriod :: b
                                 -> AnalyticsPeriod
                                 -> Maybe StartTime
                                 -> Maybe EndTime
                                 -> m (Either DBError (HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)))

class (MonadIO m, DatabaseBackend b m) => HasDBBackend m b | m -> b where
    getDBBackend :: m b

class MonadIO m => HasCookieKey m where
    getCookieKey :: m WCS.Key

class MonadIO m => HasCookieConfig m where
    getCookieConfig :: m CookieConfig

class MonadIO m => HasLoggerBackend m where
    getLogger :: m (Maybe Logger)

class MonadIO m => HasAppConfig m where
    getAppConfig :: m AppConfig
