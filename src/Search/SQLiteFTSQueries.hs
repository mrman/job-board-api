{-# LANGUAGE OverloadedStrings #-}

module Search.SQLiteFTSQueries ( jobQueryHistoryTable
                               , jqhRecordedAt
                               , makeJobQuerySQL
                               , refreshJobsIndex
                               , refreshTagsIndex
                               , searchTags
                               , sheFields
                               ) where

import           Data.Maybe (isNothing, fromMaybe)
import           Data.Monoid ((<>))
import           Database.SQLite.Simple as S
import           Types
import           Database.SQLite.Types
import           Util (wrapList, insertFieldsIntoTable, makeOrderPhrase, genericCreatedAt, jobPostingDateFieldName)
import qualified Data.Text as DT
import qualified Database.SQLite.Simple.Types as ST

jobsSearchTable :: DT.Text
jobsSearchTable = "search_active_jobs"

tagsSearchTable :: DT.Text
tagsSearchTable = "search_tags"

jobQueryHistoryTable :: DT.Text
jobQueryHistoryTable = "history_job_query"

jqhRecordedAt :: DT.Text
jqhRecordedAt = "recordedAt"

-- | Fields for search history entries
sheFields :: [DT.Text]
sheFields = ["term", "industries", "companies", "tags", "recordedAt"]

genericIdField :: DT.Text
genericIdField = "id"

instance DBEntity SearchHistoryEntry where
    entityTableName _ = jobQueryHistoryTable
    idField _ = genericIdField

    validateWithErrors a = []

instance InsertableDBEntity SearchHistoryEntry where
    entityInsertFields _ = sheFields
    insertQuery she = insertFieldsIntoTable (entityTableName she) (entityInsertFields she)

-- | Create a chain of ORs for a given list of values (ex. "one OR two OR three")
makeOrChain :: Show a => [a] -> DT.Text
makeOrChain = DT.intercalate " OR " . (DT.pack . show <$>)

-- | Make a MATCH SQL phrase with ORs between the terms that need to be matched to the given field (ex. "field MATCH 'one OR two OR three'")
makeOrMatchPhrase :: Show a => [a] -> FieldName -> DT.Text
makeOrMatchPhrase values field = if DT.null orChain then "" else field <> " MATCH '" <> orChain <> "' "
  where
    orChain = makeOrChain values

makePrefixMatchPhrase :: FieldName -> [String] -> DT.Text
makePrefixMatchPhrase f =  DT.intercalate " OR " . map ((fieldMatch<>) . ("'"<>) . (<>"'") . (<>"*") . DT.pack)
    where
      fieldMatch = f <> " MATCH "

-- | Creates a query that returns the document IDs for matching jobs, given a JobQuery
-- NOTE: only active jobs are in search!
makeJobQuerySQL :: JobQuery -> (ST.Query, [S.NamedParam])
makeJobQuerySQL jq@(JobQuery term is cs limit offset order tags includeInactive) = (query, queryParams)
  where
    termPhrase = if null term then "" else jobsSearchTable <> " MATCH :term "
    queryParams = if null term then [] else [":term" S.:= (term<>"*")]

    industriesPhrase = makeOrMatchPhrase is "industry"
    companiesPhrase = makeOrMatchPhrase cs "employerId"
    tagsPhrase = makePrefixMatchPhrase "tags" tags

    wherePhrase = if DT.null filterPhrases then "" else "WHERE"
    nonEmptyPhrases = filter (not . DT.null) [termPhrase, industriesPhrase, companiesPhrase, tagsPhrase]
    filterPhrases = if null nonEmptyPhrases then "" else DT.intercalate " AND " nonEmptyPhrases

    limitPhrase = maybe "" (("LIMIT "<>) . DT.pack . show) limit
    offsetPhrase = maybe "" (("OFFSET "<>) . DT.pack . show) offset
    generatedOrderPhrase = makeOrderPhrase order
    orderPhrase = if DT.null generatedOrderPhrase then "ORDER BY COALESCE(lastCheckedAt, postingDate) DESC" else generatedOrderPhrase

    query = ST.Query $ DT.intercalate " " [ "SELECT docid"
                                          , "FROM " <> jobsSearchTable
                                          , wherePhrase
                                          , filterPhrases
                                          , orderPhrase
                                          , limitPhrase
                                          , offsetPhrase
                                          ]
-- | Refresh the jobs index
refreshJobsIndex :: ST.Query
refreshJobsIndex = ST.Query "INSERT OR REPLACE INTO search_active_jobs(docid, title, description, industry, jobType, employerId, postingDate, applyLink, tags, lastCheckedAt) \
                            \ SELECT id, title, description, industry, jobType, employerId, postingDate, applyLink, tags, lastCheckedAt \
                            \ FROM jobs \
                            \ WHERE isActive=1"


-- | Refresh the tags index
refreshTagsIndex :: ST.Query
refreshTagsIndex = ST.Query "INSERT OR REPLACE INTO search_tags(docid, name, names, cssColor) \
                            \ SELECT id, name, names, cssColor \
                            \ FROM tags"

-- | Creates a query that returns the document IDs for matching jobs, given a JobQuery
-- TODO: Fix search overlap for a term like EN_US
-- TODO: Fix search for utf8 terms
searchTags :: ST.Query
searchTags = ST.Query $ "SELECT docid FROM " <> tagsSearchTable <> " WHERE " <> tagsSearchTable <> " MATCH :term"
