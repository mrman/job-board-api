{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE ScopedTypeVariables    #-}
{-# LANGUAGE LambdaCase             #-}

module Search.SearchBackend ( makeConnectedSearchBackend
                            , SearchBackend(..)
                            , SearchBackendM(..)
                            , SQLiteFTS(..)
                            , HasSearchBackend(..)
                            ) where

import           Config (LoggerConfig(..), SearchConfig(..))
import           Control.Exception (Exception, SomeException, try, throw)
import           Control.Monad.Catch (handle)
import           Control.Monad.IO.Class
import           Control.Monad.State (when)
import           Data.DateTime (DateTime, getCurrentTime)
import           Data.Either (either)
import           Data.Maybe (isJust)
import           Data.Monoid ((<>))
import           Data.Proxy (Proxy(..))
import           Database.SQLite.Backend (noConnection)
import           Database.SQLite3 (exec)
import           System.Log.Logger (Logger, Priority(..))
import           Types (HasLogger(..), DatabaseBackend(..), SqliteBackend(..), JobQuery(..), PaginatedList(..), JobID, TagID, InsertableDBEntity(..), SearchHistoryEntry(..), Limit, Offset, Order(..), ModelWithID, Massaged(..), DBError)
import           Util (extractSingleNumber, convertToCountQuery, insertFieldsIntoTable, genSelectAllFromTableQuery, doQueryAndPaginate_)
import qualified Data.Text as DT
import qualified Database.SQLite.Simple as S
import qualified Search.SQLiteFTSQueries as SDBQ
import qualified System.IO.ExceptionFree as ExceptionFree

-- | SQLite FTS-based search backend
data SQLiteFTS = SQLiteFTS { searchCfg       :: SearchConfig
                           , searchLogger    :: Maybe Logger
                           , searchDBConn    :: Maybe S.Connection
                           , searchDBBackend :: SqliteBackend
                           }
-- | Errors that can be thrown from a search backend
data SearchBackendError = FailedToConnect
                        | UnexpectedFailure
                        | MigrationFailure
                        | ServersOverloaded
                        | NestedDBError DBError
                        | UnexpectedSearchInternalError String
                        | QueryFailed deriving (Eq)

instance Exception SearchBackendError where

instance Show SearchBackendError where
    show UnexpectedFailure = "An unexpected failure occurred"
    show MigrationFailure  = "Failed to migrate/setup search DB"
    show ServersOverloaded = "Search servers are overloaded... Please try your search again."
    show QueryFailed = "The query failed to return any results."

-- | Create a connected search backend
makeConnectedSearchBackend :: MonadIO m => SearchConfig -> Maybe Logger -> SqliteBackend -> m (Either SearchBackendError SQLiteFTS)
makeConnectedSearchBackend c maybeL db = liftIO $ connectSearch SQLiteFTS { searchCfg=c
                                                                          , searchLogger=maybeL
                                                                          , searchDBConn=Nothing
                                                                          , searchDBBackend=db
                                                                          }

noSearchConnection :: MonadIO m => m (Either SearchBackendError a)
noSearchConnection = return $ Left FailedToConnect

-- | Retrieve SQL required for migration, using search config
getMigrationSQL :: MonadIO m => SearchConfig -> m DT.Text
getMigrationSQL cfg = liftIO readAndPack
    where
      fpath = searchFTSMigrationFile cfg
      readAndPack = ExceptionFree.readFile fpath
                    >>= \case
                        Left err -> throw err
                        Right contents -> pure $ DT.pack contents


-- | Perform the migrations for search config (usually just one large migration)
doMigration_ :: MonadIO m => SearchConfig -> S.Connection -> m ()
doMigration_ cfg c = getMigrationSQL cfg
                     >>= liftIO . Database.SQLite3.exec (S.connectionHandle c)

-- | Log an error and return
logErrAndReturn :: MonadIO m => SQLiteFTS -> String -> SomeException -> m SQLiteFTS
logErrAndReturn s msgPrefix err = logMsg s ERROR message
                                  >> return s
    where
      message = msgPrefix <> ": " <> show err


-- | Base requirements to be considered a search backend
class SearchBackend s where
    getSearchLogger :: s -> Maybe Logger
    getSearchConfig :: s -> SearchConfig

-- | Requirements for Search backends in monadic contexts
class (SearchBackend s, Monad m) => SearchBackendM s m where
    -- | Connect to the search backend
    connectSearch :: s -> m (Either SearchBackendError s)

    -- | Initialize the search backend database
    initialize :: s -> m (Either SearchBackendError s)

    -- | Search for jobs, given a job query (returns the list of jobIDs along with the total amount)
    searchJobs :: Massaged JobQuery -> s -> m (Either SearchBackendError (PaginatedList JobID))

    -- | Attempt a refresh of the jobs search index (repopuplate virtual table)
    refreshJobsIndex :: s -> m (Either SearchBackendError ())

    -- | Save a particular search to the search_history table
    recordSearch :: JobQuery -> s -> m (Either SearchBackendError ())

    -- | Retrieve search history
    getJobSearchHistory :: Maybe Limit -> Maybe Offset -> s -> m (Either SearchBackendError (PaginatedList (ModelWithID SearchHistoryEntry)))

    -- | Search for tags, given a short term
    searchTags :: String -> s -> m (Either SearchBackendError (PaginatedList TagID))

    -- | Attempt a refresh of the tags search index (repopuplate virtual table)
    refreshTagsIndex :: s -> m (Either SearchBackendError ())

    -- | Disconnect
    disconnect :: s -> m s

instance SearchBackend SQLiteFTS where
    getSearchLogger = searchLogger
    getSearchConfig = searchCfg

instance MonadIO m => SearchBackendM SQLiteFTS m where
    connectSearch s = liftIO connectSearch_
        where
          cfg = searchCfg s
          addr = searchAddr cfg
          traceQueries = searchTraceQueries cfg
          handleSuccess conn = when traceQueries (S.setTrace conn (Just (logTextMsg s DEBUG)))
                               >> pure (s { searchDBConn=Just conn })
          connectSearch_ = logMsg s INFO ("Connecting to SQLITE FTS search backend @ (" <> addr <> ")")
                           >> try (S.open addr)
                           >>= either (logErrAndReturn s "Failed to connect to DB address") handleSuccess
                           >>= initialize

    initialize s = maybe noSearchConnection handler (searchDBConn s)
        where
          handler = liftIO . try . setup
          setup c = doMigration_ (searchCfg s) c
                    >> logMsg s INFO "Search successfully migrated"
                    >> pure s

    searchJobs mjq@(Massaged jq) s = maybe noSearchConnection handler (searchDBConn s)
        where
          (query, params) = SDBQ.makeJobQuerySQL jq
          logQuery = logMsg s DEBUG $ "Fn searchJobs query:" <> show query
          doSearch c = fmap S.fromOnly <$> (S.queryNamed c query params :: IO [S.Only JobID])
          getTotal c = extractSingleNumber <$> (S.queryNamed c (convertToCountQuery query) params :: IO [S.Only Int])

          handler = liftIO . recordAndPerformSearch

          recordAndPerformSearch c = logQuery
                                     >> recordSearch jq s
                                     >> getTotal c
                                     >>= \total -> doSearch c
                                     >>= \rows -> pure $ Right (PaginatedList rows total)

    refreshJobsIndex s = maybe noSearchConnection handler (searchDBConn s)
        where
          handler = liftIO . doRefresh
          doRefresh c = logMsg s DEBUG "Refreshing job search index..."
                        >> S.execute_ c SDBQ.refreshJobsIndex
                        >> pure (Right ())

    recordSearch (JobQuery term is cs order _ _ tags includeActive) = maybe noSearchConnection handler . searchDBConn
        where
          query = insertFieldsIntoTable SDBQ.jobQueryHistoryTable SDBQ.sheFields
          handler = liftIO . updateSearchHistory
          updateSearchHistory c = getCurrentTime
                                  >>= \t -> S.execute c query (SearchHistoryEntry term is cs tags t)
                                  >> pure (Right ())

    getJobSearchHistory limit offset = maybe noSearchConnection handler . searchDBConn
        where
          query = genSelectAllFromTableQuery SDBQ.jobQueryHistoryTable
          handler = liftIO . try . runQuery
          runQuery c = doQueryAndPaginate_ limit offset (Just (DateTimeDESC SDBQ.jqhRecordedAt)) query Nothing c
                       >>= pure . either (throw . NestedDBError) id

    searchTags term s = maybe noSearchConnection handler (searchDBConn s)
        where
          params = [":term" S.:= (term<>"*")]
          query = SDBQ.searchTags
          logQuery = logMsg s DEBUG $ "Fn searchJobs query:" <> show query
          doSearch c = fmap S.fromOnly <$> (S.queryNamed c query params :: IO [S.Only TagID])
          getTotal c = extractSingleNumber <$> (S.queryNamed c (convertToCountQuery query) params :: IO [S.Only Int])
          handler = liftIO . doTagSearch
          doTagSearch c = logQuery
                          >> getTotal c
                          >>= \total -> doSearch c
                          >>= \rows -> pure $ Right (PaginatedList rows total)

    refreshTagsIndex s = maybe noSearchConnection handler (searchDBConn s)
        where
          handler = liftIO . doRefresh
          doRefresh c = logMsg s DEBUG "Refreshing tag search index..."
                        >> S.execute_ c SDBQ.refreshTagsIndex
                        >> pure (Right ())

    disconnect s = maybe (return s) handler (searchDBConn s)
        where
          handler = liftIO . closeAndRemoveConn
          closeAndRemoveConn c = S.close c
                                 >> return s { searchDBConn=Nothing }

instance HasLogger SQLiteFTS where
    getComponentLogger (SQLiteFTS _ l _ _) = l

-- | Monadic contexts that have search backends
class (MonadIO m, SearchBackendM s m) => HasSearchBackend m s | m -> s where
    getSearchBackend :: m s
