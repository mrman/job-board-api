module Routes (API) where


type APIV1 = "industries" :> Get '[JSON] (EnvelopedResponse [JobIndustry])
    :<|> "addresses" :> Capture "addressId" AddressID :> Get '[JSON] (EnvelopedResponse (ModelWithID Address))
    :<|> "password-reset" :> "initiate" :> QueryParam "email" Email :> Post '[JSON] (EnvelopedResponse ())
    :<|> "password-reset" :> "complete" :> QueryParam "token" PasswordResetToken :> QueryParam "password" Password :> Post '[JSON] (EnvelopedResponse ())
    :<|> JobsAPIV1
    :<|> CompaniesAPIV1
    :<|> URLBounceConfigsAPIV1
    :<|> UsersAPIV1
    :<|> StarredJobsAPIV1
    :<|> AuthAPIV1
    :<|> SupportAPIV1
    :<|> LocationAPIV1
    :<|> TagsAPIV1
    :<|> SubscriptionAPIV1
    :<|> EmailAPIV1
    :<|> PromotionAPIV1
    :<|> AnalyticsAPIV1
