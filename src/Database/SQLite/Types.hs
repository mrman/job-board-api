{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ExplicitForAll        #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Database.SQLite.Types where

import           Data.Aeson (Value(..), ToJSON(..), FromJSON(..), eitherDecode)
import           Data.Aeson.Types (parseEither, fromJSON, Result(..))
import           Data.Maybe (fromJust, isJust, fromMaybe)
import           Data.Monoid ((<>))
import           Data.Text.Encoding (decodeUtf8, encodeUtf8)
import           Data.Typeable (Typeable, Proxy(..))
import           Database.SQLite.Simple.FromField
import           Database.SQLite.Simple.FromRow (RowParser)
import           Database.SQLite.Simple.Ok
import           Database.SQLite.Simple.ToField
import           Types
import           Util
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.HashMap.Strict as HMS
import qualified Data.List as DL
import qualified Data.Text as DT
import qualified Database.SQLite.Simple as S
import qualified Database.SQLite.Simple.Types as ST

---------------
-- Utilities --
---------------

-- | Utility function for parsing simple text fields stored in the database
parseSimpleTextField :: (Read a, Typeable a) => FieldParser a
parseSimpleTextField f = case fieldData f of
                           S.SQLText t -> Ok (read (DT.unpack t))
                           _ -> returnError S.ConversionFailed f "Expected type to be stored as text"

-- | Helper for serializing simple text fields
--   (this shouldn't be necessary, might have to do with not-properly-derived newtypes?...)
serializeSimpleTextField :: (Show a, Typeable a) => a -> S.SQLData
serializeSimpleTextField = S.SQLText . DT.pack . show

-- | Utility function for parsing JSON
parseJSONTextField :: (Read a, Typeable a, FromJSON a) => FieldParser a
parseJSONTextField f = case fieldData f of
                         S.SQLText t -> case eitherDecode (BSL8.fromStrict (encodeUtf8 t)) of
                                          Left err -> returnError S.ConversionFailed f $ "Invalid JSON: " <> show t
                                          Right a -> Ok a
                         _ -> returnError S.ConversionFailed f "Expected type to be valid JSON stored as text"

---------------------
-- Model instances --
---------------------

-- Model instances
instance FromField JobIndustry where
    fromField = parseSimpleTextField
instance ToField JobIndustry where
    toField = serializeSimpleTextField

instance FromField [JobIndustry] where
    fromField = parseSimpleTextField
instance ToField [JobIndustry] where
    toField = serializeSimpleTextField

instance FromField JobType where
    fromField = parseSimpleTextField
instance ToField JobType where
    toField = serializeSimpleTextField

instance FromField Currency where
    fromField = parseSimpleTextField
instance ToField Currency where
    toField = serializeSimpleTextField

instance FromField TaskType where
    fromField = parseSimpleTextField
instance ToField TaskType where
    toField = serializeSimpleTextField

instance FromField TaskStatus where
    fromField = parseSimpleTextField
instance ToField TaskStatus where
    toField = serializeSimpleTextField

instance FromField Value where
    fromField = parseJSONTextField

instance ToField Value where
    toField = serializeSimpleTextField . toJSON

instance DBEntity Tag where
    entityTableName _ = "tags"
    idField _ = "id"

    validateWithErrors t = [] ++ allValidLanguages
        where
          languages = HMS.keys (tagNames t)
          allValidLanguages = if all isValidLanguage languages then [] else [InvalidValue "tagNames" "Invalid language key detected. Only valid keys like 'EN_US' are allowed."]

instance S.FromRow Tag where
    fromRow = Tag <$> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow Tag where
    toRow t = [ toField (tagName t)
              , toField (tagNames t)
              , toField (tagCSSColor t)
              , toField (tagCreatedAt t)
              ]

instance FromField (HMS.HashMap DT.Text TagName) where
    fromField = parseSimpleTextField
instance ToField (HMS.HashMap DT.Text TagName) where
    toField = serializeSimpleTextField

instance S.FromRow Job where
    fromRow = Job <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow Job where
    -- ^ Convert job to row, without ID, which should be managed by Backend (PK)
    toRow j = [ toField (title j)
              , toField (description j)
              , toField (industry j)
              , toField (jobType j)
              , toField (minSalary j)
              , toField (maxSalary j)
              , toField (salaryCurrency j)
              , toField (postingDate j)
              , toField (employerId j)
              , toField (isActive j)
              , toField (applyLink j)
              , toField (jobURLBounceConfigId j)
              , toField (tags j)
              , toField (lastCheckedAt j)
              ]

instance FromField [Tag] where
    fromField = parseSimpleTextField
instance ToField [Tag] where
    toField = serializeSimpleTextField

instance S.FromRow JobWithCompany where
    -- Job, then company
    fromRow = (S.fromRow :: RowParser (ModelWithID Job))
              >>= \j -> (S.fromRow :: RowParser (ModelWithID Company) )
              >>= \c -> return $ JobWithCompany j c

instance S.FromRow JobPostingRequest where
    fromRow = JobPostingRequest <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field

instance S.ToRow JobPostingRequest where
    -- ^ Convert job to row, without ID, which should be managed by Backend (PK)
    toRow req = [toField (rTitle req),
                 toField (rDescription req),
                 toField (rIndustry req),
                 toField (rJobType req),
                 toField (rMinSalary req),
                 toField (rMaxSalary req),
                 toField (rSalaryCurrency req),
                 toField (rPosterEmail req),
                 toField (rApplyLink req),
                 toField (rCompanyId req),
                 toField (rApprovedJobId req),
                 toField (rRequestedAt req)]

instance S.ToRow SearchHistoryEntry where
    toRow s = [ toField (sheTerm s)
              , toField (sheIndustries s)
              , toField (sheCompanies s)
              , toField (sheTags s)
              , toField (sheRecordedAt s)
              ]

instance S.FromRow SearchHistoryEntry where
    fromRow = SearchHistoryEntry <$> S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance FromField Role where
    fromField = parseSimpleTextField

instance ToField Role where
    toField = serializeSimpleTextField

instance FromField Permission where
    fromField = parseSimpleTextField

instance ToField Permission where
    toField = serializeSimpleTextField

instance S.FromRow PermissionRecord where
    fromRow = PermissionRecord <$> S.field <*> S.field <*> S.field <*> S.field

instance FromField [CompanyID] where
    fromField = parseSimpleTextField

instance ToField [CompanyID] where
    toField = serializeSimpleTextField

instance FromField [String] where
    fromField = parseSimpleTextField

instance ToField [String] where
    toField = serializeSimpleTextField


instance S.FromRow URLBounceConfig where
    fromRow = URLBounceConfig <$> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow URLBounceConfig where
    toRow r = [toField (bounceName r),
               toField (bounceTargetUrl r),
               toField (bounceIsActive r),
               toField (bounceCreatedAt r)]

instance S.FromRow URLBounceInfo where
    fromRow = URLBounceInfo <$> S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow URLBounceInfo where
    toRow r = [ toField (bouncedHost r)
              , toField (bouncedAt r)
              , toField (bouncedReferer r)
              , toField (bouncedUserAgent r)
              , toField (bounceConfigId r)
              ]

instance FromField SupportRequestType where
    fromField = parseSimpleTextField

instance ToField SupportRequestType where
    toField = serializeSimpleTextField

instance S.FromRow SupportRequest where
    fromRow = SupportRequest <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field

instance S.ToRow SupportRequest where
    toRow r = [ toField (srType r)
              , toField (srTitle r)
              , toField (srDescription r)
              , toField (srIsResolved r)
              , toField (srResolutionNotes r)
              , toField (srSubmittedBy r)
              , toField (srCreatedAt r)
              ]

instance FromField TokenType where
    fromField = parseSimpleTextField

instance ToField TokenType where
    toField = serializeSimpleTextField

instance S.FromRow Token where
    fromRow = Token <$> S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow Token where
    toRow r = [ toField (tType r)
              , toField (tToken r)
              , toField (tCreatorId r)
              , toField (tCreatedAt r)
              , toField (tExpiresAt r)
              ]

instance S.FromRow EmailSubscription where
    fromRow = EmailSubscription <$> S.field <*> S.field <*> S.field <*> S.field <*> S.field  <*> S.field

instance S.ToRow EmailSubscription where
    toRow r = [ toField (esEmailAddress r)
              , toField (esConfirmationCode r)
              , toField (esConfirmationDate r)
              , toField (esUnsubscribeCode r)
              , toField (esUnsubscribeDate r)
              , toField (esSubscribeDate r)
              ]

instance ToField PromotionType where toField = serializeSimpleTextField
instance FromField PromotionType where fromField = parseSimpleTextField

instance S.FromRow Promotion where
    fromRow = Promotion <$> S.field <*> S.field <*> S.field <*> S.field <*> S.field <*>
              S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow Promotion where
    toRow p = [ toField (prName p)
              , toField (prDescription p)
              , toField (prType p)
              , toField (prPromoterId p)
              , toField (prObjectId p)
              , toField (prStart p)
              , toField (prEnd p)
              , toField (prCreatedBy p)
              , toField (prIsActive p)
              , toField (prCreatedAt p)
              ]

instance DBEntity Promotion where
    entityTableName _ = "promotions"
    idField _ = "id"

    validateWithErrors p = [] ++ validName
        where
          validName = [InvalidValue "prName" "Name must be provided" | null (prName p)]

instance S.FromRow User where
    fromRow = User <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow User where
    -- ^ Convert user to row, without ID, which should be managed by Backend (PK)
    toRow u = [ toField (firstName u)
              , toField (lastName u)
              , toField (emailAddress u)
              , toField (password u)
              , toField (policyAndSalt u)
              , toField (userRole u)
              , toField (joinedAt u)
              , toField (homeLocationId u)
              , toField (lastLoggedIn u)
              ]


instance S.FromRow Company where
    fromRow = Company <$> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field <*> S.field

instance S.ToRow Company where
    -- ^ Convert company to row, without ID, which should be managed by Backend (PK)
    toRow c = [ toField (companyName c)
              , toField (companyDescription c)
              , toField (companyCultureDescription c)
              , toField (companyHomepageURL c)
              , toField (companyIconURI c)
              , toField (companyBannerImageURI c)
              , toField (companyPrimaryColorCSS c)
              , toField (companyCreatedAt c)
              , toField (companyLastUpdatedAt c)
              , toField (companyAddressID c)
              ]

userEmailFieldName :: DT.Text
userEmailFieldName = "emailAddress"

userPasswordFieldName :: DT.Text
userPasswordFieldName = "password"

genericIDField :: DT.Text
genericIDField = "id"

genericStatusField :: DT.Text
genericStatusField = "status"

genericCreatorIdField :: DT.Text
genericCreatorIdField = "creatorId"

genericExpiresAtField :: DT.Text
genericExpiresAtField = "expiresAt"

usersTableName :: DT.Text
usersTableName = "users"

bounceRateField :: DT.Text
bounceRateField = "bounceRate"

instance DBEntity User where
    entityTableName _ = usersTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          validationFns = [\u -> if (null . emailAddress) u then Just (InvalidValue "emailAddress" "Email address misssing") else Nothing]

addressesTableName :: DT.Text
addressesTableName = "addresses"

jobsTableName :: DT.Text
jobsTableName = "jobs"

companiesTableName :: DT.Text
companiesTableName = "companies"


instance DBEntity Company where
    entityTableName _ = companiesTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          hasInvalidCSSColor = not . isCSSColor . companyPrimaryColorCSS
          validationFns = [\c -> if (null . companyName) c then Just (InvalidValue "companyName" "company name is misssing") else Nothing
                          ,\c -> if hasInvalidCSSColor c then Just (InvalidValue "companyPrimaryColorCSS" "Invalid CSS Company color") else Nothing
                          ]

urlBounceConfigsTableName :: DT.Text
urlBounceConfigsTableName = "url_bounce_configs"

urlBounceConfigFKName :: DT.Text
urlBounceConfigFKName = "bounceConfigId"

urlBounceConfigsNameFieldName :: DT.Text
urlBounceConfigsNameFieldName = "name"

urlBounceInfoTableName :: DT.Text
urlBounceInfoTableName = "url_bounce_info"

jobPostingRequestIdFieldName :: DT.Text
jobPostingRequestIdFieldName = genericIDField

approvedJobIdFieldName :: DT.Text
approvedJobIdFieldName = "approvedJobId"

permissionsTableName :: DT.Text
permissionsTableName = "permissions"

rolesTableName :: DT.Text
rolesTableName = "roles"

usersStarredJobsTableName :: DT.Text
usersStarredJobsTableName = "users_starred_jobs"

userFKName :: DT.Text
userFKName = "userId"

starredJobIDFKName :: DT.Text
starredJobIDFKName = "starredJobId"

companiesRepresentativesTableName :: DT.Text
companiesRepresentativesTableName = "companies_representatives"

supportRequestsTableName :: DT.Text
supportRequestsTableName = "support_requests"

instance DBEntity SupportRequest where
    entityTableName _ = supportRequestsTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          validationFns = [\sr -> if (null . srTitle) sr then Just (InvalidValue "title" "support request title is empty") else Nothing]

supportRequestsIsResolvedFieldName :: DT.Text
supportRequestsIsResolvedFieldName = "isResolved"

jprCompanyFKName :: DT.Text
jprCompanyFKName = "companyId"

jprRequestedAt :: DT.Text
jprRequestedAt = "requestedAt"

jobCompanyFKName :: DT.Text
jobCompanyFKName = "employerId"

tagsTableName :: DT.Text
tagsTableName = "tags"

tagFKName :: DT.Text
tagFKName = "tagId"

surround :: Char -> String -> String
surround c s = [c] ++ s ++ [c]

sqlLike :: String -> String
sqlLike = surround '%'

jobPostingRequestsTableName :: DT.Text
jobPostingRequestsTableName = "job_posting_requests"

jobPostingRequestFields :: [DT.Text]
jobPostingRequestFields = [ "title", "description", "industry", "jobType", "minSalary", "maxSalary", "salaryCurrency", "posterEmail", "applyLink", "companyId", approvedJobIdFieldName, "requestedAt"]

jprApprovedJobIdColumn :: SQLColumnName JobPostingRequest
jprApprovedJobIdColumn = approvedJobIdFieldName

jprMaxDescriptionLength :: Int
jprMaxDescriptionLength = 300

instance DBEntity JobPostingRequest where
    entityTableName _ = jobPostingRequestsTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          validationFns = [\j -> if (length . rDescription) j > jprMaxDescriptionLength then Just (InvalidValue "description" "Description is too long") else Nothing]

instance UpdatableDBEntity JobPostingRequest where
    entityUpdateFields _ = jobPostingRequestFields

instance FilterClause JobPostingRequest JobPostingRequestFilter where
    genWhere OnlyPending  = (jprApprovedJobIdColumn, IsNull)
    genWhere OnlyApproved = (jprApprovedJobIdColumn, IsNonNull)

tokensTableName :: DT.Text
tokensTableName = "tokens"

tokensTokenFieldName :: DT.Text
tokensTokenFieldName = "token"

instance DBEntity Token where
    entityTableName _ = tokensTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          validationFns = [\t -> if (DT.null . tToken) t then Just (InvalidValue "token" "token is empty") else Nothing]

emailSubscriptionsTableName :: DT.Text
emailSubscriptionsTableName = "email_subscriptions"

emailSubscriptionFKName :: DT.Text
emailSubscriptionFKName = "emailSubscriptionId"

instance DBEntity EmailSubscription where
    entityTableName _ = emailSubscriptionsTableName
    idField _ = genericIDField

    validateWithErrors = map fromJust . filter isJust . zipWith (\f a -> f a) validationFns . replicate (length validationFns)
        where
          validationFns = [\t -> if (DT.null . esEmailAddress) t then Just (InvalidValue "emailAddress" "emailAddress is empty") else Nothing]

emailSubscriptionFields :: [DT.Text]
emailSubscriptionFields = ["emailAddress", "confirmationCode", "confirmationDate", "unsubCode", "unsubDate", "subscribeDate"]

esConfirmationCodeFieldName :: DT.Text
esConfirmationCodeFieldName = emailSubscriptionFields !! 1

esConfirmationDateFieldName :: DT.Text
esConfirmationDateFieldName = emailSubscriptionFields !! 2

esSubscribeDateFieldName :: DT.Text
esSubscribeDateFieldName = last emailSubscriptionFields

esUnSubscribeCodeFieldName :: DT.Text
esUnSubscribeCodeFieldName = emailSubscriptionFields !! 3

esUnSubscribeDateFieldName :: DT.Text
esUnSubscribeDateFieldName = emailSubscriptionFields !! 4

instance InsertableDBEntity EmailSubscription where
    entityInsertFields _ = emailSubscriptionFields
    insertQuery es = insertFieldsIntoTable (entityTableName es) (entityInsertFields es)

emailSubscriptionTagsTableName :: DT.Text
emailSubscriptionTagsTableName = "email_subscription_tags"

userFields :: [DT.Text]
userFields = ["firstName", "lastName", "emailAddress", "password", "policyAndSalt", "role", "joinedAt", "homeLocationId", "lastLoggedIn"]

instance InsertableDBEntity User where
    entityInsertFields _ = userFields
    insertQuery u = insertFieldsIntoTable (entityTableName u) (entityInsertFields u)

companyFields :: [DT.Text]
companyFields = ["name", "description", "cultureDescription", "homepageUrl", "iconUri", "bannerImageUri", "primaryColorCSS", "createdAt", "lastUpdatedAt", "addressId"]

insertCompany :: ST.Query
insertCompany = insertFieldsIntoTable companiesTableName companyFields

instance InsertableDBEntity Company where
    entityInsertFields _ = companyFields
    insertQuery c = insertFieldsIntoTable (entityTableName c) (entityInsertFields c)

supportRequestFields :: [DT.Text]
supportRequestFields = ["type", "title", "description", "isResolved", "resolutionNotes", "submittedBy", "createdAt"]

instance InsertableDBEntity SupportRequest where
    entityInsertFields _ = supportRequestFields
    insertQuery sr = insertFieldsIntoTable (entityTableName sr) (entityInsertFields sr)

tagFields :: [DT.Text]
tagFields = ["name", "names", "cssColor", "createdAt"]

insertTag :: ST.Query
insertTag = insertFieldsIntoTable tagsTableName tagFields

updateTagByID :: PrimaryKey -> ST.Query
updateTagByID = updateRowInTableWithPK tagsTableName tagFields

instance InsertableDBEntity Tag where
    entityInsertFields _ = tagFields
    insertQuery t = insertFieldsIntoTable (entityTableName t) (entityInsertFields t)

tokenFields :: [DT.Text]
tokenFields = ["type", "token", "creatorId", "createdAt", "expiresAt"]

instance InsertableDBEntity Token where
    entityInsertFields _ = tokenFields
    insertQuery t = insertFieldsIntoTable (entityTableName t) (entityInsertFields t)

-- This is kept because it's needed for insert, and because SQLite doesn't support right joins properly...
-- Unfortunately this means it has to be kept in sync with the Job type (maybe some relflection/object analysis should be done to generate this instead)
jobFields :: [DT.Text]
jobFields = ["title", "description", "industry", "jobType", "minSalary", "maxSalary", "salaryCurrency", "postingDate", "employerId", "isActive", "applyLink", "urlBounceConfigId", "tags", "lastCheckedAt"]

jobURLBounceConfigIdField :: DT.Text
jobURLBounceConfigIdField = "urlBounceConfigId"

sqlInQuotedList :: (Show a) => [a] -> DT.Text
sqlInQuotedList s = DT.pack ("(" ++ DL.intercalate "," contents ++ ")")
              where
                contents = map (surround '"' . show) s

makeSQLValueList :: (Show a) => [a] -> DT.Text
makeSQLValueList l = DT.pack ("(" ++ DL.intercalate "," (show <$> l) ++ ")")

insertJob :: ST.Query
insertJob = insertFieldsIntoTable jobsTableName jobFields

updateJobByID :: PrimaryKey -> ST.Query
updateJobByID = updateRowInTableWithPK jobsTableName jobFields

updateRowInTableWithPK :: TableName -> [DT.Text] -> PrimaryKey -> ST.Query
updateRowInTableWithPK table fields pk = ST.Query $ DT.concat ["UPDATE OR ABORT ", table, " SET ", fieldsEqQuestionMarks, " WHERE ", genericIDField, " = ", pkStr]
    where
      pkStr = (DT.pack . show) pk
      fieldsEqQuestionMarks = DT.intercalate "," (map (`DT.append`"=?") fields)

insertURLBounceConfig :: ST.Query
insertURLBounceConfig = insertFieldsIntoTable urlBounceConfigsTableName fields
    where
      fields = ["name", "targetURL", "isActive", "createdAt"] :: [DT.Text]

urlBounceInfoFields :: [DT.Text]
urlBounceInfoFields = ["bouncedHost", "bouncedAt", "referer", "userAgent", "bounceConfigId"]

insertURLBounceInfo :: ST.Query
insertURLBounceInfo = insertFieldsIntoTable urlBounceInfoTableName fields
    where
      fields = urlBounceInfoFields

bouncedAtFieldName :: DT.Text
bouncedAtFieldName = "bouncedAt"

bouncedAtColumn :: SQLColumnName URLBounceInfo
bouncedAtColumn = bouncedAtFieldName

instance DBEntity URLBounceInfo where
    entityTableName _ = urlBounceInfoTableName
    idField _ = genericIDField

    validateWithErrors _ = []

instance UpdatableDBEntity URLBounceInfo where
    entityUpdateFields _ = urlBounceInfoFields

instance InsertableDBEntity URLBounceInfo where
    entityInsertFields _ = urlBounceInfoFields
    insertQuery t = insertFieldsIntoTable (entityTableName t) (entityInsertFields t)

instance FilterClause URLBounceInfo URLBounceInfoFilter where
    genWhere (BouncedBefore date) = (bouncedAtColumn, LessThan (toField date))
    genWhere (BouncedAfter date)  = (bouncedAtColumn, GreaterThanOrEqualTo (toField date))

insertJobPostingRequest :: ST.Query
insertJobPostingRequest = insertFieldsIntoTable jobPostingRequestsTableName fields
    where
      fields = ["title", "description", "industry", "jobType", "minSalary", "maxSalary", "salaryCurrency", "posterEmail", "applyLink", "companyId", "approvedJobId", "requestedAt"] :: [DT.Text]

updateJobActivityByID :: ST.Query
updateJobActivityByID = "UPDATE jobs SET isActive=:active WHERE id=:id"

 -- ^ query to find the permissions for a given user
selectPermissionsForUser :: ST.Query
selectPermissionsForUser = "SELECT * FROM permissions where grantedToUser=:id OR grantedToRole=:role"

-- ^ query to add a permissions for a given user
insertPermissionForUser :: ST.Query
insertPermissionForUser = "INSERT INTO permissions (grantedToUser, permissionName) VALUES (:id,:permission)"

updateUserPassword :: ST.Query
updateUserPassword  = "UPDATE users SET password=:password, policyAndSalt=:policyAndSalt WHERE id=:id"

deletePermissionForUser :: ST.Query -- ^ query to remove a permissions for a given user
deletePermissionForUser = "DELETE FROM permissions WHERE grantedToUser=:id AND permissionName=:permission"

-- ^ Generate SELECT query to get a row from any table by a single field comparison (=)
genSelectFromTableByFieldQuery :: TableName -> FieldName -> ST.Query
genSelectFromTableByFieldQuery table field = ST.Query $ "SELECT * FROM " <> table <> " WHERE " <>  field <> "=?"

genSelectFromTableByFieldNullQuery :: TableName -> FieldName -> ST.Query
genSelectFromTableByFieldNullQuery table field = ST.Query $ "SELECT * FROM " <> table <> " WHERE " <> field <> " IS NULL"

genSelectCountFromTableByFieldQuery :: TableName -> FieldName -> ST.Query
genSelectCountFromTableByFieldQuery table field = ST.Query $ "SELECT COUNT(*) FROM " <> table <> " WHERE " <> field <> "=?"

genSelectCountFromTableByFieldNullQuery :: TableName -> FieldName -> ST.Query
genSelectCountFromTableByFieldNullQuery table field = ST.Query $ "SELECT COUNT(*) FROM " <> table <> " WHERE " <> field <> " IS NULL"

genSelectRandomRowFromTableQuery :: TableName -> ST.Query
genSelectRandomRowFromTableQuery table = ST.Query $ "SELECT * FROM " <> table <> " WHERE id IN (SELECT id FROM " <> table <> " ORDER BY RANDOM() LIMIT 1)"

genDeleteFromTableByFieldQuery :: TableName -> FieldName -> ST.Query
genDeleteFromTableByFieldQuery table field = ST.Query $ "DELETE FROM " <> table <> " WHERE " <>  field <> "=?"

genCountAllRowsQuery :: DT.Text -> ST.Query -- ^ Generate SELECT query to count rows in table
genCountAllRowsQuery table = ST.Query $ "SELECT COUNT(*) FROM "<> table

genSingleFieldUpdateQuery :: TableName -> FieldName -> ST.Query
genSingleFieldUpdateQuery t f = ST.Query $ "UPDATE " <> t <> " SET " <> f <> " = :value WHERE id=:id"

-- ^ mappend with "." inbetween
(<.>) :: DT.Text -> DT.Text -> DT.Text
f <.> s = f <>"."<> s

-- ^ mappend with "=" inbetween
(<=>) :: DT.Text -> DT.Text -> DT.Text
f <=> s = f <>"="<> s

jobAndCompanyFields :: DT.Text
jobAndCompanyFields = DT.intercalate "," $ jobFields' <> companyFields'
    where
      jobFields' = fmap (jobsTableName<.>) $ genericIDField:jobFields
      companyFields' = fmap (companiesTableName<.>) $ genericIDField:companyFields

promotionFields :: [DT.Text]
promotionFields = ["name", "description", "type", "promoterId", "promotedObjectId", "start", "end", "createdBy", "isActive", "createdAt"]

promotionsTableName :: DT.Text
promotionsTableName = "promotions"

promotionTypeFieldName :: DT.Text
promotionTypeFieldName = "type"

instance InsertableDBEntity Promotion where
    entityInsertFields _ = promotionFields
    insertQuery p = insertFieldsIntoTable (entityTableName p) (entityInsertFields p)

instance DBEntity Task where
    entityTableName _ = "tasks"
    idField _ = "id"

    validateWithErrors p = []

instance S.FromRow Task where
    fromRow = Task <$> S.field <*> S.field <*> S.field
              <*> S.field <*> S.field <*> S.field <*> S.field
              <*> S.field

instance S.ToRow Task where
    toRow t = [ toField (taskType t)
              , toField (taskStatus t)
              , toField (taskJsonData t)
              , toField (taskScheduledBy t)
              , toField (taskKilledBy t)
              , toField (taskRunAt t)
              , toField (taskCreatedAt t)
              , toField (taskError t)
              ]

taskFields :: [DT.Text]
taskFields = ["taskType", "status", "jsonData", "scheduledBy", "killedBy", "runAt", "createdAt", "error"]

tasksTableName :: DT.Text
tasksTableName = "tasks"

taskRunAtFieldName :: DT.Text
taskRunAtFieldName = "runAt"

tasksErrorFieldName :: DT.Text
tasksErrorFieldName = "error"

instance InsertableDBEntity Task where
    entityInsertFields _ = taskFields
    insertQuery t = insertFieldsIntoTable (entityTableName t) (entityInsertFields t)

--------------------
-- Query builders --
--------------------

makeOrList :: DT.Text -> [String] -> DT.Text
makeOrList f =  (<>" )") . ("( "<>) . DT.intercalate " OR " . map ((f<>) . DT.pack . (" LIKE "<>) . surround '"' . surround '%')

-- ^ Convert a JobQuery to a SQL query. Full powered search (both active and inactive jobs)
convertJobQuery :: JobQuery -> ST.Query
convertJobQuery jq = ST.Query $ DT.concat [ "SELECT " <> jobAndCompanyFields <> " "
                                          , "FROM " <> jobsTableName <> " "
                                          , "JOIN " <> companiesTableName <> " "
                                          ,  "ON " <> jobsTableName<.>jobCompanyFKName <=> companiesTableName<.>genericIDField <> " "
                                          , "WHERE "
                                          , "(" <> jobsTableName<.>"title" <> " LIKE :term "
                                          , " OR " <> jobsTableName<.>"description" <> " LIKE :term"
                                          , " OR " <> jobsTableName<.>"industry" <> " LIKE :term ) "
                                          , andIndustryCheck
                                          , andCompaniesCheck
                                          , andTagsCheck
                                          , addActiveCheck
                                          ]
    where
      industries = jqIndustries jq
      andIndustryCheck = if null industries then "" else " AND " <> jobsTableName<.>"industry" <> " IN " <> sqlInQuotedList industries
      companies = jqCompanies jq
      andCompaniesCheck = if null companies then "" else " AND " <> jobsTableName<.>jobCompanyFKName <> " IN " <> makeSQLValueList companies
      tags = jqTags jq
      -- NOTE: Tags check is super inefficient, basically make sa large string of ORs, checking if the "tags" serialized object is like any of the individual tags.
      andTagsCheck = if null tags then "" else " AND " <> makeOrList (jobsTableName<.>"tags") tags
      addActiveCheck = if jqIncludeInactive jq then "" else " AND " <> jobsTableName <.> "isActive = 1"

searchJobsWithIndustries :: [JobIndustry] -> ST.Query
searchJobsWithIndustries industries = ST.Query $ DT.concat [ "SELECT "<> jobAndCompanyFields <> " "
                                                           , "FROM " <> jobsTableName <> " "
                                                           , "JOIN " <> companiesTableName <> " ON " <> jobsTableName<.>jobCompanyFKName <=> companiesTableName<.>genericIDField <> " "
                                                           , "WHERE (" <> jobsTableName<.>"title" <> " LIKE :term "
                                                           , " OR " <> jobsTableName<.>"description" <> " LIKE :term"
                                                           , ")"
                                                           , "AND " <> jobsTableName<.>"industry", " IN ", sqlInQuotedList industries
                                                           ]

searchActiveJobsWithIndustries :: [JobIndustry] -> ST.Query
searchActiveJobsWithIndustries industries = ST.Query $ DT.concat [ "SELECT * FROM "
                                                                 , jobsTableName, " "
                                                                 , "WHERE "<> genericIsActiveFieldName <> " = 1 "
                                                                 , "AND (title LIKE :term OR description LIKE :term) "
                                                                 , "AND industry IN ", sqlInQuotedList industries
                                                                 ]

getJobsByIDs :: [JobID] -> ST.Query
getJobsByIDs ids = ST.Query $ DT.intercalate " " ["SELECT * FROM " <> jobsTableName
                                                 , "JOIN " <> companiesTableName <> " ON " <> jobsTableName<.>jobCompanyFKName <=> companiesTableName<.>genericIDField
                                                 , "WHERE " <> jobsTableName<.>genericIDField <> " IN " <> idList
                                                 , "AND " <> genericIsActiveFieldName <> " = 1"
                                                 , "ORDER BY " <> jobPostingDateFieldName <> " DESC"
                                                 ]
    where
      idList = makeSQLValueList ids

getEntitiesByIds :: forall entity. TableName -> Order entity -> [Int] -> ST.Query
getEntitiesByIds table order ids = ST.Query $ DT.intercalate " " ["SELECT * FROM " <> table
                                                                 , "WHERE " <> table<.>genericIDField <> " IN " <> idList
                                                                 , makeOrderPhrase (Just order)
                                                                 ]
    where
      idList = makeSQLValueList ids

getJobsStarredByUser :: ST.Query
getJobsStarredByUser = ST.Query $ DT.concat ["SELECT " <> jobAndCompanyFields <> " "
                                            , "FROM " <> usersStarredJobsTableName <> " "
                                            , "JOIN " <> jobsTableName <> " ON " <> usersStarredJobsTableName<.>"starredJobId" <=> jobsTableName<.>genericIDField <> " "
                                            , "JOIN " <> companiesTableName <> " ON " <> jobsTableName<.>jobCompanyFKName <=> companiesTableName<.>genericIDField <> " "
                                            , "WHERE userId=?"
                                            ]

insertTupleRelation :: TableName -> FieldName -> FieldName -> ST.Query
insertTupleRelation tableName f1 f2 = ST.Query $ DT.concat ["INSERT INTO ", tableName, " (", f1, ",", f2, ", createdAt) VALUES (:v1,:v2,:date)"]

removeTupleRelation :: TableName -> FieldName -> FieldName -> ST.Query
removeTupleRelation tableName f1 f2 = ST.Query $ DT.concat ["DELETE FROM ", tableName, " WHERE ", f1, "=:v1 AND ", f2, "=:v2"]

-- ^ Get the rows of a table that are linked to another in a tuple relation
getTupleRelatedRowsQuery :: TableName   -- ^ The name of the relation table
                         -> FieldName   -- ^ The field in the relation to check against :value
                         -> FieldName   -- ^ The field in the relation table to use as an FK into join table
                         -> TableName   -- ^ The name of the table to join (retrieved records will be from this table)
                         -> FieldName   -- ^ The field in the join table to join with
                         -> [FieldName] -- ^ The list of fields to retrieve from teh join table (usually all of them)
                         -> ST.Query
getTupleRelatedRowsQuery relationTableName fieldToCheck relationFK joinTableName joinTableField fields =
    ST.Query $ DT.concat [ "SELECT ", prefixedFields, " "
                         , "FROM ", relationTableName, " "
                         , "JOIN ", joinTableName, " "
                         , "ON ", relationTableName, ".", relationFK, " = ", joinTableName, ".", joinTableField, " "
                         , "WHERE ", fieldToCheck, " = :value"
                         ]
    where
      prefixedFields = DT.intercalate "," . map (\s -> DT.concat [joinTableName, ".", s]) $ fields

activeJobCountForCompany :: ST.Query
activeJobCountForCompany = "SELECT COUNT(*) FROM jobs WHERE employerId = :cid AND isActive = 1"

outclicksOnActiveJobsForCompany :: ST.Query
outclicksOnActiveJobsForCompany = ST.Query $ DT.concat [ "SELECT COUNT(*) FROM ", jobsTableName, " AS j "
                                                       , "JOIN ", urlBounceConfigsTableName, " AS ubc ON j.urlBounceConfigId = ubc.id "
                                                       , "JOIN ", urlBounceInfoTableName, " AS ubi ON ubi.bounceConfigId = ubc.id "
                                                       , "WHERE j.employerId = :cid "
                                                       , "AND j." <> genericIsActiveFieldName <> " = 1"
                                                       ]

timesActiveJobsFavoritedForCompany :: ST.Query
timesActiveJobsFavoritedForCompany = ST.Query $ DT.concat [ "SELECT COUNT(*) FROM ", usersStarredJobsTableName, " AS usj "
                                                          , "JOIN jobs AS j ON usj.starredJobId = j.id "
                                                          , "JOIN companies ON j.employerId = :cid "
                                                          , "AND j." <> genericIsActiveFieldName <> " = 1"
                                                          ]

getDBVersion :: ST.Query
getDBVersion = "PRAGMA user_version"

idEquals :: DT.Text -> DBEntityID -> DT.Text
idEquals text = ((text <> "=") <>) . DT.pack . show

genUpdateQueryForEntity :: (DBEntity obj, UpdatableDBEntity obj) => DBEntityID -> obj -> ST.Query
genUpdateQueryForEntity eid obj = ST.Query $ DT.concat [ "UPDATE ", entityTableName obj, " "
                                                       , "SET ", fieldSets, " "
                                                       , "WHERE ", idField obj `idEquals` eid
                                                       ]
    where
      objFields = entityUpdateFields obj
      fieldSets = DT.intercalate "," . zipWith mappend objFields . map ("="<>) $ replicate (length objFields) "?"

updateLastLoggedInForUser :: ST.Query
updateLastLoggedInForUser = ST.Query $ "UPDATE " <> usersTableName <> " SET lastLoggedIn=:date WHERE " <> userEmailFieldName <> "=:email "

deleteTokensOfTypeForCreator :: ST.Query
deleteTokensOfTypeForCreator = ST.Query $ "DELETE FROM  " <> tokensTableName <> " WHERE creatorId=:creatorId and type=:tokenType"

selectEmailSubscribers :: ST.Query
selectEmailSubscribers = genSelectFromTableByFieldNullQuery emailSubscriptionsTableName esUnSubscribeDateFieldName

confirmEmailSubscriptionByID :: ST.Query
confirmEmailSubscriptionByID = ST.Query $ "UPDATE " <> emailSubscriptionsTableName <> " SET " <> esConfirmationDateFieldName <> "=:now, " <> esUnSubscribeDateFieldName <> "=NULL WHERE id=:id"


genPromotionQuery :: forall entity. Maybe PromotionType -> IncludeInactive -> Maybe (Order entity) -> ST.Query
genPromotionQuery pType includeInactive order = ST.Query $ DT.concat ["SELECT * FROM "
                                                                     , promotionsTableName <> " "
                                                                     , checkPhrase <> " "
                                                                     , orderPhrase
                                                                     ]
    where
      activeCheck = makeGenericIncludeActiveCheck includeInactive
      typeCheck = maybe "" ((promotionTypeFieldName <>) . (<> "'") . (" = '" <>) . DT.pack . show) pType
      nonEmptyChecks = filter (not . DT.null) [activeCheck, typeCheck]
      checkPhrase = if null nonEmptyChecks then "" else DT.intercalate " AND " nonEmptyChecks
      generatedOrderPhrase = makeOrderPhrase order
      orderPhrase = if DT.null generatedOrderPhrase then " ORDER BY " <> genericIsActiveFieldName <> " DESC" else generatedOrderPhrase

-- | Create where params to match where predicate
makeWhereParams :: DT.Text -> SqlWherePredicate -> [S.NamedParam]
makeWhereParams col IsNull             = [] -- handled by the filter phrasee (IS NULL)
makeWhereParams col IsNonNull          = [] -- handled by filter phrase (IS NOT NULL)
makeWhereParams col (EqualTo sqlValue) = [col S.:= sqlValue]
makeWhereParams col (GreaterThanOrEqualTo sqlValue) = [col S.:= sqlValue]
makeWhereParams col (LessThan sqlValue) = [col S.:= sqlValue]

-- | Create a filter phrase given some column and a predicate
makeFilterPhrase :: DT.Text -> SqlWherePredicate -> DT.Text
makeFilterPhrase col IsNull          = DT.intercalate " " [col, "IS NULL"]
makeFilterPhrase col IsNonNull       = DT.intercalate " " [col, "IS NOT NULL"]
makeFilterPhrase col (EqualTo param) = col <> "=:" <> col <> "_p"
makeFilterPhrase col (GreaterThanOrEqualTo param) = col <> ">=:" <> col <> "_p"
makeFilterPhrase col (LessThan param) = col <> "<:" <> col <> "_p"

-- | Augments an existing query which is presumed to have *no* WHERE clauses with some where clauses
addFilterClauses :: forall entity filter. (DBEntity entity, FilterClause entity filter) => ST.Query -> [filter] -> (ST.Query, [S.NamedParam])
addFilterClauses originalQuery filters = (updatedQuery, params)
  where
    -- Used for handling possibly ambiguous column names (prepends table name)
    prependTableName = (entityTableName (Proxy :: Proxy entity) <.>)

    -- convert all the filters to (column, SQLWherePredicate) pairs
    columnWherePredicatePairs :: [(SQLColumnName entity, SqlWherePredicate)]
    columnWherePredicatePairs = genWhere <$> filters

    -- Iteratively build list of phrases and named params
    buildPhrasesAndParams acc@(phrases, params) (col, whereP) = let newParams = makeWhereParams col whereP
                                                                    filterPhrase = makeFilterPhrase col whereP
                                                                    -- prepend table name to avoid collisions
                                                                    absFilterPhrase = prependTableName filterPhrase
                                                                in (phrases <> [absFilterPhrase], params <> newParams)

    (filterPhrases, params) = foldl buildPhrasesAndParams (mempty, mempty) columnWherePredicatePairs

    updatedQuery = ST.Query $ DT.intercalate " " [ ST.fromQuery originalQuery
                                                 , if null filterPhrases then "" else "WHERE", DT.intercalate "AND" filterPhrases
                                                 ]

genJobsByBounceRateQuery :: ST.Query
genJobsByBounceRateQuery = ST.Query $ DT.intercalate " " [ "SELECT " <> selection
                                                         , "FROM " <> jobsTableName
                                                         , "JOIN " <> companiesTableName <> " ON " <> companiesTableJoinCondition
                                                         , "JOIN " <> urlBounceInfoTableName <> " ON " <> bounceInfoJoinCondition
                                                         , "GROUP BY " <> urlBounceInfoTableName <.> urlBounceConfigFKName
                                                         ]
    where
      selection = "COUNT(*) as " <> bounceRateField <> "," <> jobAndCompanyFields
      bounceInfoJoinCondition = jobsTableName <.> jobURLBounceConfigIdField <=> urlBounceInfoTableName <.> urlBounceConfigFKName
      companiesTableJoinCondition = companiesTableName <.> genericIDField <=> jobsTableName <.> jobCompanyFKName

genURLBounceInfoWithinRangeQuery :: Maybe StartTime -> Maybe EndTime -> (ST.Query, [S.NamedParam])
genURLBounceInfoWithinRangeQuery start end = (queryWithBeginAndEnd, beginParams <> endParams)
    where
      query = genSelectAllFromTableQuery urlBounceInfoTableName
      (queryWithBegin, beginParams) = maybe (query, []) (\t -> addFilterClauses query [BouncedAfter t]) start
      (queryWithBeginAndEnd, endParams) = maybe (queryWithBegin, beginParams) (\t -> addFilterClauses queryWithBegin [BouncedBefore t]) end
