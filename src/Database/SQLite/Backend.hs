{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE DataKinds                  #-}
{-# LANGUAGE AllowAmbiguousTypes        #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE PolyKinds                  #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TypeOperators              #-}
{-# LANGUAGE InstanceSigs               #-}
{-# LANGUAGE LambdaCase               #-}

module Database.SQLite.Backend ( makeBackend
                               , noConnection ) where

import           Config (BackendConfig(..))
import           Control.Exception (Exception, SomeException, onException, throw, handle, catches, Handler(..))
import           Control.Monad.IO.Class
import           Control.Monad.State (when, join)
import           Data.Aeson.Types (Value)
import           Data.DateTime (DateTime, getCurrentTime)
import           Data.Either.Combinators (rightToMaybe)
import           Data.Foldable (forM_)
import           Data.Functor.Identity (runIdentity)
import           Data.Maybe (fromMaybe, fromJust, isNothing)
import           Data.Monoid ((<>))
import           Data.Natural (Natural, natural, indeterm)
import           Data.UUID (toText, toString, null)
import           Database.SQLite.Types
import           Database.SQLite3 (SQLError(..))
import           Safe (headMay)
import           System.FilePath ((</>))
import           System.IO (readFile)
import           System.Log.Logger (Logger, Priority(..))
import           System.Random (getStdGen, randomR)
import           Types
import           Util
import qualified Crypto.BCrypt as BCrypt
import qualified Data.ByteString.Char8 as BSC
import qualified Data.Text as DT
import qualified Data.UUID.V4 as UUID
import qualified Database.SQLite.Simple as S
import qualified Database.SQLite.Simple.ToField as STF
import qualified Database.SQLite3
import qualified System.IO.ExceptionFree as ExceptionFree

--------------
-- Backend  --
--------------

-- | Make a backend with a given configuration
makeBackend :: BackendConfig -> Maybe Logger -> SqliteBackend
makeBackend c l = SqliteBackend { backendCfg=c
                                , backendLogger=l
                                , backendConn=Nothing
                                }

hashPolicy :: BCrypt.HashingPolicy
hashPolicy = BCrypt.HashingPolicy 12 "$2y$"

hashpw :: Password -> BSC.ByteString -> Maybe BSC.ByteString
hashpw = BCrypt.hashPassword . BSC.pack

 -- | Get a row from any table by using a single field comparison (=)
getRowBySimpleField :: (S.FromRow x, STF.ToField a, MonadIO m)
                       => TableName         -- ^ Table to look in
                           -> SqliteBackend -- ^ Backend to use
                           -> FieldName     -- ^ Field name to use
                           -> a             -- ^ The object that will be turned into a field value (via STF.ToField)
                           -> m (Either DBError x)
getRowBySimpleField table b field value =
    let
        stmt = genSelectFromTableByFieldQuery table field
    in case backendConn b of
      Nothing -> return (Left FailedToConnect)
      Just c  -> do
        rows <- liftIO (S.query c stmt (S.Only value))

        case rows of
          (r:_) -> return $ Right r
          _ -> return $ Left $ UnexpectedDBFailure $ "Failed to find in table [" <> table <> "] querying for column [" <> field <> "]"

getRowBySimpleField_ :: (Show a, S.FromRow x, STF.ToField a, MonadIO m) => TableName -> FieldName -> a -> S.Connection -> m (Either DBError x)
getRowBySimpleField_ tbl field value c = liftIO (S.query c (genSelectFromTableByFieldQuery tbl field) (S.Only value))
                                         >>= pure . maybe failure Right . headMay
    where
      failure = Left $ FailedToFindEntity $ "Failed to find row in [" <> tbl <> "] w/ field [" <> field <> "]"

deleteRowBySimpleField_ :: (Show a, STF.ToField a) => TableName -> FieldName -> a -> S.Connection -> IO (Either DBError Int)
deleteRowBySimpleField_ tbl field value = handle (genericQueryFailure "row delete query") . runQuery
    where
      stmt = genDeleteFromTableByFieldQuery tbl field
      runQuery c = S.execute c stmt (S.Only value)
                   >> S.changes c
                   >>= pure . Right

getRowsBySimpleField_ :: ( S.FromRow entity, STF.ToField v, Show v )
                         => TableName
                             -> FieldName
                             -> v
                             -> Maybe Limit
                             -> Maybe Offset
                             -> Maybe (Order entity)
                             -> S.Connection
                             -> IO (Either DBError (PaginatedList entity))
getRowsBySimpleField_ tableName fieldName value limit offset order c = Right . makePaginatedList <$> S.query c query (S.Only value)
    where
      query = withLimitOffsetAndOrder limit offset order $ genSelectFromTableByFieldQuery tableName fieldName

countRowsByField_ :: (MonadIO m, STF.ToField v) => TableName -> FieldName -> Maybe v -> S.Connection -> m (Either DBError Int)
countRowsByField_ tbl field value = liftIO . handle (genericQueryFailure "row count query") . handler
    where
      queryFn = case value of
                  Nothing -> genSelectCountFromTableByFieldNullQuery
                  _ -> genSelectCountFromTableByFieldQuery

      query = queryFn tbl field

      runQuery c = case value of
                     Nothing -> S.query_ c query
                     _ -> S.query c query (S.Only value)

      handler c = runQuery c
                   >>= pure . Right . extractSingleNumber

updateSimpleField_ :: (MonadIO m, STF.ToField a) => TableName -> PrimaryKey -> FieldName -> a -> S.Connection -> m Int
updateSimpleField_ tbl pk field value c = liftIO $ S.withTransaction c transaction
    where
      transaction = S.executeNamed c (genSingleFieldUpdateQuery tbl field) [":id" S.:= pk, ":value" S.:= value]
                    >> S.changes c

-- Add job posting request
addJobPostingRequest_ :: MonadIO m => S.Connection -> JobPostingRequest -> m (Either DBError (ModelWithID JobPostingRequest))
addJobPostingRequest_ c req = liftIO $ S.withTransaction c $ do
                                S.execute c insertJobPostingRequest req
                                rowId <- S.lastInsertRowId c
                                getRowBySimpleField_ jobPostingRequestsTableName jobPostingRequestIdFieldName (fromIntegral rowId :: Int) c

-- Add some entity that has ToRow and FromRow to the database
addEntity_ :: (MonadIO m, S.ToRow entity, S.FromRow entity)
              => TableName
                  -> S.Query
                  -> entity
                  -> FieldName
                  -> S.Connection
                  -> m (Either DBError (ModelWithID entity))
addEntity_ tableName q entity field c = liftIO $ S.withTransaction c $ doInsert c
    where
      doInsert c = S.execute c q entity
                   >> S.lastInsertRowId c
                   >>= \rowId -> getRowBySimpleField_ tableName field (fromIntegral rowId :: Int) c

-- Convert posting request to job
convertPostingRequestToJob_ :: ( MonadIO m
                               , URLBounceConfigStore SqliteBackend m
                               , JobStore SqliteBackend m
                               , JobStore SqliteBackend m
                               ) => SqliteBackend -> ModelWithID JobPostingRequest -> CompanyID -> m (Either DBError (ModelWithID Job))
convertPostingRequestToJob_ b (ModelWithID reqId req) cid = do
  now <- liftIO getCurrentTime

  -- Generate a UUID to use for this job's bounce config
  uuid <- liftIO UUID.nextRandom
  when (Data.UUID.null uuid) (error "UUID generation for bounce URL failed")

  -- Create bounce config for the given job
  bcfg <- addURLBounceConfig b (URLBounceConfig (Just (toString uuid)) (rApplyLink req) True now)
  let bcid = case bcfg of
               Left _ -> error "Bounce URL creation failed"
               Right v -> mId v

  -- Create the job
  let job = fromMaybe (error "Failed to convert job from posting request") $ makeActiveJobFromPostingRequest req cid bcid now

  -- Add the job
  added <- addJob b job
  case added of
    Left err -> return $ Left err
    -- Ensure the posting request has been updated before returning the job
    Right j@(ModelWithID jid _) -> updatePostingRequestWithPostedJobID b reqId jid
                                   >> return (Right j)

updatePostingRequestWithPostedJobID :: ( MonadIO m
                                       , JobStore SqliteBackend m
                                       ) => SqliteBackend -> PrimaryKey -> PrimaryKey -> m (Either DBError (ModelWithID Job))
updatePostingRequestWithPostedJobID b rid jid = getConnOrFail b
                                                >>= updateSimpleField_ jobPostingRequestsTableName rid approvedJobIdFieldName jid
                                                -- Check changed row count
                                                >>= \case
                                                    1 -> getJobByID b jid
                                                    _ -> pure $ Left $ UnexpectedDBFailure "Unexpected number of operations occurred"

-- | Update a simple field on an entity and return the entity
updateSimpleFieldAndReturnEntityOnSuccess_ :: ( MonadIO m
                                              , S.FromRow obj
                                              , STF.ToField v )
                                             => TableName
                                                 -> PrimaryKey
                                                 -> FieldName
                                                 -> v
                                                 -> S.Connection
                                                 -> m (Either DBError (ModelWithID obj))
updateSimpleFieldAndReturnEntityOnSuccess_ tbl pk field val c = updateSimpleField_ tbl pk field val c
                                                                >>= \case
                                                                       1 -> getRowBySimpleField_ tbl genericIDField pk c
                                                                       _ -> return failure
    where
      failure = Left $ FailedToUpdateEntity $ "failed to update [" <> tbl <> "] w/ predicate on field [" <> field <> "]"

-- | Helper for handling generic query failures
genericQueryFailure :: MonadIO m => DT.Text -> SomeException -> m (Either DBError a)
genericQueryFailure desc ex = pure $ Left $ QueryFailure $ desc <> ": " <> DT.pack (show ex)

-- Get all users from the database
countAllRowsInTable :: (MonadIO m) => TableName -> S.Connection -> m (Either DBError Int)
countAllRowsInTable t c = liftIO $ handle (genericQueryFailure "count all rows") run
    where
      query = genCountAllRowsQuery t
      run = (S.query_ c query :: IO [S.Only Int])
            >>= pure . Right . S.fromOnly . head

insertEntity_ :: (MonadIO m, S.ToRow obj) => S.Query -> obj -> S.Connection -> m (Either DBError (ModelWithID obj))
insertEntity_ insertQuery entity c = execTransaction
                                     >>= \case
                                         (Right rowId) -> return $ Right $ ModelWithID (fromIntegral rowId) entity
                                         (Left err)    -> pure $ Left err
    where
      errorHandlers = [
       -- | Detect unique constraint failure to get more specific about DB failure
       Handler (\ (sqlEx :: SQLError) -> let details = sqlErrorDetails sqlEx
                                         in if "UNIQUE constraint failed" `DT.isInfixOf` details
                                            then pure $ Left DuplicateRecordExisted
                                            else pure $ Left $ FailedToCreateEntity details
               )]
      execTransaction = liftIO $ catches (S.withTransaction c transaction) errorHandlers
      transaction = S.execute c insertQuery entity
                    >> S.lastInsertRowId c
                    >>= pure . Right

updateDBEntity_ :: (MonadIO m, S.ToRow obj, DBEntity obj, UpdatableDBEntity obj) => DBEntityID -> obj -> S.Connection -> m DBEntityID
updateDBEntity_ eid e c = let updateQuery = genUpdateQueryForEntity eid e
                              updateAndGetId = S.execute c updateQuery e >> S.lastInsertRowId c
                          in liftIO $ S.withTransaction c (fromIntegral <$> updateAndGetId)

getFullListOfEntity_ :: (MonadIO m, S.FromRow obj) => TableName -> S.Connection -> m (Maybe (PaginatedList (ModelWithID obj)))
getFullListOfEntity_ tableName c = Just . makePaginatedList <$> doQuery
    where
      doQuery = liftIO $ S.query_ c query
      query = genSelectAllFromTableQuery tableName

modifyTupleTable_ :: ( MonadIO m
                     , STF.ToField a
                     , STF.ToField b
                     )
                    => TableName
                        -> GenericOperation -- ^ Operation to perform
                        -> FieldName        -- ^ LHS field
                        -> a                -- ^ LHS value
                        -> FieldName        -- ^ RHS field (FK)
                        -> b                -- ^ RHS value (FK)
                        -> S.Connection
                        -> m (Either DBError Int)
modifyTupleTable_ tableName op f1 v1 f2 v2 c = liftIO getCurrentTime
                                               >>= pure . genParams
                                               >>= runTransaction c
    where
      relationOpFn = case op of
                       Add -> insertTupleRelation
                       Remove -> removeTupleRelation
      query = relationOpFn tableName f1 f2
      genParams now = case op of
                    Add -> [":v1" S.:= v1, ":v2" S.:= v2, ":date" S.:= now]
                    Remove -> [":v1" S.:= v1, ":v2" S.:= v2]
      runTransaction c = liftIO . handle (genericQueryFailure "tuple operation") . S.withTransaction c . runQuery c
      runQuery c params = S.executeNamed c query params
                          >> S.changes c
                          >>= pure . Right

-- ^ Pull all related rows that match a certain condition from a relation table
getTupleRelatedRows_ :: ( MonadIO m
                        , STF.ToField val
                        , S.FromRow obj
                        , Show obj
                        )
                       => TableName -- ^ Relation table name
                           -> FieldName -- ^ The field to check
                           -> val -- ^ Value to check for
                           -> FieldName -- ^ The (other) field in the relation table to use as an FK into join table
                           -> TableName -- ^ Table to be joined
                           -> FieldName -- ^ The (other) field in the relation table to use as an FK into join table
                           -> [FieldName] -- ^ Fields to pull
                           -> S.Connection
                           -> m (Either DBError (PaginatedList (ModelWithID obj)))
getTupleRelatedRows_ relationTableName fieldToCheck value relationFK joinTableName joinTablePK fieldsToRetrieve = doQueryAndPaginate_ Nothing Nothing Nothing query (Just [":value" S.:= value])
    where
      query = getTupleRelatedRowsQuery relationTableName fieldToCheck relationFK joinTableName joinTablePK fieldsToRetrieve

getRandomRow_ :: ( MonadIO m
                 , S.FromRow obj
                 ) => TableName -> S.Connection -> m (Either DBError (ModelWithID obj))
getRandomRow_ tableName c = liftIO $ handleUnexpectedDBError $ doQuery c
    where
      query = genSelectRandomRowFromTableQuery tableName
      noRandomPickErr = QueryFailure $ "random pick from table [" <> tableName <> "] failed (empty?)"
      doQuery c = S.query_ c query
                  >>= pure . headMay
                  >>= ifNothingThrowError noRandomPickErr
                  >>= pure . Right

ensurePasswordMatches :: MonadIO m => Password -> Either DBError (ModelWithID User) -> m (Either DBError (ModelWithID User))
ensurePasswordMatches _ (Left err) = return $ Left err
ensurePasswordMatches p mdl@(Right (ModelWithID _ u)) = return result
    where
      hashed = BSC.pack . password $ u
      matches = BCrypt.validatePassword hashed (BSC.pack p)
      result = if matches
                then mdl
                else Left $ FailedToFindEntity "No user with matching credentials"

updatePasswordForUser_ :: MonadIO m => Password -> S.Connection -> Either DBError (ModelWithID User) -> m (Either DBError Bool)
updatePasswordForUser_ _ _ (Left err) = return $ Left err
updatePasswordForUser_ newPass c (Right (ModelWithID uid _)) = liftIO $ handleUnexpectedDBError handler
    where
      makeTransaction salt hashed  = S.executeNamed c updateUserPassword [ ":id" S.:= uid
                                                                         , ":password" S.:= BSC.unpack hashed
                                                                         , ":policyAndSalt" S.:= BSC.unpack salt
                                                                         ]
                    >> S.changes c
      handler = BCrypt.genSaltUsingPolicy hashPolicy
                >>= ifNothingThrowError (UnexpectedDBFailure "salt generation failed")
                >>= \salt -> pure (hashpw newPass salt)
                >>= ifNothingThrowError (UnexpectedDBFailure "hashing failed")
                >>= S.withTransaction c . makeTransaction salt
                >>= return . Right . (==1)

-- | Enum list of guaranteed-present (through migrations) locations that are OK to use as user home locations
supportedUserHomeLocations :: [ModelWithID Location]
supportedUserHomeLocations = [ ModelWithID 1 (Location 1 "United States of America")
                             , ModelWithID 2 (Location 2 "Japan")
                             ]

-- | Retrieve migration SQL for a given backend version
getMigrationSQL :: MonadIO m => BackendConfig -> BackendVersion -> m DT.Text
getMigrationSQL cfg version = liftIO work
    where
      migrationFilePath = (dbMigrationsPath cfg </>) $ (++".sql") $ show version
      work = ExceptionFree.readFile migrationFilePath
             >>= \case
                 Left err       -> throw err
                 Right contents -> pure (DT.pack contents)

-- | Perform a single migration
doMigration_ :: MonadIO m => BackendConfig -> S.Connection -> BackendVersion -> m ()
doMigration_ cfg c version = getMigrationSQL cfg version
                             >>= liftIO . Database.SQLite3.exec (S.connectionHandle c)

-- | Retrieve the version of the backend
getBackendVersion_ :: MonadIO m => S.Connection -> m BackendVersion
getBackendVersion_ c = liftIO (S.query_ c getDBVersion)
                       >>= \r -> return $ extractSingleNumber r

-- | Update the last logged in date (to now) for a user that's just logged in
updateUserLastLoggedIn :: MonadIO m => SqliteBackend -> Email -> m DateTime
updateUserLastLoggedIn b email = liftIO updateAndReturnTime
    where
      updateAndReturnTime = getCurrentTime
                            >>= \now -> maybe (return ()) (handle now) (backendConn b)
                            >> pure now
      handle now c = S.executeNamed c updateLastLoggedInForUser [":email" S.:= email, ":date" S.:= now]

-- | Insert and retrieve an entity
insertEntityAndGet_ :: (MonadIO m, InsertableDBEntity a, DBEntity a, S.FromRow a) => a -> S.Connection -> m (Either DBError (ModelWithID a))
insertEntityAndGet_ obj c = doInsert obj c
                            >>= \id -> getRowBySimpleField_ (entityTableName obj) (idField obj) id c

-- | Create a token (password reset token/other types of tokens)
makeToken :: MonadIO m => UserID -> TokenType -> m Token
makeToken userID = liftIO . makeToken_
    where
      makeToken_ tokenType = UUID.nextRandom
                             >>= \token -> getCurrentTime
                             >>= \now -> pure Token { tType=tokenType
                                                    , tToken=toText token
                                                    , tCreatorId=userID
                                                    , tCreatedAt=now
                                                    , tExpiresAt=Nothing
                                                    }
-- | Retrieve active promotions
getActivePromotions_ :: MonadIO m => Maybe PromotionType -> IncludeInactive -> S.Connection -> m (Either DBError [ModelWithID Promotion])
getActivePromotions_ pType includeInactive c = (Right <$>) . liftIO $ S.query_ c (genPromotionQuery pType includeInactive Nothing)

getConnOrFail :: MonadIO m => SqliteBackend -> m S.Connection
getConnOrFail = maybe (throw FailedToConnect) pure . backendConn

-- | Helper for converting A plain integer to an entity count
toEntityCount :: Either DBError Int -> Either DBError (EntityCount a)
toEntityCount (Left e) = Left e
toEntityCount (Right v) = let result = natural (toInteger v)
                          in if result == indeterm
                             then Left $ UnexpectedDBFailure "Entity count was not a natural number"
                             else Right $ EntityCount result

noConnection :: MonadIO m => m (Either DBError a)
noConnection = return $ Left FailedToConnect

-- | Handle unexpected errors
handleUnexpectedDBError :: forall v. IO (Either DBError v) -> IO (Either DBError v)
handleUnexpectedDBError = handle pureUnexpectedDBFailure
    where
      pureUnexpectedDBFailure :: SomeException -> IO (Either DBError v)
      pureUnexpectedDBFailure ex = pure $ Left $ UnexpectedDBFailure $ DT.pack $ show ex

-- | Column names are shared for Job & JobWithCompany (since JobWithCompany is an aggregate)
instance ConvertableColumn Job JobWithCompany where
instance ConvertableColumn JobQuery JobWithCompany

instance MonadIO m => DatabaseBackend SqliteBackend m where
    -- Connect the backend
    -- The backend is assumed to be in proper form for the app at time of deployment.
    -- Migrations are handled externally, meaning care must be taken when deploying new versions of the app
    connect b = liftIO $ onException openConn (return b)
        where
          cfg = backendCfg b
          address = dbAddr cfg
          traceQueries = dbTraceQueries cfg
          openConn = S.open address
                     >>= \c -> when traceQueries (S.setTrace c (Just (logTextMsg b DEBUG)))
                     >> pure (b { backendConn=Just c})

    disconnect b = maybe (return b) (liftIO . handle) (backendConn b)
        where
          handle c = S.close c >> return b {backendConn=Nothing}

    getCurrentVersion = maybe (error "Failed to get version, database disconnected") getBackendVersion_ . backendConn

    migrateToVersion target b = maybe (return Nothing) handle (backendConn b)
        where
          cfg = backendCfg b
          logMigrationAttempt v = logMsg b INFO ("Attempting migration to DB version [" ++ show v ++ "]...")
          handle c = getCurrentVersion b
                     >>= \v -> logMsg b INFO ("DB Currently at version [" ++ show v ++ "], migrating to version [" ++ show target ++ "]")
                               >> forM_ [v+1..target] (\nextVersion -> logMigrationAttempt nextVersion >> doMigration_ cfg c nextVersion)
                     >> return (Just b)


instance MonadIO m => EntityStore SqliteBackend m where
    create :: InsertableDBEntity entity => b -> entity -> m (Either DBError (ModelWithID e))
    create = undefined

    getById :: DBEntity entity => b -> ID -> m (Either DBError (ModelWithID entity))
    getById = undefined

    getBySimpleField :: InsertableDBEntity entity => b -> SQLColumnName entity -> Value -> m (Either DBError (ModelWithID entity))
    getBySimpleField = undefined

    deleteById :: DBEntity entity => b -> ID -> m (Either DBError (ModelWithID entity))
    deleteById = undefined

    updateById :: UpdatableDBEntity entity => b -> ID -> [(SQLColumnName entity, S.SQLData)] -> m (Either DBError (ModelWithID entity))
    updateById = undefined

    list :: DBEntity entity => b -> PaginationOptions -> m (Either DBError (PaginatedList (ModelWithID entity)))
    list = undefined

    count :: DBEntity entity => b -> m (Either DBError (EntityCount entity))
    count = undefined


instance MonadIO m => UserStore SqliteBackend m where
    addUser b user pass = maybe noConnection (liftIO . handle) (backendConn b)
        where
          returnHashError = pure $ Left $ UnexpectedDBFailure "Failed to hash password"
          returnSaltGenFailure = pure $ Left $ UnexpectedDBFailure "Failed to generate password salt"

          query = insertFieldsIntoTable usersTableName (entityInsertFields user)

          handle :: S.Connection -> IO (Either DBError (ModelWithID User))
          handle conn = BCrypt.genSaltUsingPolicy hashPolicy
                        >>= maybe returnSaltGenFailure (hashAndStuff conn)

          -- Hash with the salt
          hashAndStuff :: S.Connection -> BSC.ByteString -> IO (Either DBError (ModelWithID User))
          hashAndStuff conn salt = maybe returnHashError (updateUser conn salt) $ hashpw pass salt

          -- Fill out the user object with timely information
          updateUser :: S.Connection -> BSC.ByteString -> BSC.ByteString -> IO (Either DBError (ModelWithID User))
          updateUser conn salt hashedPassword = getCurrentTime
                                                -- Update the user object
                                                >>= \now -> pure (user { password=BSC.unpack hashedPassword
                                                                       , policyAndSalt=(Just . BSC.unpack) salt
                                                                       , joinedAt=now
                                                                       , lastLoggedIn=now
                                                                       })
                                                -- Save the updated user object and fetch it fresh
                                                >>= S.withTransaction conn . updateAndGetUser conn

          -- Update the user in the DB and re-fetch the user
          updateAndGetUser :: S.Connection -> User -> IO (Either DBError (ModelWithID User))
          updateAndGetUser conn updatedUser = S.execute conn query updatedUser
                                              >> S.lastInsertRowId conn
                                              >>= getUserByID b . fromIntegral

    getAllUsers = maybe noConnection handle . backendConn
        where
          handle = doQueryAndPaginate_ Nothing Nothing Nothing (genSelectAllFromTableQuery usersTableName) Nothing

    getUserCount = maybe noConnection handler . backendConn
        where
          handler = (toEntityCount <$>) . countAllRowsInTable usersTableName

    getUserByEmail b = getRowBySimpleField usersTableName b userEmailFieldName

    getUserByID b = getRowBySimpleField usersTableName b genericIDField

instance MonadIO m => UserManager SqliteBackend m where
    loginUser b email pass = getUserByEmail b email
                             >>= either (pure . Left) validate
        where
          updateLastLogin uid user = updateUserLastLoggedIn b email
                                     >>= \t -> pure $ Right $ ModelWithID uid (user {lastLoggedIn=t})

          noUserError = pure $ Left $ FailedToFindEntity "No user with matching credentials"

          validate (ModelWithID uid u) = let storedhash = BSC.pack (password u)
                                             passwordBytes = BSC.pack pass
                                         in if BCrypt.validatePassword storedhash passwordBytes
                                            then updateLastLogin uid u
                                            else noUserError

    completePasswordReset b token newPass = maybe noConnection handle (backendConn b)
        where
          handle c = liftIO $ handleUnexpectedDBError $ updatePassword c
          updatePassword c = getTokenByContent b token
                             >>= throwIfLeft
                             -- Get the user associated to the token
                             >>= \(ModelWithID _ token) -> getUserByID b (tCreatorId token)
                             >>= throwIfLeft
                             -- Update the password for the user
                             >>= \user -> updatePasswordForUser_ newPass c (Right user)
                             >> logMsg b INFO ("Successfully reset password for user with email "<> (show . mId) user)
                             -- Remove the token
                             >> removeTokensByTypeForCreatorWithID b PasswordResetToken (mId user)
                             >> pure (Right user)

    processPasswordChangeRequest b pcr uid = maybe noConnection handle (backendConn b)
        where
          handle c = getRowBySimpleField_ usersTableName genericIDField uid c
                   >>= ensurePasswordMatches (pcrOldPassword pcr)
                   >>= updatePasswordForUser_ (pcrNewPassword pcr) c

    getPermissionsForUser b uid r = maybe noConnection handler (backendConn b)
        where
          params = [":id" S.:= uid, ":role" S.:= show r]
          runQuery c = Right <$> S.queryNamed c selectPermissionsForUser params
          handler = liftIO . handle (genericQueryFailure "permission selection") . runQuery

    addPermissionForUser b uid p = maybe noConnection handler (backendConn b)
        where
          params = [":id" S.:= uid, ":permission" S.:= p]
          transaction c = S.executeNamed c insertPermissionForUser params >> S.changes c
          runQuery c = Right <$> S.withTransaction c (transaction c)
          handler = liftIO . handle (genericQueryFailure "add permission") . runQuery

    removePermissionForUser b uid p = maybe noConnection handler (backendConn b)
        where
          params = [":id" S.:= uid, ":permission" S.:= p]
          transaction c = S.executeNamed c deletePermissionForUser params >> S.changes c
          runQuery c = Right <$> S.withTransaction c (transaction c)
          handler = liftIO . handle (genericQueryFailure "remove permission") . runQuery

    getStarredJobsForUser b uid = maybe noConnection handler (backendConn b)
        where
          runQuery c = Right . makePaginatedList <$> S.query c getJobsStarredByUser (S.Only uid)
          handler = liftIO . handle (genericQueryFailure "get starred jobs") . runQuery

    operateOnStarredJobForUser b op uid jid = maybe noConnection handler (backendConn b)
        where
          handler = (toEntityCount <$>) . modifyTupleTable_ usersStarredJobsTableName op userFKName uid starredJobIDFKName jid

    getSupportedUserHomeLocations _ = return $ Right $ makePaginatedList supportedUserHomeLocations

    getInterestingJobsForUser b u@(ModelWithID _ user) (ModelWithID sid _) = liftIO $ handleUnexpectedDBError handler
        where
          limit = Just 5
          offset = Nothing
          sort = Just (DateTimeDESC jobPostingDateFieldName)
          buildJq names = Massaged $ JobQuery "" [] [] limit offset sort names False
          handler = getTagsForEmailSubscription b sid
                    >>= throwIfLeft
                    >>= pure . (tagName <$>) . (model <$>) . items
                    >>= findJobs b . buildJq
                    >>= throwIfLeft
                    >>= pure . Right

    getCurrentSponsoredCompanyForUser b u = maybe noConnection handle (backendConn b)
        where
          handle c = getActivePromotions_ (Just CompanyPromotion) False c
                     >>= throwIfLeft
                     >>= pure . headMay
                     >>= maybe (pure (Right Nothing)) getPromotionCompany

          noMatchingCompany pid = FailedToFindEntity $ "A sponsored company for promotion [" <> showText pid <> "]"

          -- Get company that matches tha that matches that promotion
          getPromotionCompany (ModelWithID pid p) = getCompanyByID b (prObjectId p)
                                                    >>= ifLeftThrow (noMatchingCompany pid)
                                                    >>= pure . Right . Just . (SponsoredCompany <$>)

instance MonadIO m => JobStore SqliteBackend m where
    getJobByID b = getRowBySimpleField jobsTableName b genericIDField

    addJob b job = maybe noConnection handle (backendConn b)
        where
          handle c = liftIO $ S.withTransaction c $ insertAndGet c
          insertAndGet c = S.execute c insertJob job
                           >> S.lastInsertRowId c
                           >>= getJobByID b . fromIntegral

    getAllJobs = maybe noConnection handler . backendConn
        where
          query = genSelectAllFromTableQuery jobsTableName
          runQuery c = S.query_ c query
                       >>= \rows -> return (Right (PaginatedList rows (length rows)))
          handler = liftIO . handle (genericQueryFailure "job retrieval query") . runQuery

    getActiveJobCount = maybe noConnection handler . backendConn
        where
          handler = (toEntityCount <$>) . countRowsByField_ jobsTableName genericIsActiveFieldName (Just True)

    -- Used by list all endpoints
    findJobs b (Massaged jq) = maybe noConnection handler (backendConn b)
        where
          term = jqTerm jq
          limit = jqLimit jq
          offset = jqOffset jq
          -- Ensure the default order is created at
          order = Just $ convertOrder $ fromMaybe (DateTimeDESC jobPostingDateFieldName) $ jqOrder jq
          query = convertJobQuery jq
          handler = doQueryAndPaginate_ limit offset order query (Just [":term" S.:= sqlLike term])

    findJobsByIDs b ids = maybe noConnection handler (backendConn b)
      where
        query = getJobsByIDs ids
        runQuery c = S.query_ c query
                     >>= pure . Right . makePaginatedList
        handler = liftIO . handle (genericQueryFailure "find jobs by id") . runQuery

    -- Convert a paginated listing of job ids to a paginated list of jobs,
    -- while keeping the original search-backend-provided total count
    hydrateJobIDs b paginatedList = maybe noConnection handler (backendConn b)
      where
        searchTotal = total paginatedList
        handler c =  fmap (\res -> res {total=searchTotal}) <$> findJobsByIDs b (items paginatedList)

    updateJob b jid job = maybe noConnection handle (backendConn b)
        where
          query = updateJobByID jid
          handle c = liftIO $ S.withTransaction c $ updateAndGet c
          updateAndGet c = S.execute c query job
                           >> getJobByID b jid

    getRandomJob = maybe noConnection handle . backendConn
        where
          handle = getRandomRow_ jobsTableName


instance MonadIO m => JobManager SqliteBackend m where
    setJobActivity b active jid  = maybe noConnection handler (backendConn b)
        where
          handler c = liftIO $ S.withTransaction c (updateAndGet c)
          updateAndGet c = S.executeNamed c updateJobActivityByID [":active" S.:= active, ":id" S.:= jid]
                           >> getJobByID b jid

instance MonadIO m => JobPostingRequestStore SqliteBackend m where
    addJobPostingRequest b req = maybe noConnection (`addJobPostingRequest_`req) (backendConn b)

    getAllJobPostingRequests b limit offset filters = maybe noConnection handle (backendConn b)
        where
          query = genSelectAllFromTableQuery jobPostingRequestsTableName
          -- convert filter clauses? filters = toFilterClauses <$> jprFilters
          (filteredQuery, params) = addFilterClauses query filters
          handle = doQueryAndPaginate_ limit offset (Just (DateTimeDESC jprRequestedAt)) filteredQuery (Just params)

    getJobPostingRequestByID b = getRowBySimpleField jobPostingRequestsTableName b jobPostingRequestIdFieldName

    updateJobPostingRequestByID b rid jpr = maybe noConnection handle (backendConn b)
        where
          handle c = updateDBEntity_ rid jpr c
                     >> getJobPostingRequestByID b rid

    -- Get the number of yet unapproved job posting requests
    getUnapprovedJobPostingRequests = maybe noConnection handler . backendConn
        where
          handler = liftIO . handle (genericQueryFailure "unapproved job posting requests") . runQuery
          runQuery = (toEntityCount <$>) . countRowsByField_ jobPostingRequestsTableName approvedJobIdFieldName (Nothing :: Maybe ForeignKey)

    -- Approve a job posting request for a given company, creating a new sale in the process
    approveJobPostingRequestByID b rid cid = getJobPostingRequestByID b rid
                                             >>= \case
                                                 Right req -> convertPostingRequestToJob_ b req cid
                                                 Left err -> return $ Left err

instance MonadIO m => URLBounceConfigStore SqliteBackend m where
    addURLBounceConfig b bounceCfg = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "add url bounce config") . runQuery
          runQuery c = getCurrentTime
                       >>= \now -> insertEntity_ insertURLBounceConfig bounceCfg { bounceCreatedAt=now } c

    findURLBounceConfigByName b = getRowBySimpleField urlBounceConfigsTableName b urlBounceConfigsNameFieldName

    findURLBounceConfigByID b cid = maybe noConnection handle (backendConn b)
        where
          handle = getRowBySimpleField_ urlBounceConfigsTableName genericIDField cid

    getAllURLBounceConfigs b limit offset = maybe noConnection handle (backendConn b)
        where
          query = genSelectAllFromTableQuery urlBounceConfigsTableName
          order = Just $ DateTimeDESC genericCreatedAt
          handle = doQueryAndPaginate_ limit offset order query Nothing

    saveURLBounceInfo b info = maybe noConnection (insertEntity_ insertURLBounceInfo info) (backendConn b)

    findURLBounceInfoForConfig b cid limit offset = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "find url bounce info for config") . runQuery
          order = Just $ DateTimeDESC genericCreatedAt
          runQuery = getRowsBySimpleField_ urlBounceInfoTableName urlBounceConfigFKName cid limit offset order

    getNumberHitsForBounceConfigByID b cid = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "bounce config hit check") . runQuery
          runQuery c = countRowsByField_ urlBounceInfoTableName urlBounceConfigFKName (Just cid) c
                       >>= throwIfLeft
                       >>= pure . Right . natural . toInteger

    setURLBounceConfigActivity b bIsActive bid = maybe noConnection handle (backendConn b)
        where
          handle = updateSimpleFieldAndReturnEntityOnSuccess_ urlBounceConfigsTableName bid genericIsActiveFieldName bIsActive

instance MonadIO m => CompanyStore SqliteBackend m where
    addCompany b company = maybe noConnection handle (backendConn b)
        where
          handle = addEntity_ companiesTableName insertCompany company genericIDField

    getAllCompanies b (Nothing :: Maybe Limit) (Nothing :: Maybe Offset) = maybe noConnection handle (backendConn b)
        where
          handle = doQueryAndPaginate_ Nothing Nothing Nothing (genSelectAllFromTableQuery companiesTableName) Nothing

    getAllCompanies b limit offset = maybe noConnection (doQueryAndPaginate_ limit offset Nothing query Nothing) (backendConn b)
        where
          query = genSelectAllFromTableQuery companiesTableName

    getCompanyByID b = getRowBySimpleField companiesTableName b genericIDField


    operateOnCompanyRepresentatives b op cid uid = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "company representative operation") . runQuery
          runQuery = (toEntityCount <$>) . modifyTupleTable_ companiesRepresentativesTableName op jprCompanyFKName cid userFKName uid

    getAllCompanyRepresentatives b cid = maybe noConnection handler (backendConn b)
        where
          fieldsToRetrieve = genericIDField:userFields
          handler = getTupleRelatedRows_ companiesRepresentativesTableName jprCompanyFKName cid userFKName usersTableName genericIDField fieldsToRetrieve

    getCompaniesRepresentedByUserWithID b uid = maybe noConnection handler (backendConn b)
        where
          fieldsToRetrieve = genericIDField:companyFields
          handler = getTupleRelatedRows_ companiesRepresentativesTableName userFKName uid jprCompanyFKName companiesTableName genericIDField fieldsToRetrieve

    getStatsForCompany b cid = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "stats gathering query") . runQuery
          runQuery c = (extractSingleNumber <$> S.queryNamed c activeJobCountForCompany [":cid" S.:= cid])
                     >>= \jobs -> extractSingleNumber <$> S.queryNamed c outclicksOnActiveJobsForCompany [":cid" S.:= cid]
                     >>= \clicks -> extractSingleNumber <$> S.queryNamed c timesActiveJobsFavoritedForCompany [":cid" S.:= cid]
                     >>= \favs -> return $ Right $ CompanyStats jobs clicks favs

instance MonadIO m => AddressStore SqliteBackend m where
    getAddressByID b = getRowBySimpleField addressesTableName b genericIDField


instance MonadIO m => SupportRequestStore SqliteBackend m where
    getAllSupportRequests b limit offset = maybe noConnection (doQueryAndPaginate_ limit offset Nothing query Nothing) (backendConn b)
        where
          query = genSelectAllFromTableQuery supportRequestsTableName

    addSupportRequest b supportRequest = maybe noConnection handle (backendConn b)
        where
          query = insertFieldsIntoTable supportRequestsTableName (entityInsertFields supportRequest)
          handle c = liftIO $ S.withTransaction c $ updateAndGet c
          updateAndGet c = S.execute c query supportRequest
                           >> S.lastInsertRowId c
                           >>= \rowId -> getRowBySimpleField_ supportRequestsTableName genericIDField (fromIntegral rowId :: Int) c

    getUnresolvedSupportRequestCount = maybe noConnection handle . backendConn
        where
          handle c = toEntityCount <$> countRowsByField_ supportRequestsTableName supportRequestsIsResolvedFieldName (Just False) c

instance MonadIO m => TagStore SqliteBackend m where
    addTag b tag = maybe noConnection handle (backendConn b)
        where
          query = insertFieldsIntoTable tagsTableName (entityInsertFields tag)
          handle c = logMsg b DEBUG ("Fn addTag query: "<> show query)
                     >> liftIO (S.withTransaction c (updateAndGet c))
          updateAndGet c = S.execute c query tag
                           >> S.lastInsertRowId c
                           >>= \rowId -> getRowBySimpleField_ tagsTableName genericIDField (fromIntegral rowId :: Int) c

    getAllTags b limit offset = maybe noConnection handle (backendConn b)
        where
          query = genSelectAllFromTableQuery tagsTableName
          handle = doQueryAndPaginate_ limit offset (Just (DateTimeDESC genericCreatedAt)) query Nothing

    findTagsByIDs b ids = maybe noConnection handler (backendConn b)
      where
        query = getEntitiesByIds tagsTableName (DateTimeDESC genericCreatedAt) ids
        runQuery c = Right . makePaginatedList <$> S.query_ c query
        handler = liftIO . handle (genericQueryFailure "find tag by ID") . runQuery

    getTagByID b = getRowBySimpleField tagsTableName b genericIDField

    updateTag b tid tag = maybe noConnection handle (backendConn b)
        where
          query = updateTagByID tid
          handle c = liftIO $ S.withTransaction c $ updateAndGet c
          updateAndGet c = S.execute c query tag
                           >> getTagByID b tid

    deleteTag b tagId = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "tag row delete") . runQuery
          runQuery = (toEntityCount <$>) . deleteRowBySimpleField_  tagsTableName genericIDField tagId

instance MonadIO m => TokenStore SqliteBackend m where
    createTokenForUser b uid tType = maybe noConnection handler (backendConn b)
        where
          handler c = makeToken uid tType
                      >>= flip insertEntityAndGet_ c

    getTokenByContent b token = maybe noConnection handle (backendConn b)
        where
          handle = getRowBySimpleField_ tokensTableName tokensTokenFieldName token

    removeTokenByID b tokenId = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "token row delete") . runQuery
          runQuery = (toEntityCount <$>) . deleteRowBySimpleField_  tokensTableName genericIDField tokenId

    removeTokensByTypeForCreatorWithID b tokenType creatorId = maybe noConnection handler (backendConn b)
        where

          params = ["creatorId" S.:= creatorId, "tokenType" S.:= tokenType]
          transaction c = S.executeNamed c deleteTokensOfTypeForCreator params
                          >> S.changes c
          runQuery c = toEntityCount . Right <$> S.withTransaction c (transaction c)
          handler = liftIO . handle (genericQueryFailure "token removal by type for creator") . runQuery

instance MonadIO m => EmailSubscriptionStore SqliteBackend m where
    saveEmailSubscription b email tagIds = maybe noConnection handler (backendConn b)
        where
          addTag c subId tagId = modifyTupleTable_ emailSubscriptionTagsTableName Add emailSubscriptionFKName subId tagFKName tagId c
          handler = liftIO . handleUnexpectedDBError . makeSub
          makeSub c = getCurrentTime
                      >>= \now -> sequence [UUID.nextRandom, UUID.nextRandom] -- Make two tokens for confirm & unsub
                      -- Create the email subscription
                      >>= \[t1, t2] -> insertEntityAndGet_ (EmailSubscription email (toText t1) Nothing (toText t2) Nothing now) c
                      >>= throwIfLeft
                      -- For every tag, ensure the relation exists between the email sub and the tag
                      >>= \sub -> sequence_ (addTag c (mId sub) <$> tagIds)
                      >> pure (Right sub)

    confirmEmailSubscription b code = maybe noConnection handler (backendConn b)
        where
          confirmAndGet c esId now = S.executeNamed c confirmEmailSubscriptionByID [":id" S.:= esId, ":now" S.:= now]
                                     >> getRowBySimpleField_ emailSubscriptionsTableName genericIDField esId c
          runQuery c = getRowBySimpleField_ emailSubscriptionsTableName esConfirmationCodeFieldName code c
                      >>= ifLeftThrow (FailedToFindEntity ("Email subscription with code " <> showText code))
                      >>= \(ModelWithID esId EmailSubscription{}) -> getCurrentTime
                      -- Update the confirmation date and the unsubdate
                      >>= S.withTransaction c . confirmAndGet c esId
          handler = liftIO . handle (genericQueryFailure "confirm email subscription") . runQuery

    cancelEmailSubscription b code = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handle (genericQueryFailure "cancel email subscription") . runQuery
          runQuery c = getRowBySimpleField_ emailSubscriptionsTableName esUnSubscribeCodeFieldName code c
                     >>= ifLeftThrow (FailedToFindEntity ("Email subscription with code " <> showText code))
                     >>= \(ModelWithID esId EmailSubscription{}) -> getCurrentTime
                     >>= \now -> updateSimpleFieldAndReturnEntityOnSuccess_ emailSubscriptionsTableName esId esUnSubscribeDateFieldName now c

    getAllEmailSubscriptions b limit offset = maybe noConnection handle (backendConn b)
        where
          query = genSelectAllFromTableQuery emailSubscriptionsTableName
          handle = doQueryAndPaginate_ limit offset (Just (DateTimeDESC esSubscribeDateFieldName)) query Nothing

    getEmailSubscribers b limit offset = maybe noConnection handle (backendConn b)
        where
          handle = doQueryAndPaginate_ limit offset (Just (DateTimeDESC esSubscribeDateFieldName)) selectEmailSubscribers Nothing

    getEmailSubscriptionByEmail b email = maybe noConnection (getRowBySimpleField_ emailSubscriptionsTableName userEmailFieldName email) (backendConn b)

    getEmailSubscriptionByID b = getRowBySimpleField emailSubscriptionsTableName b genericIDField

    getTagsForEmailSubscription b sid = maybe noConnection handler (backendConn b)
        where
          fieldsToRetrieve = genericIDField : tagFields
          runQuery c = getTupleRelatedRows_ emailSubscriptionTagsTableName emailSubscriptionFKName sid tagFKName tagsTableName genericIDField fieldsToRetrieve c
                       >>= ifLeftThrow (FailedToFindEntities ("Tags with email subscription id " <> showText sid))
                       >>= pure . Right
          handler = liftIO . handle (genericQueryFailure "get tags for email subscription") . runQuery

instance MonadIO m => PromotionStore SqliteBackend m where
    addPromotion b pr = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handleUnexpectedDBError . doInsert
          doInsert c = insertEntityAndGet_ pr c
                       -- rethrow a more helpful error
                       >>= ifLeftThrow (FailedToCreateEntity ("Promotion with name " <> showText (prName pr)))
                       >>= pure . Right

makeTask :: MonadIO m => TaskType -> TaskData -> Maybe UserID -> m Task
makeTask tt td schedulerId = liftIO getCurrentTime
                             >>= \now -> pure Task { taskType=tt
                                                   , taskStatus=NotRunning
                                                   , taskJsonData=td
                                                   , taskScheduledBy=schedulerId
                                                   , taskKilledBy=Nothing
                                                   , taskRunAt=Nothing
                                                   , taskCreatedAt=now
                                                   , taskError=Nothing
                                                   }

instance MonadIO m => TaskStore SqliteBackend m where
    addTask :: SqliteBackend -> TaskType -> TaskData -> Maybe UserID -> m (Either DBError (ModelWithID Task))
    addTask b taskType taskData maybeScheduler = maybe noConnection handler (backendConn b)
        where
          handler = liftIO . handleUnexpectedDBError . doInsert
          doInsert c = makeTask taskType taskData maybeScheduler
                       >>= flip insertEntityAndGet_ c
                       -- rethrow a more helpful error
                       >>= ifLeftThrow (FailedToCreateEntity ("Task with type " <> showText taskType <> " by user [" <> showText maybeScheduler <> "]"))
                       >>= pure . Right

    getTaskByID b = getRowBySimpleField tasksTableName b genericIDField

    getAllTasks b limit offset = maybe noConnection handle (backendConn b)
        where
          query = genSelectAllFromTableQuery tasksTableName
          handle = doQueryAndPaginate_ limit offset (Just (DateTimeDESC genericCreatedAt)) query Nothing

instance MonadIO m => TaskManager SqliteBackend m where
    updateTaskStatusByID b tid newStatus = maybe noConnection handle (backendConn b)
        where
          handle = updateSimpleFieldAndReturnEntityOnSuccess_ tasksTableName tid genericStatusField newStatus

    recordTaskStartedByID b tid = maybe noConnection handle (backendConn b)
        where
          handle c = liftIO getCurrentTime
                     >>= \now -> updateSimpleField_ tasksTableName tid taskRunAtFieldName now c
                     >> updateSimpleField_ tasksTableName tid genericStatusField Running c
                     >> pure (Right ())

    updateTaskErrorByID b tid maybeErrStr = maybe noConnection handle (backendConn b)
        where
          handle = updateSimpleFieldAndReturnEntityOnSuccess_ tasksTableName tid tasksErrorFieldName maybeErrStr


instance MonadIO m => AnalyticsStore SqliteBackend m where
    getJobsByBounceRate b limit offset = maybe noConnection handle (backendConn b)
        where
          query = genJobsByBounceRateQuery
          handle = doQueryAndPaginate_ limit offset (Just (DESC bounceRateField)) query Nothing

    getBouncesBinnedByTimePeriod b binType start end = maybe noConnection handle (backendConn b)
        where
          (query, params) = genURLBounceInfoWithinRangeQuery start end

          getBouncesForPeriod :: S.Connection -> m [ModelWithID URLBounceInfo]
          getBouncesForPeriod c = liftIO $ S.queryNamed c query params

          handle c = getBouncesForPeriod c
                     >>= pure . Right . analyticsBucketCountsBy binType

-- ^ Define the logger instances for backends
instance HasLogger SqliteBackend where
    getComponentLogger = backendLogger
