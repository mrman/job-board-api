{-# LANGUAGE OverloadedStrings      #-}

module Config ( AppConfig(..)
              , AppEnvironment(..)
              , BackendConfig(..)
              , BrandingConfig(..)
              , CacheConfig(..)
              , CookieConfig(..)
              , TaskQueueConfig(..)
              , LoggerConfig(..)
              , MailerBackendType(..)
              , MailerConfig(..)
              , MetricsConfig(..)
              , SearchConfig(..)
              , TracingBackendType(..)
              , TracingConfig(..)
              , UserContentStoreConfig(..)
              , defaultDevConfig
              , defaultProdConfig
              , defaultTestConfig
              , getCurrentEnvConfig
              , getDefaultOrEnvValue
              , makeAuthCookieWithSettings
              , makeInstantExpireCookieWithSettings
              , oneSecondMs
              , replaceDBPaths
              ) where

import           Control.Exception (Exception, SomeException, handle, try, throw)
import           Data.List (intercalate)
import           Data.Maybe (fromJust, fromMaybe)
import           Data.Time.Clock (secondsToDiffTime)
import           Network.Socket (HostName)
import           Network.Socket.Internal (PortNumber)
import           Network.Wai.Handler.Warp (Port)
import           System.Environment (lookupEnv)
import           System.IO (FilePath)
import           System.Log.Logger (Priority(..))
import qualified Data.ByteString.Char8 as BSC
import qualified Data.DateTime as DT
import qualified Data.Map as M
import qualified Data.Text as DT
import qualified Network.Mail.SMTP as SMTP
import qualified Web.Cookie as WC

throwIfLeft :: (Exception l, Monad m) => Either l r -> m r
throwIfLeft (Left err) = throw err
throwIfLeft (Right v) = pure v

dbConfigVars :: [String]
dbConfigVars = [ "DB_NAME"             -- Name of the database to use
               , "DB_ADDR"             -- Database address (in case of SQLITE, it is a file)
               , "DB_USER"             -- Database user (unused if SQLITE)
               , "DB_PASSWORD"         -- Database user password (unused if SQLITE)
               , "DB_LOG_LEVEL"        -- Log level for the database
               , "DB_TRACE_QUERIES"    -- Trace all queries
               , "DB_VERSION"          -- Expected database version (migrations will happen to make this true)
               , "DB_MIGRATION_FOLDER" -- Where to find Backend migration files
               ]

appConfigVars :: [String]
appConfigVars = [ "PORT"                  -- Port on which to run the app
                , "ENVIRONMENT"           -- Environments on which the app runs (ex. Development, Production, Test)
                , "LOG_LEVEL"             -- Global log level for the app
                , "FRONTEND_FOLDER"       -- Directory containing the frontend HTML/JS code for the site
                , "BACKEND_EXTERNAL_URL"  -- External URL from which the API can be accessed
                , "FRONTEND_EXTERNAL_URL" -- External URL from which the admin app can be accessed
                , "ADMIN_EXTERNAL_URL"    -- External URL from which the frontend can be accessed
                , "ROOT_USER_EMAIL"       -- Email of the root user that will be auto-created if not present
                , "ROOT_USER_PASSWORD"    -- Password of the root user that will be auto-created if not present
                ]

cookieConfigVars :: [String]
cookieConfigVars = [ "COOKIE_HTTP_ONLY"        -- Whether the cookie is HTTP only
                   , "COOKIE_MAX_AGE_SECONDS"  -- Cookie max age in seconds
                   , "COOKIE_SECURE"           -- Whether to use a secure cookie
                   , "COOKIE_DOMAIN"           -- Cookie Domain
                   , "COOKIE_PATH"             -- Cookie Path
                   , "COOKIE_SECRET_FILE_PATH"  -- Cookie Path
                   ]

mailerConfigVars :: [String]
mailerConfigVars = [ "MAILER_BACKEND_TYPE"     -- MailerType to use
                   , "MAILER_HOSTNAME"         -- Hostname to use when sending mail
                   , "MAILER_PORT_NUMBER"      -- Port number on which to access SMTP
                   , "MAILER_USERNAME"         -- Username to use when sending mail
                   , "MAILER_PASSWORD"         -- Password to use when sending mail
                   , "MAILER_LOG_LEVEL"        -- Log level for the mailer (overrides)
                   , "MAILER_TEMPLATES_FOLDER" -- Path to the templates folder for template emails
                   , "MAILER_FROM_ADDR"        -- Address to use for the "from" of emails sent by API
                   ]

loggerConfigVars :: [String]
loggerConfigVars = [ "LOGGER_EMAIL_PRIORITY"  -- Priority at which to send log emails
                   , "LOGGER_EMAIL_TO_ADDR"   -- Address to which to send log emails
                   , "LOGGER_EMAIL_FROM_ADDR" -- Address from which to send log emails
                   ]

ucsConfigVars :: [String]
ucsConfigVars = [ "UCS_SCHEME"                -- Scheme to use for locally stored user content storage (ex. "local:", "s3:")
                , "UCS_PUBLIC_SCHEME"         -- Scheme to use for public URLs
                , "UCS_LOCAL_BASE_DIR"        -- Folder that will be used for local files (only used if UCS_SCHEME is "local:")
                , "UCS_LOCAL_PUBLIC_BASE_URL" -- Base (possibly relative) URL for images (ex. "https://jobs.example.com/", "/")
                , "UCS_LOG_LEVEL"             -- Log level for the user content storage backend
                ]

searchConfigVars :: [String]
searchConfigVars = [ "SEARCH_ADDR"               -- Address of search server (can also be local file)
                   , "SEARCH_LOG_LEVEL"          -- Log level for the search backend
                   , "SEARCH_TRACE_QUERIES"      -- Trace all queries
                   , "SEARCH_FTS_MIGRATION_FILE" -- Path to migration file search db (should only be one
                   ]

cacheConfigVars :: [String]
cacheConfigVars = [ "CACHE_LOG_LEVEL"                     -- Log level for the cache backend
                  , "CACHE_DEFAULT_TIMED_INVALIDATION_MS" -- Default amount of milliseconds to wait before invalidating timed caches
                  ]

metricsConfigVars :: [String]
metricsConfigVars = [ "METRICS_LOG_LEVEL"                   -- Log level for the metrics backend
                    , "METRICS_INSTRUMENT_METRICS_ENDPOINT" -- Whether to include the metrics endpoint itself in instrumentation
                    , "METRICS_INSTRUMENT_APP"              -- Add app-wide metrics (default: true)
                    ]

tracingConfigVars :: [String]
tracingConfigVars = [ "TRACING_LOG_LEVEL"                   -- Log level for the tracing backend
                    , "TRACING_BACKEND_TYPE"                -- Type of tracing backend in use (i.e. "Local" or "Zipkin")
                    , "TRACING_SERVICE_TAGS"                -- Comma separated list of tags to use for this service
                    , "TRACING_SAMPLING_RATE"               -- Double from 0.0 to 1.0 that represents how much to sample
                    , "TRACING_SERVICE_NAME"                -- Service name
                    , "TRACING_INSTRUMENT_APP"              -- Add app-wide tracing per top level route (default: true)
                    , "TRACING_AGENT_UDP_ADDR"              -- Host of the tracing agent to contact over UDP
                    , "TRACING_AGENT_UDP_PORT"              -- Port of the tracing agent to contact over UDP
                    , "TRACING_AGENT_HTTP_ADDR"             -- Host of the tracing agent to contact over HTTP
                    , "TRACING_AGENT_HTTP_PORT"             -- Port of the tracing agent to contact over HTTP
                    ]


brandingConfigVars :: [String]
brandingConfigVars = [ "BRANDING_NAME"            -- Name of the service this API is being used in, for any branding (ex. in email templates)
                     , "BRANDING_SUBSCRIBE_URL"   -- Subscribe link that's used for registration emails (token is appended as a query param)
                     , "BRANDING_UNSUBSCRIBE_URL" -- Subscribe link that's used for registration emails (token is appended as a query param)
                     ]

taskQueueConfigVars :: [String]
taskQueueConfigVars = [ "TASK_QUEUE_BACKEND_TYPE"   -- Type of task queue backend to use (i.e. "LocalSqlite")
                      , "TASK_QUEUE_LOG_LEVEL"      -- Log level for task queue
                      ]

data TracingBackendType = LocalTracingBackend
                        | Jaeger
                        | Zipkin deriving (Eq, Show, Read)

data MailerBackendType = LocalMailerBackend
                       | SMTP deriving (Eq, Show, Read)

data TaskQueueBackendType = LocalSqlite deriving (Eq, Show, Read)

data AppEnvironment = Production
                    | Development
                    | Test deriving (Ord, Eq, Enum, Show, Read)

data BackendConfig = BackendConfig { dbName           :: String
                                   , dbAddr           :: String
                                   , dbUser           :: String
                                   , dbPass           :: String
                                   , dbLogLevel       :: Priority
                                   , dbTraceQueries   :: Bool
                                   , dbVersion        :: Int
                                   , dbMigrationsPath :: FilePath
                                   } deriving (Eq)

data CookieConfig = CookieConfig { cookieHttpOnly       :: Bool
                                 , cookieMaxAgeSeconds  :: Integer
                                 , cookieIsSecure       :: Bool
                                 , cookieDomain         :: String
                                 , cookiePath           :: String
                                 , cookieSecretFilePath :: FilePath
                                 } deriving (Eq)

data MailerConfig = MailerConfig { mailHostname      :: HostName
                                 , mailPortNumber    :: PortNumber
                                 , mailUsername      :: SMTP.UserName
                                 , mailPassword      :: SMTP.Password
                                 , mailLogLevel      :: Priority
                                 , mailTemplatesPath :: FilePath
                                 , mailFromAddress   :: String
                                 , mailBackendType   :: MailerBackendType
                                 } deriving (Eq)

data LoggerConfig = LoggerConfig { loggerEmailPriority :: Priority
                                 , loggerEmailToAddr   :: String
                                 , loggerEmailFromAddr :: String
                                 } deriving (Eq)

data UserContentStoreConfig = UserContentStoreConfig { ucsScheme             :: String
                                                     , ucsPublicScheme       :: String
                                                     , ucsLocalBaseDir       :: String
                                                     , ucsLocalPublicBaseURL :: String
                                                     , ucsLogLevel           :: Priority
                                                     } deriving (Eq)

data SearchConfig = SearchConfig { searchAddr             :: String
                                 , searchLogLevel         :: Priority
                                 , searchTraceQueries     :: Bool
                                 , searchFTSMigrationFile :: FilePath
                                 } deriving (Eq)

data CacheConfig = CacheConfig { cacheLogLevel                   :: Priority
                               , cacheDefaultTimedInvalidationMs :: Int
                               } deriving (Eq)

data MetricsConfig = MetricsConfig { metricsLogLevel               :: Priority
                                   , metricsEndpointURLs           :: [DT.Text]
                                   , metricsAppWide                :: Bool
                                   , metricsIncludeMetricsEndpoint :: Bool
                                   } deriving (Eq)

data TracingConfig = TracingConfig { tracingLogLevel               :: Priority
                                   , tracingBackendType            :: TracingBackendType
                                   , tracingServiceTags            :: [DT.Text]
                                   , tracingSamplingRate           :: Double
                                   , tracingServiceName            :: DT.Text
                                   , tracingAppWide                :: Bool
                                   , tracingAgentUDPAddr           :: DT.Text
                                   , tracingAgentUDPPort           :: Int
                                   , tracingAgentHTTPAddr          :: DT.Text
                                   , tracingAgentHTTPPort          :: Int
                                   } deriving (Eq)

data BrandingConfig = BrandingConfig { brandingName :: String
                                     , brandingSubscribeURL :: String
                                     , brandingUnsubscribeURL :: String
                                     } deriving (Eq)

data TaskQueueConfig = TaskQueueConfig { taskQueueBackendType :: TaskQueueBackendType
                                       , taskQueueLogLevel    :: Priority
                                       } deriving (Eq)

data AppConfig = AppConfig { appPort                :: Port
                           , appEnvironment         :: AppEnvironment
                           , appLogLevel            :: Priority
                           , appFrontendFolder      :: FilePath
                           , appBackendExternalURL  :: String
                           , appAdminExternalURL    :: String
                           , appFrontendExternalURL :: String
                           , appRootUserEmail       :: String
                           , appRootUserPassword    :: String

                           -- Config for app components
                           , appBackendConfig  :: BackendConfig
                           , appCookieConfig   :: CookieConfig
                           , appMailerConfig   :: MailerConfig
                           , appLoggerConfig   :: LoggerConfig
                           , appUCSConfig      :: UserContentStoreConfig
                           , appSearchConfig   :: SearchConfig
                           , appCacheConfig    :: CacheConfig
                           , appMetricsConfig  :: MetricsConfig
                           , appTracingConfig  :: TracingConfig
                           , appBrandingConfig :: BrandingConfig
                           , appTaskQueueConfig :: TaskQueueConfig
                           } deriving (Eq)

configLinePadAmount :: Int
configLinePadAmount = 40

fiveMinutesMs :: Int
fiveMinutesMs = 60000000

oneSecondMs :: Int
oneSecondMs = 1000000

makeConfigLines :: [String] -> [String] -> [String]
makeConfigLines = zipWith (\name val -> dotted name ++ " " ++ val)
    where
      dotted c = c ++ ((`replicate`'.') . (configLinePadAmount-) . length) c

instance Show BackendConfig where
    show cfg = "BackendConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines dbConfigVars vals)
        where
          vals = map (\f -> f cfg) [ dbName
                                   , show . dbAddr
                                   , dbUser
                                   , dbPass
                                   , show . dbLogLevel
                                   , show . dbTraceQueries
                                   , show . dbVersion
                                   , show . dbMigrationsPath
                                   ]

instance Show CookieConfig where
    show cfg = "CookieConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines cookieConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . cookieHttpOnly
                                   , show . cookieMaxAgeSeconds
                                   , show . cookieIsSecure
                                   , cookieDomain
                                   , cookiePath
                                   , cookieSecretFilePath
                                   ]

instance Show MailerConfig where
    show cfg = "MailerConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines mailerConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . mailBackendType
                                   , mailHostname
                                   , show . mailPortNumber
                                   , mailUsername
                                   , mailPassword
                                   , show . mailLogLevel
                                   , show . mailTemplatesPath
                                   , mailFromAddress
                                   ]

instance Show LoggerConfig where
    show cfg = "LoggerConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines loggerConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . loggerEmailPriority
                                   , loggerEmailToAddr
                                   , loggerEmailFromAddr
                                   ]

instance Show UserContentStoreConfig where
    show cfg = "UserContentStoreConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines ucsConfigVars vals)
        where
          vals = map (\f -> f cfg) [ ucsScheme
                                   , ucsPublicScheme
                                   , ucsLocalBaseDir
                                   , ucsLocalPublicBaseURL
                                   , show . ucsLogLevel
                                   ]

instance Show SearchConfig where
    show cfg = "SearchConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines searchConfigVars vals)
        where
          vals = map (\f -> f cfg) [ searchAddr
                                   , show . searchLogLevel
                                   , show . searchTraceQueries
                                   , searchFTSMigrationFile
                                   ]

instance Show CacheConfig where
    show cfg = "CacheConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines cacheConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . cacheLogLevel
                                   , show . cacheDefaultTimedInvalidationMs
                                   ]

instance Show MetricsConfig where
    show cfg = "MetricsConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines metricsConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . metricsLogLevel
                                   , show . metricsEndpointURLs
                                   , show . metricsAppWide
                                   , show . metricsIncludeMetricsEndpoint
                                   ]

instance Show TracingConfig where
    show cfg = "TracingConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines tracingConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . tracingLogLevel
                                   , show . tracingBackendType
                                   , show . tracingServiceTags
                                   , show . tracingSamplingRate
                                   , DT.unpack . tracingServiceName
                                   , show . tracingAppWide
                                   , show . tracingAgentUDPAddr
                                   , show . tracingAgentUDPPort
                                   , DT.unpack . tracingAgentHTTPAddr
                                   , show . tracingAgentHTTPPort
                                   ]

instance Show BrandingConfig where
    show cfg = "BrandingConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines brandingConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . brandingName
                                   , brandingSubscribeURL
                                   , brandingUnsubscribeURL
                                   ]

instance Show TaskQueueConfig where
    show cfg = "TaskQueueConfig:\n\t" ++ intercalate "\n\t" (makeConfigLines taskQueueConfigVars vals)
        where
          vals = map (\f -> f cfg) [ show . taskQueueBackendType
                                   , show . taskQueueLogLevel
                                   ]

instance Show AppConfig where
    show cfg = "AppConfig:\n\t" ++ appConfigLines
               ++ "\n" ++ show (appBackendConfig cfg)
               ++ "\n" ++ show (appCookieConfig cfg)
               ++ "\n" ++ show (appMailerConfig cfg)
               ++ "\n" ++ show (appLoggerConfig cfg)
               ++ "\n" ++ show (appUCSConfig cfg)
               ++ "\n" ++ show (appSearchConfig cfg)
               ++ "\n" ++ show (appCacheConfig cfg)
               ++ "\n" ++ show (appMetricsConfig cfg)
               ++ "\n" ++ show (appTracingConfig cfg)
               ++ "\n" ++ show (appBrandingConfig cfg)
               ++ "\n" ++ show (appTaskQueueConfig cfg)
        where
          appConfigLines = intercalate "\n\t" (makeConfigLines appConfigVars vals)
          vals = map (\f -> f cfg) [ show . appPort
                                   , show . appEnvironment
                                   , show . appLogLevel
                                   , appFrontendFolder
                                   , appBackendExternalURL
                                   , appAdminExternalURL
                                   , appFrontendExternalURL
                                   , appRootUserEmail
                                   , appRootUserPassword
                                   ]

getDefaultOrEnvValue :: String -> String -> IO String
getDefaultOrEnvValue defaultValue = (fromMaybe defaultValue <$>) . lookupEnv

readEnvValueFromGetter :: Read a => String -> (String -> IO String) -> IO a
readEnvValueFromGetter envVarName getter = getter envVarName >>= carefulRead
    where
      doError = const $ error $ "Failed to read value for ENV " ++ envVarName
      carefulRead v = (try (pure (read v)) :: Read a => IO (Either SomeException a))
                      >>= either doError pure

defaultTestConfig :: AppConfig
defaultTestConfig = AppConfig { appPort=5001
                              , appEnvironment=Development
                              , appLogLevel=ERROR
                              , appFrontendFolder="../web"
                              , appBackendExternalURL="localhost:5001"
                              , appAdminExternalURL="localhost:5002"
                              , appFrontendExternalURL="localhost:5003"
                              , appRootUserEmail="root@example.com"
                              , appRootUserPassword="root"

                              , appBackendConfig=BackendConfig { dbName="job_board"
                                                               , dbAddr=":memory:"
                                                               , dbUser=""
                                                               , dbPass=""
                                                               , dbLogLevel=ERROR
                                                               , dbTraceQueries=False
                                                               , dbVersion=20
                                                               , dbMigrationsPath="infra/db/sql/migrations"
                                                               }
                              , appCookieConfig=CookieConfig { cookieHttpOnly=False
                                                             , cookieMaxAgeSeconds=36000000
                                                             , cookieIsSecure=False
                                                             , cookieDomain=""
                                                             , cookiePath=""
                                                             , cookieSecretFilePath="./infra/runtime/cookie-config/cookie-key.aes"
                                                             }
                              , appMailerConfig=MailerConfig { mailBackendType=LocalMailerBackend
                                                             , mailHostname=""
                                                             , mailPortNumber=0
                                                             , mailUsername=""
                                                             , mailPassword=""
                                                             , mailLogLevel=ERROR
                                                             , mailTemplatesPath="infra/email/templates"
                                                             , mailFromAddress="noreply@example.com"
                                                             }
                              , appLoggerConfig=LoggerConfig { loggerEmailPriority=WARNING
                                                             , loggerEmailToAddr=""
                                                             , loggerEmailFromAddr=""
                                                             }
                              , appUCSConfig=UserContentStoreConfig { ucsScheme="local:"
                                                                    , ucsPublicScheme="file:"
                                                                    , ucsLocalBaseDir="infra/ucs/runtime"
                                                                    , ucsLocalPublicBaseURL="//localhost:5001/ucs"
                                                                    , ucsLogLevel=ERROR
                                                                    }
                              , appSearchConfig=SearchConfig { searchAddr=":memory:"
                                                             , searchLogLevel=ERROR
                                                             , searchTraceQueries=False
                                                             , searchFTSMigrationFile="infra/db/sql/search-migrations/0.sql"
                                                             }
                              , appCacheConfig=CacheConfig { cacheLogLevel=ERROR
                                                           , cacheDefaultTimedInvalidationMs=oneSecondMs
                                                           }
                              , appMetricsConfig=MetricsConfig { metricsLogLevel=ERROR
                                                               , metricsEndpointURLs=["metrics"]
                                                               , metricsAppWide=True
                                                               , metricsIncludeMetricsEndpoint=True
                                                               }
                              , appTracingConfig=TracingConfig { tracingLogLevel=ERROR
                                                               , tracingBackendType=LocalTracingBackend
                                                               , tracingServiceTags=["api"]
                                                               , tracingSamplingRate=1.0
                                                               , tracingServiceName="job-board"
                                                               , tracingAppWide=True
                                                               , tracingAgentUDPAddr="127.0.0.1"
                                                               , tracingAgentUDPPort=6831
                                                               , tracingAgentHTTPAddr="127.0.0.1"
                                                               , tracingAgentHTTPPort=9411
                                                               }
                              , appBrandingConfig=BrandingConfig { brandingName="job-board-api"
                                                                 , brandingSubscribeURL="http://localhost:5003/#/mailing-list"
                                                                 , brandingUnsubscribeURL="http://localhost:5003/#/mailing-list"
                                                                 }
                              , appTaskQueueConfig=TaskQueueConfig { taskQueueBackendType=LocalSqlite
                                                                   , taskQueueLogLevel=ERROR
                                                                   }
                              }

defaultDevConfig :: AppConfig
defaultDevConfig = AppConfig { appPort=5001
                             , appEnvironment=Development
                             , appLogLevel=DEBUG
                             , appFrontendFolder="../web"
                             , appBackendExternalURL="localhost:5001"
                             , appAdminExternalURL="localhost:5002"
                             , appFrontendExternalURL="localhost:5003"
                             , appRootUserEmail="root@example.com"
                             , appRootUserPassword="root"

                             , appBackendConfig=BackendConfig { dbName="job_board"
                                                              , dbAddr="infra/runtime/db/db.sqlite"
                                                              , dbUser=""
                                                              , dbPass=""
                                                              , dbLogLevel=DEBUG
                                                              , dbTraceQueries=False
                                                              , dbVersion=20
                                                              , dbMigrationsPath="infra/db/sql/migrations"
                                                              }
                             , appCookieConfig=CookieConfig { cookieHttpOnly=True
                                                            , cookieMaxAgeSeconds=36000000
                                                            , cookieIsSecure=False
                                                            , cookieDomain=""
                                                            , cookiePath=""
                                                            , cookieSecretFilePath="./infra/runtime/cookie-config/cookie-key.aes"
                                                            }
                             , appMailerConfig=MailerConfig { mailBackendType=LocalMailerBackend
                                                            , mailHostname=""
                                                            , mailPortNumber=0
                                                            , mailUsername=""
                                                            , mailPassword=""
                                                            , mailLogLevel=DEBUG
                                                            , mailTemplatesPath="infra/email/templates"
                                                            , mailFromAddress="noreply@example.com"
                                                            }
                             , appLoggerConfig=LoggerConfig { loggerEmailPriority=WARNING
                                                             , loggerEmailToAddr=""
                                                             , loggerEmailFromAddr=""
                                                            }
                             , appUCSConfig=UserContentStoreConfig { ucsScheme="local:"
                                                                   , ucsPublicScheme="file:" -- no scheme, so frontend URLs go to "/whatever"
                                                                   , ucsLocalBaseDir="infra/ucs/runtime"
                                                                   , ucsLocalPublicBaseURL="//localhost:5001/ucs"
                                                                   , ucsLogLevel=INFO
                                                                   }
                             , appSearchConfig=SearchConfig { searchAddr="infra/runtime/db/db.sqlite"
                                                            , searchLogLevel=DEBUG
                                                            , searchTraceQueries=False
                                                            , searchFTSMigrationFile="infra/db/sql/search-migrations/0.sql"
                                                            }
                             , appCacheConfig=CacheConfig { cacheLogLevel=DEBUG
                                                           , cacheDefaultTimedInvalidationMs=fiveMinutesMs
                                                          }
                              , appMetricsConfig=MetricsConfig { metricsLogLevel=DEBUG
                                                               , metricsEndpointURLs=["metrics"]
                                                               , metricsAppWide=False
                                                               , metricsIncludeMetricsEndpoint=True
                                                               }
                              , appTracingConfig=TracingConfig { tracingLogLevel=DEBUG
                                                               , tracingBackendType=Zipkin
                                                               , tracingServiceTags=["api"]
                                                               , tracingSamplingRate=1.0
                                                               , tracingServiceName="job-board"
                                                               , tracingAppWide=False
                                                               , tracingAgentUDPAddr="127.0.0.1"
                                                               , tracingAgentUDPPort=6831
                                                               , tracingAgentHTTPAddr="127.0.0.1"
                                                               , tracingAgentHTTPPort=9411
                                                               }
                              , appBrandingConfig=BrandingConfig { brandingName="job-board-api"
                                                                 , brandingSubscribeURL="http://localhost:5003/#/mailing-list"
                                                                 , brandingUnsubscribeURL="http//localhost:5003/#/mailing-list"
                                                                 }
                              , appTaskQueueConfig=TaskQueueConfig { taskQueueBackendType=LocalSqlite
                                                                   , taskQueueLogLevel=INFO
                                                                   }
                             }

defaultProdConfig :: AppConfig
defaultProdConfig = AppConfig { appPort=5001
                              , appEnvironment=Production
                              , appLogLevel=INFO
                              , appFrontendFolder="../web/dist"
                              , appBackendExternalURL="api.example.com"
                              , appAdminExternalURL="admin.example.com"
                              , appFrontendExternalURL="example.com"
                              , appRootUserEmail="root@example.com"
                              , appRootUserPassword="root"

                              , appBackendConfig=BackendConfig { dbName="job_board"
                                                               , dbAddr="/var/app/job-board/data/db.sqlite"
                                                               , dbUser=""
                                                               , dbPass=""
                                                               , dbLogLevel=INFO
                                                               , dbTraceQueries=False
                                                               , dbVersion=20
                                                               , dbMigrationsPath="/var/app/job-board/data/migrations"
                                                               }
                              , appCookieConfig=CookieConfig { cookieHttpOnly=True
                                                             , cookieMaxAgeSeconds=36000000
                                                             , cookieIsSecure=True
                                                             , cookieDomain="jobs.example.com"
                                                             , cookiePath="/"
                                                             , cookieSecretFilePath="/var/app/job-board/data/cookie-config/cookie-key.aes"
                                                             }
                              , appMailerConfig=MailerConfig { mailBackendType=LocalMailerBackend
                                                             , mailHostname=""
                                                             , mailPortNumber=0
                                                             , mailUsername=""
                                                             , mailPassword=""
                                                             , mailLogLevel=INFO
                                                             , mailTemplatesPath="/var/app/job-board/data/email/templates"
                                                             , mailFromAddress="noreply@example.com"
                                                             }
                              , appLoggerConfig=LoggerConfig { loggerEmailPriority=WARNING
                                                             , loggerEmailToAddr="support+logs@example.com"
                                                             , loggerEmailFromAddr="support@example.com"
                                                             }
                              , appUCSConfig=UserContentStoreConfig { ucsScheme="local:"
                                                                    , ucsPublicScheme="https:"
                                                                    , ucsLocalBaseDir="/var/app/job-board/data/user-generated-content"
                                                                    , ucsLocalPublicBaseURL="//jobs.example.com/ucs"
                                                                    , ucsLogLevel=INFO
                                                                    }
                              , appSearchConfig=SearchConfig { searchAddr="/var/app/job-board/data/db.sqlite"
                                                             , searchLogLevel=INFO
                                                             , searchTraceQueries=False
                                                             , searchFTSMigrationFile="/var/app/job-board/data/search-migrations/0.sql"
                                                             }
                              , appCacheConfig=CacheConfig { cacheLogLevel=INFO
                                                           , cacheDefaultTimedInvalidationMs=fiveMinutesMs
                                                           }
                              , appMetricsConfig=MetricsConfig { metricsLogLevel=INFO
                                                               , metricsEndpointURLs=["metrics"]
                                                               , metricsAppWide=True
                                                               , metricsIncludeMetricsEndpoint=True
                                                               }
                              , appTracingConfig=TracingConfig { tracingLogLevel=INFO
                                                               , tracingBackendType=Zipkin
                                                               , tracingServiceTags=["api"]
                                                               , tracingSamplingRate=0.5
                                                               , tracingServiceName="job-board"
                                                               , tracingAppWide=True
                                                               , tracingAgentUDPAddr="127.0.0.1"
                                                               , tracingAgentUDPPort=6831
                                                               , tracingAgentHTTPAddr="127.0.0.1"
                                                               , tracingAgentHTTPPort=9411
                                                               }
                              , appBrandingConfig=BrandingConfig { brandingName="job-board-api"
                                                                 , brandingSubscribeURL="example.com/email/subscribe"
                                                                 , brandingUnsubscribeURL="example.com/email/unsubscribe"
                                                                 }
                              , appTaskQueueConfig=TaskQueueConfig { taskQueueBackendType=LocalSqlite
                                                                   , taskQueueLogLevel=INFO
                                                                   }
                              }

defaultConfigs :: M.Map AppEnvironment AppConfig
defaultConfigs = M.fromList [(Test, defaultTestConfig), (Development, defaultDevConfig), (Production, defaultProdConfig)]

parseCommaSeparatedOptionalList :: DT.Text -> [DT.Text]
parseCommaSeparatedOptionalList s
    | DT.null s = []
    | otherwise = maybe [] (DT.splitOn ",") (DT.stripPrefix "Just " s)

class OverridableFromEnv a where
    overrideWithEnv :: a -> IO (Either SomeException a)

instance OverridableFromEnv BackendConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            name           <- getDefaultOrEnvValue (dbName cfg)                   "DB_NAME"
            addr           <- getDefaultOrEnvValue (dbAddr cfg)                   "DB_ADDR"
            user           <- getDefaultOrEnvValue (dbUser cfg)                   "DB_USER"
            password       <- getDefaultOrEnvValue (dbPass cfg)                   "DB_PASSWORD"
            migrationsPath <- getDefaultOrEnvValue (dbMigrationsPath cfg)         "DB_MIGRATION_FOLDER"

            logLevel       <- readEnvValueFromGetter "DB_LOG_LEVEL"     $ getDefaultOrEnvValue ((show . dbLogLevel) cfg)
            traceQueries   <- readEnvValueFromGetter "DB_TRACE_QUERIES" $ getDefaultOrEnvValue ((show . dbTraceQueries) cfg)

            return $ cfg { dbName=name
                         , dbAddr=addr
                         , dbUser=user
                         , dbPass=password
                         , dbMigrationsPath=migrationsPath
                         , dbLogLevel=logLevel
                         , dbTraceQueries=traceQueries
                         }

instance OverridableFromEnv MailerConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            hostname       <- getDefaultOrEnvValue (mailHostname cfg)      "MAILER_HOSTNAME"
            user           <- getDefaultOrEnvValue (mailUsername cfg)      "MAILER_USER"
            password       <- getDefaultOrEnvValue (mailPassword cfg)      "MAILER_PASSWORD"
            fromAddr       <- getDefaultOrEnvValue (mailFromAddress cfg)   "MAILER_FROM_ADDR"
            templatesPath  <- getDefaultOrEnvValue (mailTemplatesPath cfg) "MAILER_TEMPLATES_FOLDER"

            portNumber     <- readEnvValueFromGetter "MAILER_PORT_NUMBER"  $ getDefaultOrEnvValue ((show . mailPortNumber) cfg)
            logLevel       <- readEnvValueFromGetter "MAILER_LOG_LEVEL"    $ getDefaultOrEnvValue ((show . mailLogLevel) cfg)
            backendType    <- readEnvValueFromGetter "MAILER_BACKEND_TYPE" $ getDefaultOrEnvValue ((show . mailBackendType) cfg)

            return $ cfg { mailBackendType=backendType
                         , mailUsername=user
                         , mailPassword=password
                         , mailHostname=hostname
                         , mailPortNumber=portNumber
                         , mailLogLevel=logLevel
                         , mailTemplatesPath=templatesPath
                         , mailFromAddress=fromAddr
                         }

instance OverridableFromEnv LoggerConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            to       <- getDefaultOrEnvValue (loggerEmailToAddr cfg)            "LOGGER_EMAIL_TO_ADDR"
            from     <- getDefaultOrEnvValue (loggerEmailFromAddr cfg)          "LOGGER_EMAIL_FROM_ADDR"

            priority <- readEnvValueFromGetter "LOGGER_EMAIL_PRIORITY" $ getDefaultOrEnvValue ((show . loggerEmailPriority) cfg)

            return $ cfg { loggerEmailToAddr=to
                         , loggerEmailFromAddr=from
                         , loggerEmailPriority=priority
                         }

instance OverridableFromEnv UserContentStoreConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            scheme             <- getDefaultOrEnvValue (ucsScheme cfg)             "UCS_SCHEME"
            localBaseDir       <- getDefaultOrEnvValue (ucsLocalBaseDir cfg)       "UCS_LOCAL_BASE_DIR"
            localPublicBaseURL <- getDefaultOrEnvValue (ucsLocalPublicBaseURL cfg) "UCS_LOCAL_PUBLIC_BASE_URL"

            logLevel           <- readEnvValueFromGetter "UCS_LOG_LEVEL" $ getDefaultOrEnvValue ((show . ucsLogLevel) cfg)

            return $ cfg { ucsScheme=scheme
                         , ucsLocalBaseDir=localBaseDir
                         , ucsLocalPublicBaseURL=localPublicBaseURL
                         , ucsLogLevel=logLevel
                         }

instance OverridableFromEnv SearchConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            addr          <- getDefaultOrEnvValue (searchAddr cfg)                  "SEARCH_ADDR"
            traceQueries  <- getDefaultOrEnvValue ((show . searchTraceQueries) cfg) "SEARCH_TRACE_QUERIES"
            migrationFile <- getDefaultOrEnvValue (searchFTSMigrationFile cfg)      "SEARCH_FTS_MIGRATION_FILE"

            logLevel      <- readEnvValueFromGetter "SEARCH_LOG_LEVEL" $ getDefaultOrEnvValue ((show . searchLogLevel) cfg)
            traceQueries  <- readEnvValueFromGetter "SEARCH_TRACE_QUERIES" $ getDefaultOrEnvValue ((show . searchTraceQueries) cfg)

            return $ cfg { searchAddr=addr
                         , searchFTSMigrationFile=migrationFile
                         , searchLogLevel=logLevel
                         , searchTraceQueries=traceQueries
                         }

instance OverridableFromEnv CacheConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            logLevel           <- readEnvValueFromGetter "CACHE_LOG_LEVEL"                     $ getDefaultOrEnvValue ((show . cacheLogLevel) cfg)
            invalidationTimeMs <- readEnvValueFromGetter "CACHE_DEFAULT_TIMED_INVALIDATION_MS" $ getDefaultOrEnvValue ((show . cacheDefaultTimedInvalidationMs) cfg)

            return $ cfg { cacheLogLevel=logLevel
                         , cacheDefaultTimedInvalidationMs=invalidationTimeMs
                         }

instance OverridableFromEnv MetricsConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            logLevel       <- readEnvValueFromGetter "METRICS_LOG_LEVEL"                $ getDefaultOrEnvValue ((show . metricsLogLevel) cfg)
            appWide        <- readEnvValueFromGetter "METRICS_INSTRUMENT_APP"           $ getDefaultOrEnvValue ((show . metricsAppWide) cfg)
            includeMetrics <- readEnvValueFromGetter "METRICS_INCLUDE_METRICS_ENDPOINT" $ getDefaultOrEnvValue ((show . metricsIncludeMetricsEndpoint) cfg)

            return $ cfg { metricsEndpointURLs=metricsEndpointURLs cfg
                         , metricsLogLevel=logLevel
                         , metricsAppWide=appWide
                         , metricsIncludeMetricsEndpoint=includeMetrics
                         }

instance OverridableFromEnv TracingConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            serviceTags    <- getDefaultOrEnvValue ((show . tracingServiceTags) cfg)   "TRACING_SERVICE_TAGS"
            agentUDPAddr   <- getDefaultOrEnvValue ((DT.unpack . tracingAgentUDPAddr) cfg) "TRACING_AGENT_UDP_ADDR"
            agentHTTPAddr  <- getDefaultOrEnvValue ((DT.unpack . tracingAgentHTTPAddr) cfg) "TRACING_AGENT_HTTP_ADDR"

            logLevel       <- readEnvValueFromGetter "TRACING_LOG_LEVEL"       $ getDefaultOrEnvValue ((show . tracingLogLevel) cfg)
            backendType    <- readEnvValueFromGetter "TRACING_BACKEND_TYPE"    $ getDefaultOrEnvValue ((show . tracingBackendType) cfg)
            serviceName    <- readEnvValueFromGetter "TRACING_SERVICE_NAME"    $ getDefaultOrEnvValue ((show . tracingServiceName) cfg)
            samplingRate   <- readEnvValueFromGetter "TRACING_SAMPLING_RATE"   $ getDefaultOrEnvValue ((show . tracingSamplingRate) cfg)
            appWide        <- readEnvValueFromGetter "TRACING_INSTRUMENT_APP"  $ getDefaultOrEnvValue ((show . tracingAppWide) cfg)
            agentUDPPort   <- readEnvValueFromGetter "TRACING_AGENT_UDP_PORT"  $ getDefaultOrEnvValue ((show . tracingAgentUDPPort) cfg)
            agentHTTPPort  <- readEnvValueFromGetter "TRACING_AGENT_HTTP_PORT" $ getDefaultOrEnvValue ((show . tracingAgentHTTPPort) cfg)

            return $ cfg { tracingLogLevel=logLevel
                         , tracingBackendType=backendType
                         , tracingServiceName=serviceName
                         , tracingServiceTags=parseCommaSeparatedOptionalList (DT.pack serviceTags)
                         , tracingSamplingRate=samplingRate
                         , tracingAppWide=appWide
                         , tracingAgentUDPAddr=DT.pack agentUDPAddr
                         , tracingAgentUDPPort=agentUDPPort
                         , tracingAgentHTTPAddr=DT.pack agentHTTPAddr
                         , tracingAgentHTTPPort=agentHTTPPort
                         }

instance OverridableFromEnv CookieConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            domain         <- getDefaultOrEnvValue (cookieDomain cfg)               "COOKIE_DOMAIN"
            path           <- getDefaultOrEnvValue (cookiePath cfg)                 "COOKIE_PATH"
            secretFilePath <- getDefaultOrEnvValue (cookieSecretFilePath cfg)       "COOKIE_SECRET_FILE_PATH"

            isHTTPOnly     <- readEnvValueFromGetter "COOKIE_HTTP_ONLY"       $ getDefaultOrEnvValue (show (cookieHttpOnly cfg))
            age            <- readEnvValueFromGetter "COOKIE_MAX_AGE_SECONDS" $ getDefaultOrEnvValue (show (cookieMaxAgeSeconds cfg))
            secure         <- readEnvValueFromGetter "COOKIE_SECURE"          $ getDefaultOrEnvValue (show (cookieIsSecure cfg))

            return cfg { cookieDomain=domain
                       , cookiePath=path
                       , cookieSecretFilePath=secretFilePath
                       , cookieHttpOnly=isHTTPOnly
                       , cookieMaxAgeSeconds=age
                       , cookieIsSecure=secure
                       }

instance OverridableFromEnv BrandingConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            name           <- getDefaultOrEnvValue ((show . brandingName) cfg)  "BRANDING_NAME"
            subURL         <- getDefaultOrEnvValue (brandingSubscribeURL cfg)   "BRANDING_SUBSCRIBE_URL"
            unsubURL       <- getDefaultOrEnvValue (brandingUnsubscribeURL cfg) "BRANDING_UNSUBSCRIBE_URL"

            return $ cfg { brandingName=name
                         , brandingSubscribeURL=subURL
                         , brandingUnsubscribeURL=unsubURL
                         }

instance OverridableFromEnv TaskQueueConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            backendType    <- getDefaultOrEnvValue ((show . taskQueueBackendType) cfg)  "TASK_QUEUE_BACKEND_TYPE"
            logLevel       <- readEnvValueFromGetter "TASK_QUEUE_LOG_LEVEL"       $ getDefaultOrEnvValue ((show . taskQueueLogLevel) cfg)

            return $ cfg { taskQueueBackendType=read backendType
                         , taskQueueLogLevel=logLevel
                         }

instance OverridableFromEnv AppConfig where
    overrideWithEnv cfg = try loadEnv
        where
          loadEnv = do
            -- App configuration
            frontendFolder      <- getDefaultOrEnvValue (appFrontendFolder cfg)      "FRONTEND_FOLDER"
            backendExternalURL  <- getDefaultOrEnvValue (appBackendExternalURL cfg)  "BACKEND_EXTERNAL_URL"
            adminExternalURL    <- getDefaultOrEnvValue (appAdminExternalURL cfg)    "ADMIN_EXTERNAL_URL"
            frontendExternalURL <- getDefaultOrEnvValue (appFrontendExternalURL cfg) "FRONTEND_EXTERNAL_URL"
            rootUserEmail       <- getDefaultOrEnvValue (appRootUserEmail cfg)       "ROOT_USER_EMAIL"
            rootUserPassword    <- getDefaultOrEnvValue (appRootUserPassword cfg)    "ROOT_USER_PASSWORD"

            port           <- readEnvValueFromGetter "PORT"        $ getDefaultOrEnvValue (show (appPort cfg))
            env            <- readEnvValueFromGetter "ENVIRONMENT" $ getDefaultOrEnvValue (show (appEnvironment cfg))
            logLevel       <- readEnvValueFromGetter "LOG_LEVEL"   $ getDefaultOrEnvValue (show (appLogLevel cfg))

            -- Subconfigurations (load them and error if any fail)
            backendCfg     <- overrideWithEnv (appBackendConfig cfg) >>= throwIfLeft
            cookieCfg      <- overrideWithEnv (appCookieConfig cfg) >>= throwIfLeft
            mailerCfg      <- overrideWithEnv (appMailerConfig cfg) >>= throwIfLeft
            loggerCfg      <- overrideWithEnv (appLoggerConfig cfg) >>= throwIfLeft
            ucsCfg         <- overrideWithEnv (appUCSConfig cfg) >>= throwIfLeft
            searchCfg      <- overrideWithEnv (appSearchConfig cfg) >>= throwIfLeft
            cacheCfg       <- overrideWithEnv (appCacheConfig cfg) >>= throwIfLeft
            metricsCfg     <- overrideWithEnv (appMetricsConfig cfg) >>= throwIfLeft
            tracingCfg     <- overrideWithEnv (appTracingConfig cfg) >>= throwIfLeft
            brandingCfg    <- overrideWithEnv (appBrandingConfig cfg) >>= throwIfLeft
            taskQueueCfg    <- overrideWithEnv (appTaskQueueConfig cfg) >>= throwIfLeft

            return cfg { appPort=port
                       , appEnvironment=env
                       , appLogLevel=logLevel
                       , appFrontendFolder=frontendFolder
                       , appBackendExternalURL=backendExternalURL
                       , appAdminExternalURL=adminExternalURL
                       , appFrontendExternalURL=frontendExternalURL
                       , appRootUserEmail=rootUserEmail
                       , appRootUserPassword=rootUserPassword

                       , appBackendConfig=backendCfg
                       , appCookieConfig=cookieCfg
                       , appMailerConfig=mailerCfg
                       , appLoggerConfig=loggerCfg
                       , appUCSConfig=ucsCfg
                       , appSearchConfig=searchCfg
                       , appCacheConfig=cacheCfg
                       , appMetricsConfig=metricsCfg
                       , appTracingConfig=tracingCfg
                       , appBrandingConfig=brandingCfg
                       , appTaskQueueConfig=taskQueueCfg
                       }

getCurrentEnvConfig :: String -> IO (Either SomeException AppConfig)
getCurrentEnvConfig e = overrideWithEnv defaultEnv
    where
      environment = read e
      defaultEnv = fromJust $ M.lookup environment defaultConfigs

makeAuthCookieWithSettings :: CookieConfig -> DT.DateTime -> BSC.ByteString -> BSC.ByteString -> WC.SetCookie
makeAuthCookieWithSettings c expiryDate name value = WC.def { WC.setCookieValue=value
                                                            , WC.setCookieName=name
                                                            , WC.setCookieHttpOnly=cookieHttpOnly c
                                                            , WC.setCookieMaxAge=maxAge
                                                            , WC.setCookieExpires=Just expiryDate
                                                            , WC.setCookieSecure=cookieIsSecure c
                                                            , WC.setCookieDomain=domain
                                                            , WC.setCookiePath=path
                                                            }
    where
      maxAge = Just $ secondsToDiffTime (cookieMaxAgeSeconds c)
      domainStr = cookieDomain c
      domain = if null domainStr then Nothing else Just (BSC.pack domainStr)
      pathStr = cookiePath c
      path = if null pathStr then Nothing else Just (BSC.pack pathStr)

makeInstantExpireCookieWithSettings :: CookieConfig -> DT.DateTime -> BSC.ByteString -> BSC.ByteString -> WC.SetCookie
makeInstantExpireCookieWithSettings cfg expiryDate name value = setZeroAge (makeAuthCookieWithSettings cfg expiryDate name value)
    where
      setZeroAge c = c { WC.setCookieMaxAge=Just $ secondsToDiffTime 0 }

-- | Replace the DB paths (backend, search) for a given AppConfig with a given path
replaceDBPaths :: AppConfig -> FilePath -> AppConfig
replaceDBPaths cfg dbPath = cfg { appBackendConfig=(appBackendConfig cfg) { dbAddr=dbPath }
                                , appSearchConfig=(appSearchConfig cfg) { searchAddr=dbPath }
                                }
