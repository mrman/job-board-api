{-# LANGUAGE OverloadedStrings #-}

module Errors where

import           Data.Monoid ((<>))
import           Data.Aeson
import           Types
import           Data.Char
import           Data.ByteString.Lazy as B
import qualified Servant as SE
import qualified Data.ByteString.Lazy.UTF8 as BSU

bytesToString = Prelude.map (chr . fromIntegral)

-----------------------------
-- Generic error machinery --
-----------------------------

-- Use enveloped response in error result
enveloped :: SE.ServerError -> SE.ServerError
enveloped e = e { SE.errBody = encoded }
    where
      errStr = (bytesToString . B.unpack) (SE.errBody e)
      resp = EnvelopedResponse "error" errStr Nothing :: EnvelopedResponse (Maybe String)
      encoded = encode resp

-- Use enveloped response in error result
convertToServantError :: Show e => e -> SE.ServerError
convertToServantError = makeGenericError . show . encode . makeResp . show
    where
      makeResp str = EnvelopedResponse "error" str Nothing :: EnvelopedResponse (Maybe String)

----------------
-- 4XX Errors --
----------------

failedToFindUser :: SE.ServerError
failedToFindUser = SE.err404 { SE.errBody = "Failed to find a matching user" }

-- | Failed to find an entity that is implied to exist
failedToFindImpliedEntity :: String -> SE.ServerError
failedToFindImpliedEntity entityName = SE.err400 { SE.errBody = "Failed to find entity" <> BSU.fromString entityName }

failedToLoginUser :: SE.ServerError
failedToLoginUser = SE.err401 { SE.errBody = "Failed to login user" }

invalidAuthHeaders :: SE.ServerError
invalidAuthHeaders = SE.err401 { SE.errBody = "Invalid authentication headers" }

invalidSession :: SE.ServerError
invalidSession = SE.err401 { SE.errBody = "Invalid/expired session" }

unauthorized :: SE.ServerError
unauthorized = SE.err401 { SE.errBody = "Unauthorized" }

loggedOut :: SE.ServerError
loggedOut = SE.err401 { SE.errBody = "Invalid Session (logged out)" }

failedToCreateNewUser :: SE.ServerError
failedToCreateNewUser = SE.err400 { SE.errBody = "Failed to create new user" }

failedToCreateCompany :: SE.ServerError
failedToCreateCompany = SE.err400 { SE.errBody = "Failed to create new company" }

failedToFindEntity :: B.ByteString -> SE.ServerError
failedToFindEntity name = SE.err400 { SE.errBody = "Failed to find " <> name <> " with the given ID" }

failedToFindCompany :: SE.ServerError
failedToFindCompany = SE.err400 { SE.errBody = "Failed to find company with the given ID" }

failedToCreateJobPostingRequest :: SE.ServerError
failedToCreateJobPostingRequest = SE.err400 { SE.errBody = "Failed to create new job posting request" }

failedToFetchJobs :: SE.ServerError
failedToFetchJobs = SE.err500 { SE.errBody = "Failed to fetch jobs" }

failedToFindJob :: SE.ServerError
failedToFindJob = SE.err404 { SE.errBody = "Failed to find a matching job" }

failedToFindJobPostingRequest :: SE.ServerError
failedToFindJobPostingRequest = SE.err404 { SE.errBody = "Failed to find a matching job posting request" }

failedToFindJobPostingRequests :: SE.ServerError
failedToFindJobPostingRequests = SE.err404 { SE.errBody = "Failed to find any job posting requests " }

notLoggedIn :: SE.ServerError
notLoggedIn = SE.err400 { SE.errBody = "Not logged in" }

make404 :: String -> SE.ServerError
make404 s = SE.err404 { SE.errBody=BSU.fromString s  }

genericBadRequest :: SE.ServerError
genericBadRequest = SE.err400 { SE.errBody = "Invalid request" }

makeBadRequestError :: String -> SE.ServerError
makeBadRequestError s = SE.err400 { SE.errBody = errStr }
    where
      errStr = BSU.fromString $ "Invalid request: " ++ s

failedToParseDataURI :: SE.ServerError
failedToParseDataURI = SE.err400 { SE.errBody = "Invalid request, contains malformed invalid data URIs" }

failedToRetrieveResource :: SE.ServerError
failedToRetrieveResource = SE.err404 { SE.errBody = "The specified resource was not found." }

failedToFindEntities :: SE.ServerError
failedToFindEntities = SE.err404 { SE.errBody = "The specified entities were not found." }

unknownBounce :: SE.ServerError
unknownBounce = SE.err400 { SE.errBody = "Unknown bounce link" }

makeInvalidEntityError :: [ValidationError] -> SE.ServerError
makeInvalidEntityError errors = SE.err400 { SE.errBody=encode resp  }
    where
      resp = EnvelopedResponse "error" "Entity has missing/invalid fields" errors :: EnvelopedResponse [ValidationError]

-- | Too many tags used
tagLimitExceeded :: Int -> SE.ServerError
tagLimitExceeded n = makeBadRequestError $ "You may only sign up for a maximum of " <> show n <> " tags"

----------------
-- 5xx Errors --
----------------

failedToChangeJobActivityStatus :: SE.ServerError
failedToChangeJobActivityStatus = SE.err500 { SE.errBody = "Failed to update activity status of job" }

failedToChangeFavoritePreference :: SE.ServerError
failedToChangeFavoritePreference = SE.err500 { SE.errBody = "Failed to update favorite preference" }

genericError :: SE.ServerError
genericError = SE.err500 { SE.errBody = "Server error occurred" }

makeGenericError :: String -> SE.ServerError
makeGenericError s = SE.err500 { SE.errBody=errStr  }
    where
      errStr = BSU.fromString $ "Server error occurred: " ++ s


errorOcurredWhileRetrievingResource :: SE.ServerError
errorOcurredWhileRetrievingResource = SE.err500 { SE.errBody = "An error occurred while trying to retrieve the specified resource" }

failedToAddResource :: SE.ServerError
failedToAddResource = SE.err400 { SE.errBody = "An error occurred while trying to create the resource. Please check your request and try again." }

failedToUpdateResource :: SE.ServerError
failedToUpdateResource = SE.err500 { SE.errBody = "An error occurred while trying to update the resource. Please try again later." }

jobSearchFailed :: SE.ServerError
jobSearchFailed = SE.err500 { SE.errBody = "Job search failed. Please try again later." }

failedToCreateEntity :: SE.ServerError
failedToCreateEntity = SE.err500 { SE.errBody = "Failed to create entity" }

-- | Failure to retrieve permission
failedToRetrievePermissions :: SE.ServerError
failedToRetrievePermissions = SE.err500 { SE.errBody = "Failed to retrieve permissions" }

-- | Failure to add a given permission
failedToAddPermission :: SE.ServerError
failedToAddPermission = SE.err500 { SE.errBody = "Failed to add permission" }

-- | Failure to revoke a permission
failedToRevokePermission :: SE.ServerError
failedToRevokePermission = SE.err500 { SE.errBody = "Failed to revoke permission" }

-- | Failure to revoke a permission
failedToCreateToken :: SE.ServerError
failedToCreateToken = SE.err500 { SE.errBody = "Failed to create token" }

-- | Failure to send email
failedToSendEmail :: String -> SE.ServerError
failedToSendEmail emailName = makeGenericError $ "Failed to send email: " ++ emailName

-- | Failure to modify entity
failedToModifyEntity :: String -> SE.ServerError
failedToModifyEntity entityName = makeGenericError $ "Failed to modify entity: " ++ entityName

-- | Failure to delete entity
failedToDeleteEntity :: String -> SE.ServerError
failedToDeleteEntity entityName = makeGenericError $ "Failed to delete entity: " ++ entityName

-- | Failure to convert and store image
failedToConvertAndStoreImage :: String -> SE.ServerError
failedToConvertAndStoreImage imageName = makeGenericError $ "Failed to convert and store image:" ++ imageName

-- | Failure to create a user session
failedToCreateSession :: SE.ServerError
failedToCreateSession = SE.err500 { SE.errBody = "Failed to create session" }
