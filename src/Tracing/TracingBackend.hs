{-# LANGUAGE OverloadedStrings        #-}
{-# LANGUAGE RankNTypes               #-}
{-# LANGUAGE ConstrainedClassMethods  #-}

module Tracing.TracingBackend ( makeConnectedTracingBackend
                              , TracingBackend
                              , Tracing(..)
                              ) where

import           Config (TracingConfig(..), TracingBackendType(..))
import           Control.Exception (SomeException, try, bracket)
import           Control.Monad (void)
import           Control.Monad.IO.Class
import           Data.ByteString.Builder
import           Data.IORef (IORef, newIORef, readIORef,  atomicModifyIORef')
import           Data.Maybe (Maybe, fromMaybe)
import           Data.Monoid ((<>))
import           Data.Time.Clock (NominalDiffTime, UTCTime, getCurrentTime, diffUTCTime)
import           Data.Time.Clock.POSIX (POSIXTime, utcTimeToPOSIXSeconds, posixSecondsToUTCTime)
import           Network.HTTP.Client (Manager, newManager, defaultManagerSettings)
import           Network.Wai (Application, Middleware, Request, requestMethod, pathInfo)
import           System.Log.Logger (Logger, Priority(..), logL)
import           System.Random (randomRIO)
import           Tracing.Core (Span(..), OpName(..), SpanContext(..), SpanId(..), TraceId(..), toSpanTag)
import           Tracing.Zipkin (publishZipkin)
import           Types (HasLogger(..))
import qualified Data.ByteString.Lazy.Char8 as BSL8
import qualified Data.Text as DT
import qualified Data.Map as DM

data TracingBackend = TracingBackend { tbCfg    :: TracingConfig
                                     , tbLogger :: Maybe Logger
                                     , tbSpans  :: IORef [Span]
                                     , tbMgr    :: Maybe Manager
                                     }

makeConnectedTracingBackend :: TracingConfig -> Maybe Logger -> IO (Either SomeException TracingBackend)
makeConnectedTracingBackend cfg maybeL = try makeBackend
    where
      makeManager = case tracingBackendType cfg of
                      LocalTracingBackend -> pure Nothing
                      Zipkin -> Just <$> newManager defaultManagerSettings
      makeBackend = newIORef []
                    >>= \spans -> TracingBackend cfg maybeL spans <$> makeManager

-- ^ Create a reporter, polymorphic by the settings that are currently in the Tracing t
getPublisher :: (MonadIO m, Tracing t, HasLogger t) => t -> [Span] -> m ()
getPublisher t = case tracingBackendType cfg of
                   LocalTracingBackend -> logPublisher t
                   Zipkin -> zipkinPublisher t
    where
      cfg = getTracingConfig t

-- ^ Publishes one or more spans to the local log
logPublisher :: (MonadIO m, HasLogger t) => t -> [Span] -> m ()
logPublisher tracer = liftIO . mapM_ (logMsg tracer INFO . show)

-- ^ Publishes one or more spans a zipkin instance
-- this method *assumes* that zipkin agents (`Zipkin`s) don't need to be re-created every time, and can be reused, so stores them
zipkinPublisher :: (HasLogger t, Tracing t, MonadIO m) => t -> [Span] -> m ()
zipkinPublisher t spans = liftIO (maybe logNoMgrErr doPublish maybeMgr)
    where
      cfg = getTracingConfig t
      maybeMgr = getTracingManager t
      host = DT.unpack $ tracingAgentHTTPAddr cfg
      port = show $ tracingAgentHTTPPort cfg
      addr = "http://" <> host <> ":"<> port <> "/api/v2/spans"
      logNoMgrErr = logMsg t ERROR "No HTTP Manager configured for Zipkin tracing backend"
      debugLogResponse = logMsg t DEBUG . ("Zipkin Resp: "<>) . show
      doPublish mgr = publishZipkin addr mgr spans
                      >>= debugLogResponse
                      >> pure ()

-- ^ Create a WAI-compatible middleware that will trace every request
makeTracingMiddleware :: (Tracing t, HasLogger t) => t -> Middleware
makeTracingMiddleware t app req respond = bracket
                                          (startRequestSpan t req)
                                          (\reqSpan -> finishRequestSpan t reqSpan >> publishSpans t)
                                          (\_ -> app req respond)

-- ^ Reduce the precision of the NominalDiffTime objects that are being sent
-- this is necessary because the following error gets sent from Jaeger (when sending a NominalDiffTime of "0.001500525s" ):
-- "Unable to process request body: json: cannot unmarshal number 1500.525 into Go struct field Span.duration of type int64\n"
-- (running regular zipkin will just produce a "failed to parse" error with no details)
reducePrecision :: (Num a, RealFrac a) => Int -> a -> a
reducePrecision places = (/ factor) . fromInteger . round . (* factor)
    where
      factor = 10 ^ places

class Tracing t where
    getTracingLogger  :: t -> Maybe Logger
    getTracingConfig  :: t -> TracingConfig
    getTracingManager :: t -> Maybe Manager
    getPublisherFn    :: (MonadIO m, Tracing t, HasLogger t) => t -> [Span] -> m ()

    -- ^ Take a single WAI Request and generate a span for it
    startRequestSpan :: t -> Request -> IO Span

    -- ^ Save a single generated span onto an internal list of spans
    finishRequestSpan :: t -> Span -> IO ()

    -- ^ Publish all recorded spans (in the internal list)
    publishSpans :: t -> IO [Span]

    -- ^ Add a single span to the internal buffer
    addSpan :: t -> Span -> IO ()

    -- ^ Remove spans (that have been recorded) from the internal buffer
    removeSpans :: t -> [Span] -> IO ()

    -- ^ Trace an application at the request level
    traceApplication :: t -> Application -> IO Application

instance HasLogger TracingBackend where
    getComponentLogger = tbLogger

instance Tracing TracingBackend where
    getTracingLogger = tbLogger
    getTracingConfig = tbCfg
    getTracingManager = tbMgr
    getPublisherFn = getPublisher

    startRequestSpan t req = getCurrentTime
                             >>= \now -> generateTraceId
                             >>= \traceId -> generateSpanId
                             >>= \spanId -> pure Span { serviceName=svcName
                                                      , operationName=operation
                                                      , context=SpanContext traceId spanId
                                                      , timestamp=utcTimeToPOSIXSeconds now
                                                      , duration=0
                                                      , relations=[]
                                                      , tags=traceTags
                                                      , baggage=mempty
                                                      , debug=False
                                                      }
        where
          cfg = getTracingConfig t
          svcName = tracingServiceName cfg
          reqURL = DT.intercalate "/" $ pathInfo req
          reqMethod = DT.pack $ show $ requestMethod req

          operation = OpName $ reqMethod <> " " <> reqURL  -- OpName $ (show (requestMethod req) <>) $ show
          traceTags = DM.fromList [("method", toSpanTag reqMethod), ("url", toSpanTag reqURL)]
          generateSpanId  = SpanId <$> randomRIO (0, maxBound)
          generateTraceId = TraceId <$> randomRIO (0, maxBound)

    -- TODO: add sample-checking code to honor double from 0.0 to 1.0

    finishRequestSpan t span = getCurrentTime
                               >>= \now -> pure (span { duration=reducePrecision 6 (diffUTCTime now timestampUTC) })
                               >>= \finishedSpan -> logMsg t DEBUG ("Finishing request span (" <> opName <> "), duration = " <> show (duration finishedSpan))
                               >> addSpan t finishedSpan
        where
          timestampUTC = posixSecondsToUTCTime $ timestamp span
          opName = show (operationName span)

    publishSpans t = readIORef (tbSpans t)
                     >>= \spans -> logMsg t DEBUG ("Publishing " <> show (length spans) <> " saved spans...")
                     >> doPublish spans
                     -- atomically remove spans that have bene published
                     >> removeSpans t spans
                     >> pure spans
        where
          doPublish = getPublisherFn t

    addSpan t newSpan = atomicModifyIORef' (tbSpans t) (\l -> (l <> [newSpan], ()))

    removeSpans t spansToRemove = atomicModifyIORef' (tbSpans t) filterSpans
        where
          extractSpanId = spanId . context
          spanIdsToRemove = map extractSpanId spansToRemove
          removeMatchingSpans = filter ((`notElem` spanIdsToRemove) . extractSpanId)
          filterSpans currentSpans = (removeMatchingSpans currentSpans, ())

    traceApplication t app = logMsg t INFO ("App-wide Tracing: " <> if appWideTracingEnabled then "ON" else "OFF")
                             >> if appWideTracingEnabled then _traceApplication app else pure app
        where
          appWideTracingEnabled = tracingAppWide $ tbCfg t
          middleware = makeTracingMiddleware t

          -- Instrument an actual application with the agent reporter
          _traceApplication :: Application -> IO Application
          _traceApplication app = liftIO (logMsg t INFO "Setting up tracing WAI middleware...")
                                  >> pure (middleware app)
