{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TaskQueue.Runner.RandomJobRefresh ( randomJobRefreshRunner ) where

import          Data.DateTime (getCurrentTime)
import          Data.Aeson.Types (object, (.=))
import          Control.Monad.IO.Class
import          Util (throwIfLeft)
import          Control.Exception(try)
import          Network.HTTP.Simple (httpLBS, getResponseStatus, parseRequest_)
import          Network.HTTP.Types.Status (status200)
import          Types ( JobStore(..)
                      , JobManager(..)
                      , TaskRunner
                      , ModelWithID(..)
                      , Job(..)
                      , TaskResult(..)
                      , TaskStatus(..)
                      )

-- | Check some given job's link to see if it still returns a 200
checkJobLink200 :: MonadIO m => String -> m Bool
checkJobLink200 link = httpLBS (parseRequest_ link)
                       >>= pure . (==status200) . getResponseStatus

randomJobRefreshRunner :: ( MonadIO m
                          , JobStore db m
                          , JobManager db m
                          ) => db -> TaskRunner m
randomJobRefreshRunner db = runner
    where
      wasValidJob jid = pure $ Right $ TaskResult Success "job link was valid" $ object [ "jobId" .= jid ]
      disabledJob jid = pure $ Right $ TaskResult Success "job link invalid, disabled job" $ object [ "jobId" .= jid ]

      disableJob db jobId = setJobActivity db False jobId
                            >>= throwIfLeft -- rethrow if update failed
                            >> disabledJob jobId

      runner _ = getRandomJob db
                 >>= throwIfLeft
                 >>= \(ModelWithID jid job) -> checkJobLink200 (applyLink job)
                 >>= \linkIsValid -> liftIO getCurrentTime
                 >>= \now -> updateJob db jid (job { lastCheckedAt=Just now })
                 >>= throwIfLeft -- rethrow if update failed
                 >> if linkIsValid then wasValidJob jid else disableJob db jid
