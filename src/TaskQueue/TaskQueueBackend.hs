{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FlexibleInstances      #-}
{-# LANGUAGE FlexibleContexts       #-}
{-# LANGUAGE InstanceSigs           #-}
{-# LANGUAGE GADTs                  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase             #-}

module TaskQueue.TaskQueueBackend ( makeConnectedTaskQueueBackend
                                  , TaskQueueBackend
                                  , TaskQueue(..)
                                  , TaskQueueM(..)
                                  , HasTaskQueueBackend
                                  ) where

import           Config (TaskQueueConfig, oneSecondMs)
import           Control.Concurrent (ThreadId, forkIO, killThread, threadDelay, myThreadId)
import           Control.Concurrent.STM (atomically)
import           Control.Concurrent.STM.TVar (TVar, newTVarIO, modifyTVar', readTVarIO)
import           Control.Exception (Exception, SomeException, handle, try)
import           Control.Monad (void, forM)
import           Control.Monad.IO.Class
import           Data.Aeson.Types (Value)
import           Util (throwIfLeft)
import           Data.Time.Clock (NominalDiffTime)
import           System.Log.Logger (Logger, Priority(..), logL)
import           TaskQueue.Runner.RandomJobRefresh
import           Types
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as DT

-- | Task queue backend, backed by some task store
data TaskQueueBackend db m where
    WithTaskStore :: (MonadIO m, TaskStore db m) => TaskQueueConfig -> Maybe Logger -> TVar TaskThreadMap -> db -> TaskQueueBackend db m

data TaskRunnerError = NoRunnerForTask
                     | InvalidConfiguration
                     | FailedToStartTask
                       deriving (Eq, Show)

instance Exception TaskRunnerError

-- | Construct a job queue backend
makeConnectedTaskQueueBackend :: ( MonadIO m
                                 , TaskStore db m
                                 )
                                => TaskQueueConfig
                                    -> Maybe Logger
                                    -> db
                                    -> m (Either TaskRunnerError (TaskQueueBackend db m))
makeConnectedTaskQueueBackend cfg maybeL db = liftIO build
    where
      build = newTVarIO HMS.empty
              >>= \dynMap -> pure $ Right $ WithTaskStore cfg maybeL dynMap db

-- | Basic requirements for a task queue
class TaskQueue tq where
    getTaskQueueLogger :: tq -> Maybe Logger
    getTaskQueueConfig :: tq -> TaskQueueConfig

-- | Requirements of a task queue in a monadic context
class (Monad m, TaskQueue tq) => TaskQueueM tq m where
    -- | Schedule a recurring tasks
    runRecurringTask :: tq -> TaskType -> TaskData -> Maybe UserID -> Seconds -> m (Either TaskRunnerError ())

    -- | Stop all recurring tasks
    stopAllRecurringTasks :: tq -> m (Either TaskRunnerError ())

instance HasLogger (TaskQueueBackend db m) where
    getComponentLogger (WithTaskStore _ maybeL _ _) = maybeL

instance TaskQueue (TaskQueueBackend db m) where
    getTaskQueueLogger = getComponentLogger
    getTaskQueueConfig (WithTaskStore cfg _ _ _) = cfg

-- | Create a runner based on the TaskType
--
--   This constructor must be able to make all used runners
makeRunner :: ( MonadIO m
              , JobStore db m
              , JobManager db m
              ) => db -> TaskType -> Maybe (TaskRunner m)
makeRunner db RandomJobRefresh = Just $ randomJobRefreshRunner db

removeThreadIdForTask :: TaskType -> ThreadId -> TVar TaskThreadMap -> IO ()
removeThreadIdForTask taskType tid tvar = atomically $ modifyTVar' tvar $ HMS.adjust (filter (==tid)) taskType

addThreadIdForTask :: TaskType -> ThreadId -> TVar TaskThreadMap -> IO ()
addThreadIdForTask taskType tid tvar = atomically $ modifyTVar' tvar $ HMS.insertWith (<>) taskType [tid]

taskCompletedMsg :: Int -> TaskResult -> String
taskCompletedMsg taskId result = "Task with ID [" <> show taskId <> "] completed with status [" <> show result <> "]"

genUnexpectedError :: SomeException -> String
genUnexpectedError err = "Unexpected error occurred while executing task: " <> show err

genKnownError :: Exception e => e -> String
genKnownError err = "Known error occurred while executing task: " <> show err

instance ( MonadIO m
         , JobManager db m, JobManager db IO
         , TaskStore db m, TaskStore db IO
         , TaskManager db m, TaskManager db IO
         ) => TaskQueueM (TaskQueueBackend db m) m where

    runRecurringTask :: TaskQueueBackend db m -> TaskType -> TaskData -> Maybe UserID -> Seconds -> m (Either TaskRunnerError ())
    runRecurringTask tqb@(WithTaskStore _ l map db) taskType taskData maybeScheduler (Seconds seconds) = maybe returnNoRunnerForTask handle (makeRunner db taskType)
        where
          returnNoRunnerForTask = pure $ Left NoRunnerForTask
          intervalMs = seconds * oneSecondMs

          -- Wrap the kickoff into m
          handle = liftIO . work

          taskStartMessage taskId = "Running recurring task of type [" <> show taskType <> "], taskID [" <> show taskId <> "]"

          -- Kick off the runner
          work runnerWithDB = forkIO (waitThenWork (runnerWithDB taskData))
                              >> pure (Right ())

          -- Loops forever, executing the work action that was provided
          waitThenWork action = myThreadId
                                -- Save the newly started thread task ID
                                >>= \tid -> addThreadIdForTask taskType tid map
                                -- Wait for the amount of intervals
                                >> threadDelay intervalMs
                                -- Create a task in the DB
                                >> addTask db taskType taskData maybeScheduler
                                >>= throwIfLeft
                                >>= \(ModelWithID taskId task) -> logMsg tqb INFO (taskStartMessage taskId)
                                -- Record that we're about to run the task
                                >> recordTaskStartedByID db taskId
                                -- Run the task
                                >> try action
                                >>= \case
                                    -- If an error occurs then update the task status but do not recur
                                    Left err -> logMsg tqb ERROR (genUnexpectedError err)
                                                >> void (updateTaskStatusByID db taskId UnexpectedFailure)
                                                >> void (updateTaskErrorByID db taskId (Just (genUnexpectedError err)))
                                    -- If the task completed successfully
                                    Right result -> case result of
                                                      -- Handle known failures
                                                      Left knownErr -> logMsg tqb WARNING (genKnownError knownErr)
                                                                       >> void (updateTaskStatusByID db taskId ExpectedFailure)
                                                                       >> void (updateTaskErrorByID db taskId (Just (genKnownError knownErr)))
                                                      -- Handle complete success
                                                      Right value -> logMsg tqb DEBUG (taskCompletedMsg taskId value)
                                                                     -- Update task DB status
                                                                     >> void (updateTaskStatusByID db taskId (triStatus value))
                                -- Recur by forking again
                                >> void (forkIO (waitThenWork action))

    stopAllRecurringTasks :: TaskQueueBackend db m -> m (Either TaskRunnerError ())
    stopAllRecurringTasks tqb@(WithTaskStore _ _ taskMap _) = liftIO stopAllRecurringTasks_
        where
          killAndRemove taskType tid = killThread tid
                                       >> removeThreadIdForTask taskType tid taskMap

          killTasksInList taskType tids = forM tids (killAndRemove taskType)

          killAllTaskThreads typesAndThreads = forM typesAndThreads $ uncurry killTasksInList

          stopAllRecurringTasks_ = readTVarIO taskMap
                                   >>= killAllTaskThreads . HMS.toList
                                   >> pure (Right ())


class (MonadIO m, TaskQueueM tqb m) => HasTaskQueueBackend m tqb | m -> tqb where
    getTaskQueueBackend :: m tqb
