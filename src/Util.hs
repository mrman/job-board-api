{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE ExplicitForAll    #-}
{-# LANGUAGE NamedFieldPuns    #-}

module Util where

import           Config (BrandingConfig(..))
import           Control.Exception (Exception, SomeException, throw)
import           Control.Monad.Except (MonadError)
import           Control.Monad.IO.Class
import           Control.Monad.State (unless)
import           Data.List (uncons, isPrefixOf, isSuffixOf, elemIndices)
import           Data.Maybe (isNothing, fromJust, fromMaybe)
import           Data.Monoid ((<>))
import           Data.Natural (Natural, natural, indeterm)
import           Data.Time.Clock (UTCTime(..), secondsToDiffTime)
import           Errors as Err
import           Servant (ServerError, Handler, throwError)
import           System.Log.Logger (Priority(..), logL)
import           Text.Regex (mkRegex, subRegex)
import           Types
import qualified Data.DateTime as DDT
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as DT
import qualified Database.SQLite.Simple as S
import qualified Database.SQLite.Simple.Types as ST

ensureNotNothing :: (MonadError e m, Monad m) => Maybe a -> e -> m a
ensureNotNothing Nothing e = throwError e
ensureNotNothing (Just x) _ = return x

ifNothingThrowIOError :: (MonadError e m, Monad m) => e -> Maybe a -> m a
ifNothingThrowIOError = flip ensureNotNothing

ifNothingThrowError :: (Exception e, Monad m) => e -> Maybe a -> m a
ifNothingThrowError e Nothing = throw e
ifNothingThrowError e (Just v) = pure v

whenNothing :: (Monad m) => m a -> Maybe a -> m a
whenNothing action Nothing  = action
whenNothing action (Just x) = return x

whenLeft :: (Monad m, Exception e) => (e -> m a) -> Either e a -> m a
whenLeft action (Left err)  = action err
whenLeft action (Right v) = return v

throwServantErrorIf :: (MonadError e m) => Bool -> e -> m ()
throwServantErrorIf True err = throwError err
throwServantErrorIf False  _ = return ()

ifLeftConvertAndThrow :: (MonadError ServerError m) => Either SomeException r -> m r
ifLeftConvertAndThrow (Left err) = throwError . Err.enveloped . Err.convertToServantError $ err
ifLeftConvertAndThrow (Right v) = pure v

servantConvertErr :: Exception e => e -> ServerError
servantConvertErr = Err.enveloped . Err.convertToServantError

ifNothingEnvelopeAndThrow :: (MonadError ServerError m) => ServerError -> Maybe r -> m r
ifNothingEnvelopeAndThrow e Nothing = throwError (Err.enveloped e)
ifNothingEnvelopeAndThrow e (Just v) = pure v

ifLeftEnvelopeAndThrow :: (MonadError ServerError m, Exception e) => ServerError -> Either e r -> m r
ifLeftEnvelopeAndThrow e (Left err) = throwError (Err.enveloped e)
ifLeftEnvelopeAndThrow e (Right v)  = pure v

throwIf :: (Exception e, Monad m) => Bool -> e -> v -> m v
throwIf True   err _ = throw err
throwIf False  _   v = pure v

throwErrorIf :: Monad m => Bool -> String -> m ()
throwErrorIf True err = error err
throwErrorIf False  _ = return ()

throwIfLeft :: (Exception l, Monad m) => Either l r -> m r
throwIfLeft (Left err) = throw err
throwIfLeft (Right v) = pure v

ifLeftThrow :: (Exception e, Monad m) => e -> Either l r -> m r
ifLeftThrow e (Left _) = throw e
ifLeftThrow _ (Right v) = pure v

extractSingleNumber :: (Num a) => [S.Only a] -> a
extractSingleNumber = S.fromOnly . maybe (S.Only (-1)) fst . uncons

trimSearchTerm :: Maybe String -> String
trimSearchTerm = DT.unpack . DT.strip . DT.pack . fromMaybe ""

showLeft :: Either SomeException a -> String
showLeft (Left err) = show err
showLeft (Right _)  = ""

-- ^ Converts a regular SELECT query to one that only gets the count.
-- Removes LIMIT if specified, and changes SELECT <whatever> to SELECT COUNT(*)
convertToCountQuery :: ST.Query -> ST.Query
convertToCountQuery q = q { ST.fromQuery=makeCountQuery oldQuery }
    where
      oldQuery = ST.fromQuery q
      queryWithoutLimit q = subRegex (mkRegex "LIMIT ([0-9]+)") q ""
      queryWithoutOffset q = subRegex (mkRegex "OFFSET ([0-9]+)") q ""
      removeLimitAndOffset = queryWithoutLimit . queryWithoutOffset
      wrapWithSubquery = (<>")") . ("SELECT COUNT(*) FROM (SELECT * "<>)
      makeCountQuery = wrapWithSubquery . DT.intercalate " " . dropWhile (/= "FROM") . DT.words . DT.pack . removeLimitAndOffset . DT.unpack

wrapList :: DT.Text -> DT.Text
wrapList = ("("<>) . (<>")")

withLimitAndOffset :: Maybe Limit -> Maybe Offset -> ST.Query -> ST.Query
withLimitAndOffset limit offset = appendClause "OFFSET" offset . appendClause "LIMIT" limit

withLimitOffsetAndOrder :: forall entity. Maybe Limit -> Maybe Offset -> Maybe (Order entity) -> ST.Query -> ST.Query
withLimitOffsetAndOrder limit offset order = appendClause "OFFSET" offset . appendClause "LIMIT" limit . appendOrder
    where
      orderPhrase = " " <> makeOrderPhrase order <> " "
      appendOrder = ST.Query . (<> orderPhrase) . ST.fromQuery

appendClause :: (Show a) => DT.Text -> Maybe a -> ST.Query -> ST.Query
appendClause name maybeClause q = q { ST.fromQuery=newQuery }
    where
      oldQuery = ST.fromQuery q
      spacedClause = " " <> name <> " "
      clause = maybe "" ((spacedClause<>) . DT.pack . show) maybeClause
      newQuery = DT.intercalate " " [oldQuery, clause]

-- ^ Do a SQL query and paginate the response
doQueryAndPaginate_ :: (MonadIO m, S.FromRow entity, Show entity)
                       => Maybe Limit  -- ^ possible limit
                           -> Maybe Offset  -- ^ possible offset
                           -> Maybe (Order entity)
                           -> S.Query  -- ^ query to perform (will be run twice, unfortunately)
                           -> Maybe [S.NamedParam] -- ^ Parameters for the query, if available
                           -> S.Connection
                           -> m (Either DBError (PaginatedList entity))
doQueryAndPaginate_ limit offset order query params c = do
  raw <- liftIO (S.queryNamed c paginatedQuery queryParams)
  count <- liftIO (S.queryNamed c countQuery queryParams :: IO [S.Only Int])
  return (Right (PaginatedList raw (extractSingleNumber count)))
    where
      queryParams = fromMaybe [] params
      paginatedQuery = withLimitOffsetAndOrder limit offset order query
      countQuery = convertToCountQuery query

insertFieldsIntoTable :: TableName -> [DT.Text] -> ST.Query
insertFieldsIntoTable tableName fields = ST.Query $ DT.concat ["INSERT INTO ", tableName , " "
                                                              , makeFieldList fields, " "
                                                              , "VALUES ", makeQuestionMarkList fields
                                                              ]

makeFieldList :: [DT.Text] -> DT.Text
makeFieldList fields = DT.concat ["(", DT.intercalate "," fields, ")"]

makeQuestionMarkList :: [DT.Text] -> DT.Text
makeQuestionMarkList = wrapList . DT.intercalate "," . map DT.pack . (`replicate` "?") . length

genSelectAllFromTableQuery :: DT.Text -> ST.Query -- ^ Generate SELECT query to get a row from any table by a single field comparison (=)
genSelectAllFromTableQuery table = ST.Query $ "SELECT * FROM " <> table

genericIsActiveFieldName :: DT.Text
genericIsActiveFieldName = "isActive"

genericCreatedAt :: DT.Text
genericCreatedAt = "createdAt"

-- | TODO: let order phrase take a prefix (in the case we have a join with some table prefix)
makeOrderPhrase :: forall entity. Maybe (Order entity) -> DT.Text
makeOrderPhrase Nothing      = ""
makeOrderPhrase (Just order) = " ORDER BY " <> makeOrderBy order Nothing

makeGenericIncludeActiveCheck :: IncludeInactive -> DT.Text
makeGenericIncludeActiveCheck include =  if include then "" else "WHERE " <> genericIsActiveFieldName <> " = 1"

jobPostingDateFieldName :: DT.Text
jobPostingDateFieldName = "postingDate"

-- ^ Lookup or compute a value from cache
lookupOrComputeAndSave :: Monad m => m (Maybe a) -> m (Maybe a) -> (a -> m ()) -> m (Maybe a)
lookupOrComputeAndSave lookupFn compute save = lookupFn >>= maybe computeAndSave (pure . Just)
    where
      computeAndSave = compute
                       >>= maybe (pure Nothing) (\res -> save res >> pure (Just res))

-- ^ Lookup or compute a value from cache, but functions on computations that produce Either (not Maybe)
lookupOrComputeAndSaveEither :: Monad m => m (Maybe v) -> m (Either err v) -> (v -> m ()) -> m (Either err v)
lookupOrComputeAndSaveEither lookupFn compute save = lookupFn >>= maybe computeAndSave (pure . Right)
    where
      computeAndSave = compute
                       >>= either (pure . Left) (\res -> save res >> pure (Right res))

throwServantErrorIfLeft :: (MonadError ServerError m, Exception e) => Either e a -> m a
throwServantErrorIfLeft (Left err) = throwError $ Err.makeGenericError (show err)
throwServantErrorIfLeft (Right v)  = return v

cleanUser :: ModelWithID User -> UserWithoutSensitiveData
cleanUser (ModelWithID uid u) = UserWithoutSensitiveData { userID=uid
                                                         , userFirstName=firstName u
                                                         , userLastName=lastName u
                                                         , userEmailAddress=emailAddress u
                                                         , role=userRole u
                                                         , userJoinedAt=joinedAt u
                                                         , userHomeLocationId=homeLocationId u
                                                         }
-- | Log a message and throw a given error
logMessageAndThrowError :: (HasLoggerBackend m, MonadError ServerError m)
                           => Priority        -- ^ Priority
                               -> String      -- ^ Error message prefix
                               -> ServerError  -- ^ Servant error to be thrown externally
                               -> m a
logMessageAndThrowError p msg err = getLogger
                                    >>= ifNothingThrowError err
                                    >>= \l -> liftIO (logL l p msg)
                                    >> throwError err

-- | Log a message including the error internally, and throw a seperate external error
logInternalThrowExternal :: (HasLoggerBackend m, MonadError ServerError m, Exception ex)
                            => Priority        -- ^ Priority
                                -> String      -- ^ Error message prefix
                                -> ServerError  -- ^ Servant error to be thrown externally
                                -> ex          -- ^ The internal error that was actually raised
                                -> m a
logInternalThrowExternal p msgPrefix servantErr ex = logMsg >> throwError servantErr
    where
      msg = msgPrefix <> ": " <> show ex
      logMsg = getLogger
               >>= ifNothingThrowError ex
               >>= \l -> liftIO (logL l p msg)

validateEntity :: (MonadError ServerError m, DBEntity e) => e -> m ()
validateEntity e = unless noErrors $ throwError (Err.makeInvalidEntityError errors)
    where
      errors = validateWithErrors e
      noErrors = null errors

makeBrandingDataFromConfig :: BrandingConfig -> BrandingData
makeBrandingDataFromConfig cfg = BrandingData { bdSiteName=brandingName cfg
                                              , bdSubURL=brandingSubscribeURL cfg
                                              , bdUnsubURL=brandingUnsubscribeURL cfg
                                              }

showText :: Show a => a -> DT.Text
showText = DT.pack . show

natToInt :: Natural -> Int
natToInt = fromIntegral . toInteger

entityCountToInt :: EntityCount a -> Int
entityCountToInt = natToInt . _etTotal

-- | Hacky function for checking if a string value is a valid CSS color
--   handles #xyz, #xxyyzz, rgba(xxx,xxx,xxx), <browser color name>
--
--   This function should *probably* just use regular expressions instead
isCSSColor :: String -> Bool
isCSSColor color = isPoundCSS || isRGBACSS || isValidCSSColorName
    where
      charCount = length color

      startsWithPound = "#" `isPrefixOf` color
      charCountOfPoundValue = charCount == 4 || charCount == 7
      isPoundCSS = startsWithPound && charCountOfPoundValue

      startsWithRGBA = "rgba(" `isPrefixOf` color
      endsWithParen = ")" `isSuffixOf` color
      hasRGBALength = charCount >= 11
      rightNumberOfCommas = (==2) . length $ elemIndices ',' color
      isRGBACSS = startsWithRGBA && hasRGBALength && rightNumberOfCommas && endsWithParen

      isValidCSSColorName = color `elem` validCSSColorNames

-- | Valid CSS color names
validCSSColorNames :: [String]
validCSSColorNames = [ "aliceblue", "antiquewhite", "aqua", "aquamarine", "azure", "beige", "bisque"
                     , "black", "blanchedalmond", "blue", "blueviolet", "brown", "burlywood"
                     , "cadetblue", "chartreuse", "chocolate", "coral", "cornflowerblue", "cornsilk"
                     , "crimson", "cyan", "darkblue", "darkcyan", "darkgoldenrod", "darkgray"
                     , "darkgreen", "darkkhaki", "darkmagenta", "darkolivegreen", "darkorange", "darkorchid"
                     , "darkred", "darksalmon", "darkseagreen", "darkslateblue", "darkslategray", "darkturquoise"
                     , "darkviolet", "deeppink", "deepskyblue", "dimgray", "dodgerblue", "firebrick"
                     , "floralwhite", "forestgreen", "fuchsia", "gainsboro", "ghostwhite", "gold"
                     , "goldenrod", "gray", "green", "greenyellow", "honeydew", "hotpink"
                     , "indianred", "indigo", "ivory", "khaki", "lavender", "lavenderblush"
                     , "lawngreen", "lemonchiffon", "lightblue", "lightcoral", "lightcyan", "lightgoldenrodyellow"
                     , "lightgrey", "lightgreen", "lightpink", "lightsalmon", "lightseagreen", "lightskyblue"
                     , "lightslategray", "lightsteelblue", "lightyellow", "lime", "limegreen", "linen"
                     , "magenta", "maroon", "mediumaquamarine", "mediumblue", "mediumorchid", "mediumpurple"
                     , "mediumseagreen", "mediumslateblue", "mediumspringgreen", "mediumturquoise", "mediumvioletred", "midnightblue"
                     , "mintcream", "mistyrose", "moccasin", "navajowhite", "navy", "oldlace"
                     , "olive", "olivedrab", "orange", "orangered", "orchid", "palegoldenrod"
                     , "palegreen", "paleturquoise", "palevioletred", "papayawhip", "peachpuff", "peru"
                     , "pink", "plum", "powderblue", "purple", "red", "rosybrown"
                     , "royalblue", "saddlebrown", "salmon", "sandybrown", "seagreen", "seashell"
                     , "sienna", "silver", "skyblue", "slateblue", "slategray", "snow"
                     , "springgreen", "steelblue", "tan", "teal", "thistle", "tomato"
                     , "turquoise", "violet", "wheat", "white", "whitesmoke", "yellow", "yellowgreen"
                     ]

analyticsBucketBy :: AnalyticsPeriod -> [ModelWithID URLBounceInfo] -> HMS.HashMap DDT.DateTime [ModelWithID URLBounceInfo]
analyticsBucketBy period = HMS.fromListWith (<>) . (makeBucketEntry period <$>)

analyticsBucketCountsBy :: AnalyticsPeriod -> [ModelWithID URLBounceInfo] -> HMS.HashMap DDT.DateTime (EntityCount URLBounceInfo)
analyticsBucketCountsBy period = (intToEntityCount <$>) . (length <$>) . analyticsBucketBy period

makeBucketEntry :: AnalyticsPeriod -> ModelWithID URLBounceInfo -> (DDT.DateTime, [ModelWithID URLBounceInfo])
makeBucketEntry Daily  binfo@(ModelWithID _ URLBounceInfo{bouncedAt}) = (bouncedAt {utctDayTime=0}, [binfo])
makeBucketEntry Hourly binfo@(ModelWithID _ URLBounceInfo{bouncedAt}) = (bouncedAt {utctDayTime=seconds - afterHourDiff}, [binfo])
    where
      afterHourDiff = secondsToDiffTime $ secondsInt `mod` 60
      seconds       = utctDayTime bouncedAt
      secondsInt    = floor $ toRational seconds

-- | Helper for converting A plain integer to an entity count
intToEntityCount :: forall a. Int -> EntityCount a
intToEntityCount v = EntityCount $ let result = natural (toInteger v)
                                   in if result == indeterm then 0 else result
