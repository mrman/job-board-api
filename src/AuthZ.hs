{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module AuthZ ( ensureUserIsAdminOrCompanyRep
             , representsCompanyWithID
             , requireRole
             ) where

import           Control.Monad.Except (MonadError, when)
import           Control.Monad.State (liftIO)
import           Control.Monad.Catch (handle)
import           Data.Maybe (fromJust)
import           Servant (throwError)
import           Servant.Server (Handler, ServerError)

import           AuthN (sessionUserID, sessionUserRole)
import           Util (throwServantErrorIfLeft, throwServantErrorIf, servantConvertErr)
import           Types ( UserID
                       , CompanyID
                       , SessionInfo
                       , Role(..)
                       , ModelWithID(..)
                       , HasDBBackend(..)
                       , PaginatedList(..)
                       , DatabaseBackend(..)
                       , CompanyStore(..)
                       , EntityStore(..)
                       )
import qualified Errors as Err

ensureUserIsAdminOrCompanyRep :: ( HasDBBackend m db, MonadError ServerError m, CompanyStore db m )
                                 => CompanyID -> SessionInfo -> m ()
ensureUserIsAdminOrCompanyRep cid sessionInfo = userId `representsCompanyWithID` cid
                                                >>= \isRep -> throwServantErrorIf (isNotAdmin && not isRep) Err.unauthorized
    where
      userId = sessionUserID sessionInfo
      isNotAdmin = (/= Administrator) $ sessionUserRole sessionInfo

representsCompanyWithID :: ( HasDBBackend m db, MonadError ServerError m, CompanyStore db m )
                           => UserID -> CompanyID -> m Bool
representsCompanyWithID uid cid = getDBBackend
                                  >>= flip getCompaniesRepresentedByUserWithID uid
                                  >>= throwServantErrorIfLeft
                                  >>= pure . (cid `elem`) . (mId <$>) . items

requireRole :: MonadError ServerError m => Role -> SessionInfo -> m ()
requireRole r sessionInfo = throwServantErrorIf (sessionUserRole sessionInfo /= r) Err.unauthorized
