{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE TypeOperators       #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE FlexibleContexts   #-}
{-# LANGUAGE LambdaCase   #-}
{-# LANGUAGE ConstraintKinds   #-}

module BounceServer ( Routes
                    , server
                    ) where

import           AuthN (FullRequest)
import           Control.Monad.Except (MonadError)
import           Control.Monad.State (liftIO)
import           Control.Exception (Exception)
import           Control.Monad.Catch (handle)
import           Network.Wai (Request, requestHeaderUserAgent, requestHeaderReferer, remoteHost)
import           Servant
import           Types
import           Util (ifNothingThrowError, throwServantErrorIfLeft, servantConvertErr)
import qualified Data.ByteString.Char8 as B8
import qualified Data.DateTime as DDT
import qualified Errors as Err

type Routes = "bounce" :> Capture "name" String :> FullRequest :> Get '[PlainText] String
    :<|> "bounce" :> "by-id" :> Capture "id" URLBounceConfigID :> FullRequest :> Get '[PlainText] String

type SupportsBounceAPI db m = (HasDBBackend m db, MonadError ServerError m, URLBounceConfigStore db m)

server :: SupportsBounceAPI db m => ServerT Routes m
server = doBounceByName
         :<|> doBounceByID

doBounceByName :: SupportsBounceAPI db m => String -> Request -> m String
doBounceByName bname req = getDBBackend
                           >>= flip findURLBounceConfigByName bname
                           >>= throwServantErrorIfLeft
                           >>= doBounce req

doBounceByID :: SupportsBounceAPI db m => URLBounceConfigID -> Request -> m String
doBounceByID bid req = getDBBackend
                       >>= flip findURLBounceConfigByID bid
                       >>= throwServantErrorIfLeft
                       >>= doBounce req

doBounce :: SupportsBounceAPI db m => Request -> ModelWithID URLBounceConfig ->  m String
doBounce req (ModelWithID cid c) = getDBBackend
                                   >>= \backend -> liftIO DDT.getCurrentTime
                                   >>= \now -> saveURLBounceInfo backend (makeURLBounceInfoFromRequest cid now req)
                                   -- Perform redirect
                                   >> throwError err302 { errHeaders = [("Location", B8.pack (bounceTargetUrl c))] }

makeURLBounceInfoFromRequest :: ForeignKey -> DDT.DateTime -> Request -> URLBounceInfo
makeURLBounceInfoFromRequest cid now r =
    let host      = show (remoteHost r)
        referer   = B8.unpack <$> requestHeaderReferer r
        userAgent = B8.unpack <$> requestHeaderUserAgent r
    in URLBounceInfo host now referer userAgent cid
