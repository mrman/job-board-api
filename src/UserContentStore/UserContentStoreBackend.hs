{-# LANGUAGE OverloadedStrings      #-}
{-# LANGUAGE DataKinds              #-}
{-# LANGUAGE MultiParamTypeClasses  #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE FlexibleInstances      #-}

module UserContentStore.UserContentStoreBackend ( makeUserContentStoreBackend
                                                , UserContentStoreBackend(..)
                                                , UserContentStore(..)
                                                , UserContentStoreM(..)
                                                , HasUCSBackend(..)
                                                ) where

import           Config (UserContentStoreConfig(..))
import           Control.Exception (SomeException(..), AssertionFailed(..), try)
import           Control.Monad.IO.Class
import           Data.ByteString.Base64 (decode)
import           Data.Digest.Pure.SHA
import           Data.Maybe (fromJust, isJust, isNothing)
import           Data.UUID (toString)
import           Data.UUID.V4 (nextRandom)
import           Network.URI (URI(..), uriPath, uriScheme)
import           System.Directory (createDirectoryIfMissing, makeAbsolute, doesPathExist)
import           System.FilePath ((</>), (<.>))
import           System.FilePath.Posix(takeBaseName)
import           System.Log.Logger (Logger)
import           Types (HasLogger(..))
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as BSC8
import qualified Data.ByteString.Lazy.Char8 as BSL8

data UserContentStoreBackend = LocalUserContentStore (Maybe Logger) UserContentStoreConfig

type FileExtension = String

imagesFolderName :: String
imagesFolderName = "images"

makeUserContentStoreBackend :: MonadIO m => UserContentStoreConfig -> Maybe Logger -> m (Either SomeException UserContentStoreBackend)
makeUserContentStoreBackend cfg maybeL = liftIO $ try $ return $ LocalUserContentStore maybeL cfg

dataScheme :: String
dataScheme = "data:"

-- FIXME: guards instead of this garbage...
parseURIPathForImage :: String -> Maybe (String, FileExtension)
parseURIPathForImage ('i':'m':'a':'g':'e':'/':'p':'n':'g':';':'b':'a':'s':'e':'6':'4':',':rest)     = Just (rest, ".png")
parseURIPathForImage ('i':'m':'a':'g':'e':'/':'j':'p':'g':';':'b':'a':'s':'e':'6':'4':',':rest)     = Just (rest, ".jpg")
parseURIPathForImage ('i':'m':'a':'g':'e':'/':'j':'p':'e':'g':';':'b':'a':'s':'e':'6':'4':',':rest) = Just (rest, ".jpeg")
parseURIPathForImage ('i':'m':'a':'g':'e':'/':'g':'i':'f':';':'b':'a':'s':'e':'6':'4':',':rest)     = Just (rest, ".gif")
parseURIPathForImage _ = Nothing

isImageDataURI :: URI -> Bool
isImageDataURI u = scheme == dataScheme && isJust (parseURIPathForImage path)
    where
      scheme = uriScheme u
      path = uriPath u

assertionFailedLeft :: String -> Either SomeException a
assertionFailedLeft = Left . SomeException . AssertionFailed

packAndDecode :: String -> BS.ByteString
packAndDecode = either (error . show) id . decode . BSC8.pack

writeURIDataToFile :: MonadIO m => FilePath -> BS.ByteString -> m (Either SomeException ())
writeURIDataToFile absPath = liftIO . try . BSC8.writeFile absPath

-- | Save Image data from a URI to a file on disk, returning the absolute path to the file on disk
saveImageDataURIToLocalFile :: MonadIO m => FilePath -> URI -> m (Either SomeException FilePath)
saveImageDataURIToLocalFile dir = maybe returnParseFailure (liftIO . uploadImage) . parseURIPathForImage . uriPath
    where
      returnParseFailure = return (assertionFailedLeft "Failed to parse image data URI")

      uploadImage (uriData, fileExt) = makeAbsolute dir
                                       -- Create the directory if it's not present
                                       >>= \absDir -> createDirectoryIfMissing True absDir
                                       -- Pack and decode the base64 binary data
                                       >> pure (packAndDecode uriData)
                                       -- Create a filename by SHA1 hashing the data
                                       >>= \binData -> pure (sha1 (BSL8.fromStrict binData))
                                       -- Build absolute path from the hash
                                       >>= pure . (absDir </>) . (<.>fileExt) . show
                                       -- If the filename exists already return it without doing any work, otherwise write the data
                                       >>=  \absPath -> doesPathExist absPath
                                       >>= \exists -> if exists
                                                      then pure (Right absPath)
                                                      else writeToDisk absPath binData

      writeToDisk path bin = writeURIDataToFile path bin
                             >>= pure . either (assertionFailedLeft . show) (pure (Right path))

-- | Basic requirements for a User Content Store
class UserContentStore ucs where
    getUserContentStoreLogger :: ucs -> Maybe Logger

    -- | Get the internal image storage location
    getImageStorageLocation :: ucs -> String

    -- | Get base URL that should be used for the public hosting of the image
    -- this is used after uploading to reflect where the image can be downloaded from
    getImagePublicBaseURL :: ucs -> String

    -- | Get the scheme to use for internal files (ex. file://)
    getScheme :: ucs -> String

    -- | Get the scheme (ex. https, http) to use
    getPublicScheme :: ucs -> String

    -- | Convert a filename to an external image URL
    convertFileNameToExternalImageURL :: ucs -> String -> String

-- | Requirements for a user content store in a monadic context
class (UserContentStore ucs, Monad m) => UserContentStoreM ucs m where
    -- ^ Convert a resource from another URI to the URI predominantly used by the content store (ex. data URI -> local disk URI)
    convertAndStoreImageDataURI :: ucs -> URI -> m (Either SomeException URI)

instance UserContentStore UserContentStoreBackend where
    getUserContentStoreLogger (LocalUserContentStore l _) = l

    getImageStorageLocation (LocalUserContentStore _ c) = ucsLocalBaseDir c </> imagesFolderName
    getImagePublicBaseURL (LocalUserContentStore _ c) = ucsLocalPublicBaseURL c </> imagesFolderName

    getScheme (LocalUserContentStore _ c) = ucsScheme c
    getPublicScheme (LocalUserContentStore _ c) = ucsPublicScheme c

    convertFileNameToExternalImageURL s = (getImagePublicBaseURL s</>)

instance MonadIO m => UserContentStoreM UserContentStoreBackend m where
    convertAndStoreImageDataURI s@(LocalUserContentStore _ _) uri
        | isImageDataURI uri = saveImageDataURIToLocalFile folder uri
                               >>= \uri -> pure $ case uri of
                                                    Left err -> Left err
                                                    Right fpath -> makeExternalURL fpath
        | otherwise          = return $ assertionFailedLeft "Unrecognized data URI"
          where
            folder = getImageStorageLocation s
            publicScheme = getPublicScheme s
            makeExternalURL path = Right URI { uriScheme=publicScheme
                                             , uriAuthority=Nothing
                                             , uriPath=convertFileNameToExternalImageURL s $ takeBaseName path
                                             , uriQuery=""
                                             , uriFragment=""
                                             }


instance HasLogger UserContentStoreBackend where
    getComponentLogger (LocalUserContentStore l _) = l

class (MonadIO m, UserContentStoreM c m) => HasUCSBackend m c | m -> c where
    getUCSBackend :: m c
