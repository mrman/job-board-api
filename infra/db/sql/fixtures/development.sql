-- Fixture for local development
INSERT OR REPLACE INTO addresses VALUES (1, '123 Sesame Street', 'Big red building', '1123-123', 'New York', 'NY', 'USA');

-- NOTE: The API must be running at localhost:5001 for the logo and background images to resolve properly for this company
INSERT OR REPLACE INTO companies VALUES (1, 'Example.Com', 'An example startup', 'Small and scrappy!', 'http://www.example.com', '//localhost:5001/ucs/images/654e64a0-a148-410e-8b06-e7e8ac2b44b6.png', '//localhost:5001/ucs/images/8aa2031e-9d31-49b7-9b8c-59969e0271f2.jpeg', '#285269', '2017-01-01 12:00', '2017-01-01 12:00', 1);

INSERT OR REPLACE INTO users VALUES (1, 'test', 'user', 'hello@example.com', '$2y$12$HySHROPYTIrLarC2Lyudteq0DfHL3a.9FYE4Mj78MmHcl85AtgbJ2','$2y$12$HySHROPYTIrLarC2Lyudte', 'Administrator', '2017-01-01 12:00', 2, '2017-01-01 12:00'); -- pass: 'test'
INSERT OR REPLACE INTO permissions VALUES (1, 'ManageJobPostings', 'Administrator', 1);

-- Create url bounce configs
INSERT OR REPLACE INTO url_bounce_configs VALUES (1, '584e65c7-0a07-4a1e-9675-1f3f3ad4b218', 'http://www.example.com/some-job', 1, '2017-01-01 12:00');
INSERT OR REPLACE INTO url_bounce_configs VALUES (2, 'dad70ddd-6f6a-4df8-a5ac-bef4b996e08d', 'http://www.example.com/another-job', 1, '2017-01-01 13:00');

-- Create jobs
INSERT OR REPLACE INTO jobs VALUES (1, "Frontend Engineer", "Help revolutionize our VueJS frontend!", "IT", "FullTime", 20000, 40000, "USD", "2017-01-01 12:00", 1, 1, "http://www.example.com?position=frontend", 1, '[Tag {tagName = "frontend", tagNames = fromList [], tagCSSColor = "gray", tagCreatedAt = 2017-12-03 10:20:53.492 UTC},Tag {tagName = "html", tagNames = fromList [], tagCSSColor = "#E34C26", tagCreatedAt = 2017-12-03 10:21:01.459 UTC},Tag {tagName = "css", tagNames = fromList [], tagCSSColor = "#1758A7", tagCreatedAt = 2017-12-03 10:21:01.459 UTC}]', NULL);
INSERT OR REPLACE INTO jobs VALUES (2, "Backend Engineer", "Help write the Haskell app of your dreams!", "IT", "Contract", 2000, 5000, "USD", "2017-01-01 12:00", 1, 1, "http://www.example.com?position=backend", 2, '[Tag {tagName = "backend", tagNames = fromList [], tagCSSColor = "black", tagCreatedAt = 2017-12-03 10:21:01.459 UTC},Tag {tagName = "haskell", tagNames = fromList [], tagCSSColor = "#5E5086", tagCreatedAt = 2017-12-03 10:21:01.459 UTC}]', NULL);

-- Add posting requests for new jobs
INSERT OR REPLACE INTO job_posting_requests VALUES (1, 'Haskell developer', 'This is a contract position', 'IT', 'Contract', null, null, null, 'joe@somewhere.com', 'somewhere.com/apply-for-the-awesome-job', null, null, '2017-05-04 06:16:03.256');
INSERT OR REPLACE INTO job_posting_requests VALUES (2, 'API Developer job' ,'Full time sale position here!', 'IT', 'FullTime', null, null, null, 'misako@somewhere.jp', 'somewhere.jp/apply-to-our-awesome-job', null, null, '2017-05-04 06:18:52.433');
INSERT OR REPLACE INTO job_posting_requests VALUES (2, 'Job with long description' ,'Qui esse et dolores nulla. Quibusdam cupiditate aliquam repellendus soluta provident quam doloremque. Vero impedit deserunt dignissimos aut quia aut laborum. Velit eius non sit perspiciatis laboriosam voluptas. Qui esse et dolores nulla. Quibusdam cupiditate aliquam repellendus soluta provident quam doloremque. Vero impedit deserunt dignissimos aut quia aut laborum. Velit eius non sit perspiciatis laboriosam voluptas.', 'BizDev', 'FullTime', null, null, null, 'george@somewhere.jp', 'somewhere.jp/apply-to-our-awesome-job-2', null, null, '2017-05-04 06:18:52.433');

-- Add a company-representative account
INSERT OR REPLACE INTO users VALUES(2, 'company', 'rep', 'rep@example.com', '$2y$12$HySHROPYTIrLarC2Lyudteq0DfHL3a.9FYE4Mj78MmHcl85AtgbJ2','$2y$12$HySHROPYTIrLarC2Lyudte', 'CompanyRepresentative', '2017-01-01 12:00', 2, '2017-01-01 12:00'); -- pass: 'test'

-- Add a recruiter account
INSERT OR REPLACE INTO users VALUES(3, 'recruiting', 'recruiter', 'recruiter@example.com', '$2y$12$HySHROPYTIrLarC2Lyudteq0DfHL3a.9FYE4Mj78MmHcl85AtgbJ2','$2y$12$HySHROPYTIrLarC2Lyudte', 'Recruiter', '2017-01-01 12:00', 2, '2017-01-01 12:00'); -- pass: 'test'

-- Make the test user a representative of Example.Com
INSERT OR REPLACE INTO companies_representatives VALUES (1, 2, datetime('now', 'utc'));

-- Make some unresolved support requests
INSERT OR REPLACE INTO support_requests VALUES (1, 'General', 'Something is broken', 'When I open the site, nothing shows up.', 1, 'This was a temporary outage caused by some bad code pushed to PROD. Fixed.', 2, '2017-02-01 12:00');
INSERT OR REPLACE INTO support_requests VALUES (1, 'General', 'Viewing jobs for my company is broken', 'When I open the jobs page, something is not right, I cannot see any of my jobs.', 0, NULL, 2, '2017-02-15 12:00');

-- Add an email subscription for the default admin user
INSERT OR REPLACE INTO email_subscriptions VALUES (1, 'hello@example.com', 'sub-code', '2017-01-02 12:05', 'unsub-code', NULL, '2017-01-02 12:00');

-- Add some tags for the email subscription
INSERT OR REPLACE INTO email_subscription_tags VALUES (1, 1, (SELECT id from tags WHERE tags.name = 'haskell'), '2017-01-01 12:00');
INSERT OR REPLACE INTO email_subscription_tags VALUES (2, 1, (SELECT id from tags WHERE tags.name = 'html'), '2017-01-01 12:00');

-- Add some URL bounces
INSERT INTO url_bounce_info (bouncedHost, bouncedAt, referer, userAgent, bounceConfigId)
VALUES ("techjobs.tokyo", DATETIME('now', 'utc'), "localhost", "test", 1);

INSERT INTO url_bounce_info (bouncedHost, bouncedAt, referer, userAgent, bounceConfigId)
VALUES ("techjobs.tokyo", DATETIME('now', 'utc'), "localhost", "test", 1);
