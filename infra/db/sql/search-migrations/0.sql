-- Create FTS table for active jobs
DROP TABLE IF EXISTS search_active_jobs;
CREATE VIRTUAL TABLE IF NOT EXISTS search_active_jobs USING fts4(title, description, industry, jobType, employerId, postingDate, applyLink, tags, lastCheckedAt, tokenize=porter);

INSERT OR REPLACE INTO search_active_jobs(docid, title, description, industry, jobType, employerId, postingDate, applyLink, tags, lastCheckedAt)
SELECT id, title, description, industry, jobType, employerId, postingDate, applyLink, tags, lastCheckedAt
FROM jobs
WHERE isActive=1;

-- Create FTS table for tags
DROP TABLE IF EXISTS search_tags;
CREATE VIRTUAL TABLE IF NOT EXISTS search_tags USING fts4(name, names, cssColor, tokenize=porter);

INSERT OR REPLACE INTO search_tags(docid, name, names, cssColor)
SELECT id, name, names, cssColor
FROM tags;
