BEGIN;
PRAGMA user_version = 13;

CREATE TABLE IF NOT EXISTS tokens ( -- Holds tokens for any use
  id INTEGER PRIMARY KEY,
  type TEXT, -- Token type (haskell types: AuthToken, PasswordResetToken, etc)
  token TEXT, -- The token itself
  creatorId INTEGER REFERENCES users(id), -- creator of the token's user ID
  createdAt TEXT, -- creation time of the token
  expiresAt TEXT -- expiry time of the token
);

CREATE INDEX IF NOT EXISTS tokens_created_at_idx on tokens(createdAt);
CREATE INDEX IF NOT EXISTS tokens_creatorId_idx on tokens(creatorId);
CREATE INDEX IF NOT EXISTS tokens_expires_at_idx on tokens(expiresAt);

COMMIT;
