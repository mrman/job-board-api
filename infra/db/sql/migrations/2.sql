BEGIN;
PRAGMA user_version = 2;

-- Create the url bounce table
CREATE TABLE IF NOT EXISTS url_bounce_configs (
       id INTEGER PRIMARY KEY,
       name TEXT UNIQUE NOT NULL,
       targetUrl TEXT UNIQUE,
       isActive INTEGER,
       createdAt TEXT);

CREATE TABLE IF NOT EXISTS url_bounce_info (
       id INTEGER PRIMARY KEY,
       bouncedHost TEXT,
       bouncedAt TEXT,
       referer TEXT,
       userAgent TEXT,
       bounceConfigId INTEGER REFERENCES url_bounce_configs(id) );

CREATE INDEX IF NOT EXISTS url_bounce_name_idx ON url_bounce_configs (name);
CREATE INDEX IF NOT EXISTS url_bounce_target_url_idx ON url_bounce_configs (targetUrl);
CREATE INDEX IF NOT EXISTS url_info_url_config_fk_idx ON url_bounce_info (bounceConfigId);

COMMIT;
