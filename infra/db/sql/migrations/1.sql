BEGIN;
PRAGMA user_version = 1;

-- Create the users table
CREATE TABLE IF NOT EXISTS users (
       id INTEGER PRIMARY KEY,
       firstName TEXT,
       lastName TEXT,
       emailAddress TEXT UNIQUE,
       password TEXT,
       policyAndSalt TEXT,
       role TEXT,
       joinedAt TEXT);

CREATE TABLE IF NOT EXISTS permissions (
       id INTEGER PRIMARY KEY,
       permissionName TEXT,
       grantedToRole TEXT,
       grantedToUser INTEGER REFERENCES users(userId),
       UNIQUE(grantedToUser, permissionName) ON CONFLICT REPLACE,
       UNIQUE(grantedToRole, permissionName) ON CONFLICT REPLACE );

CREATE TABLE IF NOT EXISTS jobs (
       id INTEGER PRIMARY KEY,
       title TEXT,
       description TEXT,
       industry TEXT,
       jobType TEXT,
       minSalary INTEGER,
       maxSalary INTEGER,
       salaryCurrency TEXT,
       postingDate TEXT,
       employerId INTEGER REFERENCES companies(id),
       isActive INTEGER,
       applyLink TEXT );

CREATE TABLE IF NOT EXISTS job_posting_requests (
       id INTEGER PRIMARY KEY,
       title TEXT,
       description TEXT,
       industry TEXT,
       jobType TEXT,
       minSalary INTEGER,
       maxSalary INTEGER,
       salaryCurrency TEXT,
       posterEmail TEXT,
       applyLink TEXT,
       companyId INTEGER,
       approvedJobId INTEGER,
       requestedAt TEXT);

CREATE TABLE IF NOT EXISTS addresses (
       id INTEGER PRIMARY KEY,
       addressLine1 TEXT,
       addressLine2 TEXT,
       zipcode TEXT,
       town TEXT,
       state TEXT,
       country TEXT );

CREATE TABLE IF NOT EXISTS company_info (
       id INTEGER PRIMARY KEY,
       companyId INTEGER REFERENCES companies(companyId),
       description TEXT,
       cultureDescription TEXT,
       homepageUrl TEXT,
       companyIconURL TEXT,
       companyBannerImageURL TEXT );

CREATE TABLE IF NOT EXISTS companies (
       id INTEGER PRIMARY KEY,
       name TEXT,
       address INTEGER REFERENCES addresses(addressId) );

-- Create Indices
CREATE UNIQUE INDEX IF NOT EXISTS emailAddress_idx ON users (emailAddress);

CREATE INDEX IF NOT EXISTS active_idx ON jobs (isActive);
CREATE INDEX IF NOT EXISTS industry_idx ON jobs (industry);
CREATE INDEX IF NOT EXISTS jobType_idx ON jobs (jobType);
CREATE INDEX IF NOT EXISTS postingDate_idx ON jobs (postingDate);
CREATE INDEX IF NOT EXISTS minSalary_idx ON jobs (minSalary);
CREATE INDEX IF NOT EXISTS maxSalary_idx ON jobs (maxSalary);

CREATE INDEX IF NOT EXISTS permissionUserGrant_idx ON permissions (grantedToUser);
CREATE INDEX IF NOT EXISTS permissionRoleGrant_idx ON permissions (grantedToRole);

COMMIT;
