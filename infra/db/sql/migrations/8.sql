BEGIN;
PRAGMA user_version = 8;

-- Rename icon and bannerimage "url"s to "uri"s
CREATE TABLE IF NOT EXISTS companies_new
(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,          -- Name of the company
  description TEXT NOT NULL,   -- Description of the company
  cultureDescription TEXT,     -- Culture description of the company
  homepageUrl TEXT,            -- URL homepage of the company
  iconUri TEXT,         -- Base64 encoded company icon
  bannerImageUri TEXT,  -- Base64 encoded company banner image
  primaryColorCSS TEXT, -- Primary color of the company, as a CSS-readable color value
  createdAt TEXT,
  lastUpdatedAt TEXT,
  addressId INTEGER REFERENCES addresses(addressId)
);

-- Re-add all the companies from the old table to the new one
INSERT OR ABORT INTO companies_new SELECT * FROM companies;

DROP TABLE IF EXISTS companies;
ALTER TABLE companies_new RENAME TO companies;

-- Re create all old indices
CREATE INDEX IF NOT EXISTS companies_name_idx ON companies (name);
CREATE INDEX IF NOT EXISTS companies_description_idx ON companies (description);
CREATE INDEX IF NOT EXISTS companies_homepage_url_idx ON companies (homepageUrl);

-- Add trigger to automatically update createdAt for job_posting_requests
CREATE TRIGGER IF NOT EXISTS job_posting_requests_requested_at_trigger
AFTER INSERT ON job_posting_requests
FOR EACH ROW
BEGIN
  UPDATE job_posting_requests SET requestedAt = datetime('now', 'utc') WHERE id = new.id;
END;

-- Add trigger to automatically update created for companies
CREATE TRIGGER IF NOT EXISTS companies_created_at_trigger
AFTER INSERT ON companies
FOR EACH ROW
BEGIN
  UPDATE companies SET createdAt = datetime('now', 'utc') WHERE id = new.id;
END;

-- Add trigger to automatically update lastUpdatedAt for companies if it doesn't come in different
CREATE TRIGGER IF NOT EXISTS companies_updated_at_trigger
AFTER UPDATE ON companies
FOR EACH ROW WHEN old.lastUpdatedAt = new.lastUpdatedAt
BEGIN
  UPDATE companies SET lastUpdatedAt = datetime('now', 'utc') WHERE id = old.id;
END;

COMMIT;
