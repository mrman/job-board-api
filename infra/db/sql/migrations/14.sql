BEGIN;
PRAGMA user_version = 14;

INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (1, "frontend", "fromList []", "gray", "2017-12-03 10:20:53.492");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (2, "backend", "fromList []", "black", "2017-12-03 10:21:01.459");

INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (3, "javascript", "fromList []", "#f7df1e", "2017-12-03 10:20:53.492");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (4, "html", "fromList []", "#E34C26", "2017-12-03 10:21:01.459");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (5, "css", "fromList []", "#1758A7", "2017-12-03 10:21:01.459");

INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (6, "nodejs", "fromList []", "#669F64", "2017-12-03 10:21:01.459");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (7, "python", "fromList []", "#3771A1", "2017-12-03 10:21:01.459");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (8, "golang", "fromList []", "#5ADAFF", "2017-12-03 10:21:01.459");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (9, "ruby", "fromList []", "#AD1401", "2017-12-03 10:21:01.459");

INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (10, "reactjs", "fromList []", "#61DAFB", "2017-12-03 10:21:01.459");
INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (11, "vuejs", "fromList []", "#41B883", "2017-12-03 10:21:01.459");

INSERT OR REPLACE INTO tags (id, name, names, cssColor, createdAt) VALUES (12, "haskell", "fromList []", "#5E5086", "2017-12-03 10:21:01.459");

COMMIT;
