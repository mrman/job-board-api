BEGIN;
PRAGMA user_version = 7;

CREATE TABLE IF NOT EXISTS addresses_new
(
  id INTEGER PRIMARY KEY,
  addressLine1 TEXT,      -- First line of the address
  addressLine2 TEXT,      -- Second line of the address
  zipcode TEXT,           -- Zip code
  town TEXT,              -- Town/City
  state TEXT,             -- State/Province
  country TEXT,           -- Country
  CONSTRAINT addresses_uniq UNIQUE (addressLine1, addressLine2, zipcode, town, state, country)
);

INSERT OR ROLLBACK INTO addresses_new SELECT id, addressLine1, addressLine2, zipcode, town, state, country FROM addresses;

DROP TABLE IF EXISTS addresses;
ALTER TABLE addresses_new RENAME TO addresses;

CREATE INDEX IF NOT EXISTS addresses_country_idx ON addresses (country);
CREATE INDEX IF NOT EXISTS addresses_state_idx ON addresses (state);
CREATE INDEX IF NOT EXISTS addresses_town_idx ON addresses (town);
CREATE INDEX IF NOT EXISTS addresses_zipcode_idx ON addresses (zipcode);

CREATE TABLE IF NOT EXISTS locations
(
  id INTEGER PRIMARY KEY,
  addressId iNTEGER NOT NULL UNIQUE REFERENCES addresses(id), -- ID of the corresponding (possibly partially filled) address
  name TEXT NOT NULL UNIQUE                                   -- Name of the location
);

INSERT OR REPLACE INTO addresses (id, country) VALUES (1,"United States of America");
INSERT OR REPLACE INTO locations (addressId, name) VALUES (1,"United States of America");

INSERT OR REPLACE INTO addresses (id, country) VALUES (2, "Japan");
INSERT OR REPLACE INTO locations (addressId, name) VALUES ( 2, "Japan");

-- Add home location to users table
ALTER TABLE users ADD COLUMN homeLocationId INTEGER REFERENCES locations(id);

COMMIT;
