BEGIN;
PRAGMA user_version = 4;

-- Merge company and company info tables

CREATE TABLE IF NOT EXISTS companies_new
(
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,          -- Name of the company
  description TEXT NOT NULL,   -- Description of the company
  cultureDescription TEXT,     -- Culture description of the company
  homepageUrl TEXT,            -- URL homepage of the company
  iconUrl TEXT,         -- Base64 encoded company icon
  bannerImageUrl TEXT,  -- Base64 encoded company banner image
  primaryColorCSS TEXT, -- Primary color of the company, as a CSS-readable color value
  createdAt TEXT,
  lastUpdatedAt TEXT,
  addressId INTEGER REFERENCES addresses(addressId)
);

-- Port all the companies from the old table to the new one
INSERT OR ROLLBACK INTO companies_new
  SELECT companyId, name, description, cultureDescription, homepageUrl,
         companyIconUrl, companyBannerImageUrl, address, NULL, datetime('now', 'utc'), datetime('now', 'utc')
  FROM companies
  JOIN company_info ON company_info.companyId = companies.id;

DROP TABLE IF EXISTS companies;
DROP TABLE IF EXISTS company_info;

ALTER TABLE companies_new RENAME TO companies;

CREATE INDEX IF NOT EXISTS companies_name_idx ON companies (name);
CREATE INDEX IF NOT EXISTS companies_description_idx ON companies (description);
CREATE INDEX IF NOT EXISTS companies_homepage_url_idx ON companies (homepageUrl);


CREATE TABLE IF NOT EXISTS companies_representatives
(
  companyId INTEGER REFERENCES companies (id) NOT NULL, -- Company for which a representative exists
  userId INTEGER REFERENCES users (id) NOT NULL,        -- User who is a representative
  createdAt TEXT,
  CONSTRAINT companies_representatives_uniq UNIQUE (companyId, userId) ON CONFLICT IGNORE
);

CREATE INDEX IF NOT EXISTS companies_representatives_user_idx ON companies_representatives (userId);
CREATE INDEX IF NOT EXISTS companies_representatives_company_idx ON companies_representatives (companyId);

-- Rebuild  users_starred_jobs with proper constraint

DROP INDEX IF EXISTS users_favorite_jobs_starredJob_idx;
DROP INDEX IF EXISTS users_favorite_jobs_starredUser_idx;

CREATE TABLE IF NOT EXISTS users_starred_jobs_new
(
  userId INTEGER REFERENCES users(id),      -- User who favorited the job
  starredJobId INTEGER REFERENCES jobs(id), -- Job which was favorited
  createdAt TEXT,
  CONSTRAINT companies_representatives_uniq UNIQUE (userId, starredJobId) ON CONFLICT IGNORE
);

-- Port all the companies from the old table to the new one
INSERT OR ROLLBACK INTO users_starred_jobs_new
  SELECT userId, starredJobId, createdAt
  FROM users_starred_jobs;

DROP TABLE users_starred_jobs;
ALTER TABLE users_starred_jobs_new RENAME TO users_starred_jobs;

COMMIT;
