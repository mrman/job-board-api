BEGIN;
PRAGMA user_version = 15;

CREATE TABLE IF NOT EXISTS email_subscriptions
(
  id INTEGER PRIMARY KEY,
  emailAddress TEXT UNIQUE NOT NULL, -- Email address used for the subscription
  confirmationCode TEXT UNIQUE NOT NULL, -- Confirmation code emailed to the address
  confirmationDate TEXT, -- Date the subscription was confirmed
  unsubCode TEXT UNIQUE NOT NULL, -- Code to use to unsubscribe
  unsubDate TEXT, -- Date this subscription was last unsubbed from by the user
  subscribeDate TEXT NULL -- Date the user (last) subscribed
);

CREATE INDEX IF NOT EXISTS email_subscriptions_emailAddress ON email_subscriptions(emailAddress);
CREATE INDEX IF NOT EXISTS email_subscriptions_confirmationCode ON email_subscriptions(confirmationCode);
CREATE INDEX IF NOT EXISTS email_subscriptions_unsubCode ON email_subscriptions(unsubCode);
CREATE INDEX IF NOT EXISTS email_subscriptions_subscribeDate ON email_subscriptions(subscribeDate);

CREATE TABLE IF NOT EXISTS email_subscription_tags
(
  id INTEGER PRIMARY KEY,
  emailSubscriptionId INTEGER NOT NULL REFERENCES email_subscriptions(id),
  tagId INTEGER NOT NULL REFERENCES tags(id),
  createdAt TEXT
);

CREATE INDEX IF NOT EXISTS email_subscription_tags_emailSubscriptionId ON email_subscription_tags(emailSubscriptionId);
CREATE INDEX IF NOT EXISTS email_subscription_tags_tagId ON email_subscription_tags(tagId);

COMMIT;
