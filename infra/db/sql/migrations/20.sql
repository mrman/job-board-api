BEGIN;
PRAGMA user_version = 20;

ALTER TABLE tasks ADD COLUMN error TEXT;

COMMIT;
