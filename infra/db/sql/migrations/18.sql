BEGIN;
PRAGMA user_version = 18;

CREATE TABLE tasks (
  id INTEGER PRIMARY KEY,
  taskType TEXT NOT NULL,           -- Type of the task
  status TEXT NOT NULL,             -- Current status of task
  jsonData TEXT,                    -- JSON string representing the data fed to the task
  scheduledBy REFERENCES users(id), -- User who scheduled the task
  killedBy REFERENCES users(id),    -- User who scheduled the task
  runAt TEXT,                       -- When the task was run
  createdAt TEXT NOT NULL           -- When the task was created
);

COMMIT;
