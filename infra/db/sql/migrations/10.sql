BEGIN;
PRAGMA user_version = 10;

-- Add column that tracks when users were last logged in
ALTER TABLE users ADD COLUMN lastLoggedIn TEXT;

-- Fill out values
UPDATE users set lastLoggedIn=joinedAt WHERE lastLoggedIn IS NULL;

COMMIT;
