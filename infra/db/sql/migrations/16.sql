BEGIN;
PRAGMA user_version = 16;

CREATE TABLE IF NOT EXISTS promotions (
  id INTEGER PRIMARY KEY,
  name TEXT NOT NULL,                               -- User provided promotion name
  description TEXT,                                 -- User provided promotion name
  type TEXT NOT NULL,                               -- Type of sponsorship (ex. company, job, etc)
  promoterId INTEGER REFERENCES users(id) NOT NULL, -- ID of the person who bought the promotion
  promotedObjectId INTEGER NOT NULL,                   -- ID of the object (table determined by promotion type) that's being promoted
  start TEXT NOT NULL,                              -- Promotion start time
  end TEXT NOT NULL,                                -- Promotion end time
  createdBy INTEGER REFERENCES users(id),           -- Promotion creator (usually matches promoterId, could be admin instead)
  isActive INTEGER NOT NULL,                        -- Promotion's active/inactive state
  createdAt TEXT NOT NULL                           -- When the promotion was created
);

CREATE INDEX IF NOT EXISTS promotions_type ON promotions(type);
CREATE INDEX IF NOT EXISTS promotions_promoterId ON promotions(promoterId);
CREATE INDEX IF NOT EXISTS promotions_promotedObjectId ON promotions(promotedObjectId);
CREATE INDEX IF NOT EXISTS promotions_start ON promotions(start);
CREATE INDEX IF NOT EXISTS promotions_end ON promotions(end);
CREATE INDEX IF NOT EXISTS promotions_createdBy ON promotions(createdBy);
CREATE INDEX IF NOT EXISTS promotions_isActive ON promotions(isActive);
CREATE INDEX IF NOT EXISTS promotions_createdAt ON promotions(createdAt);

COMMIT;
