BEGIN;
PRAGMA user_version = 11;

-- Add column that tracks tags on jobs
ALTER TABLE jobs ADD COLUMN tags TEXT;

-- Create table to hold all tags
CREATE TABLE IF NOT EXISTS tags
(
  id INTEGER PRIMARY KEY,
  name TEXT UNIQUE, -- EN_US name of the tag
  names TEXT, -- Names in all languages (including english)
  cssColor TEXT, -- CSS color that should be used for the tag
  createdAt TEXT
);

CREATE INDEX IF NOT EXISTS tags_name_idx ON tags(name);

-- Create table for search analytics, holds every search query ever performed
CREATE TABLE IF NOT EXISTS history_job_query
(
  id INTEGER PRIMARY KEY,
  term TEXT,       -- Term specified in the query
  industries TEXT, -- Industries used in the job query
  companies TEXT,  -- Companies specified in the job query
  tags TEXT,       -- Tags used in the job query
  recordedAt TEXT  -- When the search was recorded
);

COMMIT;
