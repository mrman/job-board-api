BEGIN;
PRAGMA user_version = 5;

CREATE TABLE IF NOT EXISTS support_requests
(
  id INTEGER PRIMARY KEY,
  type TEXT NOT NULL,                        -- Type (problem area) of support request
  title TEXT NOT NULL,                       -- Title of support request
  description TEXT NOT NULL,                 -- Description of the company
  isResolved INTEGER REFERENCES users(id),   -- User that submitted the request
  resolutionNotes TEXT REFERENCES users(id), -- User that submitted the request
  submittedBy INTEGER REFERENCES users(id),  -- User that submitted the request
  createdAt TEXT NOT NULL                    -- User that submitted the request
);

CREATE INDEX IF NOT EXISTS support_request_type_idx ON support_requests (type);
CREATE INDEX IF NOT EXISTS support_request_title_idx ON support_requests (title);
CREATE INDEX IF NOT EXISTS support_request_submitted_by_idx ON support_requests (submittedBy);
CREATE INDEX IF NOT EXISTS support_request_is_resolved_idx ON support_requests (isResolved);
CREATE INDEX IF NOT EXISTS support_request_created_at_idx ON support_requests (createdAt);

COMMIT;
