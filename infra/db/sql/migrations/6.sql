BEGIN;
PRAGMA user_version = 6;

ALTER TABLE jobs ADD COLUMN urlBounceConfigId INTEGER REFERENCES url_bounce_configs(id);

CREATE INDEX IF NOT EXISTS jobs_url_bounce_config_fk_idx ON jobs (urlBounceConfigId);

COMMIT;
