BEGIN;
PRAGMA user_version = 12;

-- Ensure that every job without tags already will have an empty list
UPDATE jobs SET tags='[]' WHERE tags IS NULL;

COMMIT;
