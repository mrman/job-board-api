BEGIN;
PRAGMA user_version = 9;

-- Rename icon and bannerimage "url"s to "uri"s
CREATE TABLE IF NOT EXISTS url_bounce_configs_new
(
  id INTEGER PRIMARY KEY,
  name TEXT UNIQUE NOT NULL, -- Human readable name of the url bounce config
  targetUrl TEXT, -- Target URL for the bounce config
  isActive INTEGER, -- Whether the bounce config is active or not
  createdAt TEXT -- When the bounce config was created
);

-- Re-add all the url_bounce_configs from the old table to the new one
INSERT OR ABORT INTO url_bounce_configs_new SELECT * FROM url_bounce_configs;

DROP TABLE IF EXISTS url_bounce_configs;
ALTER TABLE url_bounce_configs_new RENAME TO url_bounce_configs;

COMMIT;
