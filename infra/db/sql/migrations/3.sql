BEGIN;
PRAGMA user_version = 3;

-- Table that contains users' favorited jobs
CREATE TABLE IF NOT EXISTS users_starred_jobs
(
  userId INTEGER REFERENCES users(id),      -- User who favorited the job
  starredJobId INTEGER REFERENCES jobs(id), -- Job which was favorited
  createdAt TEXT
);

CREATE INDEX IF NOT EXISTS users_favorite_jobs_starredUser_idx ON users_starred_jobs (userId);
CREATE INDEX IF NOT EXISTS users_favorite_jobs_starredJob_idx ON users_starred_jobs (starredJobId);

COMMIT;
