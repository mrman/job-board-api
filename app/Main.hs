module Main where

import           APIServer as API
import           Config (appBackendConfig, appMailerConfig, getDefaultOrEnvValue, getCurrentEnvConfig)
import           Data.Aeson (encode)
import           Data.Proxy
import           Mailer.MailerBackend (Mailer(..), MailTemplate(..), MailTemplateData(..))
import           Servant.Swagger (toSwagger)
import           System.Environment (getArgs)
import           Types (BrandingData(..), ServerInfo(..))
import           Util (throwIfLeft)
import qualified App
import qualified Data.ByteString.Lazy.Char8 as BSL8

data SubCommand = PrintSwaggerJSON
                | MigrateBackend
                | RenderEmailTemplate MailTemplate MailTemplateData
                | RunServer deriving (Eq, Show, Read)

class Runnable a where
    run :: a -> IO ()

instance Runnable SubCommand where
    run PrintSwaggerJSON = BSL8.putStrLn $ encode $ toSwagger (Proxy :: Proxy API.V1)
    run MigrateBackend   = getDefaultOrEnvValue "Development" "ENVIRONMENT"
                           >>= getCurrentEnvConfig
                           >>= throwIfLeft
                           >>= pure . appBackendConfig
                           >>= App.startDBBackend
                           >> return ()
    run (RenderEmailTemplate template d) = getDefaultOrEnvValue "Development" "ENVIRONMENT"
                                           >>= getCurrentEnvConfig
                                           >>= throwIfLeft
                                           >>= pure . appMailerConfig
                                           >>= \cfg -> App.startMailerBackend cfg brandingData serverInfo
                                           >>= \mailer -> pure (renderTemplate template d mailer)
                                           >>= throwIfLeft
                                           >>= print
        where
          brandingData = BrandingData "noreply@example.com" "example.com/subscribe" "example.com/unsubscribe"
          serverInfo = ServerInfo "nowhere" "nowhere" "nowhere"
    run RunServer = getDefaultOrEnvValue "Development" "ENVIRONMENT"
                    >>= getCurrentEnvConfig
                    >>= throwIfLeft
                    >>= App.startApp

main :: IO ()
main = do
  args <- getArgs
  case args of
    (cmd:_) -> run (read cmd :: SubCommand)
    _ -> error "usage: job-board-api < RunServer | MigrateBackend | PrintSwaggerJSON >"
