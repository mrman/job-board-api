{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE LambdaCase            #-}

module JobsSpec ( spec ) where

import           Config (defaultTestConfig)
import           Control.Monad.State (when)
import           Data.Aeson.Types (toJSON)
import           Data.Proxy (Proxy(..))
import           Servant.API
import           Servant.Client (client, Client, ClientEnv, parseBaseUrl, runClientM, ClientM)
import           Servant.Client.Core.Auth (AuthenticatedRequest)
import           ServantTestUtil (cookieAuthRequest, doUserLogin, buildClientEnv)
import           Test.Hspec
import           TestFixtures (testJPR, testCompany, adminUserAndPassword)
import           TestUtil (withAppInstance, shouldBeRight, TestAppInstanceInfo)
import           Types
import qualified API.V1.Companies as CompaniesAPI
import qualified API.V1.Jobs as JobsAPI
import qualified Data.ByteString.Char8 as B8
import qualified Data.HashMap.Strict as HMS
import qualified Data.Text as DT

-- | Generated client functions for the Jobs API
createJobPostingRequest :: JobPostingRequest -> ClientM (EnvelopedResponse (ModelWithID JobPostingRequest))
findJobByID :: JobID -> ClientM (EnvelopedResponse (ModelWithID Job))
patchJobByID :: AuthenticatedRequest (AuthProtect "cookie-auth")
                -> JobID
                -> MergePatchChangeObject
                -> ClientM (EnvelopedResponse (ModelWithID Job))
( createJobPostingRequest
  :<|> listJobPostingRequests
  :<|> listPendingJobPostingRequests
  :<|> findJobPostingRequestByID
  :<|> patchJobPostingRequestByID
  :<|> approveJPR
  :<|> findJobByID
  :<|> changeJobActivityEnable
  :<|> changeJobActivityDisable
  :<|> jobSearch
  :<|> jobSearchAdmin
  :<|> updateJobByID
  :<|> patchJobByID ) = client (Proxy :: Proxy JobsAPI.Routes)

-- | Generated client functions for the Company API
createCompany :: AuthenticatedRequest (AuthProtect "cookie-auth")
              -> Company
              -> ClientM (EnvelopedResponse (ModelWithID Company))
( allCompanies
  :<|> findCompanyByID
  :<|> createCompany
  :<|> allRepresentativesForCompany
  :<|> addRepresentativeFromCompany
  :<|> removeRepresentativeFromCompany
  :<|> findRepresentedCompaniesByRepID
  :<|> companyStats
  :<|> listJobsForCompany
  :<|> activateJobForCompany
  :<|> disableJobForCompany
  :<|> industries ) = client (Proxy :: Proxy CompaniesAPI.Routes)


-- End to end tests are for the whole application
spec :: Spec
spec = around (withAppInstance defaultTestConfig) tests
    where
      tests = jprCachingTests
              >> jobPatchingTests

jprCachingTests :: SpecWith TestAppInstanceInfo
jprCachingTests = describe "JPR Caching" promotionTriggerTest

promotionTriggerTest :: SpecWith TestAppInstanceInfo
promotionTriggerTest = it "should be triggered after a JPR is promoted" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Create a company to be the employer
                             >> runClientM (createCompany authenticatedReq testCompany) clientEnv
                             >>= shouldBeRight
                             >>= \createCompanyResp -> when (status createCompanyResp /= "success") (error "Failed to create company")
                             -- | Create the job posting
                             >> let jpr = testJPR { rCompanyId=Just $ mId $ respData createCompanyResp }
                                               in runClientM (createJobPostingRequest jpr) clientEnv
                             >>= shouldBeRight
                             >>= \createJPRResp -> when (status createJPRResp /= "success") (error "JPR creation failed")
                             -- | Get the available jobs (job search with no query/filters/etc)
                             >> runClientM (jobSearch Nothing [] [] Nothing Nothing []) clientEnv
                             >>= shouldBeRight
                             >>= \originalJobsResp -> when (status originalJobsResp /= "success") (error "Job retrieval failed")
                             -- | Approve the JPR, using a fake SessionInfo for the admin user
                             >> let jprId     = mId $ respData createJPRResp
                                in runClientM (approveJPR authenticatedReq jprId) clientEnv
                             >>= shouldBeRight
                             >>= \approvedJPRResp -> when (status approvedJPRResp /= "success") (error "JPR approval failed")
                             -- | Retrieve the available jobs again
                             >> runClientM (jobSearch Nothing [] [] Nothing Nothing []) clientEnv
                             >>= shouldBeRight
                             >>= \updatedJobsResp -> when (status updatedJobsResp /= "success") (error "Updated retrieval failed")
                             -- | Ensure that totals changed, the approved job should have shown up (total should now be 1)
                             --   which means the cache should have been invalidated
                             >> let totalAfterUpdate = total $ respData updatedJobsResp
                                    originalTotal    = total $ respData originalJobsResp
                                in originalTotal `shouldNotBe` totalAfterUpdate

-- Job patching tests

jobPatchingTests :: SpecWith TestAppInstanceInfo
jobPatchingTests = describe "Job Patching" simpleJobTitlePatch

-- | Test whether patching the title of a job works
simpleJobTitlePatch :: SpecWith TestAppInstanceInfo
simpleJobTitlePatch = it "works for a title change" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Create a company to be the employer
                             >> runClientM (createCompany authenticatedReq testCompany) clientEnv
                             >>= shouldBeRight
                             >>= \createCompanyResp -> when (status createCompanyResp /= "success") (error "Failed to create company")
                             -- | Create the job posting
                             >> let jpr = testJPR { rCompanyId=Just $ mId $ respData createCompanyResp }
                                               in runClientM (createJobPostingRequest jpr) clientEnv
                             >>= shouldBeRight
                             >>= \createJPRResp -> when (status createJPRResp /= "success") (error "JPR creation failed")
                             -- | Approve the JPR, using a fake SessionInfo for the admin user
                             >> let jprId     = mId $ respData createJPRResp
                                in runClientM (approveJPR authenticatedReq jprId) clientEnv
                             >>= shouldBeRight
                             >>= \approvedJPRResp -> when (status approvedJPRResp /= "success") (error "JPR approval failed")
                             -- | Retrieve the jobs by ID (should have same title as JPR)
                             >> let jobId = mId $ respData approvedJPRResp
                                in runClientM (findJobByID jobId) clientEnv
                             >>= shouldBeRight
                             >>= \approveJobResp -> when (status approveJobResp /= "success") (error "Post-approve Job retrieval failed")
                             -- | Check that the job title starts out the same as the JPR
                             >> let jprTitle = rTitle $ model $ respData createJPRResp
                                    jobTitle = title $ model $ respData approveJobResp
                                in when (jprTitle /= jobTitle) (error "Approved job title does not match JPR")
                             -- | Patch the job's title
                             >> let newTitle = "new title"
                                    update    = HMS.singleton "title" (toJSON @String newTitle)
                                in runClientM (patchJobByID authenticatedReq jobId update) clientEnv
                             -- | Retrieve the job by ID again (should be new title as JPR)
                             >> runClientM (findJobByID jobId) clientEnv
                             >>= shouldBeRight
                             >>= \updatedJobResp -> when (status updatedJobResp /= "success") (error "Updated job retrieval failed")
                             -- | Ensure that totals changed, the approved job should have shown up (total should now be 1)
                             --   which means the cache should have been invalidated
                             >> let titleAfterUpdate = title $ model $ respData updatedJobResp
                                in titleAfterUpdate `shouldBe` newTitle
