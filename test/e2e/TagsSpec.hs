{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE LambdaCase            #-}

module TagsSpec ( spec ) where

import           Config (defaultTestConfig)
import           Control.Monad.State (when)
import           Data.Aeson.Types (toJSON)
import           Data.Proxy (Proxy(..))
import           Servant.API
import           Servant.Client (client, Client, ClientEnv, parseBaseUrl, runClientM, ClientM)
import           Servant.Client.Core.Auth (mkAuthenticatedRequest, AuthenticatedRequest)
import           ServantTestUtil (cookieAuthRequest, doUserLogin, buildClientEnv)
import           Test.Hspec
import           TestFixtures (testTag, adminUserAndPassword)
import           TestUtil (withAppInstance, containsLeftValue, shouldBeRight, TestAppInstanceInfo)
import           Types
import qualified API.V1.Tags as TagsAPI
import qualified Data.HashMap.Strict as HMS

-- | Generated client functions for the Tags API
createTag :: AuthenticatedRequest (AuthProtect "cookie-auth")
          -> Tag
          -> ClientM (EnvelopedResponse (ModelWithID Tag))
exportTags :: AuthenticatedRequest (AuthProtect "cookie-auth")
              -> ClientM (EnvelopedResponse [Tag])
importTags :: AuthenticatedRequest (AuthProtect "cookie-auth")
              -> [Tag]
              -> ClientM (EnvelopedResponse [TagImportResult])
listTags :: Maybe Limit
         -> Maybe Offset
         -> ClientM (EnvelopedResponse (PaginatedList (ModelWithID Tag)))
patchTagByID :: AuthenticatedRequest (AuthProtect "cookie-auth")
             -> TagID
             -> MergePatchChangeObject
             -> ClientM (EnvelopedResponse (ModelWithID Tag))
findTagByID :: TagID
            -> ClientM (EnvelopedResponse (ModelWithID Tag))
deleteTagByID :: AuthenticatedRequest (AuthProtect "cookie-auth")
              -> TagID
              -> ClientM (EnvelopedResponse (EntityCount Tag))
( listTags
  :<|> tagFTS
  :<|> createTag
  :<|> exportTags
  :<|> importTags
  :<|> patchTagByID
  :<|> findTagByID
  :<|> deleteTagByID ) = client (Proxy :: Proxy TagsAPI.Routes)

-- End to end tests are for the whole application
spec :: Spec
spec = around (withAppInstance defaultTestConfig) tests
    where
      tests = tagImportTests
              >> tagExportTests
              >> tagPatchTests
              >> tagDeleteTests

-- Tag import tests

tagImportTests :: SpecWith TestAppInstanceInfo
tagImportTests = describe "Tag importing" simpleTagImport

simpleTagImport :: SpecWith TestAppInstanceInfo
simpleTagImport = it "should work" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Get the available tags
                             >> runClientM (listTags Nothing Nothing) clientEnv
                             >>= shouldBeRight
                             >>= \originalListResp -> when (status originalListResp /= "success") (error "Tag listing failed")
                             -- | Import a list of tags that don't exist yet (in the test DB)
                             >> runClientM (importTags authenticatedReq [testTag]) clientEnv
                             >>= shouldBeRight
                             >>= \tagImportResp -> when (status tagImportResp /= "success") (error "Failed to import tags")
                             -- | Retrieve all tags again
                             >> runClientM (listTags Nothing Nothing) clientEnv
                             >>= shouldBeRight
                             >>= \updatedListResp -> when (status updatedListResp /= "success") (error "Post-import tag listing failed")
                             -- | Ensure exactly one tag was added
                             >> let totalAfterUpdate = total $ respData updatedListResp
                                    originalTotal    = total $ respData originalListResp
                                in totalAfterUpdate `shouldBe` (originalTotal + 1)

-- Tag export tests

tagExportTests :: SpecWith TestAppInstanceInfo
tagExportTests = describe "Tag Exporting" simpleTagExport

simpleTagExport :: SpecWith TestAppInstanceInfo
simpleTagExport = it "should work" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Export a list of tags that don't exist yet (in the test DB)
                             >> runClientM (exportTags authenticatedReq) clientEnv
                             >>= shouldBeRight
                             >>= \tagExportResp -> when (status tagExportResp /= "success") (error "Failed to export tags")
                             >> let totalTags = length $ respData tagExportResp
                                in totalTags `shouldSatisfy` (>0)

-- Tag patch tests

tagPatchTests :: SpecWith TestAppInstanceInfo
tagPatchTests = describe "Tag Patching" simpleTagPatch

simpleTagPatch :: SpecWith TestAppInstanceInfo
simpleTagPatch = it "should work for cssColor" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Create a tag
                             >> runClientM (createTag authenticatedReq testTag) clientEnv
                             >>= shouldBeRight
                             >>= \createTagResp -> when (status createTagResp /= "success") (error "Failed to create tag")
                             -- | Ensure that the color is a certain value
                             >> let originalTagColor = tagCSSColor $ model $ respData createTagResp
                                in when (originalTagColor /= tagCSSColor testTag) (error "Post-create tag color is wrong")
                             -- | Edit the tag's color
                             >> let tagId    = mId $ respData createTagResp
                                    newColor = "blue"
                                    update   = HMS.singleton "tagCSSColor" (toJSON @String newColor)
                                in runClientM (patchTagByID authenticatedReq tagId update) clientEnv
                             >>= shouldBeRight
                             >>= \patchTagResp -> when (status patchTagResp /= "success") (error "Failed to patch tag")
                             -- | Retrieve the tag by ID
                             >> runClientM (findTagByID tagId) clientEnv
                             >>= shouldBeRight
                             >>= \updatedTagResp -> when (status updatedTagResp /= "success") (error "Post-patch Tag retrieval failed")
                             -- | Ensure that the tag has the changed color the tag by ID
                             >> let patchedTagColor = tagCSSColor $ model $ respData updatedTagResp
                                in patchedTagColor `shouldBe` newColor

-- Tag delete tests

tagDeleteTests :: SpecWith TestAppInstanceInfo
tagDeleteTests = describe "Tag Deletion" simpleTagDelete

simpleTagDelete :: SpecWith TestAppInstanceInfo
simpleTagDelete = it "should work" runTest
    where
      runTest (port, _, _) = buildClientEnv port
                             -- | Do login and get the cookie for the admin user
                             >>= \clientEnv -> doUserLogin clientEnv adminUserAndPassword
                             >>= \authenticatedReq -> pure ()
                             -- | Create a tag
                             >> runClientM (createTag authenticatedReq testTag) clientEnv
                             >>= shouldBeRight
                             >>= \createTagResp -> when (status createTagResp /= "success") (error "Failed to create tag")
                             -- | Delete the tag
                             >> let tagId    = mId $ respData createTagResp
                                in runClientM (deleteTagByID authenticatedReq tagId) clientEnv
                             >>= shouldBeRight
                             >>= \deleteTagResp -> when (status deleteTagResp /= "success") (error "Failed to delete tag")
                             >> let (EntityCount deletedCount) = respData deleteTagResp
                                in when (toInteger deletedCount /= 1) (error "Deleted entity count != 1")
                             -- | Ensure the tag is gone
                             >> runClientM (findTagByID tagId) clientEnv
                             >>= containsLeftValue
