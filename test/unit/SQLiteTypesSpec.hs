{-# LANGUAGE OverloadedStrings #-}

module SQLiteTypesSpec ( spec ) where

import Test.Hspec

import           Types
import           Database.SQLite.Types
import qualified Database.SQLite.Simple.Types as ST

qSelectAllJPR :: ST.Query
qSelectAllJPR = ST.Query "SELECT * FROM job_posting_requests"

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "SQLite Types and methods" $ -- do
         it "adds filter clauses properly to job posting request queries" $
            addFilterClauses qSelectAllJPR [OnlyPending] `shouldBe` ( qSelectAllJPR <>" WHERE job_posting_requests.approvedJobId IS NULL"
                                                                    , []
                                                                    )
