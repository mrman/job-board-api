{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module MailerBackendSpec ( spec ) where

import Config (AppConfig(..), defaultTestConfig, oneSecondMs)
import Control.Concurrent (threadDelay)
import Control.Exception (SomeException, bracket)
import Data.ByteString (ByteString)
import Data.Maybe (isJust, isNothing)
import Mailer.MailerBackend (Mailer(..), MailerBackend(..), makeConnectedMailerBackend, MailTemplate(..), MailTemplateData(..), MailerBackendError)
import Test.Hspec
import TestFixtures (brandingData, serverInfo, testUser, testTag, jwcList, testCompany)
import TestUtil (shouldBeRight, containsRightValue)
import Util (throwIfLeft)
import Types (Company, CompanyID, ModelWithID(..), PaginatedList(..), UserID, Tag(..), TagID, User(..), SponsoredCompany(..))

withMailerBackend :: (MailerBackend -> IO a) -> IO a
withMailerBackend = bracket makeBackend (\_ -> return ())
  where
    makeBackend = makeConnectedMailerBackend (appMailerConfig defaultTestConfig) Nothing brandingData serverInfo
                  >>= throwIfLeft

getMailDataForTemplate :: MailTemplate -> MailTemplateData
getMailDataForTemplate ConfirmEmailSubscription = ConfirmEmailSubscription_ serverInfo brandingData "user@somewhere" [tagName testTag] "FAKE_CONF_CODE"
getMailDataForTemplate PasswordResetInitiated = PasswordResetInitiated_ brandingData testUser "FAKE_RESET_TOKEN"
getMailDataForTemplate PasswordResetCompleted = PasswordResetCompleted_ brandingData testUser
getMailDataForTemplate TestEmail = TestEmail_ serverInfo brandingData (emailAddress testUser) testUser "unsub-code"
getMailDataForTemplate WeeklyNewsletter = WeeklyNewsletter_ serverInfo brandingData testUser "unsub-code" (items jwcList) (Just (SponsoredCompany testCompany))

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withMailerBackend $ do
  describe "setup" $ it "works without a logger" $ \m -> isNothing (getMailerLogger m) `shouldBe` True

  describe "templates" $ do
    it "generates subscription confirmation email" $ \m -> pure (renderTemplate ConfirmEmailSubscription (getMailDataForTemplate ConfirmEmailSubscription) m)
                                                           >>= containsRightValue

    it "generates password reset initiated email" $ \m -> pure (renderTemplate PasswordResetInitiated (getMailDataForTemplate PasswordResetInitiated) m)
                                                          >>= containsRightValue

    it "generates password reset completed email" $ \m -> pure (renderTemplate PasswordResetCompleted (getMailDataForTemplate PasswordResetCompleted) m)
                                                          >>= containsRightValue

    it "generates test emails properly" $ \m -> pure (renderTemplate TestEmail (getMailDataForTemplate TestEmail) m)
                                                >>= containsRightValue

    it "generates weekly newsletter emails properly" $ \m -> pure (renderTemplate WeeklyNewsletter (getMailDataForTemplate WeeklyNewsletter) m)
                                                             >>= containsRightValue
