{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module TaskQueueBackendSpec ( spec ) where

import           Control.Concurrent (threadDelay)
import           Control.Monad.IO.Class
import           Config (AppConfig(..), BackendConfig, defaultTestConfig, oneSecondMs)
import           Control.Exception (bracket)
import           App (startDBBackend)
import           TaskQueue.TaskQueueBackend (makeConnectedTaskQueueBackend, TaskQueueBackend(..), TaskQueueM(..))
import           Test.Hspec
import           TestFixtures
import           TestUtil (shouldBeRight, shouldBeRight, shouldBeRight, containsRightValue)
import           Util (throwIfLeft)
import           Types
import qualified Data.Text as DT

withTestBackends :: ((SqliteBackend, TaskQueueBackend SqliteBackend IO) -> IO a) -> IO a
withTestBackends = bracket build cleanup
    where
      dbConfig = appBackendConfig defaultTestConfig
      tqConfig = appTaskQueueConfig defaultTestConfig

      build = startDBBackend dbConfig
              >>= \db -> makeConnectedTaskQueueBackend tqConfig Nothing db
              >>= throwIfLeft
              >>= \tq -> pure (db, tq)

      cleanup (db, tq) = stopAllRecurringTasks tq
                         >> disconnect db

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withTestBackends $ do
         describe "component" $
           it "does not error stopping with no tasks" $ \(_, tq) -> stopAllRecurringTasks tq
                                                                    >>= shouldBeRight

         describe "job refresh" $
           it "works with 1 second" $ \(db, tq) -> makeActiveTestJob db
                                                   -- Start the recurring task repeating every second
                                                   >>= \(_, ModelWithID jid _) -> runRecurringTask tq RandomJobRefresh Nothing Nothing (Seconds 1)
                                                   -- Wait 3 seconds giving the recurring task a chance to run
                                                   >> threadDelay (oneSecondMs * 3)
                                                   -- Check that the checkedAt time is non-null
                                                   >> getJobByID db jid
                                                   >>= shouldBeRight
                                                   >>= \(ModelWithID _ job) -> lastCheckedAt job `shouldNotBe` Nothing
