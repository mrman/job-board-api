{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module CacheBackendSpec ( spec ) where

import Cache.CacheBackend
import Config (AppConfig(..), defaultTestConfig, oneSecondMs)
import Control.Concurrent (threadDelay)
import Control.Exception (bracket)
import Data.Maybe (isJust, isNothing)
import Test.Hspec
import TestFixtures (basicMassagedJobQuery, jwcList, testCompanyStats, testCompany, testTag)
import TestUtil (shouldBeSomething)
import Types (Company, CompanyID, ModelWithID(..), PaginatedList(..), UserID, Tag, TagID)

withCacheBackend :: (CacheBackend -> IO a) -> IO a
withCacheBackend = bracket makeBackend (\_ -> return ())
  where
    makeBackend = makeConnectedCacheBackend (appCacheConfig defaultTestConfig) Nothing
                  >>= either (\e -> error ("Failed to create backend:" ++ show e)) return

activeOnlyJobCacheKey :: CacheKey
activeOnlyJobCacheKey = ActiveJobOnlyFTS basicMassagedJobQuery

allJobCacheKey :: CacheKey
allJobCacheKey = JobFTS basicMassagedJobQuery

companyStatsCacheKey :: CacheKey
companyStatsCacheKey = CompanyStats (1 :: CompanyID)

starredJobsCacheKey :: CacheKey
starredJobsCacheKey = StarredJobs (1 :: UserID)

companyListingCacheKey :: CacheKey
companyListingCacheKey = CompanyListing (Just 10) (Just 0)

testCompanyListing :: PaginatedList (ModelWithID Company)
testCompanyListing = PaginatedList [ModelWithID 1 testCompany] 1

tagListingCacheKey :: CacheKey
tagListingCacheKey = TagListing (Just 10) (Just 0)

testTagListing :: PaginatedList (ModelWithID Tag)
testTagListing = PaginatedList [ModelWithID 1 testTag] 1

tagFTSResultsCacheKey :: CacheKey
tagFTSResultsCacheKey = TagFTS "test"

testTagFTSResults :: PaginatedList TagID
testTagFTSResults = PaginatedList [1] 1

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withCacheBackend $ do
  describe "setup" $ it "works without a logger" $ \c -> isNothing (getCacheLogger c) `shouldBe` True

  describe "value insertion" $ do
    it "inserts job FTS results" $ \c -> insertJobListing allJobCacheKey c jwcList `shouldReturn` ()
    it "inserts active job only FTS results" $ \c -> insertJobListing activeOnlyJobCacheKey c jwcList `shouldReturn` ()
    it "inserts company stats" $ \c -> insertCompanyStats companyStatsCacheKey c testCompanyStats `shouldReturn` ()
    it "inserts starred jobs" $ \c -> insertStarredJobs starredJobsCacheKey c jwcList `shouldReturn` ()
    it "inserts company listing" $ \c -> insertCompanyListing companyListingCacheKey c testCompanyListing `shouldReturn` ()
    it "inserts tag listing" $ \c -> insertTagListing tagListingCacheKey c testTagListing `shouldReturn` ()
    it "inserts tag FTS results" $ \c -> insertTagFTSResults tagFTSResultsCacheKey c testTagFTSResults `shouldReturn` ()

  describe "value retrieval" $ do
    it "retrieves inserted job FTS results" $ \c -> insertJobListing allJobCacheKey c jwcList
                                                    >> getJobListing allJobCacheKey c
                                                    >>= shouldBeSomething
                                                    >>= (`shouldBe`jwcList)
    it "retrieves inserted active job only FTS results" $ \c -> insertJobListing activeOnlyJobCacheKey c jwcList
                                                                >> getJobListing activeOnlyJobCacheKey c
                                                                >>= shouldBeSomething
                                                                >>= (`shouldBe`jwcList)
    it "retrieves inserted company stats" $ \c -> insertCompanyStats companyStatsCacheKey c testCompanyStats
                                                  >> getCompanyStats companyStatsCacheKey c
                                                  >>= shouldBeSomething
                                                  >>= (`shouldBe`testCompanyStats)
    it "retrieves inserted starred jobs" $ \c -> insertStarredJobs starredJobsCacheKey c jwcList
                                                 >> getStarredJobs starredJobsCacheKey c
                                                 >>= shouldBeSomething
                                                 >>= (`shouldBe`jwcList)
    it "retrieves inserted company listing" $ \c -> insertCompanyListing companyListingCacheKey c testCompanyListing
                                                    >> getCompanyListing companyListingCacheKey c
                                                    >>= shouldBeSomething
                                                    >>= (`shouldBe`testCompanyListing)
    it "retrieves inserted tag listing" $ \c -> insertTagListing tagListingCacheKey c testTagListing
                                                >> getTagListing tagListingCacheKey c
                                                >>= shouldBeSomething
                                                >>= (`shouldBe`testTagListing)
    it "retrieves inserted tag FTS results" $ \c -> insertTagFTSResults tagFTSResultsCacheKey c testTagFTSResults
                                                    >> getTagFTSResults tagFTSResultsCacheKey c
                                                    >>= shouldBeSomething
                                                    >>= (`shouldBe`testTagFTSResults)

  describe "hasValue" $ do
    it "hasValue doesn't find not-inserted job FTS results" $ \c -> hasValue allJobCacheKey c `shouldReturn` False
    it "hasValue doesn't find not-inserted active job only FTS results" $ \c -> hasValue activeOnlyJobCacheKey c `shouldReturn` False
    it "hasValue doesn't find not-inserted company stats" $ \c -> hasValue companyStatsCacheKey c `shouldReturn` False
    it "hasValue doesn't find not-inserted starred jobs" $ \c -> hasValue starredJobsCacheKey c `shouldReturn` False
    it "hasValue doesn't find not-inserted company listing" $ \c -> hasValue companyListingCacheKey c `shouldReturn` False

    it "hasValue finds inserted job FTS results" $ \c -> insertJobListing allJobCacheKey c jwcList
                                                         >> hasValue allJobCacheKey c
                                                         >>= (`shouldBe`True)
    it "hasValue finds inserted active job only FTS results" $ \c -> insertJobListing activeOnlyJobCacheKey c jwcList
                                                                     >> hasValue activeOnlyJobCacheKey c
                                                                     >>= (`shouldBe`True)
    it "hasValue finds inserted company stats" $ \c -> insertCompanyStats companyStatsCacheKey c testCompanyStats
                                                       >> hasValue companyStatsCacheKey c
                                                       >>= (`shouldBe`True)
    it "hasValue finds inserted starred jobs" $ \c -> insertStarredJobs starredJobsCacheKey c jwcList
                                                      >> hasValue starredJobsCacheKey c
                                                      >>= (`shouldBe`True)
    it "hasValue finds inserted company listing" $ \c -> insertCompanyListing companyListingCacheKey c testCompanyListing
                                                         >> hasValue companyListingCacheKey c
                                                         >>= (`shouldBe`True)
    it "hasValue finds inserted tag listing" $ \c -> insertTagListing tagListingCacheKey c testTagListing
                                                     >> hasValue tagListingCacheKey c
                                                     >>= (`shouldBe`True)
    it "hasValue finds inserted tag FTSResults" $ \c -> insertTagFTSResults tagFTSResultsCacheKey c testTagFTSResults
                                                        >> hasValue tagFTSResultsCacheKey c
                                                        >>= (`shouldBe`True)

  describe "invalidation" $ do
    it "works for job FTS results" $ \c -> insertJobListing allJobCacheKey c jwcList
                                           >> getJobListing allJobCacheKey c
                                           >>= shouldBeSomething
                                           >> invalidate allJobCacheKey c
                                           >> getJobListing allJobCacheKey c
                                           >>= (`shouldBe`Nothing)

    it "works for active job only FTS results" $ \c -> insertJobListing activeOnlyJobCacheKey c jwcList
                                                       >> getJobListing activeOnlyJobCacheKey c
                                                       >>= shouldBeSomething
                                                       >> invalidate activeOnlyJobCacheKey c
                                                       >> getJobListing activeOnlyJobCacheKey c
                                                       >>= (`shouldBe`Nothing)

    it "works for company stats" $ \c -> insertCompanyStats companyStatsCacheKey c testCompanyStats
                                         >> getCompanyStats companyStatsCacheKey c
                                         >>= shouldBeSomething
                                         >> invalidate companyStatsCacheKey c
                                         >> getCompanyStats companyStatsCacheKey c
                                         >>= (`shouldBe`Nothing)

    it "works for starred jobs" $ \c -> insertStarredJobs starredJobsCacheKey c jwcList
                                        >> getStarredJobs starredJobsCacheKey c
                                        >>= shouldBeSomething
                                        >> invalidate starredJobsCacheKey c
                                        >> getStarredJobs starredJobsCacheKey c
                                        >>= (`shouldBe`Nothing)

    it "works for company listing" $ \c -> insertCompanyListing companyListingCacheKey c testCompanyListing
                                           >> getCompanyListing companyListingCacheKey c
                                           >>= shouldBeSomething
                                           >> invalidate companyListingCacheKey c
                                           >> getCompanyListing companyListingCacheKey c
                                           >>= (`shouldBe`Nothing)

    it "works for tag listing" $ \c -> insertTagListing tagListingCacheKey c testTagListing
                                           >> getTagListing tagListingCacheKey c
                                           >>= shouldBeSomething
                                           >> invalidate tagListingCacheKey c
                                           >> getTagListing tagListingCacheKey c
                                           >>= (`shouldBe`Nothing)

    it "works for tag FTS results" $ \c -> insertTagFTSResults tagFTSResultsCacheKey c testTagFTSResults
                                           >> getTagFTSResults tagFTSResultsCacheKey c
                                           >>= shouldBeSomething
                                           >> invalidate tagFTSResultsCacheKey c
                                           >> getTagFTSResults tagFTSResultsCacheKey c
                                           >>= (`shouldBe`Nothing)

  describe "timed invalidation" $
           it "works for company stats" $ \c -> insertCompanyStats companyStatsCacheKey c testCompanyStats
                                                >> getCompanyStats companyStatsCacheKey c
                                                >>= shouldBeSomething
                                                >> threadDelay (oneSecondMs `div` 2) -- defaultThread delay is one second, wait 2 just in case
                                                >> getCompanyStats companyStatsCacheKey c
                                                >>= \beforeDeletion -> threadDelay oneSecondMs -- defaultThread delay is one second, wait 2 just in case
                                                >> getCompanyStats companyStatsCacheKey c
                                                >>= \afterDeletion -> isJust beforeDeletion && isNothing afterDeletion `shouldBe` True
