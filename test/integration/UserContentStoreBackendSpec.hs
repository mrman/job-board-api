{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module UserContentStoreBackendSpec ( spec ) where

import Data.List (isInfixOf)
import Config (AppConfig(..), UserContentStoreConfig, defaultTestConfig)
import Control.Exception (bracket)
import Data.Either (isRight)
import Data.Maybe (isNothing)
import Test.Hspec
import TestFixtures (loadLogoImageURI, loadLogoFlippedVertImageURI)
import UserContentStore.UserContentStoreBackend
import Util (throwIfLeft)
import TestUtil (shouldBeRight)
import Network.URI (uriScheme, uriPath)
import System.Directory (doesPathExist, makeAbsolute)
import Safe (lastMay)
import Data.List.Split (splitOn)

backendConfig :: UserContentStoreConfig
backendConfig = appUCSConfig defaultTestConfig

withTestBackend :: (UserContentStoreBackend -> IO a) -> IO a
withTestBackend = bracket makeBackend return
    where
      makeBackend = makeUserContentStoreBackend backendConfig Nothing
                    >>= throwIfLeft

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withTestBackend $ do
         describe "logger" $ it "has no logger" $ \ucs -> isNothing (getUserContentStoreLogger ucs) `shouldBe` True
         describe "image storage location" $ it "has one" $ \ucs -> getImageStorageLocation ucs `shouldNotBe` ""

         describe "b64 data URI -> saved image conversion and storage" $
                  it "works" $ \ucs -> loadLogoImageURI
                                       >>= convertAndStoreImageDataURI ucs
                                       >>= (`shouldSatisfy` isRight)

         describe "retrieving a saved image" $
                  it "works" $ \ucs -> loadLogoImageURI
                                       >>= convertAndStoreImageDataURI ucs
                                       >>= (`shouldSatisfy` isRight)

         describe "upload SHA1 summing" $ do
                  it "creates same URIs for duplicates" $ \ucs -> loadLogoImageURI -- load the logo image
                                                                  >>= convertAndStoreImageDataURI ucs
                                                                  >>= shouldBeRight
                                                                 -- Load the same logo image again
                                                                  >>= \originalURI -> loadLogoImageURI
                                                                  >>= convertAndStoreImageDataURI ucs
                                                                  >>= shouldBeRight
                                                                 -- Ensure that the same URI is generated
                                                                  >>= \duplicateURI -> originalURI `shouldBe` duplicateURI

                  it "creates different URIs for non-duplicates" $ \ucs -> loadLogoImageURI -- load the logo image
                                                                           >>= convertAndStoreImageDataURI ucs
                                                                           >>= shouldBeRight
                                                                           -- Load the vertically flipped version of the logo image
                                                                           >>= \originalURI -> loadLogoFlippedVertImageURI
                                                                           >>= convertAndStoreImageDataURI ucs
                                                                           >>= shouldBeRight
                                                                          -- URIs should be different
                                                                           >>= \flippedURI -> originalURI `shouldNotBe` flippedURI

         describe "uploaded image URIs" $ do
                  it "contains the external public base URL" $ \ucs -> loadLogoImageURI
                                                                       >>= convertAndStoreImageDataURI ucs
                                                                       >>= shouldBeRight
                                                                       >>= flip shouldContain (getImagePublicBaseURL ucs) . uriPath

                  it "are saved to the image folder" $ \ucs -> loadLogoImageURI
                                                               >>= convertAndStoreImageDataURI ucs
                                                               >>= shouldBeRight
                                                               -- TODO: check for the filename in (getImageStorageLocation ugc) / (basename of URI)
                                                               >>= \uri -> case lastMay $ splitOn "/" (uriPath uri) of
                                                                             Just fname -> makeAbsolute (getImageStorageLocation ucs <> "/" <> fname <> ".jpeg")
                                                                                           >>= flip shouldReturn True . doesPathExist
                                                                             Nothing -> expectationFailure "Failed"
