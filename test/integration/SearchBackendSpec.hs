{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}

module SearchBackendSpec ( spec ) where

import Control.Concurrent (threadDelay)
import Config (AppConfig(..), BackendConfig(..), SearchConfig(..), defaultTestConfig)
import Control.Exception (bracket)
import Control.Monad (void, (>=>))
import Data.Either (isRight)
import Data.Maybe (isJust, isNothing)
import App (startDBBackend)
import Search.SearchBackend
import System.IO.Temp (withSystemTempFile)
import Test.Hspec
import TestFixtures (makeActiveTestJob, emptyJobQuery, makeSimpleMassagedJobQuery, testTag, test2Tag, makeJob, makeAndSaveTestCompany, makeAndSaveURLBounceConfig)
import TestUtil (shouldBeRight, shouldBeSomething, containsRightValue, containsLeftValue, tempDBFileTemplate)
import Util (entityCountToInt)
import Types

makeTestSearchConfig :: FilePath -> SearchConfig
makeTestSearchConfig path = cfg { searchAddr=path }
  where
    cfg = appSearchConfig defaultTestConfig

makeTestBackendConfig :: FilePath -> BackendConfig
makeTestBackendConfig path = cfg { dbAddr=path }
  where
    cfg = appBackendConfig defaultTestConfig

-- ^ Use a temporary system file to power the backend, passing the same file to search backend, since SQLiteFTS works on the same DB
withSearchBackend :: (SQLiteFTS -> IO a) -> IO a
withSearchBackend action = withSystemTempFile tempDBFileTemplate $ \path _ -> bracket (makeBackends path) disconnectBackends action
  where
    makeBackends path = startDBBackend (makeTestBackendConfig path)
                        >>= makeConnectedSearchBackend (makeTestSearchConfig path) Nothing
                        >>= either (\e -> error ("Failed to create backend:" ++ show e)) return
    disconnectBackends s = Search.SearchBackend.disconnect s
                           >> Types.disconnect (searchDBBackend s)

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withSearchBackend $ do
  describe "setup" $ do
    it "works without a logger" $ \s -> isNothing (searchLogger s) `shouldBe` True
    it "has a DB connection" $ \s -> isJust (searchDBConn s) `shouldBe` True

  describe "initialization" $ do
    it "works with no data" $ initialize >=> containsRightValue
    it "works with data added to the backend" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                      >> initialize s
                                                      >>= containsRightValue

  describe "search with a single term" $
    it "finds a single job by term" $ \s -> makeActiveTestJob (searchDBBackend s)
                                            >> initialize s
                                            >>= shouldBeRight
                                            >>= searchJobs (makeSimpleMassagedJobQuery "Test Job")
                                            >>= shouldBeRight
                                            >>= shouldBe 1 . length

  describe "search with a single term" $ do
    it "finds a single job by term" $ \s -> makeActiveTestJob (searchDBBackend s)
                                            >> initialize s
                                            >>= shouldBeRight
                                            >>= searchJobs (makeSimpleMassagedJobQuery "エンジニア")
                                            >>= shouldBeRight
                                            >>= shouldBe 1 . length

    it "finds a single job by unicode term (japanese)" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                               >> initialize s
                                                               >>= shouldBeRight
                                                               >>= searchJobs (makeSimpleMassagedJobQuery "エンジニア")
                                                               >>= shouldBeRight
                                                               >>= shouldBe 1 . length

    it "fails to find a single job with bad term" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                          >> initialize s
                                                          >>= shouldBeRight
                                                          >>= searchJobs (makeSimpleMassagedJobQuery "NOPE")
                                                          >>= shouldBeRight
                                                          >>= shouldBe 0 . length

    it "finds all jobs with no term" $ \s -> makeActiveTestJob (searchDBBackend s)
                                             >> makeActiveTestJob (searchDBBackend s)
                                             >> initialize s
                                             >>= shouldBeRight
                                             >>= searchJobs (makeSimpleMassagedJobQuery "")
                                             >>= shouldBeRight
                                             >>= shouldBe 2 . length

  describe "search by industry" $ do
    it "finds IT job" $ \s -> makeActiveTestJob (searchDBBackend s) -- default industry is IT
                                                >> initialize s
                                                >>= shouldBeRight
                                                >>= searchJobs (Massaged (emptyJobQuery { jqIndustries=[IT] }))
                                                >>= shouldBeRight
                                                >>= shouldBe 1 . length

    it "fails to find IT job searching with BizDev" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                            >> initialize s
                                                            >>= shouldBeRight
                                                            >>= searchJobs (Massaged (emptyJobQuery { jqIndustries=[BizDev] }))
                                                            >>= shouldBeRight
                                                            >>= shouldBe 0 . length


  describe "search by company ID" $ do
    it "finds job posted by company" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                >>= \(ModelWithID cid _, _) -> initialize s
                                                                                 >>= shouldBeRight
                                                                                 >>= searchJobs (Massaged (emptyJobQuery { jqCompanies=[cid] }))
                                                                                 >>= shouldBeRight
                                                                                 >>= shouldMatchList [cid] . items

    it "fails to find jobs searching with invalid ID" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                              >> initialize s
                                                              >>= shouldBeRight
                                                              >>= searchJobs (Massaged emptyJobQuery{jqCompanies=[123]}) -- no job should have employerId 123
                                                              >>= shouldBeRight
                                                              >>= (`shouldBe`0) . length

  describe "search by tag" $
           it "finds test tag" $ \s -> makeActiveTestJob (searchDBBackend s)
                                       -- Make three jobs, one without tag (default), one with "test" tag, another with "test2" tag
                                       >> makeAndSaveTestCompany (searchDBBackend s)
                                       >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig (searchDBBackend s)
                                       >>= \(ModelWithID bid _) -> addJob (searchDBBackend s) ((makeJob True Nothing cid bid) {tags=[testTag]})
                                       >> addJob (searchDBBackend s) ((makeJob True Nothing cid bid) {tags=[test2Tag]})
                                       >> initialize s
                                       >>= shouldBeRight
                                       >>= searchJobs (Massaged (emptyJobQuery { jqTags=["test"]}))
                                       >>= shouldBeRight
                                      -- both tags names match the name "test", should (+prefix) match both
                                       >>= (`shouldBe`2) . total

  describe "search result pagination" $
           it "returns the right total" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                >> makeActiveTestJob (searchDBBackend s)
                                                >> initialize s
                                                >>= shouldBeRight
                                                >>= searchJobs (Massaged emptyJobQuery{jqLimit=Just 1}) -- limit of 1, no offset
                                                >>= shouldBeRight
                                                >>= \(PaginatedList items total) -> (total == 2) && (length items == 1) `shouldBe` True


  describe "tags" $
           it "searching for a tag by name" $ \s -> makeActiveTestJob (searchDBBackend s)
                                                    -- Make three jobs, one without tag (default), one with "test" tag, another with "test2" tag
                                                    >> addTag (searchDBBackend s) testTag -- persist the test fixture tag in the DB
                                                    >> addTag (searchDBBackend s) test2Tag -- persist the test2 fixture tag in the DB
                                                    >> initialize s
                                                    >>= shouldBeRight
                                                    >>= searchTags "test"
                                                    >>= shouldBeRight
                                                    -- both tags names match "test", should prefix match
                                                    >>= (`shouldBe`2) . total

  describe "history" $
           it "records searches" $ \s -> makeActiveTestJob (searchDBBackend s)
                                         >> initialize s
                                         >>= shouldBeRight
                                         >>= \s -> searchJobs (Massaged (emptyJobQuery { jqTerm="s1" })) s
                                         -- Wait 1 second to put time between searches (SQLite DATETIME comparison precision sucks)
                                         >> threadDelay 1000000
                                         >> searchJobs (Massaged (emptyJobQuery { jqTerm="s2" })) s
                                         >> getJobSearchHistory Nothing Nothing s
                                         >>= shouldBeRight
                                        -- Note that order is reversed (last first
                                         >>= \(PaginatedList [ModelWithID _ s, ModelWithID _ s'] c) -> (c == 2) && (sheTerm s == "s2") && (sheTerm s' == "s1") `shouldBe` True
