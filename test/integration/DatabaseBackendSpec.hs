{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE TypeApplications  #-}

module DatabaseBackendSpec ( spec ) where

import           Control.Concurrent (threadDelay)
import           App (startDBBackend)
import           Config (AppConfig(..), BackendConfig, defaultTestConfig)
import           Control.Exception (bracket)
import           Control.Monad (void, when, unless)
import           Data.DateTime (getCurrentTime)
import           Data.Time.Clock (UTCTime(..))
import           Data.Time.Calendar (addDays)
import           Test.Hspec
import           TestFixtures
import           TestUtil (shouldBeRight, shouldBeRight, shouldBeRight, containsRightValue)
import           Types
import           Util (entityCountToInt)
import qualified Data.HashMap.Strict as HMS
import qualified Data.Map as M
import qualified Data.Text as DT

defaultBackendTestConfig :: BackendConfig
defaultBackendTestConfig = appBackendConfig defaultTestConfig

withTestBackend :: (SqliteBackend -> IO a) -> IO a
withTestBackend = bracket (startDBBackend defaultBackendTestConfig) disconnect

main :: IO ()
main = hspec spec

spec :: Spec
spec = around withTestBackend $ do
         describe "versioning" $ it "has a version" $ \b -> getCurrentVersion b `shouldNotReturn` -1

         describe "employers" $ do
           it "can add employers" $ \b -> addCompany b testCompany
                                          >>= containsRightValue

           it "can retrieve an added employer" $ \b -> addCompany b testCompany
                                                       >>= shouldBeRight
                                                       >>= getCompanyByID b . mId
                                                       >>= containsRightValue

         describe "url bounce cfgs" $ do
                                  it "can be added" $ \b -> addURLBounceConfig b testURLBounceConfig
                                                            >>= containsRightValue
                                  it "are listed in desc order" $ \b -> pure (makeTestURLBounceConfig "fst")
                                                                        -- Add the first bounce config
                                                                        >>= \cfg -> addURLBounceConfig b cfg
                                                                        >>= shouldBeRight
                                                                        >>= \first -> pure ()
                                                                        -- Wait 1 second to put time between searches (SQLite DATETIME comparison precision sucks)
                                                                        >> threadDelay 1000000
                                                                        -- Add the second bounce config
                                                                        >> addURLBounceConfig b (makeTestURLBounceConfig "snd")
                                                                        >>= shouldBeRight
                                                                        -- Retrieve all the bounce configs
                                                                        >>= \second -> getAllURLBounceConfigs b Nothing Nothing
                                                                        >>= shouldBeRight
                                                                        >>= flip shouldBe [second, first] . items

         describe "jobs" $ do
           it "can add jobs" $ \b -> void (makeActiveTestJob b)
           it "can retrieve an added job" $ \b -> makeActiveTestJob b
                                                  >>= getJobByID b . mId . snd
                                                  >>= containsRightValue

         describe "search" $ do
           it "returns active job" $ \b -> makeActiveTestJob b
                                           >>= \(_, j) -> findJobs b (massage emptyJobQuery)
                                           >>= shouldBeRight
                                           >>= shouldMatchList [j] . (jwcJob<$>) . items

           it "returns inactive job" $ \b -> makeActiveTestJob b
                                             >> makeTestJob b
                                             >> findJobs b (massage (emptyJobQuery { jqIncludeInactive=True }))
                                             >>= shouldBeRight
                                             >>= shouldBe 2 . length . items

           it "works by single tag" $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                      >>= \(ModelWithID bid _) -> makeAndSaveURLBounceConfig b
                                      -- Create 3 jobs, one with no tags, one with tags and one with tags'
                                      >>= \(ModelWithID cid _) -> addJob b ((makeJob True Nothing cid bid) {tags=[]})
                                      >> addJob b ((makeJob True Nothing cid bid) {tags=[testTag]})
                                      >> addJob b ((makeJob True Nothing cid bid) {tags=[test2Tag]})
                                      >> findJobs b (Massaged (emptyJobQuery { jqTags=["test2"] }))
                                      >>= shouldBeRight
                                      -- only one job has the "test2" tag
                                      >>= (`shouldBe`1) . total

           it "works by multi tag" $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                            >>= \(ModelWithID bid _) -> makeAndSaveURLBounceConfig b
                                           -- Create 3 jobs, one with no tags, one with tags and one with tags'
                                            >>= \(ModelWithID cid _) -> addJob b ((makeJob True Nothing cid bid) {tags=[]})
                                            >> addJob b ((makeJob True Nothing cid bid) {tags=[testTag]})
                                            >> addJob b ((makeJob True Nothing cid bid) {tags=[test2Tag]})
                                            >> findJobs b (Massaged (emptyJobQuery { jqTags=["test2", "test"] }))
                                            >>= shouldBeRight
                                            -- two jobs have given tags
                                            >>= (`shouldBe`2) . total

           it "works by multi tag single term " $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                                      >>= \(ModelWithID bid _) -> makeAndSaveURLBounceConfig b
                                                     -- Create 3 jobs, one with no tags, one with tags and one with tags'
                                                      >>= \(ModelWithID cid _) -> addJob b ((makeJob True Nothing cid bid) {tags=[]})
                                                      >> addJob b ((makeJob True Nothing cid bid) {tags=[testTag]})
                                                      >> addJob b ((makeJob True Nothing cid bid) {tags=[test2Tag]})
                                                      >> findJobs b (Massaged (emptyJobQuery { jqTags=["test"] }))
                                                      >>= shouldBeRight
                                                      -- two jobs match the tag "test" vaguely
                                                      >>= (`shouldBe`2) . total

           it "works by no tag " $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                         >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig b
                                         -- Create 3 jobs, one with no tags, one with tags and one with tags'
                                         >>= \(ModelWithID bid _) -> addJob b ((makeJob True Nothing cid bid) {tags=[]})
                                         >> addJob b ((makeJob True Nothing cid bid) {tags=[testTag]})
                                         >> addJob b ((makeJob True Nothing cid bid) {tags=[test2Tag]})
                                         >> findJobs b (Massaged emptyJobQuery)
                                         >>= shouldBeRight
                                         -- three jobs match no tags/empty query (because term is still part of query)
                                         >>= (`shouldBe`3) . total

           it "works by industry" $ \b -> makeActiveTestJob b -- default industry is IT
                                          >> makeTestJob b
                                          >> findJobs b (Massaged (emptyJobQuery { jqIndustries=[IT], jqIncludeInactive=True }))
                                          >>= shouldBeRight
                                          -- both active and inactive jobs should be returned (same default industry)
                                          >>= (`shouldBe`2) . total

           it "works by employer (company)" $ \b -> makeActiveTestJob b
                                                    >> makeTestJob b
                                                    >> makeAndSaveTestCompany b
                                                    >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig b
                                                    >>= \(ModelWithID bid _) -> addJob b ((makeJob True Nothing cid bid) {employerId=cid})
                                                    >>= shouldBeRight
                                                    >> findJobs b (Massaged (emptyJobQuery { jqCompanies=[cid] }))
                                                    >>= shouldBeRight
                                                    -- active and inactive should be ignored, only the job with new employer
                                                    >>= (`shouldBe`1) . total

           it "retrieves createdAt DESC by default" $ \b -> makeActiveTestJob b
                                                            -- Wait 1 second to put time between searches (SQLite DATETIME comparison precision sucks)
                                                            >>= \(_, first) -> threadDelay 1000000
                                                            -- Make another job, wait
                                                            >> makeTestJob b
                                                            >>= \(_, second) -> threadDelay 1000000
                                                            -- Make another job, wait
                                                            >> makeActiveTestJob b
                                                            >>= \(_, third) -> findJobs b (massage emptyJobQuery)
                                                            >>= shouldBeRight
                                                            -- The jobs returned should be in the createdAt DESC order by default
                                                            -- inactive jobs don't show up by default
                                                            >>= (`shouldBe` [third, first]) . (jwcJob <$>) . items

           describe "tags" $ do
             it "retrieving all tags works " $ \b -> addTag b testTag
                                                     >> addTag b test2Tag
                                                     >> getAllTags b (Just 10) (Just 0)
                                                     >>= containsRightValue

             it "all tags pagination " $ \b -> addTag b testTag
                                               >> addTag b test2Tag
                                               >> getAllTags b (Just 1) (Just 1)
                                               >>= containsRightValue

             it "getting tag by ID " $ \b -> addTag b test2Tag -- Add test2 tag first this time
                                             >> addTag b testTag
                                             >>= shouldBeRight
                                             >>= findTagsByIDs b . (:[])  . mId
                                             >>= shouldBeRight
                                             >>= \(PaginatedList [ModelWithID _ t] count) -> (count == 1 && testTag == t) `shouldBe` True

         describe "users" $ do
           it "can add users" $ \b -> addUser b testUser testUserPassword
                                      >>= containsRightValue

           it "can get user count" $ \b -> addUser b testUser testUserPassword
                                           >> getUserCount b
                                           >>= containsRightValue

           it "find user using email and pass" $ \b -> addUser b testUser testUserPassword
                                                       >>= shouldBeRight
                                                       >>= \(ModelWithID _ u) -> loginUser b (emailAddress u) testUserPassword
                                                       >>= containsRightValue

           it "lastLoggedIn time is updated" $ \b -> addUser b testUser testUserPassword
                                                     >>= shouldBeRight
                                                     >>= \(ModelWithID _ afterCreation) -> loginUser b (emailAddress testUser) testUserPassword
                                                     >>= shouldBeRight
                                                     >>= \(ModelWithID _ afterLogin) -> lastLoggedIn afterLogin `shouldNotBe` lastLoggedIn afterCreation

           it "can create password reset tokens" $ \b -> addUser b testUser testUserPassword
                                                         >>= shouldBeRight
                                                         >>= \(ModelWithID uid u) -> createTokenForUser b uid PasswordResetToken
                                                         >>= containsRightValue

           it "can retrieve tokens by the token value" $ \b -> addUser b testUser testUserPassword
                                                               >>= shouldBeRight
                                                               >>= \(ModelWithID uid u) -> createTokenForUser b uid PasswordResetToken
                                                               >>= shouldBeRight
                                                               >>= \(ModelWithID tid t) -> getTokenByContent b (tToken t)
                                                               >>= containsRightValue

           it "can remove tokens by id" $ \b -> addUser b testUser testUserPassword
                                                >>= shouldBeRight
                                                >>= \(ModelWithID uid u) -> createTokenForUser b uid PasswordResetToken
                                                >>= shouldBeRight
                                                >>= \(ModelWithID tid t) -> getTokenByContent b (tToken t)
                                                >>= shouldBeRight
                                                >>  removeTokenByID b tid
                                                >>= shouldBeRight
                                                >>= (`shouldBe` 1) . entityCountToInt

         describe "subs" $ do
           it "can create new email subscriptions" $ \b -> saveEmailSubscription b (DT.pack (emailAddress testUser)) []
                                                           >>= containsRightValue

           it "can create new email subscriptions (with tags)" $ \b -> addTag b testTag
                                                                       >>= shouldBeRight
                                                                       >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                         >>= containsRightValue

           it "can confirm new subscriptions" $ \b -> addTag b testTag
                                                      >>= shouldBeRight
                                                      >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                      >>= shouldBeRight
                                                      >>= \(ModelWithID esId es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                      >>= shouldBeRight
                                                      >>= \(ModelWithID _ after) -> esConfirmationDate after `shouldNotBe` Nothing

           it "can retrieve all subscriptions" $ \b -> addTag b testTag
                                                       >>= shouldBeRight
                                                       >>= \(ModelWithID tid _) -> saveEmailSubscription b "user1@example.com" [tid]
                                                       >> saveEmailSubscription b "user2@example.com" [tid]
                                                       >> getAllEmailSubscriptions b Nothing Nothing
                                                       >>= shouldBeRight
                                                       >>= \(PaginatedList list count) -> length list == 2 && count == 2 `shouldBe` True

           it "can cancel a subscription" $ \b -> addTag b testTag
                                                  >>= shouldBeRight
                                                  >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                  >>= shouldBeRight
                                                  >>= \(ModelWithID esId es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                  >>= shouldBeRight
                                                  >>= \(ModelWithID _ after) -> cancelEmailSubscription b (esUnsubscribeCode es)
                                                  >>= shouldBeRight
                                                  >>= \(ModelWithID _ afterCancel) -> esUnsubscribeDate afterCancel `shouldNotBe` Nothing

           it "can get the list of subscribers" $ \b -> addTag b testTag
                                                      >>= shouldBeRight
                                                      >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                      >>= shouldBeRight
                                                      >>= \(ModelWithID esId es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                      >>= shouldBeRight
                                                      >> getEmailSubscribers b Nothing Nothing
                                                      >>= shouldBeRight
                                                      >>= \(PaginatedList list count) -> length list == 1 && count == 1 `shouldBe` True

           it "does not return unsubbed users in list of subscribers" $ \b -> addTag b testTag
                                                                             >>= shouldBeRight
                                                                             >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                                             >>= shouldBeRight
                                                                             >>= \(ModelWithID esId es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                                             >> cancelEmailSubscription b (esUnsubscribeCode es)
                                                                             >> getEmailSubscribers b Nothing Nothing
                                                                             >>= shouldBeRight
                                                                             >>= \(PaginatedList list count) -> Prelude.null list && count == 0 `shouldBe` True

         describe "subs" $ do
           it "get tags for subs" $ \b -> addTag b testTag
                                          >>= shouldBeRight
                                          >>= \(ModelWithID tid _) -> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                          >>= shouldBeRight
                                          >>= getTagsForEmailSubscription b . mId
                                          >>= shouldBeRight
                                          >>= (`shouldBe` 1) . length . items

           it "retrieves interesting jobs for user" $ \b -> addUser b testUser testUserPassword
                                                            >>= shouldBeRight
                                                            -- Create a new tag
                                                            >>= \user -> addTag b testTag
                                                            >>= shouldBeRight
                                                            -- Make a test job & tag it
                                                            >>= \(ModelWithID tid tag) -> makeActiveTestJob b
                                                            >>= \(_, ModelWithID jid j) -> updateJob b jid (j { tags=[testTag] })
                                                            -- Make an untagged testjob
                                                            >> makeActiveTestJob b
                                                            -- Create & confirm an email subscription user with a subscription with that tag
                                                            >> saveEmailSubscription b (DT.pack (emailAddress testUser)) []
                                                            >>= shouldBeRight
                                                            >>= \sub@(ModelWithID _ es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                            >>= shouldBeRight
                                                            >> getInterestingJobsForUser b user sub
                                                            >>= shouldBeRight
                                                           -- Both the untagged and tagged job should be returned since no tags were specified
                                                            >>= (`shouldBe` 2) . length . items

           it "retrieves interesting jobs for user (with tag)" $ \b -> addUser b testUser testUserPassword
                                                                       >>= shouldBeRight
                                                                       -- Create a new tag
                                                                       >>= \user -> addTag b testTag
                                                                       >>= shouldBeRight
                                                                       -- Make a test job & tag it
                                                                       >>= \(ModelWithID tid tag) -> makeActiveTestJob b
                                                                       >>= \(_, ModelWithID jid j) -> updateJob b jid (j { tags=[testTag] })
                                                                       >>= shouldBeRight
                                                                       -- Create & confirm an email subscription user with a subscription with that tag
                                                                       >> saveEmailSubscription b (DT.pack (emailAddress testUser)) [tid]
                                                                       >>= shouldBeRight
                                                                       >>= \sub@(ModelWithID _ es) -> confirmEmailSubscription b (esConfirmationCode es)
                                                                       >>= shouldBeRight
                                                                       >> getInterestingJobsForUser b user sub
                                                                       >>= shouldBeRight
                                                                       >>= (`shouldBe` 1) . length . items


           it "gets sponsored company for user" $ \b -> getCurrentTime
                                                        -- Add user
                                                        >>= \now -> addUser b testUser testUserPassword
                                                        >>= shouldBeRight
                                                        -- Create company
                                                        >>= \user@(ModelWithID uid _) -> makeAndSaveTestCompany b
                                                        -- Create a current promotion for company
                                                        >>= \(ModelWithID cid _) -> addPromotion b (makeCompanyPromotion True now cid uid)
                                                        -- Get the promotions for the current user
                                                        >> getCurrentSponsoredCompanyForUser b user
                                                        >>= containsRightValue

           it "ignores inactive promotion" $ \b -> getCurrentTime
                                                   -- Add user
                                                   >>= \now -> addUser b testUser testUserPassword
                                                   >>= shouldBeRight
                                                   -- Create company
                                                   >>= \user@(ModelWithID uid _) -> makeAndSaveTestCompany b
                                                   -- Create a current promotion for company
                                                   >>= \(ModelWithID cid _) -> addPromotion b (makeCompanyPromotion False now cid uid)
                                                   -- Get the promotions for the current user
                                                   >> getCurrentSponsoredCompanyForUser b user
                                                   >>= containsRightValue

         describe "job posting requests" $ do
           it "can create job posting requests" $ \b -> addJobPostingRequest b testJPR
                                                        >>= containsRightValue

           it "can job posting request by ID" $ \b -> addJobPostingRequest b testJPR
                                                      >>= shouldBeRight
                                                      >>= getJobPostingRequestByID b . mId
                                                      >>= containsRightValue

           it "can retrieve all job posting requests" $ \b -> addJobPostingRequest b testJPR
                                                              >>= shouldBeRight
                                                              >> getAllJobPostingRequests b Nothing Nothing ([] :: [JobPostingRequestFilter])
                                                              >>= shouldBeRight
                                                              >>= (`shouldBe` 1) . length . items

         describe "analytics" $ do
           it "has jobs by bounce rate" $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                                >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig b
                                                -- Create a job with the bounce config
                                                >>= \(ModelWithID bid _) -> addJob b ((makeJob True Nothing cid bid) {employerId=cid})
                                                >>= shouldBeRight
                                                -- Make two bounces
                                                >>= \(ModelWithID jid _) -> makeTestURLBounceInfo bid
                                                >>= saveURLBounceInfo b
                                                >> makeTestURLBounceInfo bid
                                                >>= saveURLBounceInfo b
                                                -- Get jobs by bounce rate
                                                >> getJobsByBounceRate b Nothing Nothing
                                                >>= shouldBeRight
                                                >>= \(PaginatedList list count) -> when (count /= 1) (error "one job should have been present")
                                                >> case list of
                                                     (b : bs) -> wbcCount b `shouldBe` 2
                                                     _ -> error "unexpectedly empty list of bounces"

           it "has bounce rates binned by hod" $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                                       >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig b
                                                       -- Create a job with the bounce config
                                                       >>= \(ModelWithID bid _) -> addJob b ((makeJob True Nothing cid bid) {employerId=cid})
                                                       >>= shouldBeRight
                                                       >>= \(ModelWithID jid _) -> pure ()
                                                       -- Make yesterday's midday and night hour
                                                       >> getCurrentTime
                                                       >>= \now -> pure (now {utctDay=addDays (-1) (utctDay now) , utctDayTime=12 * 60 * 60})
                                                       >>= \yesterday12PM -> pure (now {utctDayTime=18 * 60 * 60})
                                                       >>= \yesterday6PM -> pure ()
                                                       -- Make two bounces in different hours of the day
                                                       >> makeTestURLBounceInfo bid
                                                       >>= \bInfo -> saveURLBounceInfo b (bInfo {bouncedAt=yesterday12PM})
                                                       >> makeTestURLBounceInfo bid
                                                       >>= \bInfo -> saveURLBounceInfo b (bInfo {bouncedAt=yesterday6PM})
                                                       -- Get bounces binned by time period for all time
                                                       >> getBouncesBinnedByTimePeriod b Hourly Nothing Nothing
                                                       >>= shouldBeRight
                                                       -- Ensure that binMap contains midday hour bin w/ correct value
                                                       >>= \binMap -> unless (HMS.member yesterday12PM binMap) (error "yesterday @ 12 should be in binMap")
                                                       >> when (entityCountToInt (binMap HMS.! yesterday12PM) /= 1) (error "yesterday @ 12pm should have 1")
                                                       -- Ensure that binMap contains midday hour bin
                                                       >> unless (HMS.member yesterday6PM  binMap) (error "yesterday @ 6pm should be in binMap")
                                                       >> when (entityCountToInt (binMap HMS.! yesterday6PM) /= 1) (error "yesterday @ 6pm should have 1")
                                                       -- Total should be 2
                                                       >> shouldBe (sum (map entityCountToInt (HMS.elems binMap))) 2

           it "has bounce rates binned by dow" $ \b -> makeAndSaveTestCompany b -- Create a company and URL bounce config
                                                       >>= \(ModelWithID cid _) -> makeAndSaveURLBounceConfig b
                                                       -- Create a job with the bounce config
                                                       >>= \(ModelWithID bid _) -> addJob b ((makeJob True Nothing cid bid) {employerId=cid})
                                                       >>= shouldBeRight
                                                       >>= \(ModelWithID jid _) -> pure ()
                                                       -- Make yesterday's midday and night hour
                                                       >> getCurrentTime
                                                       >>= \now -> pure (now {utctDay=addDays (-1) (utctDay now) , utctDayTime=18 * 60 * 60})
                                                       >>= \yesterday12PM -> pure (yesterday12PM {utctDayTime=0})
                                                       >>= \yesterday12AM -> pure (yesterday12PM {utctDay=addDays (-1) (utctDay yesterday12PM)})
                                                       >>= \twoDaysAgo12PM -> pure (twoDaysAgo12PM {utctDayTime=0})
                                                       >>= \twoDaysAgo12AM -> pure ()
                                                       -- Make two bounces in different hours of the day
                                                       >> makeTestURLBounceInfo bid
                                                       >>= \bInfo -> saveURLBounceInfo b (bInfo {bouncedAt=yesterday12PM})
                                                       >> makeTestURLBounceInfo bid
                                                       >>= \bInfo -> saveURLBounceInfo b (bInfo {bouncedAt=twoDaysAgo12PM})
                                                       -- Get bounces binned by time period for all time
                                                       >> getBouncesBinnedByTimePeriod b Daily Nothing Nothing
                                                       >>= shouldBeRight
                                                       -- Ensure that binMap contains midday hour bin w/ correct value
                                                       >>= \binMap -> unless (HMS.member yesterday12AM binMap) (error "yesterday should be in binMap")
                                                       >> when (entityCountToInt (binMap HMS.! yesterday12AM) /= 1) (error "yesterday should have 1")
                                                       -- Ensure that binMap contains midday hour bin
                                                       >> unless (HMS.member twoDaysAgo12AM  binMap) (error "two days ago should be in binMap")
                                                       >> when (entityCountToInt (binMap HMS.! twoDaysAgo12AM) /= 1) (error "two days ago should have 1")
                                                       -- Total should be 2
                                                       >> shouldBe (sum (map entityCountToInt (HMS.elems binMap))) 2
