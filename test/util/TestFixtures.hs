{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}

module TestFixtures where

import Config (defaultTestConfig, AppConfig(..))
import Control.Monad.IO.Class (MonadIO, liftIO)
import Data.DateTime (DateTime, addMinutes, getCurrentTime, fromGregorian)
import Data.HashMap.Strict as HMS
import Data.Maybe (fromJust, fromMaybe)
import Data.Monoid ((<>))
import Data.UUID.V4 (nextRandom)
import Network.URI (URI, parseURI)
import System.Directory (getCurrentDirectory, makeAbsolute)
import TestUtil (shouldBeRight)
import Types

staticCreationTime :: DateTime
staticCreationTime = read "2017-07-19 11:59:53.318211719 UTC"

testUserPassword :: Password
testUserPassword = "test"

testUser :: User
testUser = User { firstName="test"
                , lastName="user"
                , emailAddress="test.user@example.com"
                , password=""
                , policyAndSalt=Nothing
                , userRole=JobSeeker
                , joinedAt=staticCreationTime
                , homeLocationId=Nothing
                , lastLoggedIn=staticCreationTime
                }

testURLBounceConfig :: URLBounceConfig
testURLBounceConfig = URLBounceConfig { bounceName=Just "test"
                                      , bounceTargetUrl="http://example.com/bounce-test"
                                      , bounceIsActive=True
                                      , bounceCreatedAt=staticCreationTime
                                      }

makeTestURLBounceConfig :: String -> URLBounceConfig
makeTestURLBounceConfig name = URLBounceConfig { bounceName=Just name
                                           , bounceTargetUrl="http://example.com/"<>name
                                           , bounceIsActive=True
                                           , bounceCreatedAt=staticCreationTime
                                           }

makeTestURLBounceInfo :: URLBounceConfigID -> IO URLBounceInfo
makeTestURLBounceInfo cid = getCurrentTime
                            >>= \now -> pure URLBounceInfo { bouncedHost="techjobs.tokyo"
                                                           , bouncedAt=now
                                                           , bouncedReferer=Just "localhost"
                                                           , bouncedUserAgent=Just "test"
                                                           , bounceConfigId=cid
                                                           }

gifDataURI :: String
gifDataURI = "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="

testCompany :: Company
testCompany = Company { companyName="test company"
                      , companyDescription=Just "test company"
                      , companyCultureDescription=Just "test company"
                      , companyHomepageURL="http://example.com"
                      , companyIconURI=gifDataURI
                      , companyBannerImageURI=gifDataURI
                      , companyPrimaryColorCSS="gray"
                      , companyCreatedAt=staticCreationTime
                      , companyLastUpdatedAt=staticCreationTime
                      , companyAddressID=Nothing
                      }

testJPR :: JobPostingRequest
testJPR = JobPostingRequest { rTitle="test jpr"
                            , rDescription="test jpr"
                            , rIndustry=IT
                            , rJobType=FullTime
                            , rMinSalary=Nothing
                            , rMaxSalary=Nothing
                            , rSalaryCurrency=Just YEN
                            , rPosterEmail="admin@example.com"
                            , rApplyLink="https://example.com"
                            , rCompanyId=Nothing
                            , rApprovedJobId=Nothing
                            , rRequestedAt=staticCreationTime
                            }

-- | Create and insert a randomized active job for use during testing, complete with company and bounce configuration
makeActiveTestJob :: ( CompanyStore db IO
                     , URLBounceConfigStore db IO
                     , JobStore db IO
                     ) => db -> IO (ModelWithID Company, ModelWithID Job)
makeActiveTestJob db = getCurrentTime
                       >>= \now -> makeTestJobWithCompany (makeJob True (Just now)) db

-- | Create and insert a randomized inactive job for use during testing, complete with company and bounce configuration
makeTestJob :: ( CompanyStore db IO
               , URLBounceConfigStore db IO
               , JobStore db IO
               ) => db -> IO (ModelWithID Company, ModelWithID Job)
makeTestJob db = getCurrentTime
                 >>= \now -> makeTestJobWithCompany (makeJob False (Just now)) db

-- | Create and insert a job for use during testing, complete with company and bounce configuration
makeTestJobWithCompany :: ( CompanyStore db IO
                          , URLBounceConfigStore db IO
                          , JobStore db IO
                          )
                          => (CompanyID -> URLBounceConfigID -> Job)
                              -> db             -- ^ Backend
                              -> IO (ModelWithID Company, ModelWithID Job)
makeTestJobWithCompany jobCtor db = addCompany db testCompany
                                    >>= shouldBeRight
                                    >>= \company@(ModelWithID cid _) -> nextRandom
                                    >>= \r -> addURLBounceConfig db (makeTestURLBounceConfig ("test-bounce-cfg" <> show r))
                                    >>= shouldBeRight
                                    >>= addJob db . jobCtor cid . mId
                                    >>= shouldBeRight
                                    >>= \job -> pure (company, job)

-- | Make a single job for use during tests
makeJob :: Bool -> Maybe DateTime -> CompanyID -> URLBounceConfigID -> Job
makeJob active maybeNow eid bcid  = Job { title="Test Job (エンジニア)"
                                        , description="Test job (エンジニア)"
                                        , industry=IT
                                        , jobType=FullTime
                                        , minSalary=Just 100
                                        , maxSalary=Just 0
                                        , salaryCurrency=Just USD
                                        , postingDate=fromMaybe staticCreationTime maybeNow
                                        , employerId=eid
                                        , isActive=active
                                        , applyLink="http://example.com/apply"
                                        , jobURLBounceConfigId=bcid
                                        , tags=[]
                                        , lastCheckedAt=Nothing
                                        }

makeAndSaveTestCompany :: CompanyStore db IO => db -> IO (ModelWithID Company)
makeAndSaveTestCompany db = addCompany db testCompany
                            >>= shouldBeRight

makeAndSaveURLBounceConfig :: URLBounceConfigStore db IO => db -> IO (ModelWithID URLBounceConfig)
makeAndSaveURLBounceConfig db = nextRandom
                                >>= \r -> addURLBounceConfig db (makeTestURLBounceConfig ("test-bounce-cfg"<>show r))
                                >>= shouldBeRight

emptyJobQuery :: JobQuery
emptyJobQuery = JobQuery { jqTerm=""
                         , jqIndustries=[]
                         , jqCompanies=[]
                         , jqLimit=Nothing
                         , jqOffset=Nothing
                         , jqOrder=Nothing
                         , jqTags=[]
                         , jqIncludeInactive=False
                         }

makeSimpleJobQuery :: String -> JobQuery
makeSimpleJobQuery s = emptyJobQuery { jqTerm=s }

makeSimpleMassagedJobQuery :: String -> Massaged JobQuery
makeSimpleMassagedJobQuery s = Massaged (makeSimpleJobQuery s)

testTag :: Tag
testTag = Tag "test" names "white" staticCreationTime
    where
      names = HMS.fromList [("EN_US", "test"), ("JA_JP", "テスト")]

test2Tag :: Tag
test2Tag = Tag "test2" names "black" staticCreationTime
    where
      names = HMS.fromList [("EN_US", "test2"), ("JA_JP", "テスト2")]

-- Query that is used when users visit the main page (first job search)
basicMassagedJobQuery :: Massaged JobQuery
basicMassagedJobQuery = Massaged $ JobQuery "" [] [] (Just 10) (Just 0) Nothing [] False

testCompanyStats :: CompanyStats
testCompanyStats = CompanyStats 1 1 1

jwcList :: PaginatedList JobWithCompany
jwcList = PaginatedList jwcs 2
    where
      jwcs = [ JobWithCompany (ModelWithID 1 (makeJob True Nothing 1 1)) (ModelWithID 1 testCompany)
             , JobWithCompany (ModelWithID 2 (makeJob True Nothing 1 2)) (ModelWithID 1 testCompany)
             ]

-- | Load the logo image
loadLogoImageURI :: MonadIO m => m URI
loadLogoImageURI = loadB64JPEGFromFixturesAssets "logo.jpeg.b64"

-- | Load the flipped version of the logo image
loadLogoFlippedVertImageURI :: MonadIO m => m URI
loadLogoFlippedVertImageURI = loadB64JPEGFromFixturesAssets "logo-flipped-vert.jpeg.b64"

-- | Load a Base64 encoded file which corresponds to an image (NOTE: the b64 must *not* have any extra spaces)
loadB64JPEGFromFixturesAssets :: MonadIO m => FilePath -> m URI
loadB64JPEGFromFixturesAssets = liftIO . loadB64JPEGFromFile_ . (fixturesAssetsRelPath<>)
    where
      fixturesAssetsRelPath = "./test/util/assets/"
      loadB64JPEGFromFile_ path = makeAbsolute path
                                  >>= readFile
                                  >>= pure . fromJust . parseURI

brandingData :: BrandingData
brandingData = BrandingData "ExampleCo" "example.com/email/subscribe" "example.com/email/unsubscribe"

serverInfo :: ServerInfo
serverInfo = ServerInfo "localhost:5001" "localhost:5002" "localhost:5003"

makeCompanyPromotion :: Bool -> DateTime -> CompanyID -> UserID -> Promotion
makeCompanyPromotion isActive startTime cid uid = Promotion { prName="Test promotion"
                                                            , prDescription=Nothing
                                                            , prType=CompanyPromotion
                                                            , prPromoterId=uid
                                                            , prObjectId=cid
                                                            , prStart=startTime
                                                            , prEnd=addMinutes 30 startTime
                                                            , prCreatedBy=uid
                                                            , prIsActive=isActive
                                                            , prCreatedAt=addMinutes (-30) startTime
                                                            }
-- | User information without sensitive data for the admin/root account that should always exist
--   *NOTE* that this information is set in the test appconfig so it has to match that
adminWithoutSensitiveData :: UserWithoutSensitiveData
adminWithoutSensitiveData = UserWithoutSensitiveData { userID=1
                                                     -- ^ Root user is *normally* the first user, but may not be
                                                     , userFirstName="root"
                                                     , userLastName="user"
                                                     , userEmailAddress=appRootUserEmail defaultTestConfig
                                                     , role=Administrator
                                                     , userJoinedAt=fromGregorian 2018 1 1 0 0 0
                                                     , userHomeLocationId=Nothing
                                                     }

-- | Session info for the admin/root account that should always exist
adminSessionInfo :: SessionInfo
adminSessionInfo = SessionInfo { sessionUserInfo=adminWithoutSensitiveData
                               , expires=fromGregorian 2999 1 1 0 0 0
                               , sessionUserPermissions=[]
                               }

-- | User & password for admin user
adminUserAndPassword :: UserEmailAndPassword
adminUserAndPassword =  UserEmailAndPassword { userEmail=appRootUserEmail defaultTestConfig
                                             , userPassword=appRootUserPassword defaultTestConfig
                                             }
