{-# LANGUAGE DataKinds    #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE LambdaCase   #-}

module ServantTestUtil where

import           AuthN (authCookieName)
import           Network.HTTP.Types.Header (hCookie)
import           Servant (AuthProtect)
import           Data.Proxy (Proxy(..))
import           Servant.API
import           Servant.Client.Core.Auth (AuthClientData, mkAuthenticatedRequest, AuthenticatedRequest)
import           Servant.Client (client, mkClientEnv, Client, ClientEnv, parseBaseUrl, runClientM, ClientM)
import           Servant.Client.Core.Request (Request, addHeader)
import           Web.Cookie (setCookieValue, parseSetCookie)
import           Data.Text.Encoding (encodeUtf8)
import           TestUtil (shouldBeRight)
import           TestFixtures (adminUserAndPassword)
import           Types
import qualified API.V1.Auth as AuthAPI
import qualified Data.ByteString.Char8 as B8
import qualified Data.Text as DT
import qualified Network.HTTP.Client as HTTPClient

-- | Used for authenticating with servant-client
type instance AuthClientData (AuthProtect "cookie-auth") = String

-- | Generated client functions for the Auth API
login :: UserEmailAndPassword -> ClientM (Headers '[Header "Set-Cookie" DT.Text] (EnvelopedResponse SessionInfo))
( login
  :<|> logout
  :<|> listPermissions
  :<|> listRoles
  :<|> adminStats
  :<|> me
  :<|> changeCurrentUserPassword ) = client (Proxy :: Proxy AuthAPI.Routes)

-- | Used with mkAuthenticateRequest to make an authenticated cookie auth request
cookieAuthRequest :: String -> Request -> Request
cookieAuthRequest c = Servant.Client.Core.Request.addHeader hCookie (B8.unpack authCookieName <> "=" <> c)

-- | Build client env for the app on a given (assumed localhost server) port
buildClientEnv :: Port -> IO ClientEnv
buildClientEnv port = parseBaseUrl ("http://localhost:" <> show port <> "/api/v1")
                      >>= \baseUrl -> HTTPClient.newManager HTTPClient.defaultManagerSettings
                      >>= \manager -> pure (mkClientEnv manager baseUrl)

-- | Do login and get the cookie for a given user, collecting the cookie to use for authenticated requests in the future
doUserLogin :: ClientEnv -> UserEmailAndPassword -> IO (AuthenticatedRequest (AuthProtect "cookie-auth"))
doUserLogin clientEnv emailAndPass = runClientM (login adminUserAndPassword) clientEnv
                                     >>= shouldBeRight
                                     >>= \loginResp -> pure (lookupResponseHeader loginResp :: ResponseHeader "Set-Cookie" DT.Text)
                                     >>= \case
                                         Header h -> pure $ B8.unpack $ setCookieValue $ parseSetCookie $ encodeUtf8 h
                                         _        -> error "login failed"
                                     -- | Repackage the auth cookie into a usable authenticated request
                                     >>= \authCookie -> pure (mkAuthenticatedRequest authCookie cookieAuthRequest)
