module TestUtil ( containsRightValue
                , containsLeftValue
                , containsSomething
                , shouldBeRight
                , shouldBeSomething
                , startAppForTest
                , tempDBFileTemplate
                , withAppInstance
                , containsNothing
                , TestAppInstanceInfo
                ) where

import App (startApp)
import Config (defaultTestConfig, AppConfig(..), BackendConfig(..), SearchConfig(..), replaceDBPaths)
import Control.Concurrent (ThreadId, forkIO, killThread, threadDelay)
import Control.Exception (bracket)
import Data.Either (Either, isLeft, isRight)
import Data.Maybe
import System.IO.Temp (emptySystemTempFile)
import System.Random (getStdRandom, randomR)
import Test.Hspec
import Types (Port)

type TestAppInstanceInfo = (Port, FilePath, ThreadId)

appPortRangeStart :: Int
appPortRangeStart = 5102

appPortRangeEnd :: Int
appPortRangeEnd = appPortRangeStart + 100

getRandomPortForApp :: IO Int
getRandomPortForApp = getRandomPortInRange "App" appPortRangeStart appPortRangeEnd

getRandomPortInRange :: String -> Int -> Int -> IO Int
getRandomPortInRange _ start end = getStdRandom (randomR (start,end))

testDBFileTemplate :: String
testDBFileTemplate = "job-board-api-e2e-test.sqlite"

startAppForTest :: AppConfig -> IO TestAppInstanceInfo
startAppForTest c = getRandomPortForApp
                    -- | Build DB file
                    >>= \port -> emptySystemTempFile testDBFileTemplate
                    -- | Replace the DB paths in the AppConfig
                    >>= \dbPath -> pure (replaceDBPaths c dbPath)
                    -- | Start the app
                    >>= \cfg -> forkIO (startApp (cfg { appPort=port }))
                    -- | Wait for app to start up (1 second)
                    >>= \tid -> threadDelay 1000000
                    >> return (port, dbPath, tid)

withAppInstance :: AppConfig -> (TestAppInstanceInfo -> IO ()) -> IO ()
withAppInstance = flip bracket cleanup . startAppForTest
    where
      cleanup (port, dbPath, tid)  = killThread tid

shouldBeSomething :: (Eq a, Show a) => Maybe a -> IO a
shouldBeSomething m = shouldNotBe m Nothing >> return (fromJust m)

shouldBeRight :: Show e => Either e a -> IO a
shouldBeRight (Left err) = error $ "Unexpected Left value:" ++ show err
shouldBeRight (Right v) = return v

containsRightValue :: Either a b -> Expectation
containsRightValue = (`shouldBe`True) . isRight

containsLeftValue :: Either a b -> Expectation
containsLeftValue = (`shouldBe`True) . isLeft

containsSomething :: Maybe a -> Expectation
containsSomething = (`shouldBe`True) . isJust

containsNothing :: Maybe a -> Expectation
containsNothing = (`shouldBe`True) . isNothing

tempDBFileTemplate :: String
tempDBFileTemplate = "job-board-test-db.sqlite"
