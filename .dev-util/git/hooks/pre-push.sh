#!/bin/env bash

# Attept to lint Haskell and JS code
LINT_CMD="make lint"
eval $LINT_CMD
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "LINT Failed!\n CMD: $LINT_CMD"
    exit 1
fi

# Run unit & integration tests
TEST_CMD="make test-unit test-int"
eval $TEST_CMD
RESULT=$?
if [ $RESULT -ne 0 ]; then
    echo -e "Haskell Unit + Integration tests Failed!\n CMD: $TEST_CMD"
    exit 1
fi
