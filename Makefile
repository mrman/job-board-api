.PHONY: all clean \
	build build-watch build-test-e2e-watch build-test-unit-watch build-test-int-watch \
	deploy lint test print-release-version print-git-sha-suffix \
	check-cloc loc-count dev-env-setup dev-env-teardown \
	swagger-json generate-swagger-json \
	local-dev-db migrate-local-db db-fixture-dev create-db clean-db \
	tests-unit tests-int tests-e2e \
	setup-infra-runtime setup-infra-runtime-dir setup-infra-runtime-cookies \
	db clean-db api served-api \
	image builder-base-image builder-image \
	publish-builder-base-image publish-builder-image \
	running-api api-deploy api-push-remote api-stop-image-remote api-run-image-remote \
	local-jaeger

all: build
clean: clean-db
test: test-unit test-int test-e2e
build: api

VERSION ?= $(shell awk '/version\:\s+([0-9\.]+)/{print $$2}' job-board-api.cabal)
SQLITE=sqlite3
CHROMEDRIVER=/usr/bin/chromedriver

PERCENT := %
GIT_SHA_SUFFIX ?= -$(shell git log -1 --pretty=$(PERCENT)h)

# Setup stack command
STACK ?= stack $(STACK_OPTS)
# Always build with profile
STACK_BUILD_OPTS ?=--profile

SQL_MIGRATIONS_FOLDER ?= infra/db/sql/migrations
SQL_FIXTURES_FOLDER ?= infra/db/sql/fixtures
SQLITE_DB_LOCAL_DIR ?= infra/runtime/db
SQLITE_DB_LOCAL ?= $(SQLITE_DB_LOCAL_DIR)/db.sqlite

API_EXEC_NAME=job-board-api
API_EXEC_PATH=target/$(API_EXEC_NAME)

CONTAINER_NAME_API=job-board-api
IMAGE_NAME_API_BUILDER_BASE=job-board-api-builder-base
IMAGE_NAME_API_BUILDER=job-board-api-builder

REGISTRY_IMAGE_NAME=registry.gitlab.com/mrman/job-board-api
REGISTRY_IMAGE_NAME_API_RELEASE=$(REGISTRY_IMAGE_NAME)/$(CONTAINER_NAME_API):$(VERSION)
REGISTRY_IMAGE_NAME_API=$(REGISTRY_IMAGE_NAME_API_RELEASE)$(GIT_SHA_SUFFIX)
REGISTRY_IMAGE_NAME_API_NO_SHA=$(REGISTRY_IMAGE_NAME_API_RELEASE)
REGISTRY_IMAGE_NAME_API_BUILDER_BASE=$(REGISTRY_IMAGE_NAME)/$(IMAGE_NAME_API_BUILDER_BASE):latest
REGISTRY_IMAGE_NAME_API_BUILDER=$(REGISTRY_IMAGE_NAME)/$(IMAGE_NAME_API_BUILDER):latest

MAKEFILE_DIR:=$(strip $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST)))))

CLOC ?= cloc

check-cloc:
	@which $(CLOC) &>/dev/null || echo "Please install cloc (https://github.com/AlDanial/cloc)"

print-release-version:
	@echo "${VERSION}"

print-git-sha-suffix:
	@echo "${GIT_SHA_SUFFIX}"

loc-count: check-cloc
	$(CLOC) --exclude-dir=.stack-work .

dev-env-setup: setup-infra-runtime
	cp ./.dev-util/git/hooks/pre-push.sh .git/hooks/pre-push
	chmod +x .git/hooks/pre-push

dev-env-teardown:
	rm .git/hooks/pre-push

setup-infra-runtime: setup-infra-runtime-dir setup-infra-runtime-cookies

# Used when the infra/runtime folders may not necessarily be set up already
# (ex. in docker containers, fresh clone of repo)
setup-infra-runtime-dir:
	@if [ -d "infra/runtime" ]; then \
		echo "[info] infra/runtime present, skipping set up..."; \
	else \
		echo "[info] infra/runtime folder missing, setting up..."; \
		mkdir -p infra/runtime; \
	fi

# Set up the cookies for a local running instance of job-board-api
setup-infra-runtime-cookies:
	@if [ -f "infra/runtime/cookie-config/cookie-key.aes" ]; then \
		echo "[info] cookie file present, skipping setup..."; \
	else \
		echo "[info] cookie file missing, creating cookie file..."; \
		mkdir -p infra/runtime/cookie-config; \
		head -c 96 /dev/urandom > infra/runtime/cookie-config/cookie-key.aes; \
	fi

###########
# Linting #
###########

lint:
	hlint src/ app/ test/

###########
# Testing #
###########

test-unit:
	$(STACK) test $(STACK_TEST_OPTS) :unit --verbosity silent -j8

test-int:
	$(STACK) test $(STACK_TEST_OPTS) :int --verbosity silent  -j8

test-e2e: setup-infra-runtime
	$(STACK) test $(STACK_TEST_OPTS) :e2e --verbosity silent -j8

#####################
# Local development #
#####################

swagger-json: api generate-swagger-json

generate-swagger-json:
	$(API_EXEC_PATH) PrintSwaggerJSON > swagger/swagger.json

local-dev-db: clean-db create-db migrate-local-db db-fixture-dev

local-jaeger:
	docker run -e \
		COLLECTOR_ZIPKIN_HTTP_PORT=9411 \
		-p 5775:5775/udp \
		-p 6831:6831/udp \
		-p 6832:6832/udp \
		-p 5778:5778 \
		-p 16686:16686 \
		-p 14268:14268 \
		-p 9411:9411 \
		-e LOG_LEVEL=debug \
		jaegertracing/all-in-one:latest

local-zipkin:
	docker run -p 9411:9411 openzipkin/zipkin

migrate-local-db:
	FRONTEND_FOLDER=$(WEB_APP_DIST_LOCATION) \
	DB_ADDR=$(SQLITE_DB_LOCAL) \
	DB_MIGRATION_FOLDER=$(SQL_MIGRATIONS_FOLDER) \
	ENVIRONMENT="Development" \
	target/job-board-api MigrateBackend

db-fixture-dev:
	cat $(SQL_FIXTURES_FOLDER)/development.sql | $(SQLITE) $(SQLITE_DB_LOCAL)

db-random-jpr:
	sqlite3 infra/runtime/db/db.sqlite 'INSERT INTO job_posting_requests (title, description, industry, jobType, posterEmail, applyLink, requestedAt) VALUES ("job_" || CAST(RANDOM() AS TEXT), "DESCRIPTION", "IT", "FullTime", "test@example.com", "example.com/" || CAST(RANDOM() AS TEXT), DATETIME());'

create-db:
	mkdir -p $(SQLITE_DB_LOCAL_DIR)
	touch $(SQLITE_DB_LOCAL)
	$(SQLITE) $(SQLITE_DB_LOCAL) ".databases"

clean-db:
	@rm $(SQLITE_DB_LOCAL) || echo "Local SQLITE DB doesn't exist"

setup:
	$(STACK) setup

api:
	mkdir -p target && \
	$(STACK) build $(STACK_BUILD_OPTS) --local-bin-path target --copy-bins

build-watch:
	$(STACK) build $(STACK_BUILD_OPTS) --local-bin-path target --copy-bins --file-watch

build-test-e2e-watch:
	$(STACK) build :e2e --file-watch

build-test-unit-watch:
	$(STACK) build :unit --file-watch

build-test-int-watch:
	$(STACK) build :int --file-watch

# Execution opts (for use with running-api)
EXEC_RTS_OPTS ?= +RTS -xc -RTS

running-api:
	FRONTEND_FOLDER=$(WEB_APP_DIST_LOCATION) \
	DB_ADDR=$(SQLITE_DB_LOCAL) \
	DB_MIGRATION_FOLDER=$(SQL_MIGRATIONS_FOLDER) \
	ENVIRONMENT="Development" \
	target/job-board-api RunServer $(EXEC_RTS_OPTS)

served-api:
	$(STACK) build $(STACK_BUILD_OPTS) && \
	$(STACK) exec $(STACK_EXEC_OPTS) $(API_EXEC_NAME)

##########
# Images #
##########

image:
	docker build -f infra/Dockerfile -t $(REGISTRY_IMAGE_NAME_API) .

# stage 1 build helper (for completely from-scratch builds)
builder-base-image:
	docker build -f infra/builder-base/Dockerfile -t $(REGISTRY_IMAGE_NAME_API_BUILDER_BASE) .

# stage 2 build helper (for incremental builds on a builder-base)
builder-image:
	docker build -f infra/builder/Dockerfile -t $(REGISTRY_IMAGE_NAME_API_BUILDER) .

#############
# Instances #
#############

container-local-dev:
	docker run \
		--rm \
		--name $(CONTAINER_NAME_API)-local-dev \
		-p 127.0.0.1:5001:5001 \
		-e ENVIRONMENT=Development \
		-e COOKIE_SECRET_FILE_PATH=/var/app/job-board/data/cookie-config/cookie-key.aes \
		\
		-e DB_ADDR=/var/app/job-board/data/db.sqlite \
		-e DB_MIGRATION_FOLDER=/var/app/job-board/data/migrations \
		\
		-e SEARCH_ADDR=/var/app/job-board/data/db.sqlite \
		-e SEARCH_FTS_MIGRATION_FILE=/var/app/job-board/data/search-migrations/0.sql \
		\
		-e MAILER_TEMPLATES_FOLDER=/var/app/job-board/data/email/templates \
		-e FRONTEND_FOLDER=/var/app/job-board/frontend \
		\
		-v `realpath infra/db/sql/migrations`:/var/app/job-board/data/migrations \
		-v `realpath infra/db/sql/search-migrations`:/var/app/job-board/data/search-migrations \
		-v `realpath infra/email`:/var/app/job-board/data/email \
		-v `realpath infra/runtime/db`:/var/app/job-board/data \
		-v `realpath infra/runtime/cookie-config`:/var/app/job-board/data/cookie-config \
		$(REGISTRY_IMAGE_NAME_API)

container-local-prod:
	docker run \
		--rm \
		--name $(CONTAINER_NAME_API)-local-prod \
		-p 127.0.0.1:5001:5001 \
		-e ENVIRONMENT=Production \
		-e COOKIE_SECRET_FILE_PATH=/var/app/job-board/data/cookie-config/cookie-key.aes \
		\
		-e DB_ADDR=/var/app/job-board/data/db.sqlite \
		-e DB_MIGRATION_FOLDER=/var/app/job-board/data/migrations \
		\
		-e SEARCH_ADDR=/var/app/job-board/data/db.sqlite \
		-e SEARCH_FTS_MIGRATION_FILE=/var/app/job-board/data/search-migrations/0.sql \
		\
		-e MAILER_TEMPLATES_FOLDER=/var/app/job-board/data/email/templates \
		-e FRONTEND_FOLDER=/var/app/job-board/frontend \
		\
		-v `realpath infra/db/sql/migrations`:/var/app/job-board/data/migrations \
		-v `realpath infra/db/sql/search-migrations`:/var/app/job-board/data/search-migrations \
		-v `realpath infra/email`:/var/app/job-board/data/email \
		-v `realpath infra/runtime/db`:/var/app/job-board/data \
		-v `realpath infra/runtime/cookie-config`:/var/app/job-board/data/cookie-config \
		$(REGISTRY_IMAGE_NAME_API)

api-static-instance:
	docker stop job-board-api && \
	docker rm job-board-api && \
	docker run \
		--name $(CONTAINER_NAME_API) \
		-p 127.0.0.1:5001:5001 \
		-e ENVIRONMENT=Development \
		-e DB_ADDR=/var/app/job-board/data/db.sqlite \
		-e DB_MIGRATION_FOLDER=/var/app/job-board/data/migrations \
		-e FRONTEND_FOLDER=/var/app/job-board/frontend \
		-v `realpath infra/runtime/db/db.sqlite`:/var/app/job-board/data/db.sqlite \
		-v `realpath infra/runtime/db/sql/migrations`:/var/app/job-board/data/migrations \
		-v `realpath web/dist`:/var/app/job-board/frontend \
		$(REGISTRY_IMAGE_NAME_API)

###########
# Publish #
###########

registry-login:
	docker login registry.gitlab.com

publish-builder-base-image:
	docker push $(REGISTRY_IMAGE_NAME_API_BUILDER_BASE)

publish-builder-image:
	docker push $(REGISTRY_IMAGE_NAME_API_BUILDER)

publish:
	docker push $(REGISTRY_IMAGE_NAME_API)

release:
	docker tag $(REGISTRY_IMAGE_NAME_API) $(REGISTRY_IMAGE_NAME_API_NO_SHA)
	docker push $(REGISTRY_IMAGE_NAME_API_NO_SHA)

##########
# Deploy #
##########

SQLITE_DB_PROD=/var/app/job-board/data/db.sqlite
DATA_FOLDER_PROD=/var/app/job-board/data
MIGRATIONS_FOLDER_PROD=$(DATA_FOLDER_PROD)/migrations

deploy: test publish

manual-deploy: test api-manual-deploy

api-manual-deploy: api-push-image-remote api-stop-container-remote api-run-image-remote tag-deployed-api

tag-deployed-api:
	-git push --delete origin deployed-api
	-git tag --delete deployed-api
	git tag deployed-api HEAD # Tag where the repo is currently, rollbacks may happen
	git push --tags

# Transfer the image to the host
api-push-image-remote:
	@echo -e "Deploying container [$(REGISTRY_IMAGE_NAME_API)] to host [$(SSH_ADDR)]"
	docker save $(REGISTRY_IMAGE_NAME_API) | bzip2 | pv | ssh $(SSH_ADDR) 'bunzip2 | docker load'

api-stop-container-remote:
	ssh $(SSH_ADDR) "docker stop $(CONTAINER_NAME_API); \
		docker rm $(CONTAINER_NAME_API);"

# Deploy image on the host by getting teh docker host ip then using it in the run command
api-run-image-remote:
	@echo -e "Connecting to host host [$(SSH_ADDR)] and starting container $(CONTAINER_NAME)..."
	ssh $(SSH_ADDR) "docker run \
		--name $(CONTAINER_NAME_API) \
		-e ENVIRONMENT=Production \
		-e DB_ADDR=$(SQLITE_DB_PROD) \
		-e DB_MIGRATION_FOLDER=$(MIGRATIONS_FOLDER_PROD) \
		-p 127.0.0.1:5001:5001 \
		-v /var/app/job-board/data/db.sqlite:$(SQLITE_DB_PROD) \
		-d $(REGISTRY_IMAGE_NAME_API)"
